
var alertMsgDiv;
var addMsg = function(){};

$(function(){
	alertMsgDiv = $('#alert-msg-div');

	addMsg = function(type, msg){
		var alertDiv = $("<div></div>").addClass("alert");
		var btn = $("<button></button>").addClass("close").attr("type", "button").attr("data-dismiss","alert").html("&times;");
		var msgField = $("<strong></strong>").html("Error!");

		switch(type){
		    case 'error' :
		    	alertDiv.addClass('alert-error');
		    	msgField.html('Error!');
			    break;
		    case 'warning' :
		    	alertDiv.addClass('alert-warning');
		    	msgField.html('Warning!');
		    	break;
		    default :
		    	alertDiv.addClass('alert-success');
		    msgField.html('Success!');

		}

		alertDiv.append(btn).append(msgField).append(' ' + msg);
		alertMsgDiv.html("");
		alertMsgDiv.append(alertDiv);

	}
});