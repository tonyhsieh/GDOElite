$.fn.exists = function () {
    return this.length !== 0;
}

;(function(window, $) {
	window.asante = (function(){
		var FB_INIT_ARGS = {
		    appId: '314362955366726',
		    // TODO should replace by following one owned by us
                    // appId: '1335775849829716',
		    status: true,
		    cookie: true,
		    xfbml: true,
		    oauth: true
		};

		return {
			FB_INIT_ARGS: FB_INIT_ARGS
		}

	})();

}(window, jQuery));
