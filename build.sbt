
name := "asante"

version := "1.1"

lazy val root = (project in file(".")).enablePlugins(PlayJava, PlayEbean)

scalaVersion := "2.11.7"

javacOptions ++= Seq("-Xlint:unchecked", "-Xlint:deprecation")

scalacOptions ++= Seq("-unchecked", "-deprecation")

resolvers ++= Seq(
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots",
  "Spy Repository" at "http://files.couchbase.com/maven2", // required to resolve `spymemcached`, the plugin's dependency.
  "Sonatype OSS Snapshots" at "https://oss.sonatype.org/content/repositories/snapshots"
)

libraryDependencies ++= Seq(
  javaWs,
  javaJpa,
  cache,
  javaCore,
  javaJdbc,
  filters,
  "org.flywaydb" %% "flyway-play" % "3.0.1",
  "com.github.mumoshu" %% "play2-memcached-play24" % "0.7.0",
  "com.fasterxml.jackson.core" % "jackson-core" % "2.8.8",
  "com.fasterxml.jackson.core" % "jackson-annotations" % "2.8.8",
  "com.fasterxml.jackson.core" % "jackson-databind" % "2.8.8.1",
  "org.bouncycastle" % "bcprov-jdk15on" % "1.48",
  "org.bouncycastle" % "bcprov-ext-jdk15on" % "1.48",
  "mysql" % "mysql-connector-java" % "5.1.42",
  "com.amazonaws" % "aws-java-sdk" % "1.11.128",
  "com.sun.mail" % "javax.mail" % "1.5.6",
  "commons-beanutils" % "commons-beanutils" % "1.8.3",
  "commons-io" % "commons-io" % "2.4",
  "com.restfb" % "restfb" % "1.39.0",
  "org.apache.commons" % "commons-collections4" % "4.1",
  "org.slf4j" % "log4j-over-slf4j" % "1.7.6",
  "com.maxmind.geoip2" % "geoip2" % "2.7.0",
  "log4j" % "apache-log4j-extras" % "1.2.17"
)



mappings in Universal ++=
  (baseDirectory.value / "public" ***).get pair relativeTo(baseDirectory.value)

// Exclude the API documentation in the generated package
sources in (Compile, doc) := Seq.empty

publishArtifact in (Compile, packageDoc) := false


// Play provides two styles of routers, one expects its actions to be injected, the
// other, legacy style, accesses its actions statically.
routesGenerator := InjectedRoutesGenerator