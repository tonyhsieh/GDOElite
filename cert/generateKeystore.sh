#!/usr/bin/env bash
printf "Make sure that following files exist in current directory:\n \
     1. asantegenie.crt (cert for asantegenie.com)\n \
     2. godaddy.crt (parent cert from godaddy)\n \
     3. key.pem (decrypted private key)\n \
     and press ENTER to continue or any other key to abort.\n\n"
read -s -n 1 key

if [[ $key = "" ]]; then
    cat asantegenie.crt godaddy.crt > cert.pem
    openssl pkcs12 -export -name asante -in cert.pem -inkey key.pem -out keystore.p12
    keytool -importkeystore -destkeystore asante.keystore -srckeystore keystore.p12 -srcstoretype pkcs12 -alias asante

    rm -f cert.pem keystore.p12

    keytool -list -v -keystore asante.keystore
else
    echo "Execute this script again when you are ready."
fi
