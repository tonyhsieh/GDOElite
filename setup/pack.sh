#!/bin/sh
ASANTE_NAME="asante-1.1"

cd ..
sbt clean clean-files
sbt dist

if [ ! -f target/universal/$ASANTE_NAME.zip ]; then
    echo "compilation error"
    exit 2
fi

unzip target/universal/$ASANTE_NAME.zip -d .
rm -f $ASANTE_NAME.zip
rm -rf $ASANTE_NAME/conf/*
cp -f conf/*.conf $ASANTE_NAME/conf
cp -f conf/logback.xml $ASANTE_NAME/conf/logback.xml
cp -vR conf/db $ASANTE_NAME/conf
cp -f setup/*.sh $ASANTE_NAME/bin
rm -f $ASANTE_NAME/bin/pack.sh
chmod a+x $ASANTE_NAME/bin/*.sh
cp -vR $ASANTE_NAME/public $ASANTE_NAME/bin
tar -zcvf $ASANTE_NAME.tgz $ASANTE_NAME
rm -rf $ASANTE_NAME

