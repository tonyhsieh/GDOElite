/**
 * Created by tony_hsieh on 2017/6/15.
 */
import com.google.inject.AbstractModule;

import javax.inject.Singleton;


public class GlobalModule extends AbstractModule {
    @Override
    protected void configure() {
        bind(Global.class).asEagerSingleton();
    }
}
