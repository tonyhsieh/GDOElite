package services;

import java.util.List;
import java.util.UUID;

import models.User;
import models.UserDevLog;
import controllers.api.v1.app.form.BaseReqPack;

import org.apache.commons.lang3.exception.ExceptionUtils;

import utils.LoggingUtils;

public abstract class UserDevLogService
{
    public static void recordUserDevLog(User user, BaseReqPack pack, String sessionId)
    {
        try
        {
            List<UserDevLog> listUserDevLog = UserDevLog.listUserDevLogByImei(pack.imei);

            UserDevLog userDevLog;

            if (listUserDevLog == null || listUserDevLog.size() == 0)
            {
                userDevLog = new UserDevLog();
                userDevLog.id = UUID.randomUUID();
            }
            else
            {
                userDevLog = listUserDevLog.get(0);
            }

            userDevLog.appVer = pack.appVer;
            userDevLog.deviceManufacturer = pack.deviceManufacturer;
            userDevLog.deviceModule = pack.deviceModule;
            userDevLog.deviceName = pack.deviceName;
            userDevLog.deviceOsName = pack.deviceOsName;
            userDevLog.deviceOsVersion = pack.deviceOsVersion;
            userDevLog.imei = pack.imei;
            userDevLog.userId = user.id;
            userDevLog.userSessionId = sessionId;
            userDevLog.save();
        }
        catch (Exception err)
        {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "recordUserDevLog() err : " + ExceptionUtils.getStackTrace(err));
        }
    }
}