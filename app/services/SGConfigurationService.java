/**
 *
 */
package services;

import java.io.IOException;
import java.util.Calendar;

import models.SmartGenie;
import models.SmartGenieConfiguration;

import org.apache.commons.lang3.time.DateFormatUtils;

import utils.db.FileEntityManager;
import frameworks.models.FileType;

/**
 * @author carloxwang
 *
 */
public abstract class SGConfigurationService{

	public static void backupCfg(String rawData, SmartGenie sg) throws IOException{
		SmartGenieConfiguration sgc = SmartGenieConfiguration.findLatestCfg(sg);

		SmartGenieConfiguration newSgc = new SmartGenieConfiguration();


		if(sgc == null){
			newSgc.cfgVersion = 1;
		}else{
			newSgc.cfgVersion = sgc.cfgVersion + 1;
		}

		String timestamp = DateFormatUtils.format(Calendar.getInstance(), "yyyyMMdd-HHmmss-SSS");
		FileEntityManager feo = new FileEntityManager("cfg-" + timestamp, rawData, FileType.SG_CONFIGURATION);

		newSgc.cfgFile = feo.fileEntity;
		newSgc.smartGenie = sg;

		newSgc.save();

	}

	public static SmartGenieConfiguration retrieveCfg(SmartGenie sg){
		SmartGenieConfiguration sgc = SmartGenieConfiguration.findLatestCfg(sg);



		return sgc;

	}

}
