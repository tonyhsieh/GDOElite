package services;

import java.util.List;

import models.SmartGenie;
import models.UserRelationship;

public abstract class UserRelationshipService{

	public static List<UserRelationship> listUserRelationshipByUser(String userId){
		return UserRelationship.listUserRelationshipByUser(userId);
	}

	public static List<UserRelationship> listUserRelationshipByOwner(String ownerId){
		return UserRelationship.listUserRelationshipByOwner(ownerId);
	}

	public static List<UserRelationship> listUserRelationshipBySG(String smartGenieId){
		return UserRelationship.listUserRelationshipBySG(smartGenieId);
	}

	public static boolean isJoined(String smartGenieId, String userId){
		return UserRelationship.isJoined(smartGenieId, userId);
	}

	public static UserRelationship findByUserNSmartGenie(String ownerId, String userId, String smartGenieId){
		return UserRelationship.findByUserNSmartGenie(ownerId, userId, smartGenieId);
	}

	public static UserRelationship findByMemberIdNSmartGenie(String memberId, SmartGenie smartGenie){
		return UserRelationship.findByMemberIdNSmartGenie(memberId, smartGenie);
	}
}
