/**
 *
 */
package services;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import models.User;
import models.UserIdentifier;

import org.apache.commons.lang3.StringUtils;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.db.ebean.Transactional;
import frameworks.models.UserIdentifierType;
import utils.LoggingUtils;

import java.util.List;
import java.util.UUID;


/**
 * @author carloxwang
 *
 */
public abstract class UserService{

	public static boolean checkUserExists(String loginId, UserIdentifierType type){
		List<UserIdentifier> ui =  Ebean.find(UserIdentifier.class)
                .where().eq("ident_data", loginId).findList();

        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "user exist " + ui.isEmpty() + ui.toString() + " type " + type.ordinal() );
		return  !ui.isEmpty() ;
	}

	@Transactional
	public static User createNewUser(UserIdentifierType identType, String identData, String pwd, String address, String email, String nickname){
		/// Creating User ///
		User u = new User();

		if( StringUtils.isNotBlank(nickname) ){
			u.nickname = nickname;
		}

		if( StringUtils.isNotBlank(email) ){
			u.email = email;
		}
        try{
            u.save();
        }catch(Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "create user save " + ExceptionUtils.getStackTrace(err));
        }

		/// Creating UserIdentifier /////
		UserIdentifier ui = new UserIdentifier();
		ui.id = UserIdentifier.genUserIdentifierId(identType, identData);
        ui.identData = identData;
        ui.identType = identType;
        ui.password = UserIdentifier.pwdHash(pwd);
        ui.user = u;

        try{
            ui.save();
        }catch(Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "create user identifier save " + ExceptionUtils.getStackTrace(err));
        }


		return u;
	}

}
