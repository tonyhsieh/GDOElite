import javax.inject.*;

import filters.LoggingFilter;
import play.*;
import play.mvc.EssentialFilter;
import play.http.HttpFilters;

@Singleton
public class Filters implements HttpFilters
{
    private final Environment env;
    private final EssentialFilter exampleFilter;

    @Inject
    public Filters(Environment env, LoggingFilter filter)
    {
        this.env = env;
        this.exampleFilter = filter;
    }

    @Override
    public EssentialFilter[] filters()
    {
        return new EssentialFilter[]{exampleFilter};
    }
}
