package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.*;

/**
 * 
 * @author johnwu
 *
 */
@Entity
public class UserRelationshipHistoryLog extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8334200244500650599L;

	@Id
	public UUID id;
	
	@Constraints.Required
	public String ownerId;
	
	@Constraints.Required
	public String userId;
	
	@Constraints.Required
	public String smartGenieId;
	
}
