package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.*;
import play.data.validation.Constraints;

import utils.LoggingUtils;

@Entity
public class UserDevLog extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7076840369607947722L;

	@Id
	@Constraints.Required
	public UUID id;
	
	@Constraints.Required
	public String userSessionId;
	
	@Constraints.Required
	public UUID userId;

	public String imei;

	public String deviceName;

	public String deviceOsName;

	public String deviceOsVersion;

	public String deviceManufacturer;

	public String deviceModule;

	public String appVer;

	public static List<UserDevLog> listUserDevLogByImei(String imei){
		return Ebean.find(UserDevLog.class).where()
				.eq("imei", imei).orderBy().desc("update_at")
				.findList();
	}
	
	public static UserDevLog getUserDevLogBySessionId(String sessionId){
		return Ebean.find(UserDevLog.class).where()
				.eq("userSessionId", sessionId)
				.findUnique();
	}

    public static List<UserDevLog> listUserDevLogByUserIdDistinctIMEI(String userId){
        String ql = "select user_dev_log.id, user_dev_log.imei from user_dev_log join (select imei , max(update_at)as ts from user_dev_log group" +
                " by imei )maxt on (user_dev_log.imei = maxt.imei and maxt.ts = user_dev_log.update_at) " +
                "join user on (user.id = user_dev_log.user_id) where user.id = '" + userId + "'; " ;

        RawSql sql = RawSqlBuilder
                .parse(ql)
                .create();
        Query<UserDevLog> query = Ebean.find(UserDevLog.class);
        query.setRawSql(sql);

        return query.findList();
    }

    @Override
    public String toString() {
        return "UserDevLog{" +
                "id=" + id +
                ", userSessionId='" + userSessionId + '\'' +
                ", userId=" + userId +
                ", imei='" + imei + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", deviceOsName='" + deviceOsName + '\'' +
                ", deviceOsVersion='" + deviceOsVersion + '\'' +
                ", deviceManufacturer='" + deviceManufacturer + '\'' +
                ", deviceModule='" + deviceModule + '\'' +
                ", appVer='" + appVer + '\'' +
                '}';
    }
}
