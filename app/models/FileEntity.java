/**
 *
 */
package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.validation.Constraints;

import frameworks.models.MimeType;
import utils.LoggingUtils;

/**
 * @author carloxwang
 *
 */
@Entity
@Table(name="file_entity")
public class FileEntity extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -4269692138475750906L;

	@Id
	public UUID id;

	@Constraints.Required
	public String fileName;

	public String fileExt;

	@Constraints.Required
	public Long fileSize;

	@Constraints.Required
	public String s3Path;

	public MimeType contentType;

	@Override
	public String toString() {
		return "FileEntity [" + (id != null ? "id=" + id + ", " : "")
				+ (fileName != null ? "fileName=" + fileName + ", " : "")
				+ (fileExt != null ? "fileExt=" + fileExt + ", " : "")
				+ (fileSize != null ? "fileSize=" + fileSize + ", " : "")
				+ (s3Path != null ? "s3Path=" + s3Path + ", " : "")
				+ (contentType != null ? "contentType=" + contentType : "")
				+ "]";
	}

	public final static FileEntity findFileEntityIdByFwGrpName(String firmwareGroupName){

		FirmwareGroup firmwareGroup = Ebean.find(FirmwareGroup.class).where()

				.eq("name", firmwareGroupName).orderBy().desc("update_at").setMaxRows(1).findUnique();

		return firmwareGroup.fileEntity;
	}


	public final static FileEntity findFileEntityIdByFwGrpNameForce(String firmwareGroupName, int forceUpgrade){

		FileEntity file = null;
        FirmwareGroup firmwareGroup = Ebean.find(FirmwareGroup.class).where()
				.eq("name", firmwareGroupName).eq("force_upgrade", forceUpgrade).findUnique();
        if(firmwareGroup != null && firmwareGroup.fileEntity != null){
            file = firmwareGroup.fileEntity;
        }

		return file;
	}

}
