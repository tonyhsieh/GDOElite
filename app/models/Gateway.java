package models;

import com.avaje.ebean.Ebean;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "email2text")
public class Gateway extends BaseEntity
{
    private static final long serialVersionUID = 4389879047512352897L;

    @Id
    public int id;
    public String countryCode;
    public String carrier;
    public String gateway;

    public static List<Gateway> list()
    {
        return Ebean.find(Gateway.class)
                .where()
                .findList();
    }

    public static Gateway getById(int id)
    {
        return Ebean.find(Gateway.class)
                .where()
                .eq("id", id)
                .findUnique();
    }
}
