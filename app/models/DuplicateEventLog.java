package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;

import play.data.validation.Constraints;

@Entity
public class DuplicateEventLog extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2747448186479730679L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;
	
	@ManyToOne
	@JoinColumn(name="smart_genie_id", nullable=false)
	public SmartGenie smartGenie;
	
	@Constraints.Required
	public String msgId;
	
	@Constraints.Required
	public int eventType;
	
	/*
	public final static List<DuplicateEventLog> listBySgId(SmartGenie sg){
		return Ebean.find(DuplicateEventLog.class)
				.where()
				.eq("smartGenie", sg)
				.order().desc("create_at")
				.findList();
	}
	
	public static Page<DuplicateEventLog> userPage(Integer page, Integer pageSize){
		Query<DuplicateEventLog> query = Ebean.find(DuplicateEventLog.class);

		query.order().desc("create_at");
		
		PagedList<DuplicateEventLog> PagedList = query.findPagedList(pageSize);

		PagedList.getFutureRowCount();

		return pagingList.getPage(page);
	}
	*/
	
	public static PagedList<DuplicateEventLog> logPageBySg(String sgId, String page, Integer pageSize){
		Query<DuplicateEventLog> query = Ebean.find(DuplicateEventLog.class)
					.where(Expr.eq("smart_genie_id", sgId));
		
		query.order().desc("create_at");

		int iPage = 0;

		try{
			iPage = Integer.parseInt(page);
		}catch(Exception err){

		}

		PagedList<DuplicateEventLog> pagingList = query.findPagedList(iPage,pageSize);


		return pagingList;
	}
}
