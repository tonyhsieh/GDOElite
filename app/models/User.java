package models;

import java.util.UUID;

import javax.persistence.*;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.*;
import com.avaje.ebean.Query;

@Entity
@Table(name = "user")
public class User extends BaseEntity
{
    private static final long serialVersionUID = 2454965209158285511L;

    @Id
    @Constraints.Required
    @Constraints.Max(40)
    public UUID id;

    public String nickname;

    @ManyToOne
    @JoinColumn(name = "owner_userId", nullable = true)
    public User owner;

    public String email;

    public String countryCode;

    public String cellPhone;

    public Boolean isAdmin = false;

    public String fbUserId;

    @ManyToOne
    @JoinColumn(name = "gateway_id")
    public Gateway gateway;

    public static User find(String userId)
    {
        return Ebean.find(User.class, userId);
    }

    public static PagedList<User> userPage(String orderBy, Boolean asc, Integer page, Integer pageSize)
    {
        Query<User> query = Ebean.find(User.class);

        if (asc)
        {
            query.order().asc(orderBy);
        }
        else
        {
            query.order().desc(orderBy);
        }

        return query.findPagedList(page, pageSize);
    }

    public static PagedList<User> userPageWithEmail(String email, String orderBy, Boolean asc, Integer page, Integer pageSize)
    {
        Query<User> query = Ebean.find(User.class)
                .where(Expr.eq("email", email));

        if (asc)
        {
            query.order().asc(orderBy);
        }
        else
        {
            query.order().desc(orderBy);
        }

        return query.findPagedList(page, pageSize);
    }

    public static User findByEmail(String email)
    {
        return Ebean.find(User.class).where()
                .eq("email", email)
                .findUnique();
    }
}
