/**
 *
 */
package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
@Entity
@Table(name="device_log")
public class DeviceLog extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -7357588476951189218L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;

	public String ownerUserId;

	public String sgId;

	public String xGId;

	@Constraints.Required
	public int eventType;

	@Constraints.Required
	public String eventData;

	public FileEntity attachment;


}
