/**
 * 
 */
package models;

import com.avaje.ebean.*;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/8/14 上午10:47:07
 */

@Entity(name = "email_blacklist")
public class EmailBlacklist extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2388689047511553872L;

	@Id
	public String id;
	
	public String email;


	
	public static EmailBlacklist findByEmail(String email){
		return Ebean.find(EmailBlacklist.class).where()
				.eq("email", email)
				.findUnique();
	}

    public static Boolean emailIsValid(String email){
        EmailBlacklist eb = Ebean.find(EmailBlacklist.class).where().eq("email", email).findUnique();
        if(eb == null){
            return true;
        }else{
            return false;
        }
    }

	public static PagedList<EmailBlacklist> emailBlacklistPage(String orderBy, Boolean asc, Integer page, Integer pageSize) {
		Query<EmailBlacklist> query = Ebean.find(EmailBlacklist.class);

		if (asc) {
			query.order().asc(orderBy);
		} else {
			query.order().desc(orderBy);
		}

		PagedList<EmailBlacklist> pagingList = query.findPagedList(page,pageSize);


		return pagingList;

	}




}
