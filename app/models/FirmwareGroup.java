package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;

/**
 *
 * @author johnwu
 *
 */

@Entity
public class FirmwareGroup extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = 7883514614862331763L;

	@Id
	@Constraints.Required
	public UUID id;

	@Constraints.Required
	public String name;

	@Constraints.Required
	public String firmwareVersion;

	public int force_upgrade;

	@ManyToOne
	@JoinColumn(name="file_entity_id", nullable=false)
	public FileEntity fileEntity;



	@Override
	public String toString() {
		return "FirmwareGroup{" +
				"id=" + id +
				", name='" + name + '\'' +
				", firmwareVersion='" + firmwareVersion + '\'' +
				", force_upgrade=" + force_upgrade +
				", fileEntity=" + fileEntity +
				'}';
	}

	public final static FirmwareGroup findByFwGrpName(String firmwareGroupName){

		FirmwareGroup firmwareGroup = Ebean.find(FirmwareGroup.class).where()
				.eq("name", firmwareGroupName).orderBy().desc("update_at").setMaxRows(1).findUnique();

		return firmwareGroup;
	}

	public final static FirmwareGroup findByFwGrpNameForce(String firmwareGroupName, int forceUpgrade){

		FirmwareGroup firmwareGroup = Ebean.find(FirmwareGroup.class).where()
				.eq("name", firmwareGroupName).eq("force_upgrade", forceUpgrade).findUnique();


		return firmwareGroup;
	}


	public static PagedList<FirmwareGroup> fgPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<FirmwareGroup> query = Ebean.find(FirmwareGroup.class);

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<FirmwareGroup> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}

	public static PagedList<FirmwareGroup> fgPage(String fgName, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<FirmwareGroup> query = Ebean.find(FirmwareGroup.class)
				.where(Expr.eq("name", fgName));

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<FirmwareGroup> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}
}
