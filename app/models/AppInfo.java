package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;
import play.data.validation.Constraints;

import java.util.List;

@Entity
public class AppInfo extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8630013660122296676L;

	@Id
	@Constraints.Max(40)
	public String id;
	
	@Constraints.Required
	public String device_os_name;
	
	@Constraints.Required
	public String version_name;
	
	@Constraints.Required
	public int version_code;
	
	public int force_upgrade;
	
	public final static AppInfo find(String id){
		return Ebean.find(AppInfo.class, id);
	}
	
	public final static AppInfo findByOsName(String device_os_name){
		
		AppInfo appInfo = Ebean.find(AppInfo.class).where()
				.eq("device_os_name", device_os_name)
				.findUnique();

				return appInfo;
	}

	public static PagedList<AppInfo> appInfoPage(String orderBy, Boolean asc, Integer page, Integer pageSize) {
		Query<AppInfo> query = Ebean.find(AppInfo.class);

		if (asc) {
			query.order().asc(orderBy);
		} else {
			query.order().desc(orderBy);
		}

		PagedList<AppInfo> pagingList = query.findPagedList(page,pageSize);


		return pagingList;

	}
}
