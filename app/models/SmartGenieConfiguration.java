package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;

@Entity
public class SmartGenieConfiguration extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = 1377021391901569178L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;

	@ManyToOne
	@JoinColumn(name="smartGenie_id", nullable=false)
	public SmartGenie smartGenie;

	@Constraints.Required
	@ManyToOne
	@JoinColumn(name="cfg_file_id", nullable=false)
	public FileEntity cfgFile;

	@Constraints.Required
	@Constraints.Min(0)
	public long cfgVersion;

	public final static SmartGenieConfiguration findLatestCfg(SmartGenie sg){
		return Ebean.find(SmartGenieConfiguration.class)
					.where()
						.eq("smartGenie", sg)
					.orderBy()
						.desc("cfgVersion")
					.setMaxRows(1)
					.findUnique();
	}
}
