package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import frameworks.models.PairingType;

import play.data.validation.Constraints;

@Entity
public class PairingHistoryLog extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2478659597473146221L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;
	
	@Constraints.Required
	public String userId;
	
	@Constraints.Required
	public String macAddr;
	
	@Constraints.Required
	public PairingType type;
}
