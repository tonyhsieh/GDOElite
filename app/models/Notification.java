package models;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import frameworks.Constants;
import play.data.validation.Constraints;

@Entity
public class Notification extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6153795866061795009L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;
	
	public String senderId;
	
	public String receiverId;
	
	@Constraints.Required
	public String smartGenieId;
	
	public String xgenieId;
	
	@Constraints.Required
	public Calendar timeStamp;
	
	public int eventType;
	
	public String eventContactId;
	
	@Constraints.Required
	public String notificationType;		/* email/sms/push notification */
	
	public String notificationTo;		/* email address/mobile no/pn id token */
	
	public String msg;					/* notification message */
	
	public String response;
	
	public String memo;
	
	public final static Notification find(String id){
		return Ebean.find(Notification.class, id);
	}
	
	public final static List<Notification> listBySenderId(String senderId){
		return Ebean.find(Notification.class)
				.where()
				.eq("senderId", senderId)
				.findList();
	}
	
	public final static List<Notification> listBySenderIdNReceiverId(String senderId, String receiverId){
		return Ebean.find(Notification.class)
				.where()
				.eq("senderId", senderId)
				.eq("receiverId", receiverId)
				.findList();
	}
	
	public final static List<Notification> listBySenderIdNReceiverIdNEventContactId(String senderId, String receiverId, String eventContactId){
		return Ebean.find(Notification.class)
				.where()
				.eq("senderId", senderId)
				.eq("receiverId", receiverId)
				.eq("eventContactId", eventContactId)
				.findList();
	}
	
	public final static Notification findByInputForm(String smartGenieId, String xgenieId, Calendar timeStamp, int eventType){
		return Ebean.find(Notification.class)
				.where()
				.eq("smartGenieId", smartGenieId)
				.eq("xgenieId", xgenieId)
				.eq("timeStamp", timeStamp)
				.eq("eventType", eventType)
				.findUnique();
	}
	
	public final static List<Notification> listBySgIdXgIdTime(String smartGenieId, String xgenieId, Calendar timeStamp){
		return Ebean.find(Notification.class)
				.where()
				.eq("smartGenieId", smartGenieId)
				.eq("xgenieId", xgenieId)
				.eq("timeStamp", timeStamp)
				.findList();
	}
}
