package models;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Embeddable;

import play.data.validation.Constraints;

@Embeddable
public class IcEventLogId implements Serializable {
	
	@Constraints.Required
	@Column(name = "xgenie_id", length = 64)
	private String xGenieId;
	
	@Column(name = "zone_id", columnDefinition = "TINYINT")
	private byte zoneId; // 1: Zone 1, 2: Zone 2, 3: Zone 3, 4: Zone 4, 5: Zone 5, 6: Zone 6
	
	@Column(name = "event_type", columnDefinition = "SMALLINT")
	private int eventType;
	
	@Column(name = "start_time", nullable = false)
	private Date startTime;
	
	public IcEventLogId(String xGenieId, byte zoneId, int eventType, Date startTime) {
		this.xGenieId = xGenieId;
		this.zoneId = zoneId;
		this.eventType = eventType;
		this.startTime = startTime;
	}
	
	public String getxGenieId() {
		return xGenieId;
	}

	public void setxGenie(String xGenieId) {
		this.xGenieId = xGenieId;
	}

	public byte getZoneId() {
		return zoneId;
	}

	public void setZoneId(byte zoneId) {
		this.zoneId = zoneId;
	}

	public int getEventType() {
		return eventType;
	}

	public void setEventType(int eventType) {
		this.eventType = eventType;
	}

	public Date getStartTime() {
		return startTime;
	}

	public void setStartTime(Date startTime) {
		this.startTime = startTime;
	}

	@Override
	public boolean equals(Object obj) {
		if (!(obj instanceof IcEventLogId)) {
			return false;
		}
		
		IcEventLogId icEventLogId = (IcEventLogId) obj;
		
		if (!this.xGenieId.equals(icEventLogId.getxGenieId())
				|| this.zoneId != icEventLogId.getZoneId()
				|| this.eventType != icEventLogId.getEventType()
				|| !this.startTime.equals(icEventLogId.getStartTime())) {
			return false;
		}
		
		return true;
	}
	
	@Override
    public int hashCode() {
        int hash = 17;
        hash = 89 * hash + (this.xGenieId != null ? this.xGenieId.hashCode() : 0);
        hash = hash + 89 * this.zoneId;
        hash = hash + 89 * this.eventType;
        hash = hash + 89 * this.startTime.hashCode();
        return hash;
    }
	
}
