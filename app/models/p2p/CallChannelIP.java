package models.p2p;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Model;
import com.avaje.ebean.annotation.ConcurrencyMode;
import com.avaje.ebean.annotation.EntityConcurrencyMode;
import models.BaseEntity;
import org.apache.commons.lang3.exception.ExceptionUtils;

import utils.ConfigUtil;
import utils.LoggingUtils;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
@Entity
@Table(name="call_channel_ip")
public class CallChannelIP extends BaseEntity {
    private static final long serialVersionUID = 4389879047512352893L;

    @Id
    @Column(name = "mac_address")
    public String macAddress;

    @Column(name = "server_ip")
    public String serverIP;

    private static String getServerInternalIP() {
        return ConfigUtil.getP2pServerInternalIP();
    }

    public static void updateMacAddress(String macAddress) {
        try {
            CallChannelIP currentRow = CallChannelIP.find(macAddress);
            if (currentRow == null) {
                currentRow = new CallChannelIP();
                currentRow.macAddress = macAddress;
                currentRow.serverIP = getServerInternalIP();
                try{
                    currentRow.save();
                }catch (Exception e){
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "currentRow save error " + ExceptionUtils.getStackTrace(e));
                }
            } else {
                currentRow.serverIP = getServerInternalIP();
                try{
                    currentRow.update();
                }catch (Exception err){
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "currentRow update error " + ExceptionUtils.getStackTrace(err));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateMacAddress error " + ExceptionUtils.getStackTrace(e));
        }
    }

    public static CallChannelIP find(String macAddress) {
        return Ebean.find(CallChannelIP.class)
            .where()
            .ieq("mac_address", macAddress)
            .findUnique();
    }

}
