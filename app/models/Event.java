/**
 * 
 */
package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

/**
 * @author johnwu
 * @version 創建時間：2013/8/14 上午10:47:07
 */

@Entity
public class Event extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2388689047511553876L;
	
	@Id	
	public int eventType;
	
	public String eventMsg;
	
	public boolean canNotify;
	
	public String comment;
	
	public static List<Event> list(){
		return Ebean.find(Event.class)
				.where()
				.findList();
	}
	
	public static Event findByType(int eventType){
		return Ebean.find(Event.class).where()
				.eq("eventType", eventType)
				.findUnique();
	}
}
