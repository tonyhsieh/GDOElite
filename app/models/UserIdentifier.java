package models;

import java.util.List;

import javax.persistence.*;

import org.apache.commons.codec.digest.DigestUtils;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import frameworks.models.UserIdentifierType;

@Entity
public class UserIdentifier extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = 7470769664448568675L;

	@Id
	public String id;

	@ManyToOne
	@JoinColumn(name = "user_id", nullable = false)
	@Constraints.Required
	public User user;

	@Constraints.Required
    @Column( name="ident_type")
	public UserIdentifierType identType;

	@Constraints.Required
    @Column( name="ident_data")
	public String identData;

	@Constraints.Required
    @Column( name="password")
	public String password;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public UserIdentifierType getIdentType() {
        return identType;
    }

    public void setIdentType(UserIdentifierType identType) {
        this.identType = identType;
    }

    public String getIdentData() {
        return identData;
    }

    public void setIdentData(String identData) {
        this.identData = identData;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public final static String genUserIdentifierId(UserIdentifierType identType, String identData){
		return DigestUtils.sha1Hex(identType.name() + "yoyoyo" + identData);
	}

	public final static String pwdHash(String plainPwd){
		return DigestUtils.sha1Hex("yo" + plainPwd + "man");
	}

	public final static List<UserIdentifier> getListUserIdentifierByUserId(String userId){
		return Ebean.find(UserIdentifier.class)
					.where(
						Expr.eq("user_id", userId)
						).findList();
	}

	public final static UserIdentifier getUserIdentifier(String identData, UserIdentifierType identType){
		String id = genUserIdentifierId(identType, identData);

		UserIdentifier ui = Ebean.find(UserIdentifier.class, id);

		return ui;
	}
}
