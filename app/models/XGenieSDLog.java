package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Entity
@Table(name="xgenie_sd_log")
public class XGenieSDLog extends BaseEntity
{
    @Id
    @Constraints.Required
    @Constraints.Max(64)
    public UUID id;

    public String user_name;
    public String sg_mac_addr;
    public String xg_mac_addr;
    public int event;
    public int interrupt;
    public Date ts;

    public static List<XGenieSDLog> findByXgMac(String xgMAC, int begin, int end, String order)
    {
        if (order == null || order.length() == 0)
        {
            order = "";
        }

        if (order.equalsIgnoreCase("asc"))
        {
            return Ebean.find(XGenieSDLog.class)
                    .where().eq("xg_mac_addr", xgMAC)
                    .order().asc("ts")
                    .setFirstRow(begin).setMaxRows(end)
                    .findList();
        }
        else
        {
            return Ebean.find(XGenieSDLog.class)
                    .where().eq("xg_mac_addr", xgMAC)
                    .order().desc("ts")
                    .setFirstRow(begin).setMaxRows(end)
                    .findList();
        }
    }
}
