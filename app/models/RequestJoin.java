package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.Query;

import frameworks.models.RequestJoinType;

import play.data.validation.*;

@Entity
public class RequestJoin extends BaseEntity {

	/**
	 *
	 */
	private static final long serialVersionUID = -8628207799119254007L;

	@Id
	@Constraints.Required
	public UUID id;

	@Constraints.Required
	public String joinUserId;

	@Constraints.Required
	public String smartGenieId;

	@Constraints.Required
	public String sgOwnerId;

	public RequestJoinType requestType;

	public final static List<RequestJoin> listRequestJoinByUserOrOwner(String userId){
		return Ebean.find(RequestJoin.class)
						.where()
							.or(Expr.eq("sgOwnerId", userId), Expr.eq("joinUserId", userId))
						.findList();
	}

	public final static List<RequestJoin> listRequestJoinByUserNType(String userId, RequestJoinType type){
		Query<RequestJoin> query = Ebean.createQuery(RequestJoin.class);

		if(type == RequestJoinType.OWNER_INVITE_USER)
			query.where(Expr.and(Expr.eq("join_user_id", userId), Expr.eq("request_type", type)));
		else
			query.where(Expr.and(Expr.eq("sg_owner_id", userId), Expr.eq("request_type", type)));

		return query.findList();
	}
}
