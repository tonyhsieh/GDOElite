package models;

import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Version;

import com.avaje.ebean.Model;

import frameworks.models.StatusCode;

@MappedSuperclass
public class BaseEntity extends Model
{
    /**
     * @author john
     */
    private static final long serialVersionUID = 4795411920778971172L;

    /**
     * Optimistic Lock
     */
    @Version
    @Column(name = "opt_lock", nullable = false)
    public Long version;

    /**
     * 記錄本筆資料建立時間
     */
    @Column(name = "create_at", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar createAt;

    /**
     * 記錄本筆資料最後一次更新時間
     */
    @Column(name = "update_at")
    @Temporal(TemporalType.TIMESTAMP)
    public Calendar updateAt;

    /**
     * 記錄本筆資料更新人 ID (login 時使用的 username)
     */
    @Column(name = "update_user_id", length = 64)
    public String updateUserId;

    /**
     * status
     */
    @Column(name = "status", nullable = false)
    public Integer status;

    /**
     * 記錄本筆資料建立人 ID (login 時使用的 username)
     */
    @Column(name = "create_user_id", nullable = false, length = 64)
    public String createUserId ;

    @PrePersist
    @PreUpdate
    public void auditBeforePersist()
    {
        if (this.createAt == null)
        {
            this.createAt = Calendar.getInstance();
        }

        this.updateAt = Calendar.getInstance();

        if (this.createUserId == null)
        {
            this.createUserId = "SYSTEM";
        }

        if (this.updateUserId == null)
        {
            this.updateUserId = "SYSTEM";
        }

        if (this.status == null)
        {
            this.status = StatusCode.GENERAL_SUCCESS.value;
        }
    }
}
