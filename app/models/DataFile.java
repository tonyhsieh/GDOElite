package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
public class DataFile extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8145445189493323604L;

	@Id
	@Constraints.Max(40)
	public String id;
	
	@Constraints.Required
	public String sg_id;
	
	@Constraints.Required
	public String file_id;
	
	@Constraints.Required
	public String content_type;
	
	public String descript;
	
	@Constraints.Required
	public String data_type;
	
	@Constraints.Required
	public String data_name;
	
	public final static DataFile find(String id){
		return Ebean.find(DataFile.class, id);
	}
	
	public final static DataFile findByTypeAndNameAndSgId(String dataType, String dataName, String sgId){
		return Ebean.find(DataFile.class)
				.where()
				.eq("data_type", dataType)
				.eq("data_name", dataName)
				.eq("sg_id", sgId)
				.findUnique();
	}
	
	public final static List<DataFile> listByTypeAndSgId(String dataType, String sgId){
		return Ebean.find(DataFile.class)
				.where()
				.eq("data_type", dataType)
				.eq("sg_id", sgId)
				.findList();
	}
	
	public final static List<DataFile> listDataFileByMacAddress(String macAddr){
		return Ebean.createQuery(DataFile.class)
				.where(Expr.eq("mac_addr", macAddr))
				.findList();
	}
	
	public final static List<DataFile> listConfigurationByDataType(String dataType){
		return Ebean.createQuery(DataFile.class)
				.where(Expr.eq("data_type", dataType))
				.findList();
	}
}
