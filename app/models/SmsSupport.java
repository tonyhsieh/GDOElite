package models;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import play.data.validation.Constraints;

@Entity
public class SmsSupport extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7777629843475207781L;

	@Id
	@Constraints.Required
	public String countryCode;
	
	@Constraints.Required
	public boolean available;
	
	public String country;
	
	public String smsCompany;
	
	public static SmsSupport findByCountryCode(String countryCode){
		return Ebean.find(SmsSupport.class).where()
				.eq("country_code", countryCode)
				.findUnique();
	}
}
