package models;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;

@Entity
@Table(name = "ic_event_log")
public class IcEventLog extends BaseEntity {
	
	@EmbeddedId
	IcEventLogId icEventLogId;
	
	@Column(name = "duration", columnDefinition = "SMALLINT")
	private int duration; // Unit: minutes; Minimum: 1
	
	@Column(name = "end_time", nullable = false)
	private Date endTime;
	
	public IcEventLog(IcEventLogId icEventLogId, int duration) {
		this.icEventLogId = icEventLogId;
		this.duration = duration;
		Calendar c = Calendar.getInstance();
		c.setTime(this.icEventLogId.getStartTime());
		c.add(Calendar.MINUTE, duration);
		this.endTime = c.getTime();
	}
	
	public IcEventLogId getIcEventLogId() {
		return this.icEventLogId;
	}

	public int getDuration() {
		return duration;
	}

	public Date getEndTime() {
		return endTime;
	}
	
	public boolean hasOverlappeRecord() {
		IcEventLogId icEventLogId = this.icEventLogId;
		Date endTime = this.endTime;
		
		List<IcEventLog> lstLog = new ArrayList<>();
		
		Calendar c = Calendar.getInstance();
		c.setTime(endTime);
		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) - 1);
		endTime = c.getTime();
		lstLog = Ebean.find(IcEventLog.class)
				.where()
				.eq("xgenie_id", icEventLogId.getxGenieId())
				.eq("zone_id", icEventLogId.getZoneId())
				.eq("event_type", icEventLogId.getEventType())
				.between("start_time", icEventLogId.getStartTime(), endTime)
				.findList();
		if (lstLog.size() > 0) {
			return true;
		}
		
		Date startTime = icEventLogId.getStartTime();
		c.setTime(startTime);
		c.set(Calendar.MINUTE, c.get(Calendar.MINUTE) + 1);
		startTime = c.getTime();
		lstLog =  Ebean.find(IcEventLog.class)
				.where()
				.eq("xgenie_id", icEventLogId.getxGenieId())
				.eq("zone_id", icEventLogId.getZoneId())
				.eq("event_type", icEventLogId.getEventType())
				.between("end_time", startTime, endTime)
				.findList();
		if (lstLog.size() > 0) {
			return true;
		}
		
		lstLog = Ebean.find(IcEventLog.class)
				.where()
				.eq("xgenie_id", icEventLogId.getxGenieId())
				.eq("zone_id", icEventLogId.getZoneId())
				.eq("event_type", icEventLogId.getEventType())
				.le("start_time", icEventLogId.getStartTime())
				.ge("end_time", endTime)
				.findList();
		if (lstLog.size() > 0) {
			return true;
		}
		
		return false;
	}

}
