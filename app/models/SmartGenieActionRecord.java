package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;

@Entity
public class SmartGenieActionRecord extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3758846995460235903L;

	@Id
	@Constraints.Required
	public String id;
	
	/*
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="smart_genie_id", nullable=false, insertable=false, updatable=false)
	public SmartGenie smartGenie;
	*/
	
	public String action;
	
	public String form;
	
	public final static SmartGenieActionRecord find(String sgId){
		return Ebean.find(SmartGenieActionRecord.class, sgId);
	}
}
