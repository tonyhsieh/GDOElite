package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.avaje.ebean.Ebean;

import play.data.validation.Constraints;

@Entity
@Table(name="event_contact")
public class EventContact extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1538137527484205565L;

	@Id
	@Constraints.Required
    @Constraints.Max(40)
	public String id;
	
	@Constraints.Required
	public int eventType;
	
	@Constraints.Required
	public String deviceMac;
	
	public static List<EventContact> listByMac(String deviceMac){
		return Ebean.find(EventContact.class)
				.where()
				.eq("device_mac", deviceMac)
				.findList();
	}
	
	public static EventContact findByMacAndEventType(String deviceMac, int eventType){
    	return Ebean.find(EventContact.class)
    			.where()
    			.eq("deviceMac", deviceMac)
    			.eq("eventType", eventType)
    			.findUnique();
    }
}
