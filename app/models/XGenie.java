package models;

import java.util.Calendar;
import java.util.List;

import javax.persistence.*;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.validation.Constraints;
import utils.LoggingUtils;
import utils.generator.CredentialGenerator;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

import com.avaje.ebean.PagedList;
import com.avaje.ebean.Query;

@Entity
public class XGenie extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = 4598582857680954515L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public String id;

	public String name;

	@Column(name="mac_addr")
	public String macAddr;

    @Column(name="device_type")
	public int deviceType;

	@ManyToOne
	@JoinColumn(name="firmware_group_id", nullable=false)
	public FirmwareGroup firmwareGroup;

	@ManyToOne
	@JoinColumn(name="smartGenie_id", nullable=false)
	public SmartGenie smartGenie;
	
	public String info;

    @Column(name="batt_change_date")
    public String battChangeDate;

	@Column(name="ip_addr")
	public String ipaddr;

	@Column(name="fw_ver")
	public String fwVer;

	public Boolean alive;

	public Boolean light;

	public Integer fps;

	@Column(name="door_status")
	public Boolean doorstatus;

    @Column(name="low_batt")
    public Boolean lowBatt;

    @Column(name="ifttt_status")
    public Boolean iftttStatus;





	public final static List<XGenie> listByFG(FirmwareGroup fg){
		return Ebean.find(XGenie.class)
				.where()
				.eq("firmwareGroup", fg)
				.findList();
	}
	
	public final static List<XGenie> listXGBySG(SmartGenie sg){
		return Ebean.find(XGenie.class)
							.where()
								.eq("smartGenie", sg)
							.findList();
	}
	
	public final static XGenie findByXgMac(String xgId){
		return Ebean.find(XGenie.class).where()
						.eq("macAddr", xgId)
						.findUnique();

	}

	public static PagedList<XGenie> xgPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<XGenie> query = Ebean.find(XGenie.class);

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<XGenie> pagingList = query.findPagedList(page,pageSize);

		return pagingList;
	}
	
	public static PagedList<XGenie> xgPageWithMacAddr(String macAddr, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<XGenie> query = Ebean.find(XGenie.class)
					.where(Expr.eq("macAddr", macAddr));

		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<XGenie> pagingList = query.findPagedList(page,pageSize);

		return pagingList;
	}
	
	public static PagedList<XGenie> xgPageWithSGenieId(String smartGenieId, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<XGenie> query = Ebean.find(XGenie.class)
					.where(Expr.eq("smartGenie_id", smartGenieId));
		
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}

		PagedList<XGenie> pagingList = query.findPagedList(page,pageSize);


		return pagingList;
	}
	
	public final static XGenie isDuplicated(String macAddress, SmartGenie reqSmartGenie, String firmwareGroupName, String strXGenieType){
		XGenie xGenie = Ebean.find(XGenie.class).where()
			.eq("macAddr", macAddress)
			.findUnique();
		
		int iXGenieType = 0;
		
		try{
			iXGenieType = Integer.parseInt(strXGenieType);
		}catch(Exception err){}
		
		XGenieType xGenieType = XGenieType.findByType(iXGenieType);
		
			if(xGenie==null){									//沒有記錄
				//以輸入的firmwareGroupName 找出 FirmwareVerId
				FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(firmwareGroupName);

				if(firmwareGroup == null)
					return null;

				xGenie = new XGenie();
				
				if(xGenieType==null){
					xGenie.name = "Passive Device";
				}else{
					xGenie.name = xGenieType.deviceName;
				}
				
				xGenie.macAddr = macAddress;
				xGenie.id = CredentialGenerator.genXGenieId(macAddress);
				xGenie.smartGenie = reqSmartGenie;
				xGenie.firmwareGroup = firmwareGroup;
				xGenie.deviceType = iXGenieType;
				return xGenie;
			}
			else{												//已經有加入到xgenie table記錄
				if(xGenie.smartGenie == null){					//有XGenie記錄,但沒SmartGenie(可能被 RemoveDevice())
					//以輸入的firmwareGroupName 找出 FirmwareVerId
					FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(firmwareGroupName);

					if(xGenieType==null){
						xGenie.name = "Passive Device";
					}else{
						xGenie.name = xGenieType.deviceName;
					}
					
					xGenie.macAddr = macAddress;
					xGenie.id = CredentialGenerator.genXGenieId(macAddress);
					xGenie.status=1;
					xGenie.firmwareGroup = firmwareGroup;
					xGenie.smartGenie = reqSmartGenie;
					xGenie.deviceType = iXGenieType;
					return xGenie;
				}
				else{
					if(xGenie.smartGenie.id.equals(reqSmartGenie.id)){	//有XGenie記錄,是否同一台SmartGenie
						return xGenie;
					}
					else{
						//return null;

						//應觀眾要求, 同一台 Asante Genie如果重覆新增同一台Passive device時,回傳值要求更改為成功
						//By Jack 2013 07 18
						return xGenie;
					}
				}
			}
	}

    public final static XGenie isDuplicatedv2(String macAddress, SmartGenie reqSmartGenie, String firmwareGroupName, String strXGenieType, String XgenieName){
        XGenie xGenie = Ebean.find(XGenie.class).where()
                .eq("macAddr", macAddress)
                .findUnique();

        int iXGenieType = 0;

        try{
            iXGenieType = Integer.parseInt(strXGenieType);
        }catch(Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "xgenie type change error " + ExceptionUtils.getStackTrace(err));
        }
        XGenieType xGenieType = XGenieType.findByType(iXGenieType);

        if(xGenie == null){									//沒有記錄
            //以輸入的firmwareGroupName 找出 FirmwareVerId
            FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(firmwareGroupName);

            if(firmwareGroup == null)
                return null;

            xGenie = new XGenie();
            xGenie.name= XgenieName;
            if(XgenieName.equals("null")){
                if(xGenieType == null){
                    xGenie.name = "Passive Device";
                }else{
                    xGenie.name = xGenieType.deviceName;
                }
            }
            xGenie.macAddr = macAddress;
            xGenie.id = CredentialGenerator.genXGenieId(macAddress);
            xGenie.smartGenie = reqSmartGenie;
            xGenie.firmwareGroup = firmwareGroup;
            xGenie.deviceType = iXGenieType;
            return xGenie;
        } else{												//已經有加入到xgenie table記錄
            if(xGenie.smartGenie == null){					//有XGenie記錄,但沒SmartGenie(可能被 RemoveDevice())
                //以輸入的firmwareGroupName 找出 FirmwareVerId
                FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(firmwareGroupName);
                xGenie.name = XgenieName;
                if(XgenieName.equals("null")){
                    if(xGenieType == null){
                        xGenie.name = "Passive Device";
                    }else{
                        xGenie.name = xGenieType.deviceName;
                    }
                }
                xGenie.macAddr = macAddress;
                xGenie.id = CredentialGenerator.genXGenieId(macAddress);
                xGenie.status=1;
                xGenie.firmwareGroup = firmwareGroup;
                xGenie.smartGenie = reqSmartGenie;
                xGenie.deviceType = iXGenieType;
                return xGenie;
            } else{
                if(xGenie.smartGenie.id.equals(reqSmartGenie.id)){	//有XGenie記錄,是否同一台SmartGenie
                    xGenie.name= XgenieName;
                    return xGenie;
                }
                else{
                    xGenie.name= XgenieName;
                    //return null;

                    //應觀眾要求, 同一台 Asante Genie如果重覆新增同一台Passive device時,回傳值要求更改為成功
                    //By Jack 2013 07 18
                    return xGenie;
                }
            }
        }
    }



	
	public final static List<XGenie> listByDevType(int devType){
		return Ebean.find(XGenie.class)
				.where()
				.eq("deviceType", devType)
				.findList();
	}
}
