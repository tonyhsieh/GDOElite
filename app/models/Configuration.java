package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
public class Configuration extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3392938965522436228L;

	@Id
	@Constraints.Max(40)
	public String id;
	
	@Constraints.Required
	public String mac_addr;
	
	@Constraints.Required
	public String descript;
	
	public boolean avilable;
	
	@Constraints.Required
	public String cfg_type;

    @Override
    public String toString() {
        return "Configuration{" +
                "id='" + id + '\'' +
                ", mac_addr='" + mac_addr + '\'' +
                ", descript='" + descript + '\'' +
                ", avilable=" + avilable +
                ", cfg_type='" + cfg_type + '\'' +
                '}';
    }

    public final static Configuration find(String id){
		return Ebean.find(Configuration.class, id);
	}
	
	public final static List<Configuration> listConfigurationByMacAddress(String macAddr){
		return Ebean.createQuery(Configuration.class)
				.where(Expr.eq("mac_addr", macAddr))
				//.order().desc("update_at")
				.findList();
	}
	
	public final static List<Configuration> listConfigurationByCfgType(String cfgType){
		return Ebean.createQuery(Configuration.class)
				.where(Expr.eq("cfg_type", cfgType))
				.findList();
	}
}
