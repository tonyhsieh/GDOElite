package models;

import java.util.UUID;

import javax.persistence.*;
import play.data.validation.*;

@Entity
public class SmartGenieConfigurationHistory extends BaseEntity {

	/**
	 * @author john
	 */
	private static final long serialVersionUID = -5919785636479366771L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;

	@ManyToOne
	@JoinColumn(name="smartGenie_id", nullable=false)
	public SmartGenie smartGenie;

	public String version;
}
