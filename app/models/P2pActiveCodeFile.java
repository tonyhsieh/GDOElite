package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.*;
import com.avaje.ebean.Query;

import play.data.validation.Constraints;

@Entity
public class P2pActiveCodeFile extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1836211207395826063L;
	
	@Id
	@Constraints.Required
	public UUID id;
	
	public String name;
	
	public int items;
	
	public String fileName;
	
	public String s3_name;
	
	public static PagedList<P2pActiveCodeFile> facPage(String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<P2pActiveCodeFile> query = Ebean.find(P2pActiveCodeFile.class);
	
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}
	
		PagedList<P2pActiveCodeFile> pagingList = query.findPagedList(page, pageSize);
	

	
		return pagingList;
	}
	
	public static PagedList<P2pActiveCodeFile> facPageWithFacName(String facName, String orderBy, Boolean asc, Integer page, Integer pageSize){
		Query<P2pActiveCodeFile> query = Ebean.find(P2pActiveCodeFile.class)
				.where(Expr.eq("name", facName));
	
		if(asc){
			query.order().asc(orderBy);
		}else{
			query.order().desc(orderBy);
		}
	
		PagedList<P2pActiveCodeFile> pagingList = query.findPagedList(page, pageSize);

	
		return pagingList;
	}
}
