package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;

import play.data.validation.Constraints;

@Entity
public class ConfigurationHistory extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8313781379550085516L;

	@Id
	@Constraints.Max(40)
	public UUID id;
	
	@Constraints.Required
	public String configuration_id;
	
	@Constraints.Required
	public String mac_addr;
	
	@Constraints.Required
	public String sg_id;
	
	@Constraints.Required
	public String descript;
	
	public boolean avilable;
	
	@Constraints.Required
	public String cfg_type;
}
