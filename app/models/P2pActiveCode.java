package models;

import com.avaje.ebean.Ebean;
import play.data.validation.Constraints;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class P2pActiveCode extends BaseEntity{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8844938470633476837L;

	@Id
	@Constraints.Max(40)
	public String mac_addr;
	
	@Constraints.Required
	public String active_code;

	//add new param
	//public String pn_id;
	
	public final static P2pActiveCode find(String mac_addr){
		return Ebean.find(P2pActiveCode.class, mac_addr);
	}
	
	public final static P2pActiveCode findByMacAddr(String macAddr){
		return Ebean.find(P2pActiveCode.class)
				.where().eq("mac_addr", macAddr)
				.findUnique();
	}
	/*
	public final static P2pActiveCode findByPnId(String pnid){
		return Ebean.find(P2pActiveCode.class)
				.where().eq("dev_pnid", pnid)
				.findUnique();
	}//*/
}
