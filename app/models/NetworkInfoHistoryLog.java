package models;

import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

@Entity
public class NetworkInfoHistoryLog extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8403326262945553237L;

	@Id
	@Constraints.Required
	@Constraints.Max(40)
	public UUID id;
	
	@Constraints.Required
	public String innIp;
	
	@Constraints.Required
	public String netMask;
	
	@ManyToOne
	@JoinColumn(name = "smart_genie_id", nullable = false)
	@Constraints.Required
	public SmartGenie smartGenie;
	
	@Constraints.Required
	public String extIp;
	
	@Constraints.Required
	public String wifiMacAddress;
}
