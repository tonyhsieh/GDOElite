package models;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.avaje.ebean.Ebean;

import play.data.validation.Constraints;

@Entity
public class XGenieType extends BaseEntity{
	
	@Id	
	public int deviceType;
	
	public String deviceName;
	
	public static XGenieType findByType(int deviceType){
		return Ebean.find(XGenieType.class, deviceType);
	}
	
	public static List<XGenieType> list(){
		return Ebean.find(XGenieType.class)
				.where()
				.findList();
	}
}
