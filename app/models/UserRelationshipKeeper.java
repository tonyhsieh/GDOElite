package models;

import java.util.List;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;

@Entity
public class UserRelationshipKeeper extends BaseEntity
{
    private static final long serialVersionUID = 8913753505045161722L;

    @Id
    @Constraints.Required
    @Constraints.Max(40)
    public UUID id;

    @Constraints.Required
    public String ownerId;

    @Constraints.Required
    public String userId;

    @Constraints.Required
    public String nickname;

    @Constraints.Required
    public String countryCode;

    @Constraints.Required
    public String cellPhone;

    @Constraints.Required
    public String email;

    @ManyToOne
    @JoinColumn(name = "gateway_id")
    public Gateway gateway;

    public static List<UserRelationshipKeeper> listByOwner(String ownerId)
    {
        return Ebean.find(UserRelationshipKeeper.class)
                .where(
                        Expr.eq("owner_id", ownerId)
                ).findList();
    }

    public static List<UserRelationshipKeeper> listByUser(String userId)
    {
        return Ebean.find(UserRelationshipKeeper.class)
                .where(
                        Expr.eq("user_id", userId)
                ).findList();
    }

    public static UserRelationshipKeeper FindByOwnerNUser(String ownerId, String userId)
    {
        return Ebean.find(UserRelationshipKeeper.class)
                .where(
                        Expr.and(
                                Expr.eq("owner_id", ownerId),
                                Expr.eq("user_id", userId)
                        )).findUnique();
    }
}
