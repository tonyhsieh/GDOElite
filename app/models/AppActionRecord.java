package models;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import play.data.validation.Constraints;

import com.avaje.ebean.Ebean;

@Entity
public class AppActionRecord extends BaseEntity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2118756620777380408L;

	@Id
	@Constraints.Required
	public String id;
	
	/*
	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="user_id", nullable=false, insertable=false, updatable=false)
	public User user;
	*/
	
	public String action;
	
	public String form;
	
	public final static AppActionRecord find(String userId){
		return Ebean.find(AppActionRecord.class, userId);
	}
}
