package utils;
import play.Configuration;
import javax.inject.Inject;

import play.Logger;
import play.api.Play;


/**
 * Created by tony_hsieh on 2016/6/7.
 */


public class LoggingUtils {
    public static Boolean LogEnable = true;
    public enum LogLevel {
        ERROR, WARN, INFO, DEBUG, TRACE
    }
    public static void log(LogLevel log_level, String message) {
        if(LoggingUtils.LogEnable){
            switch(log_level) {
                case TRACE:
                    Logger.trace(message);
                    break;
                case DEBUG:
                    Logger.debug(message);
                    break;
                case INFO:
                    Logger.info(message);
                    break;
                case WARN:
                    Logger.warn(message);
                    break;
                case ERROR:
                    Logger.error(message);
                    break;
            }
        }
    }








}

