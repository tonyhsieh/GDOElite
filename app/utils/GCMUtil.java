package utils;

import java.util.List;
import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import javax.inject.Inject;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import play.libs.ws.*;
import play.Configuration;

import utils.json.vo.GCMDataVO;
import utils.json.vo.GCMNode;

public class GCMUtil
{
	@Inject Configuration configuration;
	@Inject WSClient ws;
	public static GCMUtil getInstance(){
		return new GCMUtil();
	}
	
	public String send(List<String> receivers, String title, String content){
		ObjectMapper om = new ObjectMapper();
		
		GCMDataVO data = new GCMDataVO();
		data.title = title;
		data.content = content;
		
		GCMNode node = new GCMNode();
		node.data = data;
		node.registration_ids = receivers;
		
		JsonNode jn = om.convertValue(node, JsonNode.class);

		CompletionStage<WSResponse> stage = ws.url(configuration.getString("gcm.url"))
				.setHeader("Authorization", "key=" + configuration.getString("gcm.api.key"))
				.setContentType("application/json")
				.post(jn);


		WSResponse response = null;
		try {
			response = stage.toCompletableFuture().get();
		} catch (InterruptedException e) {
			e.printStackTrace();
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "GCM resp status : " + response + " / msg : ");


		return response.getBody();
	}
}
