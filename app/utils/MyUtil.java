package utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.codec.binary.Base64;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import java.util.UUID;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

import frameworks.models.StatusCode;
import json.models.api.v1.BasicRespPack;
import play.Mode;
import play.mvc.Http;

public class MyUtil
{
    public static TimeZone getTimeZone()
    {
        String timeZone = ConfigUtil.getTimeZone();
        if (timeZone == null || timeZone.length() == 0)
        {
            return TimeZone.getDefault();
        }
        return TimeZone.getTimeZone(timeZone);
    }

    public static Calendar getCalendar()
    {
        return Calendar.getInstance(getTimeZone());
    }

    public static String getCurrentTime()
    {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS z");
        TimeZone timeZone = getTimeZone();
        if (timeZone == null)
        {
            timeZone = TimeZone.getDefault();
        }
        dateFormat.setTimeZone(timeZone);
        Calendar cal = getCalendar();
        return dateFormat.format(cal.getTime());
    }

    public static String newUUID()
    {
        return UUID.randomUUID().toString();
    }

    private static JsonNode _getJsonNode(Object object)
    {
        ObjectMapper objectMapper = OMManager.getInstance().getObjectMapper();
        try
        {
            return objectMapper.valueToTree(object);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return objectMapper.createObjectNode();
        }
    }

    public static JsonNode getBasicResponse(Http.RequestHeader request, StatusCode statusCode, String message)
    {
        BasicRespPack basicRespPack = new BasicRespPack();
        basicRespPack.status.code = statusCode.value;
        basicRespPack.status.message = message;
        return _getJsonNode(basicRespPack);
    }

    public static String getClientIp(Http.RequestHeader request)
    {
        if (request != null)
        {
            String x = request.getHeader("x-forwarded-for");
            if (x != null && x.length() > 0)
            {
                return x;
            }

            x = request.getHeader("X-Forwarded-For");
            if (x != null && x.length() > 0)
            {
                return x;
            }

            return request.remoteAddress();
        }
        return null;
    }

    public static String getDeployEnv()
    {
        play.api.Application app = play.api.Play.current();

        if (play.api.Play.isProd(app))
        {
            return Mode.PROD.name();
        }

        if (play.api.Play.isTest(app))
        {
            return Mode.TEST.name();
        }

        if (play.api.Play.isDev(app))
        {
            return Mode.DEV.name();
        }

        return "impossible";
    }

    public static String encrypt(String key, String strToEncrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
            final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey);
            return Base64.encodeBase64String(cipher.doFinal(strToEncrypt.getBytes()));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }

    public static String decrypt(String key, String strToDecrypt)
    {
        try
        {
            Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5PADDING");
            final SecretKeySpec secretKey = new SecretKeySpec(key.getBytes(), "AES");
            cipher.init(Cipher.DECRYPT_MODE, secretKey);
            return new String(cipher.doFinal(Base64.decodeBase64(strToDecrypt)));
        }
        catch (Exception e)
        {
            throw new RuntimeException(e);
        }
    }
}
