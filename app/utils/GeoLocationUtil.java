package utils;


import com.maxmind.geoip2.DatabaseReader;
import com.maxmind.geoip2.model.*;
import com.maxmind.geoip2.record.Location;

import models.Coordinate;

import org.apache.commons.lang3.exception.ExceptionUtils;

import utils.ConfigUtil;
import utils.LoggingUtils;

import java.io.File;
import java.net.InetAddress;
import java.util.Calendar;
import java.util.TimeZone;

/**
 * Created by tony_hsieh on 2017/1/20.
 */
public class GeoLocationUtil
{
    public static int getTimeZoneOffset(String ipAddress)
    {
        File database = new File(ConfigUtil.getMaxMindPath());
        InetAddress ip = null;
        DatabaseReader reader = null;
        CityResponse response = null;
        int ny_offset_ms = 0;
        int ny_offset_hour = 0;
        try
        {
            reader = new DatabaseReader.Builder(database).build();
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP reader error" + ExceptionUtils.getStackTrace(exp));
        }
        try
        {
            ip = com.google.common.net.InetAddresses.forString(ipAddress);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "geoIP Inet" + ip);
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP Inet change type failed" + ExceptionUtils.getStackTrace(exp));
        }
        try
        {
            response = reader.city(ip);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "geoIP city response" + response);
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP city response exception" + ExceptionUtils.getStackTrace(exp));
            return 0;
        }
        Location location = response.getLocation();
        TimeZone tz = TimeZone.getTimeZone(location.getTimeZone());
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "location get time zone" + location.getTimeZone() + "Time Zone " + tz);
        Calendar tz_cal = Calendar.getInstance(tz);
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Calendar " + tz_cal);
        ny_offset_ms = tz_cal.get(Calendar.ZONE_OFFSET) + tz_cal.get(Calendar.DST_OFFSET);
        ny_offset_hour = ny_offset_ms / 3600000;
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "tz_cal.get(Calendar.ZONE_OFFSET) " + tz_cal.get(Calendar.ZONE_OFFSET) + "tz_cal.get(Calendar.DST_OFFSET)" + tz_cal.get(Calendar.DST_OFFSET));
        return ny_offset_hour;
    }

    public static Coordinate getCoordinate(String ipAddress)
    {
        File database = new File("D:\\GeoLite2-City.mmdb\\GeoLite2-City.mmdb");
        InetAddress ip = null;
        DatabaseReader reader = null;
        CityResponse response = null;
        Coordinate coordinate = null;
        try
        {
            reader = new DatabaseReader.Builder(database).build();
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP reader error" + ExceptionUtils.getStackTrace(exp));
        }
        try
        {
            ip = com.google.common.net.InetAddresses.forString(ipAddress);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "geoIP Inet" + ip);
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP Inet change type failed" + ExceptionUtils.getStackTrace(exp));
        }
        try
        {
            response = reader.city(ip);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "geoIP city response" + response);
        }
        catch (Exception exp)
        {
            exp.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "geoIP city response exception" + ExceptionUtils.getStackTrace(exp));
            coordinate.lat = new Double(0);
            coordinate.lng = new Double(0);
        }
        Location location = response.getLocation();
        coordinate.lng = location.getLongitude();
        coordinate.lat = location.getLatitude();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "location coordinate lng" + coordinate.lng + "location coordinate lat" + coordinate.lat);
        return coordinate;
    }
}


