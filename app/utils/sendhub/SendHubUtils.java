/**
 * 
 */
package utils.sendhub;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.ExecutionException;
import javax.inject.Inject;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.libs.ws.*;
import play.mvc.*;
import play.Configuration;



import utils.LoggingUtils;
import utils.sendhub.json.vo.req.AddContactVO;
import utils.sendhub.json.vo.req.EditContactVO;
import utils.sendhub.json.vo.req.SendMessageVO;
import utils.sendhub.json.vo.resp.AddContactResp;
import utils.sendhub.json.vo.resp.EditContactResp;

/**
 * @author johnwu
 * @version 創建時間：2013/8/16 下午3:28:57
 */

public class SendHubUtils {
    @Inject
    static Configuration configuration;
    @Inject
    static WSClient ws;
	private static String sendhubKey = configuration.getString("sendhub.key");;
	private static String sendhubNumber =  configuration.getString("sendhub.number");
	private static Long groupId = configuration.getLong("sendhub.gourp.id.asante");
	
	public static boolean sendMessage(List<Long> contacts, String text) throws JsonGenerationException, JsonMappingException, IOException{
		ObjectMapper om = new ObjectMapper();
		
		SendMessageVO shsmn = new SendMessageVO();
		
		shsmn.contacts = contacts;
		shsmn.text = text;
		
		JsonNode jn = om.convertValue(shsmn, JsonNode.class);

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, om.writeValueAsString(shsmn));

		String url = "https://api.sendhub.com/v1/messages/";

        CompletionStage<WSResponse> stage = createRequestHolder(url).post(jn);
        WSResponse response = null;
        try {
            response = stage.toCompletableFuture().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
        return (response.getStatus() == 201)?true:false;
	}

	public static AddContactResp addContact(String cellPhone, String userName){

		
		ObjectMapper om = new ObjectMapper();
		
		AddContactVO shan = new AddContactVO();
		
		List<Long> groups = new ArrayList<Long>();
		
		groups.add(groupId);
		
		shan.number = cellPhone;
		shan.name = userName;
		shan.groups = groups;
		
		JsonNode jn = om.convertValue(shan, JsonNode.class);
		
		String url = "https://api.sendhub.com/v1/contacts/";

        CompletionStage<WSResponse> stage = createRequestHolder(url).post(jn);

        WSResponse response = null;

        try {
            response = stage.toCompletableFuture().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

		AddContactResp acResp = null;
		
		try {
			acResp = (response.getStatus()==201)?om.readValue(response.getBody(), AddContactResp.class):null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return acResp;
	}
	
	public static EditContactResp editContact(String contactId, String cellphone, String name){
		//Logger.info("editContact");
		
		String url = "https://api.sendhub.com/v1/contacts/" + contactId + "/";
		
		ObjectMapper om = new ObjectMapper();
		
		EditContactVO ecVO = new EditContactVO();
		ecVO.id = contactId;
		ecVO.name = name;
		ecVO.number = cellphone;
		
		JsonNode jn = om.convertValue(ecVO, JsonNode.class);

        CompletionStage<WSResponse> stage = createRequestHolder(url).put(jn);
        WSResponse response = null;
        try {
            response = stage.toCompletableFuture().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
		
		EditContactResp ecResp;
		try {
			ecResp = (response.getStatus()==202)?om.readValue(response.getBody(), EditContactResp.class):null;
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			ecResp = null;
		}
		
		return ecResp;
	}
	
	public static boolean deleteContact(String shId){
		
		String url = "https://api.sendhub.com/v1/contacts/" + shId + "/";

        CompletionStage<WSResponse> stage = createRequestHolder(url).delete();
        WSResponse response = null;
        try {
            response = stage.toCompletableFuture().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
		
		return (response.getStatus() == 204)?true:false;
	}
	
	private static WSRequest createRequestHolder(String url){
		return ws.url(url)
			.setContentType("application/json")
			.setQueryParameter("username", sendhubNumber)
			.setQueryParameter("api_key", sendhubKey);
	}
}
