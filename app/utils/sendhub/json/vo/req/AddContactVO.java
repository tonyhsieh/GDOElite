/**
 * 
 */
package utils.sendhub.json.vo.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/8/22 上午9:47:51
 */
public class AddContactVO implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8612438585964343753L;

	public List<Long> groups;
	public String number;
	public String name;
}
