/**
 * 
 */
package utils.sendhub.json.vo.req;

import java.io.Serializable;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/8/16 下午12:33:34
 */

public class SendMessageVO implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 805037120244192503L;
	public List<Long> contacts;
	public String text;
}
