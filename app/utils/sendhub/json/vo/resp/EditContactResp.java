/**
 * 
 */
package utils.sendhub.json.vo.resp;

import java.io.Serializable;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/8/28 上午11:15:04
 */
public class EditContactResp implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8961539028772639311L;

	public boolean blocked;
    public String contact_id;
    public String date_created;
    public String date_modified;
    public boolean deleted;
    public List<String> groups;
    public Long id;
    public String id_str;
    public boolean is_owned;
    public String keyword;
    public boolean local_gateway;
    public String name;
    public String number;
    public String pk;
    public String resource_uri;
    public boolean screened;
}
