/**
 * 
 */
package utils.sendhub.json.vo.resp;

import java.io.Serializable;
import java.util.List;

/**
 * @author johnwu
 * @version 創建時間：2013/8/22 上午10:27:46
 */
public class AddContactResp implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6334793274524429494L;

	public boolean blocked;
	
	public String date_created;
	
	public String date_modified;
	
	public boolean deleted;
	
	public List<String> groups;
	
	public Long id;
	
	public String id_str;
	
	public boolean is_owned;
	
	public String keyword;
	
	public boolean local_gateway;
	
	public String name;
	
	public String number;
	
	public String resource_uri;
	
	public boolean screened;
}
