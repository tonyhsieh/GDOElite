package utils;

import io.netty.handler.codec.base64.Base64;
import io.netty.handler.ssl.SslHandler;
import org.yaml.snakeyaml.external.biz.base64Coder.Base64Coder;
import play.Configuration;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.security.KeyStore;

/**
 * Created by tony_hsieh on 2017/6/29.
 */
public class SSLUtil {
    private static SSLContext serverContext = null;
    private static String pkcs12Base64 = "";

    public static SSLContext getServerContext(Configuration configuration) {
        if(serverContext == null){
            try {
                pkcs12Base64 = configuration.getString("ssl.cert");
                serverContext = SSLContext.getInstance("TLS");
                KeyStore ks = KeyStore.getInstance("PKCS12");
                ks.load(new FileInputStream(pkcs12Base64), "secret".toCharArray() );
                KeyManagerFactory kmf = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
                kmf.init(ks, "secret".toCharArray());

                // truststore
                KeyStore ts = KeyStore.getInstance("PKCS12");
                ts.load(new FileInputStream(pkcs12Base64), "secret".toCharArray());
                // set up trust manager factory to use our trust store
                TrustManagerFactory tmf = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
                tmf.init(ts);

                // Initialize the SSLContext to work with our key managers.
                serverContext.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            } catch (Exception e) {
                throw new Error("Failed to initialize the Netty SSLContext", e);
            }
        }
        return serverContext;
    }


}
