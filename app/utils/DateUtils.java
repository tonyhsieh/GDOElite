/**
 *
 */
package utils;

import java.util.Calendar;

import org.apache.commons.lang3.time.DateFormatUtils;

import frameworks.Constants;

/**
 * @author carloxwang
 *
 */
public class DateUtils {

	public final static String formatDate( Calendar c, String pattern){
		return DateFormatUtils.format(c, pattern);
	}
	public final static String formatDate( Calendar c){
		return DateFormatUtils.format(c, Constants.JSON_CALENDAR_PATTERN);
	}

}
