package utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.RegionUtils;
import com.amazonaws.services.sqs.AmazonSQS;
import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.AmazonSQSClientBuilder;
import com.amazonaws.services.sqs.model.CreateQueueRequest;
import com.amazonaws.services.sqs.model.DeleteMessageRequest;
import com.amazonaws.services.sqs.model.DeleteQueueRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlRequest;
import com.amazonaws.services.sqs.model.GetQueueUrlResult;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * 
 * @author johnwu
 *
 */

public class SQSManager
{
	private static String delaySeconds;
	private static String maximumMessageSize;
	private static String messageRetentionPeriod;
	private static String receiveMessageWaitTimeSeconds;
	private static String visibilityTimeout;
	private static int maxNumberOfMessages;
	private static int waitTimeSeconds;
	
	private static final int DEFAULT_MESSAGE_DELAY_TIME = 5;
	
    private static AmazonSQS amazonSQS;
        
    private SQSManager(String awsRegion){
        AmazonSQSClientBuilder builder = AmazonSQSClientBuilder.standard();
        builder.setEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("sqs." + awsRegion + ".amazonaws.com", awsRegion)
        );
        AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(ConfigUtil.getAccessKey(), ConfigUtil.getSecretKey()));
        builder.setCredentials(credentialsProvider);
        amazonSQS = builder.build();
    }
    
	public static SQSManager getInstance(String awsRegion){
        delaySeconds = ConfigUtil.getQueueAttrDelaySeconds();
        maximumMessageSize = ConfigUtil.getQueueAttrMaximumMessageSize();
        messageRetentionPeriod = ConfigUtil.getQueueAttrMessageRetentionPeriod();
        receiveMessageWaitTimeSeconds = ConfigUtil.getQueueAttrReceiveMessageWaitTimeSeconds();
        visibilityTimeout = ConfigUtil.getQueueAttrVisibilityTimeOut();
        maxNumberOfMessages = ConfigUtil.getReceiveMessageMaxNumberOfMessages();
        waitTimeSeconds = ConfigUtil.getQueueReceiveMessageWaitTimeSeconds();
        return new SQSManager(awsRegion);
	}
	
	public String createQueue(String queueName, Map<String, String> attributes){		
		CreateQueueRequest request = new CreateQueueRequest();
		request.setQueueName(queueName);
		request.setAttributes(attributes);
		
		return amazonSQS.createQueue(request).getQueueUrl();
	}
	
	public String createQueue(String queueName){
		Map<String, String> attributes = new HashMap<String, String>();
		
		attributes.put("DelaySeconds", delaySeconds);
		attributes.put("MaximumMessageSize", maximumMessageSize);
		attributes.put("MessageRetentionPeriod", messageRetentionPeriod);
		//attributes.put("Policy", SQS_ATTR_Policy));
		attributes.put("ReceiveMessageWaitTimeSeconds", receiveMessageWaitTimeSeconds);
		attributes.put("VisibilityTimeout", visibilityTimeout);
		//Logger.info("SQSManager->" + attributes.toString());
		return createQueue(queueName, attributes);
	}
	
	public void deleteQueue(String queueUrl){
		amazonSQS.deleteQueue(new DeleteQueueRequest(queueUrl));
	}
	
	public String getQueue(String queueName){
		try{
			GetQueueUrlResult result = amazonSQS.getQueueUrl(new GetQueueUrlRequest(queueName));
			return result.getQueueUrl();
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	
	public String sendMessage(String queueUrl, String messageBody){
		SendMessageRequest request = new SendMessageRequest();
		request.setQueueUrl(queueUrl);
		request.setMessageBody(messageBody);
		request.setDelaySeconds(DEFAULT_MESSAGE_DELAY_TIME);
		
		return amazonSQS.sendMessage(request).getMessageId();
	}
	
	public List<Message> receiveMessage(String queueUrl){
		ReceiveMessageRequest request = new ReceiveMessageRequest();
		request.setQueueUrl(queueUrl);
		request.setMaxNumberOfMessages(maxNumberOfMessages);
		request.setWaitTimeSeconds(waitTimeSeconds);
		
		return amazonSQS.receiveMessage(request).getMessages();
	}
	
	public void deleteMessage(String queueUrl, Message message){
		amazonSQS.deleteMessage(new DeleteMessageRequest(queueUrl, message.getReceiptHandle()));
	}
}
