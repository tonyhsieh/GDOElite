package utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.simpleemail.AWSJavaMailTransport;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.ListVerifiedEmailAddressesResult;
import com.amazonaws.services.simpleemail.model.VerifyEmailAddressRequest;

import play.Configuration;

import javax.inject.Inject;

/**
 * @author carloxwang
 */
public class MailUtil
{
    @Inject
    static Configuration configuration;

    private static String FROM;

    private static MailUtil instance;

    private AmazonSimpleEmailService ses;
    private Properties props;

    private MailUtil()
    {
        try
        {
            FROM = configuration.getString("email.sender");
        }
        catch (Exception err)
        {
            FROM = "noreply@asantegenie.com";
        }

        String accessKey = "AKIAJSOTSXC7JNMQQVFQ";
        String secretKey = "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";

        AmazonSimpleEmailServiceClientBuilder builder
                = AmazonSimpleEmailServiceClientBuilder.standard();
        builder.setEndpointConfiguration(
                new AwsClientBuilder.EndpointConfiguration("email.us-east-1.amazonaws.com", "us-east-1")
        );
        AWSCredentialsProvider credentialsProvider = new AWSStaticCredentialsProvider(new BasicAWSCredentials(accessKey, secretKey));
        builder.setCredentials(credentialsProvider);
        ses = builder.build();

        /*
         * Before you can send email via Amazon SES, you need to verify that you
         * own the email address from which you???ll be sending email. This will
         * trigger a verification email, which will contain a link that you can
         * click on to complete the verification process.
         */

        verifyEmailAddress(ses, FROM);

		/*
         * Setup JavaMail to use the Amazon Simple Email Service by specifying
		 * the "aws" protocol.
		 */
        props = new Properties();
        props.setProperty("mail.transport.protocol", "aws");

        /*
         * Setting mail.aws.user and mail.aws.password are optional. Setting
         * these will allow you to send mail using the static transport send()
         * convince method.  It will also allow you to call connect() with no
         * parameters. Otherwise, a user name and password must be specified
         * in connect.
         */
        props.setProperty("mail.aws.user", accessKey);
        props.setProperty("mail.aws.password", secretKey);
    }

    public static MailUtil instance()
    {
        if (instance == null)
        {
            instance = new MailUtil();
        }
        return instance;
    }

    public void sendMail(String to, String title, String content)
    {
        Session session = Session.getInstance(props);

        try
        {
            // Create a new Message
            Message msg = new MimeMessage(session);
            msg.setFrom(new InternetAddress(FROM));
            msg.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
            msg.setSubject(title);
            msg.setText(content);
            msg.addHeader("Content-Type", "text/html");
            msg.saveChanges();

            // Reuse one Transport object for sending all your messages
            // for better performance
            Transport t = new AWSJavaMailTransport(session, null);
            t.connect();
            t.sendMessage(msg, null);

            LogUtil.sendMailLog(FROM, to, title, content, "AKIAJSOTSXC7JNMQQVFQ");

            // Close your transport when you're completely done sending
            // all your messages
            t.close();
        }
        catch (AddressException e)
        {
            e.printStackTrace();
            System.out.println("Caught an AddressException, which means one or more of your "
                    + "addresses are improperly formatted.");
        }
        catch (MessagingException e)
        {
            e.printStackTrace();
            System.out.println("Caught a MessagingException, which means that there was a "
                    + "problem sending your message to Amazon's E-mail Service check the "
                    + "stack trace for more information.");
        }
    }

    /**
     * Sends a request to Amazon Simple Email Service to verify the specified
     * email address. This triggers a verification email, which will contain a
     * link that you can click on to complete the verification process.
     *
     * @param ses     The Amazon Simple Email Service client to use when making
     *                requests to Amazon SES.
     * @param address The email address to verify.
     */
    private void verifyEmailAddress(AmazonSimpleEmailService ses, String address)
    {
        ListVerifiedEmailAddressesResult verifiedEmails = ses.listVerifiedEmailAddresses();
        if (verifiedEmails.getVerifiedEmailAddresses().contains(address)) return;

        ses.verifyEmailAddress(new VerifyEmailAddressRequest().withEmailAddress(address));
    }

}
