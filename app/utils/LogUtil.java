package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.CredentialsProvider;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicCredentialsProvider;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

public class LogUtil
{
    public static void sendMailLog(final String sender, final String receiver, final String title, final String content, final String accessKey)
    {
        try
        {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("systemName", "asante");
            jsonObject.put("sender", sender);
            jsonObject.put("receiver", receiver);
            jsonObject.put("title", title);
            jsonObject.put("content", content);
            jsonObject.put("accessKey", accessKey);
            log(jsonObject.toString());
        }
        catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public static void log(final String msg)
    {
        try
        {
            play.Logger.info(msg);

            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " log string " + msg);
            log2Http(msg);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
    }

    public static void log2Http(final String msg)
    {
        if (msg == null || msg.length() == 0)
        {
            return;
        }

        String logUrl = ConfigUtil.getLogRemoteHttpUrl();
        if (logUrl == null || logUrl.length() == 0)
        {
            return;
        }

        new Thread(() ->
        {
            try
            {
                CredentialsProvider provider = new BasicCredentialsProvider();

                UsernamePasswordCredentials credentials = new UsernamePasswordCredentials(ConfigUtil.getLogRemoteHttpUser(), ConfigUtil.getLogRemoteHttpPassword());
                provider.setCredentials(AuthScope.ANY, credentials);
                RequestConfig requestConfig = RequestConfig.custom().setConnectTimeout(20 * 1000).build();
                HttpClient httpClient = HttpClientBuilder.create().setDefaultCredentialsProvider(provider)
                        .setDefaultRequestConfig(requestConfig).build();

                ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
                ObjectNode node;
                try
                {
                    node = (ObjectNode) mapper.readTree(msg);
                }
                catch (JsonProcessingException | ClassCastException ee)
                {
                    node = mapper.createObjectNode();
                    node.put("origin", msg);
                }
                node.put("ts", System.currentTimeMillis());

                if (node.has("loginPwd"))
                {
                    node.remove("loginPwd");
                }

                if (node.has("password"))
                {
                    node.remove("password");
                }

                HttpPost request = new HttpPost(ConfigUtil.getLogRemoteHttpUrl());
                StringEntity params = new StringEntity(mapper.writeValueAsString(node));
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "logutil 2 " + mapper.writeValueAsString(node));
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "logutil " + params.toString());
                request.addHeader("content-type", "application/json");
                request.setEntity(params);
                HttpResponse response = httpClient.execute(request);
                HttpEntity entity = response.getEntity();
                String result = EntityUtils.toString(entity, "UTF-8");
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "logUtil sent to server response" + result);
            }
            catch (Exception e)
            {
                e.printStackTrace();
            }
        }).start();
    }
}
