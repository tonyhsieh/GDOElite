package utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.ObjectMapper;

public class OMManager
{
    private static OMManager instance = new OMManager();

    private ObjectMapper objectMapper;

    private OMManager()
    {
        objectMapper = new ObjectMapper();
        objectMapper.getFactory().configure(JsonGenerator.Feature.ESCAPE_NON_ASCII, true);
    }

    public static OMManager getInstance()
    {
        return instance;
    }

    public ObjectMapper getObjectMapper()
    {
        return objectMapper;
    }
}
