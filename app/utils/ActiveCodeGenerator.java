package utils;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * Created by tony_hsieh on 2016/11/9.
 */
public class ActiveCodeGenerator {
    public static String codeGenerator(int n) {
        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ".toCharArray();
        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < n; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        String output = sb.toString();
        return output;
    }
    public static void main(){
        for(int i=0; i<100; i++){
            List<String> codes = new ArrayList<String>();
            String code = codeGenerator(12);
            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, " Active code " + code );
        }
    }

}


