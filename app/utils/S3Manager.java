package utils;

import java.io.File;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.regions.RegionUtils;


import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.GetObjectRequest;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 *
 * @author johnwu
 *
 */

public class S3Manager
{
    private static String s3Bucket;
    private static AmazonS3 amazonS3;

    public enum StageS3Bucket {
        SG_FIRMWARE(ConfigUtil.getS3SgFirmware()),
        XG_FIRMWARE(ConfigUtil.getS3XgFirmware()),
        EVENT_LOG_ATTACHMENT(ConfigUtil.getS3EventLogAttachment()),
        SG_CONFIGURATION(ConfigUtil.getS3SgConfiguration()),
        CONFIGURATION_IRRIGATION_CONTROLLER( ConfigUtil.getS3ConfigurationIrrigationController()),
        SG_PHOTO(ConfigUtil.getS3SgPhoto()),
        ACTIVE_CODE( ConfigUtil.getS3ActiveCode());

        public String value;

        StageS3Bucket(String value) {
			this.value = value;
		}
    }

    public enum ProdS3Bucket {
        SG_FIRMWARE(ConfigUtil.getS3SgFirmware()),
        XG_FIRMWARE(ConfigUtil.getS3XgFirmware()),
        EVENT_LOG_ATTACHMENT(ConfigUtil.getS3EventLogAttachment()),
        SG_CONFIGURATION(ConfigUtil.getS3SgConfiguration()),
        CONFIGURATION_IRRIGATION_CONTROLLER( ConfigUtil.getS3ConfigurationIrrigationController()),
        SG_PHOTO(ConfigUtil.getS3SgPhoto()),
        ACTIVE_CODE(ConfigUtil.getS3ActiveCode());


        public String value;

        ProdS3Bucket(String value) {
        this.value = value;
        }
    }

    public enum DevS3Bucket {
        SG_FIRMWARE(ConfigUtil.getS3SgFirmware()),
        XG_FIRMWARE(ConfigUtil.getS3XgFirmware()),
        EVENT_LOG_ATTACHMENT(ConfigUtil.getS3EventLogAttachment()),
        SG_CONFIGURATION(ConfigUtil.getS3SgConfiguration()),
        CONFIGURATION_IRRIGATION_CONTROLLER(ConfigUtil.getS3ConfigurationIrrigationController()),
        SG_PHOTO(ConfigUtil.getS3SgPhoto()),
        ACTIVE_CODE( ConfigUtil.getS3ActiveCode());

        public String value;

        DevS3Bucket(String value) {
            this.value = value;
        }
    }

	public S3Manager(String bucket){
		s3Bucket = bucket;
	}

	public static S3Manager getInstance(DevS3Bucket bucket){
        init();
		return new S3Manager(bucket.value);
	}
	
	public static S3Manager getInstance(ProdS3Bucket bucket){
	    init();
		return new S3Manager(bucket.value);
	}

    public static S3Manager getInstance(StageS3Bucket bucket){
	    init();
        return new S3Manager(bucket.value);
    }

    public static S3Manager getInstance(String bucketname) {
        init();
        return new S3Manager(bucketname);
    }

    private static void init()
    {
        try{
            amazonS3 = AmazonS3ClientBuilder.standard().withRegion(ConfigUtil.getS3Region())
                    .withCredentials(new AWSStaticCredentialsProvider(new BasicAWSCredentials(ConfigUtil.getAccessKey(), ConfigUtil.getSecretKey())))
                    .build();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "amazon S3 init error" + ExceptionUtils.getStackTrace(err));
        }
    }

    public boolean putFile(File file, String actualFileName) {
        if (amazonS3 == null) {
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Could not save because amazonS3 was null");
            throw new RuntimeException("Could not save");
        }
        else {
        	if(!amazonS3.doesBucketExist(s3Bucket))
        		amazonS3.createBucket(s3Bucket);
            PutObjectRequest putObjectRequest = new PutObjectRequest(s3Bucket, actualFileName, file);
            putObjectRequest.withCannedAcl(CannedAccessControlList.PublicRead); // public for all
            amazonS3.putObject(putObjectRequest); // upload file
            return true;
        }
    }

    public boolean deleteFile(String actualFileName) {
        if (amazonS3 == null) {
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Could not delete because amazonS3 was null");
            throw new RuntimeException("Could not delete");
        }
        else {
        	amazonS3.deleteObject(s3Bucket, actualFileName);
        	return true;
        }
    }

    public S3Object getFile(String actualFileName){
    	if (amazonS3 == null) {
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Could not get file because amazonS3 was null");
            throw new RuntimeException("Could not get file");
        }
        else {
        	try{
	        	GetObjectRequest getObjectRequest = new GetObjectRequest(s3Bucket, actualFileName);
	        	return amazonS3.getObject(getObjectRequest);
        	}catch(Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getFile error" + ExceptionUtils.getStackTrace(e));
        		throw new RuntimeException("get file error");
        	}
        }
    }
}
