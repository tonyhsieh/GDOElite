/**
 *
 */
package utils.db;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.Map;

import models.FileEntity;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;


import org.apache.commons.lang3.exception.ExceptionUtils;

import utils.ConfigUtil;
import utils.LoggingUtils;
import utils.S3Manager;
import utils.S3Manager.ProdS3Bucket;
import utils.S3Manager.DevS3Bucket;
import utils.S3Manager.StageS3Bucket;

import com.amazonaws.services.s3.model.S3Object;

import frameworks.models.FileType;
import frameworks.models.MimeType;

/**
 * @author carloxwang
 *
 */
public class FileEntityManager
{
	private final static Map<FileType, DevS3Bucket> devS3bucketMap = new HashMap<FileType, DevS3Bucket>();

	private final static Map<FileType, StageS3Bucket> stageS3bucketMap = new HashMap<FileType, StageS3Bucket>();
	
	private final static Map<FileType, ProdS3Bucket> prodS3bucketMap = new HashMap<FileType, ProdS3Bucket>();

	private final static String tmpDir = System.getProperty("java.io.tmpdir");

	public FileEntity fileEntity;
	static{

		if (ConfigUtil.getApplicationEnvironment().equals("PROD")) {
			prodS3bucketMap.put(FileType.EVENT_ATTACHMENT, ProdS3Bucket.EVENT_LOG_ATTACHMENT);
			prodS3bucketMap.put(FileType.SG_CONFIGURATION, ProdS3Bucket.SG_CONFIGURATION);
			prodS3bucketMap.put(FileType.SG_FIRMWARE, ProdS3Bucket.SG_FIRMWARE);
			prodS3bucketMap.put(FileType.XG_FIRMWARE, ProdS3Bucket.XG_FIRMWARE);
			prodS3bucketMap.put(FileType.CONFIGURATION_IRRIGATION_CONTROLLER, ProdS3Bucket.CONFIGURATION_IRRIGATION_CONTROLLER);
			prodS3bucketMap.put(FileType.ACTIVE_CODE, ProdS3Bucket.ACTIVE_CODE);

		} else if (ConfigUtil.getApplicationEnvironment().equals("DEV")) {
			devS3bucketMap.put(FileType.EVENT_ATTACHMENT, DevS3Bucket.EVENT_LOG_ATTACHMENT);
			devS3bucketMap.put(FileType.SG_CONFIGURATION, DevS3Bucket.SG_CONFIGURATION);
			devS3bucketMap.put(FileType.SG_FIRMWARE, DevS3Bucket.SG_FIRMWARE);
			devS3bucketMap.put(FileType.XG_FIRMWARE, DevS3Bucket.XG_FIRMWARE);
			devS3bucketMap.put(FileType.CONFIGURATION_IRRIGATION_CONTROLLER, DevS3Bucket.CONFIGURATION_IRRIGATION_CONTROLLER);
			devS3bucketMap.put(FileType.ACTIVE_CODE, DevS3Bucket.ACTIVE_CODE);
			devS3bucketMap.put(FileType.SG_PHOTO, DevS3Bucket.SG_PHOTO);

		} else{
			stageS3bucketMap.put(FileType.EVENT_ATTACHMENT, StageS3Bucket.EVENT_LOG_ATTACHMENT);
			stageS3bucketMap.put(FileType.SG_CONFIGURATION, StageS3Bucket.SG_CONFIGURATION);
			stageS3bucketMap.put(FileType.SG_FIRMWARE, StageS3Bucket.SG_FIRMWARE);
			stageS3bucketMap.put(FileType.XG_FIRMWARE, StageS3Bucket.XG_FIRMWARE);
			stageS3bucketMap.put(FileType.CONFIGURATION_IRRIGATION_CONTROLLER, StageS3Bucket.CONFIGURATION_IRRIGATION_CONTROLLER);
			stageS3bucketMap.put(FileType.ACTIVE_CODE, StageS3Bucket.ACTIVE_CODE);
			stageS3bucketMap.put(FileType.SG_PHOTO, StageS3Bucket.SG_PHOTO);

		}
	}

	public final static File getFile(FileEntity fe, FileType type) throws IOException{

		S3Object s3Obj = null;
		
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			S3Manager.getInstance(prodS3bucketMap.get(type)).getFile(fe.s3Path);
		}else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			S3Manager.getInstance(devS3bucketMap.get(type)).getFile(fe.s3Path);
		}else{
			S3Manager.getInstance(stageS3bucketMap.get(type)).getFile(fe.s3Path);
		}

		InputStream is = s3Obj.getObjectContent();
		File tmpFile = new File(genTmpFileForder() + fe.id.toString());

		OutputStream os = FileUtils.openOutputStream(tmpFile);
		int r = 0;
		while((r = is.read() ) != -1){
			os.write(r);
		}

		return tmpFile;
	}

	public FileEntityManager(String fileName, String content, FileType type) throws IOException	{
		this(fileName, content, type, null, null);
	}
	
	
	public FileEntityManager(String fileName, String content, FileType type, MimeType mimeType) throws IOException	{
		this(fileName, content, type, mimeType, null);
	}

	public FileEntityManager(String fileName, String content, FileType type, MimeType mimeType, String pathPrefix) throws IOException{

		File tmpFile = new File(genTmpFileForder() + fileName);

		FileUtils.write(tmpFile, content, "utf8");


		fileEntity = new FileEntity();

		fileEntity.fileName = FilenameUtils.getBaseName(fileName);
		fileEntity.fileExt = FilenameUtils.getExtension(fileName);
		fileEntity.fileSize = tmpFile.length();

		fileEntity.s3Path = StringUtils.isBlank(pathPrefix)? fileName : pathPrefix + fileName;

		fileEntity.contentType = mimeType;

		fileEntity.save();

		S3Manager s3Manager = null;
		
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			s3Manager = S3Manager.getInstance(prodS3bucketMap.get(type));
		}else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			s3Manager = S3Manager.getInstance(devS3bucketMap.get(type));
		}else {
            s3Manager = S3Manager.getInstance(stageS3bucketMap.get(type));
        }

		s3Manager.putFile(tmpFile, fileEntity.s3Path);
	}
	
	//Add by Jack /////////////////////////////////////////////////////////
	//For General API Configuration ///////////////////////////////////////
	public FileEntityManager(String fileName, String content, FileType type, String configurationType) throws IOException{
		this(fileName, content, type, null, null, configurationType);
	}
	
	public FileEntityManager(String fileName, String content, FileType type, MimeType mimeType, String pathPrefix, String configurationType) throws IOException{
		
		File tmpFile = new File(genTmpFileForderForConfiguration() + configurationType + "/" + fileName);

		FileUtils.write(tmpFile, content, "utf8");

		fileEntity = new FileEntity();

		fileEntity.fileName = FilenameUtils.getBaseName(fileName);
		fileEntity.fileExt = FilenameUtils.getExtension(fileName);
		fileEntity.fileSize = tmpFile.length();

		fileEntity.s3Path = StringUtils.isBlank(pathPrefix)? fileName : pathPrefix + fileName;

		fileEntity.contentType = mimeType;

		fileEntity.save();

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, "FileEntityManager tmpFile : " + tmpFile);

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, "FileEntityManager fileEntity.s3Path : " + fileEntity.s3Path);

		S3Manager s3Manager = null;
		
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			s3Manager = S3Manager.getInstance(prodS3bucketMap.get(type));
		} else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			s3Manager = S3Manager.getInstance(devS3bucketMap.get(type));
		} else{
            s3Manager = S3Manager.getInstance(stageS3bucketMap.get(type));
        }
        try{
            s3Manager.putFile(tmpFile, fileEntity.s3Path);
        }catch(Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "S3 put file error " + ExceptionUtils.getStackTrace(err));
        }

	}
	
	private static String genTmpFileForderForConfiguration(){
		/*
		String path = tmpDir;

		return path + ".";
		*/
		
		String path = tmpDir;
		if(!path.endsWith("/")){
			path += "/";
		}

		return path + System.currentTimeMillis() + "/";
	}
	////////////////////////////////////////////////////////////////////////////////////////////

	public FileEntityManager(File file, String fileName, FileType type){
		this(file, fileName, type, null, null);
	}

	public FileEntityManager(File file, String fileName, FileType type, MimeType mimeType){
		this(file, fileName, type, mimeType, null);
	}

	public FileEntityManager(File file, String fileName, FileType type, MimeType mimeType, String pathPrefix){

		fileEntity = new FileEntity();
		fileEntity.fileName = FilenameUtils.getBaseName(fileName);
		fileEntity.fileExt = FilenameUtils.getExtension(fileName);
		fileEntity.fileSize = file.length();
		fileEntity.s3Path = StringUtils.isBlank(pathPrefix)? fileName : pathPrefix + fileName;
		fileEntity.contentType = mimeType;
		try{
            fileEntity.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "file entity save error " + ExceptionUtils.getStackTrace(err));
        }

		S3Manager s3Manager;
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			s3Manager = S3Manager.getInstance(prodS3bucketMap.get(type));
		}else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			s3Manager = S3Manager.getInstance(devS3bucketMap.get(type));
		}else{
            s3Manager = S3Manager.getInstance(stageS3bucketMap.get(type));
        }
		s3Manager.putFile(file, fileEntity.s3Path);
	}

	private static String genTmpFileForder(){
		String path = tmpDir;
		if(!path.endsWith("/")){
			path += "/";
		}

		return path + System.currentTimeMillis() + "/";
	}


	public static void deleteS3file(String s3Path, FileType type){
        S3Manager s3Manager;
        if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
            s3Manager = S3Manager.getInstance(prodS3bucketMap.get(type));
        }else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
            s3Manager = S3Manager.getInstance(devS3bucketMap.get(type));
        }else{
            s3Manager = S3Manager.getInstance(stageS3bucketMap.get(type));
        }
        s3Manager.deleteFile(s3Path);

    }
	
	//by Jack Chuang
	//For get passive device configuration text file --> JSON String for return
	public final static String getStringFromTextFile(String fileId, FileType type, String configurationType) throws IOException{
		/*
		S3Object s3Obj = S3Manager.getInstance(s3bucketMap.get(type)).getFile(fileId);
		*/
		
		S3Object s3Obj = null;
		
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			s3Obj = S3Manager.getInstance(prodS3bucketMap.get(type)).getFile(fileId);
		}else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			s3Obj = S3Manager.getInstance(devS3bucketMap.get(type)).getFile(fileId);
		}else{
            s3Obj = S3Manager.getInstance(stageS3bucketMap.get(type)).getFile(fileId);
        }

		InputStream is = s3Obj.getObjectContent();
		//File tmpFile = new File(genTmpFileForder() + cfgId);
		File tmpFile = new File(genTmpFileForderForConfiguration() + fileId);

		OutputStream os = FileUtils.openOutputStream(tmpFile);
		int r = 0;
		while((r = is.read() ) != -1){
			os.write(r);
		}
		
		return FileUtils.readFileToString(tmpFile);
	}
	
	public final static String getDownloadUrl(String fileId, FileType type, String configurationType) throws IOException{
		
		String ret = ConfigUtil.getS3BucketsUrl();
		
		/*
		S3Object s3Obj = S3Manager.getInstance(s3bucketMap.get(type)).getFile(fileId);
		*/
		
		S3Object s3Obj = null;
		
		if(ConfigUtil.getApplicationEnvironment().equals("PROD")){
			s3Obj = S3Manager.getInstance(prodS3bucketMap.get(type)).getFile(fileId);
		}else if (ConfigUtil.getApplicationEnvironment().equals("DEV")){
			s3Obj = S3Manager.getInstance(devS3bucketMap.get(type)).getFile(fileId);
		}else{
            s3Obj = S3Manager.getInstance(stageS3bucketMap.get(type)).getFile(fileId);
        }
		
		ret += s3Obj.getBucketName() + "/" + fileId;
		
		return ret;
	}
}
