package utils;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.message.BasicNameValuePair;

import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import play.Configuration;


/**
 * validate phone number using APIs provided by https://www.phone-validator.net/
 */
public class PhoneNumberValidationUtil
{
    public static boolean validate(final String phoneNumber, final String countryCode)
    {
        final String API_URL = ConfigUtil.getEmail2textAPI();
        if (API_URL == null || API_URL.length() == 0)
        {
            throw new RuntimeException("Invalid phone number validator API url!");
        }

        final String API_KEY = ConfigUtil.getEmail2textAPIKey();
        if (API_KEY == null || API_KEY.length() == 0)
        {
            throw new RuntimeException("Invalid phone number validator API key!");
        }

        String _countryCode = countryCode;
        if (_countryCode == null || _countryCode.length() == 0)
        {
            _countryCode = "us";
        }
        final String locale = "en";

        HttpClient httpClient;
        try
        {
            httpClient = HttpClientBuilder.create().build();
            HttpPost request = new HttpPost(API_URL);
            List<NameValuePair> pairs = new ArrayList<>();
            pairs.add(new BasicNameValuePair("PhoneNumber", phoneNumber));
            pairs.add(new BasicNameValuePair("CountryCode", _countryCode));
            pairs.add(new BasicNameValuePair("Locale", locale));
            pairs.add(new BasicNameValuePair("APIKey", API_KEY));
            request.setEntity(new UrlEncodedFormEntity(pairs));

            HttpResponse response = httpClient.execute(request);
            HttpEntity entity = response.getEntity();
            String result = EntityUtils.toString(entity, "UTF-8");

            LoggingUtils.log(LoggingUtils.LogLevel.INFO, result);

            JSONObject jsonObject = new JSONObject(result);
            String status = jsonObject.getString("status");

            switch (status)
            {
                case "VALID_CONFIRMED":
                case "VALID_UNCONFIRMED":
                    return true;

                case "INVALID":
                    return false;

                default:
                    throw new RuntimeException("unknown status: "  + status);
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();

            throw new RuntimeException("Exception occurred: " + e.getMessage());
        }
    }
}
