/**
 * 
 */
package utils.notification;
/**
 * @author johnwu
 * @version 創建時間：2013/8/15 下午4:19:46
 */
public class SMSNotificationFactory extends NotificationFactory {
	public SMSNotificationFactory(){
		super.notification = new SMSNotification();
	}
}
