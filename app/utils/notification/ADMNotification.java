package utils.notification;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLEncoder;

import javax.net.ssl.HttpsURLConnection;

import org.json.JSONObject;

import utils.LoggingUtils;

public class ADMNotification {

	private String strDateTime;
	
	public ADMNotification(String clientId, String clientSecret, String registrationId, String title, String content){
		
		//Logger.info("Run ADMNotification.class ADMNotification()");
		
		int iStartDateTime = content.indexOf("(");
		int iEndDateTime = content.indexOf(")");
		strDateTime = content.substring(iStartDateTime + 1, iEndDateTime - 1);
		
		try {
			
			String accessToken = getAuthToken(clientId, clientSecret);
			
			sendMessageToDevice(registrationId, accessToken, title, content);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Run ADMNotification.class ADMNotification() err : " + e.toString());
		}
	}
	
	/**
	  * To obtain an access token, make an HTTPS request to Amazon
	  * and include your client_id and client_secret values.
	  */
	public String getAuthToken(String clientId, String clientSecret) throws Exception
	{   
		//Logger.info("Run ADMNotification.class getAuthToken()");
		
	     // Encode the body of your request, including your clientID and clientSecret values.
	     String body = "grant_type="    + URLEncoder.encode("client_credentials", "UTF-8") + "&" +
	                   "scope="         + URLEncoder.encode("messaging:push", "UTF-8")     + "&" +
	                   "client_id="     + URLEncoder.encode(clientId, "UTF-8")             + "&" +
	                   "client_secret=" + URLEncoder.encode(clientSecret, "UTF-8");
	        
	     // Create a new URL object with the base URL for the access token request.
	     URL authUrl = new URL("https://api.amazon.com/auth/O2/token");
	       
	     // Generate the HTTPS connection. You cannot make a connection over HTTP.
	     HttpsURLConnection con = (HttpsURLConnection) authUrl.openConnection();
	     con.setDoOutput( true );
	     con.setRequestMethod( "POST" );
	       
	     // Set the Content-Type header.
	     con.setRequestProperty( "Content-Type" , "application/x-www-form-urlencoded" );
	     con.setRequestProperty( "Charset" , "UTF-8" );
	     // Send the encoded parameters on the connection.
	     OutputStream os = con.getOutputStream();
	     os.write(body.getBytes( "UTF-8" ));
	     os.flush();
	     con.connect();
	      
	     // Convert the response into a String object.
	     String responseContent = parseResponse(con.getInputStream());
	       
	     // Create a new JSONObject to hold the access token and extract
	     // the token from the response.
	     org.json. JSONObject parsedObject = new org.json.JSONObject(responseContent);
	     String accessToken = parsedObject.getString("access_token");
	     
	     //Logger.info("Run ADMNotification.class getAuthToken() accessToken : " + accessToken);
	     
	     return accessToken;
	}
	 
	
	
	/**
	 * Request that ADM deliver your message to a specific instance of your app.
	 */
	public void sendMessageToDevice(String registrationId, String accessToken, String title, String content) throws Exception
	{      
		//Logger.info("Run ADMNotification.class sendMessageToDevice()");
		
		//For debug
		//registrationId = "amzn1.adm-registration.v3.Y29tLmFtYXpvbi5EZXZpY2VNZXNzYWdpbmcuUmVnaXN0cmF0aW9uSWRFbmNyeXB0aW9uS2V5ITEhaWN5cng5ZytaNno1Rm5FSzlXOWNtSnppb2hDRHlqejVHYVNqR1NIK1ZEKzFzTGFpRjBCcFZ5MG9Ic2xUay9wMi9oUjFsZFQycm8xWU9RQ2UzUUhWUGlmSGtJMmYwa3B5aWRmc3lxR2tscnA1eWN0bFpNN1FoQWRzd0k5V2pUWmJqUkVXU2lCeHpEYlFya2xBWEhDZ01aZGh5U2dCN1prblRRN2ttaGR3VEdKMzdtcFFKdTNxcGxIMGt0ejVvTnNKNk8xRVI0dEgzcStuOVJYT21mYlFYb3pHaUxLK0hGdDlNTzZPWENxckUxbFBIYXAzMmhycFZBVytPVWxqeXpDTkZhUUEzaXBrVHdxa1NHVzBWb0g5S0FvWndzaCttN0JOWWVLNjBTREZMOWQxY1VGWHlRRHBVeDNFcThuSHlzRGxrc20yc3BKRHVHTFdMLzRMZTc3dHJzUG51Nm5lZzlMc00vVVQ0LytiRDhRPSFkN3J3YjJrRElrUmxYOVBKMkVZR0ZBPT0";
		
		//Logger.info("Run ADMNotification.class sendMessageToDevice() registrationId : " + registrationId);
		
	    // JSON payload representation of the message.
	    JSONObject payload = new JSONObject();
	     
	    // Define the key/value pairs for your message content and add them to the
	    // message payload.
	    
	    JSONObject data = new JSONObject();
	    
	    data.put("from", title);
	    data.put("message", content);
	    data.put("time", strDateTime);
	    
	    payload.put("data", data);  
	     
	    // Add a consolidation key. If multiple messages are pending delivery for a particular
	    // app instance with the same consolidation key, ADM will attempt to delivery the most
	    // recently added item.
	    payload.put("consolidationKey", "ADM_Enqueue_Sample");
	    
	    // Add an expires-after value to the message of 1 day. If the targeted app instance does not
	    // come online within the expires-after window, the message will not be delivered.
	    payload.put("expiresAfter", 86400);    
	    
	    //Logger.info("payload : " + payload);
	      
	    // Convert the message from a JSON object to a string.
	    String payloadString = payload.toString();
	     
	    // Establish the base URL, including the section to be replaced by the registration
	    // ID for the desired app instance. Because we are using String.format to create
	    // the URL, the %1$s characters specify the section to be replaced.
	    String admUrlTemplate = "https://api.amazon.com/messaging/registrations/%1$s/messages";
	     
	    URL admUrl = new URL(String.format(admUrlTemplate, registrationId));
	      
	    // Generate the HTTPS connection for the POST request. You cannot make a connection
	    // over HTTP.
	    HttpsURLConnection conn = (HttpsURLConnection) admUrl.openConnection();
	    conn.setRequestMethod("POST");
	    conn.setDoOutput(true);
	     
	    // Set the content type and accept headers.
	    conn.setRequestProperty("content-type", "application/json");
	    conn.setRequestProperty("accept", "application/json");
	    conn.setRequestProperty("X-Amzn-Type-Version ", "com.amazon.device.messaging.ADMMessage@1.0");
	    conn.setRequestProperty("X-Amzn-Accept-Type", "com.amazon.device.messaging.ADMSendResult@1.0");
	     
	    // Add the authorization token as a header.
	    conn.setRequestProperty("Authorization", "Bearer " + accessToken);
	                 
	    // Obtain the output stream for the connection and write the message payload to it.
	    OutputStream os = conn.getOutputStream();
	    os.write(payloadString.getBytes(), 0, payloadString.getBytes().length);
	    os.flush();
	    conn.connect();
	    
	    // Obtain the response code from the connection.
	    int responseCode = conn.getResponseCode();
	 
	    // Check if we received a failure response, and if so, get the reason for the failure.
	    if( responseCode != 200)
	    {  
	        if( responseCode == 401 )
	        {
	            // If a 401 response code was received, the access token has expired. The token should be refreshed
	            // and this request may be retried.
	        }
	        
	        String errorContent = parseResponse(conn.getErrorStream());             
	        throw new RuntimeException(String.format("ERROR: The enqueue request failed with a " +
	                                         "%d response code, with the following message: %s",
	                                         responseCode, errorContent));
	    }
	    else
	    {
	        // The request was successful. The response contains the canonical Registration ID for the specific instance of your
	        // app, which may be different that the one used for the request.
	         
	        String responseContent = parseResponse(conn.getInputStream());          
	        JSONObject parsedObject = new JSONObject(responseContent);
	         
	        String canonicalRegistrationId = parsedObject.getString("registrationID");
	        
	        // Check if the two Registration IDs are different.
	        if(!canonicalRegistrationId.equals(registrationId))
	        {              
	            // At this point the data structure that stores the Registration ID values should be updated
	            // with the correct Registration ID for this particular app instance.
	        }
	    }
	     
	}
	 
	private String parseResponse(InputStream in) throws Exception
	{
	    // Read from the input stream and convert into a String.
	    InputStreamReader inputStream = new InputStreamReader(in);
	    BufferedReader buff = new BufferedReader(inputStream);
	     
	    StringBuilder sb = new StringBuilder();
	    String line = buff.readLine();
	    while(line != null)
	   {         
	      sb.append(line);
	      line = buff.readLine();
	    }
	     
	    return sb.toString();
	}
}
