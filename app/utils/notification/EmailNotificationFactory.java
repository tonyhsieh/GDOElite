package utils.notification;

/**
 * 
 * @author johnwu
 *
 */
public class EmailNotificationFactory extends NotificationFactory{

	public EmailNotificationFactory(){
		super.notification = new EmailNotification();
	}
}
