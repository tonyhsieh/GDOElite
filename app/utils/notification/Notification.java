package utils.notification;

import java.io.IOException;

/**
 * 
 * @author johnwu
 *
 */
interface Notification {

	public void pushNotification(String to, String title, String content) throws IOException;
	
	public void getPushNotification();
}
