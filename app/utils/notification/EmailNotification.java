package utils.notification;

import java.io.IOException;

import models.EmailBlacklist;
import utils.LoggingUtils;
import utils.MailUtil;

/**
 * 
 * @author johnwu
 *
 */
public class EmailNotification implements Notification {

	private MailUtil post = MailUtil.instance();
	
	@Override
	public void pushNotification(String email, String title, String content) throws IOException{
		// TODO Auto-generated method stub
		if(EmailBlacklist.emailIsValid(email)){
			post.sendMail(email, title, content);
		}else{
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Email is not valid :" + email);
        }
	}

	@Override
	public void getPushNotification() {
		// TODO Auto-generated method stub

	}

}
