package utils.notification;

/**
 * 
 * @author johnwu
 *
 */
public class APNotificationFactory extends NotificationFactory {
	public APNotificationFactory(){
		super.notification = new APNotification();
	}
}
