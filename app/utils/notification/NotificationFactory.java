package utils.notification;

import java.io.IOException;

/**
 * 
 * @author johnwu
 *
 */
public class NotificationFactory {
	protected Notification notification;
	
	public void pushNotification(String to, String title, String content) throws IOException{
		notification.pushNotification(to, title, content);
	}
	
	public void getPushNotification(){
		notification.getPushNotification();
	}
}
