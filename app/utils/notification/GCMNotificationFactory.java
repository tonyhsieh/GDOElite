package utils.notification;

/**
 * 
 * @author johnwu
 *
 */
public class GCMNotificationFactory extends NotificationFactory {
	public GCMNotificationFactory(){
		super.notification = new GCMNotification();
	}
}
