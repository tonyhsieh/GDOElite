package utils.notification;

import java.util.Calendar;
import java.util.List;

public class DeviceEventQueueMsg{
	
	public String id;
	
	public int eventType;
	
	public String xGenieMac;
	
	public String sgId;
	
	public Calendar messageTimeStamp;
	
	///////////////////////////////////////////
	
	public List<String> listOwnerEmail;
	
	public String title;
	
	public String content;
	
	public Boolean isEmail;
	
	public Boolean isPushNotification;
}