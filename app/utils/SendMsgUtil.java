package utils;

import com.avaje.ebean.Ebean;

import models.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.TimeZone;

import controllers.api.v1.app.Email2TextController;
import utils.generator.CredentialGenerator;
import utils.sendhub.SendHubUtils;

public class SendMsgUtil
{
    // messages triggered by AG
    public static void sendMsg(final String xGenieMac, final String sgId,
                               final int eventType, final String strTimeStamp)
    {
        new Thread(() ->
        {
            try
            {
                String strMsgId = getMsgId(strTimeStamp, sgId, xGenieMac, eventType);

                Event event = Event.findByType(eventType);
                if (event == null)
                {
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Failed to find Event with eventType: " + event);
                    return;
                }

                if (StringUtils.isNotBlank(xGenieMac))
                {
                    XGenie xGenie = XGenie.findByXgMac(xGenieMac);

                    if (xGenie != null && xGenie.smartGenie != null)
                    {
                        SmartGenie smartGenie = Ebean.find(SmartGenie.class, xGenie.smartGenie.id);

                        EventContact ec = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(xGenie.macAddr, eventType));

                        if (ec != null)
                        {
                            List<Contact> contactList = Contact.listByEventContact(ec.id);

                            for (Contact contact : contactList)
                            {
                                UserRelationship ur = UserRelationship.findByUserNSmartGenie(smartGenie.owner.id.toString(), contact.userId, smartGenie.id);

                                if (contact.notifyEmail)
                                {
                                    String email = ((ur != null) ? ur.email : smartGenie.owner.email);
                                    email(email, "Passive Device Notification", xGenie.name + " " + event.eventMsg);
                                }

                                if (contact.notifyPushNotification)
                                {
                                    String strDate = strMsgId.substring(6, 10) + "/" +
                                            strMsgId.substring(10, 12) + "/" +
                                            strMsgId.substring(12, 14) + "T" +
                                            strMsgId.substring(14, 16) + ":" +
                                            strMsgId.substring(16, 18) + ":" +
                                            strMsgId.substring(18, 20);

                                    SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);

                                    SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");

                                    df.setTimeZone(TimeZone.getTimeZone("GMT+0"));

                                    Date date = df.parse(strDate);
                                    Calendar cal = Calendar.getInstance();
                                    cal.setTime(date);

                                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                                    if (sgLog.timezone > 0)
                                    {
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + String.valueOf(sgLog.timezone)));
                                    }
                                    else if (sgLog.timezone < 0)
                                    {
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT" + String.valueOf(sgLog.timezone)));
                                    }
                                    else
                                    {
                                        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                    }

                                    User user = Ebean.find(User.class, contact.userId);
                                    push(user, "Passive Device Notification", xGenie.name + " " + event.eventMsg + "(" + sdf.format(cal.getTime()) + ")");
                                }

                                if (contact.notifySMS)
                                {
                                    if (event.eventType == 10002 || event.eventType == 10003 ||
                                            event.eventType == 10100 || event.eventType == 10101 ||
                                            event.eventType == 10102 || event.eventType == 10103 ||
                                            event.eventType == -10150 || event.eventType == -10151)
                                    {
                                        sendEmail2Text(contact.userId,
                                                "Passive Device Notification",
                                                xGenie.name + " "
                                                        + event.eventMsg + " "
                                                        + ",Message ID:" + strMsgId);
                                        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,
                                                " AG send "
                                                        + " Email2Text to " + (ur != null ? ur.email : "(no UR)")
                                                        + " title: Passive Device Notification "
                                                        + " content: "
                                                        + (xGenie != null ? xGenie.name : "(no XGenie)") + " "
                                                        + (event != null ? event.eventMsg : "(no event)")
                                                        + " " + ",Message ID:" + strMsgId);
                                    }
                                }
                            }
                        }
                        else
                        {
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Failed to find Event Contact with: " + xGenie.macAddr + ", " + eventType);
                        }
                    }
                    else
                    {
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Failed to find xGenie with MAC: " + xGenieMac);
                    }
                }
                else  // Asante Genie Event (without passive device mac address)
                {
                    SmartGenie sg = Ebean.find(SmartGenie.class, sgId);

                    if (sg != null && sg.owner != null)
                    {
                        User owner = Ebean.find(User.class, sg.owner.id.toString());

                        // Check Contact -> mac address, event type, and owner
                        // that notification setting.
                        EventContact ec = EventContact.findByMacAndEventType(sg.macAddress, event.eventType);


                        // test event triggered by mobile devices
                        if (event.eventType == 100)
                        {
                            String strDate = strMsgId.substring(6, 10) + "/" +
                                    strMsgId.substring(10, 12) + "/" +
                                    strMsgId.substring(12, 14) + "T" +
                                    strMsgId.substring(14, 16) + ":" +
                                    strMsgId.substring(16, 18) + ":" +
                                    strMsgId.substring(18, 20);

                            SmartGenieLog sgLog = SmartGenieLog.find(sg.id);

                            SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");

                            df.setTimeZone(TimeZone.getTimeZone("GMT+0"));

                            Date date = df.parse(strDate);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(date);

                            SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                            if (sgLog.timezone > 0)
                            {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + String.valueOf(sgLog.timezone)));
                            }
                            else if (sgLog.timezone < 0)
                            {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT" + String.valueOf(sgLog.timezone)));
                            }
                            else
                            {
                                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                            }

                            push(owner, "Asante Notification", sg.name + " " + event.eventMsg + "(" + sdf.format(cal.getTime()) + ")");
                        }
                        else
                        {
                            if (ec == null)
                            {
                                LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                        "Failed to find Event Contact with: " + sg.macAddress + ", " + event.eventType);
                                return;
                            }


                            // Event Type != 100
                            Contact contact = Contact.findByEventContactNUserId(ec.id, owner.id.toString());
                            if (contact == null)
                            {
                                LoggingUtils.log(LoggingUtils.LogLevel.ERROR,
                                        "Failed to find Contact with: " + ec.id + ", " + owner.id.toString());
                                return;
                            }

                            if (contact.notifySMS)
                            {
                                String content;
                                if (!"".equals(strMsgId))
                                {
                                    content = sg.name + " " + event.eventMsg + " "
                                            + ",Message ID:" + strMsgId;
                                }
                                else
                                {
                                    content = sg.name + " " + event.eventMsg;
                                }

                                sendEmail2Text(contact.userId, "Asante Notification", content);
                                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, " AG sends "
                                        + " Email2Text to " + sg.owner.email + " title: Asante Notification "
                                        + " content: " + content);
                            }

                            if (contact.notifyEmail)
                            {
                                email(owner.email, "Asante Notification", sg.name + " " + event.eventMsg);
                            }

                            if (contact.notifyPushNotification)
                            {
                                String strDate = strMsgId.substring(6, 10) + "/" +
                                        strMsgId.substring(10, 12) + "/" +
                                        strMsgId.substring(12, 14) + "T" +
                                        strMsgId.substring(14, 16) + ":" +
                                        strMsgId.substring(16, 18) + ":" +
                                        strMsgId.substring(18, 20);

                                SmartGenieLog sgLog = SmartGenieLog.find(sg.id);

                                SimpleDateFormat df = new SimpleDateFormat("yyyy/MM/dd'T'HH:mm:ss");

                                df.setTimeZone(TimeZone.getTimeZone("GMT+0"));

                                Date date = df.parse(strDate);
                                Calendar cal = Calendar.getInstance();
                                cal.setTime(date);

                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

                                if (sgLog.timezone > 0)
                                {
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT+" + String.valueOf(sgLog.timezone)));
                                }
                                else if (sgLog.timezone < 0)
                                {
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT" + String.valueOf(sgLog.timezone)));
                                }
                                else
                                {
                                    sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
                                }

                                if (contact.notifyEmail)
                                {
                                    push(owner, "Asante Notification", sg.name + " " + event.eventMsg + "(" + sdf.format(cal.getTime()) + ")");
                                }
                            }
                        }
                    }
                    else
                    {
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "SG is null or its owner is null for: " + sgId);
                    }
                }

            }
            catch (Exception e)
            {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtils.getStackTrace(e));
            }

        }).start();
    }

    private static String getMsgId(String strTimeStamp, String sgId, String xGenieMac, int eventType)
    {
        String strMsgTmp = strTimeStamp.substring(0, 19);
        char[] chMsgTmp = strMsgTmp.toCharArray();

        StringBuilder sb = new StringBuilder();
        for (char c : chMsgTmp)
        {
            if (c != '-' && c != ':' && c != ' ' && c != '/')
            {
                sb.append(c);
            }
        }

        SmartGenie sg = SmartGenie.find(sgId);

        String strMsgId;
        if (xGenieMac == null || xGenieMac.length() == 0)
        {
            strMsgId = sg.macAddress.substring(6);
        }
        else
        {
            strMsgId = xGenieMac.substring(6);
        }
        strMsgId += sb.toString();
        strMsgId += String.valueOf(eventType);
        return strMsgId;
    }

    public static void sendMsgByAdminConsole(final List<String> listOwnerEmail,
                                             final String title, final String content,
                                             final boolean isEmail, final boolean isPushNotification)
    {
        new Thread(() ->
        {
            for (String s : listOwnerEmail)
            {
                if (isEmail)
                {
                    email(s, title, content);
                }

                if (isPushNotification)
                {
                    User user = User.findByEmail(s);
                    push(user, title, content);
                }
            }
        }).start();
    }

    private static void sms(String to, String content) throws IOException
    {
        List<Long> contacts = new ArrayList<>(1);
        contacts.add(Long.parseLong(to));

        if (!SendHubUtils.sendMessage(contacts, content))
        {
            throw new RuntimeException("Failed to call SendHub " + to + " -> " + content);
        }
    }

    private static void sendEmail2Text(final String userId,
                                       final String title, final String content)
    {
        final String TAG = "sendEmail2Text: ";

        if (title == null || content == null)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "title or content is null");
            return;
        }

        User user = User.find(userId);
        if (user == null)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "no such user: " + userId);
            return;
        }

        if (user.gateway == null || user.cellPhone == null)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "no setup: " + userId);
            return;
        }

        String emailAddress = Email2TextController.getEmail2Text(user.cellPhone, user.gateway);

        if (StringUtils.isBlank(emailAddress))
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, TAG + "invalid email address");
            return;
        }

        email(emailAddress, title, content);
    }

    private static void email(String receiver, String title, String message) {
        if(EmailBlacklist.emailIsValid(receiver)){
            try {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("receiver", receiver);
                jsonObject.put("type", "email");
                jsonObject.put("title", title);
                jsonObject.put("message", message);
                send2transporter(jsonObject);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }else{
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "email is invalid: " + receiver);
        }
    }

    private static void push(User user, String title, String content)
    {
        gcm(user, title, content);
        apns(user, content);
    }

    private static void gcm(User user, String title, String content)
    {
        List<PnRelationship> pnList = PnRelationship.listByUserAndOs(user, "android");
        for (PnRelationship pnR : pnList)
        {
            try
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("receiver", pnR.pnId);
                jsonObject.put("type", "push-android");

                JSONObject message = new JSONObject();
                message.put("title", title);
                message.put("content", content);

                jsonObject.put("message", message);
                send2transporter(jsonObject);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private static void apns(User user, String content)
    {
        final String type = (ConfigUtil.isAPNSProduction() ? "push-ios-prod" : "push-ios-sandbox");
        List<PnRelationship> pnList = PnRelationship.listByUserAndOs(user, "ios");
        for (PnRelationship pnR : pnList)
        {
            try
            {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("receiver", pnR.pnId);
                jsonObject.put("type", type);
                jsonObject.put("message", content);
                send2transporter(jsonObject);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
                throw new RuntimeException(e.getMessage());
            }
        }
    }

    private static void send2transporter(final JSONObject jsonObject)
    {
        try
        {
            HttpClient httpClient = HttpClientBuilder.create().build();

            JSONObject jsonObject1 = new JSONObject(jsonObject);

            Iterator iterator = jsonObject.keys();
            while (iterator.hasNext())
            {
                String key = (String) iterator.next();

                if (key.equals("message"))
                {
                    if(jsonObject.get(key).getClass().equals(String.class)){
                        jsonObject1.put(key, "[" + getEnvStr() + "]" + jsonObject.get(key));
                    }else{
                        JSONObject json_message = jsonObject.getJSONObject(key);
                        json_message.put("content",  "[" + getEnvStr() + "]"+ json_message.get("content")) ;
                        jsonObject1.put(key, json_message.toString());
                    }
                }
                else
                {
                    jsonObject1.put(key, jsonObject.get(key));
                }
            }

            HttpPost request = new HttpPost(ConfigUtil.getTransporterUrl());
            StringEntity params = new StringEntity(jsonObject1.toString());
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            HttpResponse httpResponse = httpClient.execute(request);
            String result = EntityUtils.toString(httpResponse.getEntity(), "UTF8");

            LoggingUtils.log(LoggingUtils.LogLevel.INFO,  "  jsonObject1 "+ jsonObject1.toString());
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "result " + result);
        }
        catch (JSONException | IOException e)
        {
            e.printStackTrace();
            throw new RuntimeException(e.getMessage());
        }
    }

    private static String getEnvStr()
    {
        String environment = ConfigUtil.getApplicationEnvironment();
        if (environment.equals("PROD"))
        {
            return "";
        }
        return environment;
    }
}
