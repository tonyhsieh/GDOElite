/**
 *
 */
package utils.generator;

import java.security.MessageDigest;
import java.util.UUID;

/**
 * @author carloxwang
 *
 */
public abstract class CredentialGenerator {

	public final static String genAuthCode(){
		String uuid = UUID.randomUUID().toString();
		return uuid;

	}

	public final static String genSmartGenieId(String info) {
		return CredentialGenerator.genSHA1Key(info);
	}

	public final static String genXGenieId(String info) {
		return CredentialGenerator.genSHA1Key(info);
	}
	
	public final static String genEventContactId(String macAddr, int eventType){
		return CredentialGenerator.genSHA1Key(macAddr + eventType);
	}

	public final static String genContactId(String eventContactId, String userId){
		return CredentialGenerator.genSHA1Key(eventContactId + userId);
	}
	
	public final static String genPushNotificationId(String contactId, String sessionId){
		return CredentialGenerator.genSHA1Key(contactId + sessionId);
	}
	
	/**hash code*/
	public final static String genSHA1Key(String info) {
		String re = null;
		try {
			String myinfo=info;

			MessageDigest alga=MessageDigest.getInstance("SHA-1");
			alga.update(myinfo.getBytes());
			byte[] digesta=alga.digest();
			re = CredentialGenerator.byte2hex(digesta);
		} catch(Exception e) {
			e.printStackTrace();
		}
		return re;
	}

	public final static String byte2hex(byte[] b) {
		String hs="";
		String stmp="";
		for (int n=0;n < b.length;n++)
		{
			stmp=(java.lang.Integer.toHexString(b[n] & 0XFF));
			if (stmp.length()==1)
				hs=hs+"0"+stmp;
			else
				hs=hs+stmp;
		}
		return hs.toUpperCase();
	}
}
