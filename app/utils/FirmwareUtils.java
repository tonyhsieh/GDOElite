package utils;

import org.apache.commons.lang3.StringUtils;



public class FirmwareUtils {
	
	public static boolean checkVer(String verNow, String verDev){
		
		boolean isNeedDownloadNewFw = false;
		boolean isCountine = true;
		
		if(verNow.equals(verDev))
			return true;
		
		String [] verNowAry = StringUtils.split(verNow, ".");
		String [] verDevAry = StringUtils.split(verDev, ".");
		
		/*
		for(int i=0; i<verDevAry.length; i++){
			
			Logger.info(Integer.valueOf(verDevAry[i]) + " | " + Integer.valueOf(verNowAry[i]));
			
			if(Integer.valueOf(verDevAry[i]) < Integer.valueOf(verNowAry[i])){
				Logger.info("false");
				return false;
			}else{
				Logger.info("true");
			}
		}
		*/
		
		if(Integer.valueOf(verDevAry[0]) < Integer.valueOf(verNowAry[0])){
			isCountine = false;
			isNeedDownloadNewFw = true;
		}else if(Integer.valueOf(verDevAry[0]) == Integer.valueOf(verNowAry[0])){
			isCountine = true;
			isNeedDownloadNewFw = false;
		}else if(Integer.valueOf(verDevAry[0]) > Integer.valueOf(verNowAry[0])){
			isCountine = false;
			isNeedDownloadNewFw = false;
		}
		
		if(Integer.valueOf(verDevAry[1]) < Integer.valueOf(verNowAry[1]) && isCountine){
			isCountine = false;
			isNeedDownloadNewFw = true;
		}else if(Integer.valueOf(verDevAry[1]) == Integer.valueOf(verNowAry[1]) && isCountine){
			isCountine = true;
			isNeedDownloadNewFw = false;
		}else if(Integer.valueOf(verDevAry[1]) > Integer.valueOf(verNowAry[1]) && isCountine){
			isCountine = false;
			isNeedDownloadNewFw = false;
		}
		
		if(Integer.valueOf(verDevAry[2]) < Integer.valueOf(verNowAry[2]) && isCountine){
			isNeedDownloadNewFw = true;
		}else if(Integer.valueOf(verDevAry[2]) == Integer.valueOf(verNowAry[2]) && isCountine){
			isCountine = true;
			isNeedDownloadNewFw = false;
		}else if(Integer.valueOf(verDevAry[2]) > Integer.valueOf(verNowAry[2]) && isCountine){
			isCountine = false;
			isNeedDownloadNewFw = false;
		}

		if(isNeedDownloadNewFw){
			return false;
		}else{
			return true;
		}

		//return true;
	}
	
	public static boolean compareVer(String scrVer, String desVer, int condition){

		if(desVer==null){
			return false;
		}
		
		String [] verSrc = StringUtils.split(scrVer, ".");
		String [] verDes = StringUtils.split(desVer, ".");
		
		for(int i=0; i<verSrc.length; i++){
			try{
				int idbg = Integer.valueOf(verSrc[i]);
			}catch(Exception err){
				return false;
			}
		}
		
		if(condition == 0){	//<
    		
			boolean isCountine = true;
			
			if(Integer.valueOf(verSrc[0]) > Integer.valueOf(verDes[0])){
				isCountine = false;
				return true;
			}else if(Integer.valueOf(verSrc[0]) < Integer.valueOf(verDes[0])){
				isCountine = false;
				return false;
			}

			if(Integer.valueOf(verSrc[1]) > Integer.valueOf(verDes[1]) && isCountine){
				isCountine = false;
				return true;
			}else if(Integer.valueOf(verSrc[1]) < Integer.valueOf(verDes[1]) && isCountine){
				isCountine = false;
				return false;
			}

			if(Integer.valueOf(verSrc[2]) > Integer.valueOf(verDes[2]) && isCountine){
				return true;
			}
			
    	}else if(condition == 1){	//=
    		if(scrVer.equals(desVer))
    			return true;
    	}else if(condition == 2){	//<=
    		boolean isCountine = true;
			
			if(Integer.valueOf(verSrc[0]) >= Integer.valueOf(verDes[0])){
				isCountine = false;
				return true;
			}else if(Integer.valueOf(verSrc[0]) < Integer.valueOf(verDes[0])){
				isCountine = false;
				return false;
			}
			
			if(Integer.valueOf(verSrc[1]) >= Integer.valueOf(verDes[1]) && isCountine){
				isCountine = false;
				return true;
			}else if(Integer.valueOf(verSrc[1]) < Integer.valueOf(verDes[1]) && isCountine){
				isCountine = false;
				return false;
			}
			
			if(Integer.valueOf(verSrc[2]) >= Integer.valueOf(verDes[2]) && isCountine){
				return true;
			}
    	}
		
		return false;
	}
}