/**
 * 
 */
package utils.json.vo;

import java.io.Serializable;
import java.util.Calendar;

import json.models.api.v1.app.CalendarSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author johnwu
 * @version 創建時間：2013/8/28 下午1:16:15
 */

public class KeepAliveQueueMsg implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8278353044787304247L;

	public String sgId;
	
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar crtTime;
}
