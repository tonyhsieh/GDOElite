package utils.json.vo;

import java.util.List;

public class GCMNode {
	public List<String> registration_ids;
	public GCMDataVO data;
	public boolean delay_while_idle = true;
}
