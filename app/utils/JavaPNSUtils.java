/**
 * 
 */
package utils;

//import org.hibernate.validator.internal.util.logging.Log_.logger;

import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;


import javapns.Push;
import javapns.communication.exceptions.CommunicationException;
import javapns.communication.exceptions.KeystoreException;
import javapns.notification.PushNotificationPayload;
import javapns.notification.PushedNotification;

import javapns.notification.ResponsePacket;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.log4j.*;
import org.apache.log4j.BasicConfigurator;
//import org.apache.log4j.BasicConfigurator;
import org.json.JSONException;


import play.Configuration;


/**
 * @author johnwu
 * @version 創建時間：2013/9/11 下午1:38:00
 */
public class JavaPNSUtils {

    @Inject
    Configuration configuration;
	
	public JavaPNSUtils(){
		
	}
	
	//public void push(){
	public ResponsePacket push(String alert, int badge, String sound, String pnId){

		ResponsePacket response = null;
		
		//Logger.info("**************************************************************");
		
		//Logger.info("JavaPNSUtils->push() apns start");
		
	    try {
	    	//Logger.info("ANPS alert : " + alert);
	    	
	    	//Logger.info("ANPS pnId : " + pnId);
	    	
	    	BasicConfigurator.configure();
	    	
	    //Logger.info("JavaPNSUtils->push() apns BasicConfigurator.configure()");
	    	
	    	PushNotificationPayload payload = null;
	    	
	    	try{
	    		//payload = new PushNotificationPayload();
	    		
	    		payload = PushNotificationPayload.complex();
	    		
	    	//Logger.info("JavaPNSUtils->push() apns after payload = PushNotificationPayload.complex()");
	    		
	    		
	    	}catch(Exception err){
                err.printStackTrace();
				LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "init payload err : " + ExceptionUtils.getStackTrace(err));
	    	}
	    	
	    	/*
	    	payload.setExpiry(1);
	    	payload.addCustomAlertActionLocKey("launch apns");
	    	payload.addCustomAlertLocKey("locKey");
	    	payload.addCustomDictionary("token", pnId);
	    	*/
	    	
	    	//Logger.info("JavaPNSUtils->push() apns content : " + alert);
	    	
	    	//payload.addCustomAlertBody(alert);
	    	payload.addAlert(alert);

	    	payload.addBadge(1);

	    	payload.addSound(sound);

	    	//Logger.info("payload format : " + payload.getPayload());
	    	
	    	//List<PushedNotification> listNotification = Push.payload(payload, "./home/ec2-user/apns_key/APNsKey.p12", "asantegenie2013", true, pnId);
	    	//List<PushedNotification> notification = Push.payload(payload, "D:\\APNsKey.p12", "asantegenie2013", false, pnId);
	    	
	    	List<PushedNotification> listNotification = null;
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "payload pnid " + pnId);
	    	try{
                javapns.notification.PushNotificationManager.setEnhancedNotificationFormatEnabled(false);
	    		listNotification = Push.payload(payload, 
	    				configuration.getString("apns.filepath"),
	    				configuration.getString("apns.passwd"),
	    				configuration.getBoolean("apns.environment"),
	    				pnId
	    				);

	    		//listNotification = Push.payload(payload, "D:\\APNsKey.p12", "asantegenie2013", false, pnId);
	    		//listNotification = Push.payload(payload, "/home/ec2-user/apns_key/APNsKey.p12", "asantegenie2013", false, pnId);
	    		
	    	//Logger.info("JavaPNSUtils->push() apns listNotification size : " + listNotification.size());
	    	//Logger.info("JavaPNSUtils->push() apns getIdentifier : " + listNotification.get(0).getIdentifier());
	    		
	    	}catch(CommunicationException err){
                err.printStackTrace();
				LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Push.test() CommunicationException : " + ExceptionUtils.getStackTrace(err));
	    	}catch(KeystoreException err){
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Push.test() KeystoreException : " + ExceptionUtils.getStackTrace(err));
	    	}catch (Exception err){
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Push payload Exception : " + ExceptionUtils.getStackTrace(err));
			}

	    	for(PushedNotification notification : listNotification){
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "JavaPNSUtils->push() apns listNotification index : " + listNotification.indexOf(notification) + "notification" + notification );
	    		//Logger.info("JavaPNSUtils->push() apns listNotification index : " + listNotification.indexOf(notification));
	    		
	    		if(notification.getResponse()!=null){
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ANPS Response notification.getResponse()!=null");
	    			//Logger.info("ANPS Response notification.getResponse()!=null");
	    			
	    			response = notification.getResponse();
	    			
	    			//Logger.info("after response = notification.getResponse()");
	    			
	    			//Logger.info("ANPS Response Message : " + notification.getResponse().getMessage());
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ANPS Response Message : " + notification.getResponse().getMessage());
	    			if(notification.isSuccessful()){
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ANPS Response isSuccessful");
	    				//Logger.info("ANPS Response isSuccessful");
	    			}else{
	    				
	    				String invalidToken = notification.getDevice().getToken();
						LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ANPS Response fail token : " + invalidToken);
	    			}
	    		}else{
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "ANPS Response notification.getResponse() == null");
	    			//Logger.info("ANPS Response notification.getResponse() == null");
	    		}
	    		
	    		//Logger.info("JavaPNSUtils->push() apns end");
	    		
	    		//Logger.info("**************************************************************");
	    	}
	    	
	    	return response;
	    	
	    	//////////////////////////////////////////////////////////////////////////
	    	/*
	        PushNotificationPayload payload = PushNotificationPayload.complex();

	        payload.addAlert("Hello World");
	        payload.addBadge(1);
	        payload.addSound("default");
	        payload.addCustomDictionary("id", "1");

	        System.out.println(payload.toString());
	        List<PushedNotification> NOTIFICATIONS = Push.payload(payload, "D:\\keystore1.p12", "123456", true, "AA67F2F18586D2C83398F9E5E5BE8BA1CD3FF80257CC74BACF2938CE144BA71D");
	        /*
	        for (PushedNotification NOTIFICATION : NOTIFICATIONS) {
	            if (NOTIFICATION.isSuccessful()) {
	                    // APPLE ACCEPTED THE NOTIFICATION AND SHOULD DELIVER IT
	                    System.out.println("PUSH NOTIFICATION SENT SUCCESSFULLY TO: " +
	                                                    NOTIFICATION.getDevice().getToken());
	                    // STILL NEED TO QUERY THE FEEDBACK SERVICE REGULARLY
	            } 
	            else {
	                    String INVALIDTOKEN = NOTIFICATION.getDevice().getToken();
	                    // ADD CODE HERE TO REMOVE INVALIDTOKEN FROM YOUR DATABASE 

	                    // FIND OUT MORE ABOUT WHAT THE PROBLEM WAS
	                    Exception THEPROBLEM = NOTIFICATION.getException();
	                    THEPROBLEM.printStackTrace();

	                    // IF THE PROBLEM WAS AN ERROR-RESPONSE PACKET RETURNED BY APPLE, GET IT  
	                    ResponsePacket THEERRORRESPONSE = NOTIFICATION.getResponse();
	                    if (THEERRORRESPONSE != null) {
	                            System.out.println(THEERRORRESPONSE.getMessage());
	                    }
	            }
	            
	      }
		*/
			
	    }
	    
	    /*
	    catch (CommunicationException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return response;
	    } 
	    catch (KeystoreException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return response;
	    } catch (JSONException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	        return response;
	    }
	    */
	    	
	    catch(Exception e){
            e.printStackTrace();
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "JavaPNSUtils->push() apns execption  : " + e.toString());
	    	return response;
	    }
	    
	}
}
