package frameworks.models;

/**
 * 
 * @author johnwu
 *
 */

public enum RequestJoinType {
	OWNER_INVITE_USER,
	USER_ASK_OWNER
}
