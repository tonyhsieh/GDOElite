/**
 *
 */
package frameworks.models;

/**
 * @author carloxwang
 *
 */
public enum FileType {

	EVENT_ATTACHMENT,
	SG_CONFIGURATION,
	SG_FIRMWARE,
	XG_FIRMWARE,
	CONFIGURATION_IRRIGATION_CONTROLLER,
	ACTIVE_CODE,
	SG_PHOTO
	;
}
