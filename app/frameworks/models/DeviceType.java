/**
 * 
 */
package frameworks.models;
/**
 * @author johnwu
 * @version 創建時間：2013/8/15 上午11:02:52
 */
public enum DeviceType {
	ASANTE_GENIE(0),
	X_GENIE(1);
	
	public final int value;
	DeviceType(int value) {
        this.value = value;
    }
}
