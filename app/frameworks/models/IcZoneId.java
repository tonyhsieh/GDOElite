package frameworks.models;

public enum IcZoneId {
	
	ALL_ZONES((byte) 0),
	ZONE_1((byte) 1),
	ZONE_2((byte) 2),
	ZONE_3((byte) 3),
	ZONE_4((byte) 4),
	ZONE_5((byte) 5),
	ZONE_6((byte) 6);
	
	private byte value;
	
	IcZoneId(byte value) {
		this.value = value;
	}
	
	public static IcZoneId fromValue(byte id) {
        return IcZoneId.values()[id];
    }
	
	public static byte toValue(IcZoneId id) {
		return id.value;
	}
	
}
