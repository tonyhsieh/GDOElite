/**
 *
 */
package frameworks.models;

/**
 * @author carloxwang
 *
 */
public enum ContactType {

	PUSH_NOTIFICATION,
	EMAIL,
	SMS,
	EMAILNSMS,
	MMS,
	CELL_NO,
	HOME_ADDRESS;
}
