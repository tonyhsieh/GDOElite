package frameworks.models;

/**
 * Created with IntelliJ IDEA.
 * User: carloxwang
 * Date: 13/4/24
 * Time: PM4:28
 */
public enum CacheKey {

    PWD_RESET("pwd_reset_"),
    USER_SESSION("user_session_"),
    SG_SESSION("sg_session_"),
    USER_SESSION_LIST("user_session_list_"),
    SG_SESSION_LIST("sg_session_list_"),
    MSG_QUEUE_ID("msg_queue_id_"),
    MSG_QUEUE_MANUAL_ID("msg_queue_manual_id_"),
    AG_PUBLIC_IP("ag_public_ip_"),
    EMAIL2TEXT_PIN("email2text_pin");

    public String value;

    CacheKey(String value){
        this.value = value;
    }
}
