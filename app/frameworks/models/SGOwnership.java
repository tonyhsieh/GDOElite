package frameworks.models;

/**
 * 
 * @author johnwu
 *
 */

public enum SGOwnership {
	OWNER,
	USER
}
