package frameworks.models;

public enum IcDataId {
	
	HOURLY((byte)-1),
	DAILY((byte)0),
	WEEKLY((byte)1),
	MONTHLY((byte)2),
	YEARLY((byte)3);
	
	private byte value;
	
	IcDataId(byte value) {
		this.value = value;
	}
	
	public static IcDataId fromValue(byte id) {
        return IcDataId.values()[id + 1];
    }
	
	public static byte toValue(IcDataId id) {
		return id.value;
	}

}
