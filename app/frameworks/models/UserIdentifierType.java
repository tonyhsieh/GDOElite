package frameworks.models;

public enum UserIdentifierType {

	GOOGLE,
	FACEBOOK,
	TWITTER,
	APPLE,
	EMAIL,
	USERNAME,
	CELL_NO
	;

}
