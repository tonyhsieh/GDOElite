package frameworks.models;

public enum ContactRelationshipType {

	OWNER,
	USER,
	FRIEND
	;
}