/**
 *
 */
package frameworks.exceptions;

import java.util.List;
import java.util.Map;

import play.data.Form;
import play.data.validation.ValidationError;

/**
 * @author carloxwang
 *
 */
public class FormDataValidationException extends RuntimeException {

	/**
	 *
	 */
	private static final long serialVersionUID = 7103107646770789734L;
	private Map<String, List<ValidationError>> errors;

	public FormDataValidationException(Form<?> form){
		errors = form.errors();
	}

	public Map<String, List<ValidationError>> errors(){
		return this.errors;
	}

	@Override
	public String toString() {
		String result = FormDataValidationException.class.getSimpleName();

		result += "\\n==========Validation Error==========";

		for(String key : errors.keySet()){
			result += "\\n " + errors.get(key);
		}

		result += "\\n==========Validation Error==========";

		return result;
	}
}
