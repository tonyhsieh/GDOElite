package filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.net.InetAddress;
import java.util.concurrent.CompletionStage;
import java.util.function.Function;

import javax.inject.Inject;

import akka.stream.Materializer;
import akka.util.ByteString;
import play.core.j.JavaResultExtractor;
import play.mvc.*;
import utils.LogUtil;
import utils.LoggingUtils;
import utils.MyUtil;
import utils.OMManager;

public class LoggingFilter extends Filter
{
    private Materializer mat;

    @Inject
    public LoggingFilter(Materializer mat)
    {
        super(mat);
        this.mat = mat;
    }

    @Override
    public CompletionStage<Result> apply(
            Function<Http.RequestHeader, CompletionStage<Result>> nextFilter,
            Http.RequestHeader requestHeader)
    {
        long startTime = System.currentTimeMillis();

        return nextFilter.apply(requestHeader).thenApply(result ->
        {
            long endTime = System.currentTimeMillis();
            long processTime = endTime - startTime;
            if(!requestHeader.uri().contains("admin") && !requestHeader.uri().contains("pwd") && !requestHeader.uri().contains("assets")) {
                try {
                    ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
                    ObjectNode node = mapper.getNodeFactory().objectNode();
                    if (!node.has("extIp")) {
                        node.put("extIp", MyUtil.getClientIp(requestHeader));
                    }
                    if (InetAddress.getLocalHost() != null) {
                        node.put("host", InetAddress.getLocalHost().getHostName());
                    }
                    node.put("method", requestHeader.method());
                    node.put("uri", requestHeader.uri());
                    node.put("httpStatus", result.status());
                    node.put("processTime", processTime);
                    try {
                        ByteString byteString = JavaResultExtractor.getBody(result, 10000L, mat);
                        node.put("respBody", byteString.decodeString("UTF-8"));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    LogUtil.log(mapper.writeValueAsString(node));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return result.withHeader("Request-Time", "" + processTime);
        });
    }
}