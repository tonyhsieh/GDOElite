package controllers.socket;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.DelimiterBasedFrameDecoder;
import io.netty.handler.codec.Delimiters;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.ssl.SslContext;
import io.netty.handler.ssl.SslHandler;
import play.Configuration;
import play.Logger;
import play.Logger.ALogger;
import utils.LoggingUtils;
import utils.SSLUtil;

import javax.net.ssl.SSLEngine;

public class P2PServerInitializer extends ChannelInitializer<SocketChannel> {

    public  Configuration configuration;
    public P2PServerInitializer(Configuration configuration) {
        this.configuration = configuration;
    }
	private final ALogger logger = Logger.of(this.getClass());

	@Override
    public void initChannel(SocketChannel channel) throws Exception {
   	 	ChannelPipeline pipeline = channel.pipeline();
        SSLEngine sslEngine = SSLUtil.getServerContext(configuration).createSSLEngine();
        sslEngine.setUseClientMode(false);
        //sslEngine.setNeedClientAuth(true);
        pipeline.addLast("ssl", new SslHandler(sslEngine));
        pipeline.addLast("framer", new DelimiterBasedFrameDecoder(8192, Delimiters.lineDelimiter()));
        pipeline.addLast("decoder", new StringDecoder());
        pipeline.addLast("encoder", new StringEncoder());
        pipeline.addLast("handler", new P2PServerHandler());

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Client:" + channel.remoteAddress() +" connected!");

        logger.info("Client:" + channel.remoteAddress() +" connected!");
    }

}