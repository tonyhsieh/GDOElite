package controllers.socket;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.v1.sg.cache.SGSessionUtils;
import controllers.api.v2.sg.SmartGenieController;
import controllers.api.v2.sg.form.JsonAsantePack;
import controllers.api.v2.sg.form.JsonReqPack;
import controllers.api.v2.sg.form.ListSwitchCfgReqPack;
import controllers.util.GeneralValidator;
import io.netty.channel.Channel;
import io.netty.channel.ChannelFutureListener;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import models.SmartGenie;
import models.p2p.CallChannelIP;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.Logger;
import utils.LoggingUtils;
import utils.OMManager;

import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class P2PServerHandler extends SimpleChannelInboundHandler<String> {

    private final Logger.ALogger logger = Logger.of(this.getClass());
    public static ConcurrentHashMap<String, Channel> macAddressToChannel = new ConcurrentHashMap<String, Channel>();
    public static ConcurrentHashMap<Channel, String> channelToMacAddress = new ConcurrentHashMap<Channel, String>();


    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "=====> chanelActive:" + ctx.channel().hashCode());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) { // (4)
        // Close the connection when an exception is raised.
        cause.printStackTrace();
        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "netty socket exceptionCaught" + ExceptionUtils.getStackTrace(cause));
        ctx.close();
    }

    private void clearHashMap(Channel channel) {
        String macAddress = channelToMacAddress.get(channel);
        if (macAddress != null) {
            Channel channelFromMap = macAddressToChannel.get(macAddress);

            if (channelFromMap != null && channelFromMap == channel) {
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "    ====> macAddressToChannel.remove(" + macAddress + ")");
                macAddressToChannel.remove(macAddress);
            }

        } else {
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "    ====> cannot find macAddress from channelToMacAddress table");
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "    ====> channelToMacAddress.remove(" + channel.hashCode() + ")");
        channelToMacAddress.remove(channel);
    }

    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Channel incoming = ctx.channel();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "=====> chanelDeactive:" + ctx.channel().hashCode());
        clearHashMap(incoming);
    }

    @Override
    protected void channelRead0(ChannelHandlerContext ctx, String msg) throws Exception {
        Channel incoming = ctx.channel();
        int chid = incoming.hashCode();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "================= Channel [" + chid + "] =========================");
        ObjectMapper mapper = new ObjectMapper();
        JsonAsantePack inputMsg = null;
        try {
            inputMsg = mapper.readValue(msg, JsonAsantePack.class);
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, " read channel Read0 error " + ExceptionUtils.getStackTrace(e));
        }
        if(inputMsg != null) {
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, " channel msg: " + inputMsg.toString());
            String macAddr = (String) channelToMacAddress.get(incoming);
            if(macAddr != null) {
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "=====> Get MacAdress from table:" + macAddr);
                if (isHashMapCorrect(macAddr, ctx)) {
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "    =====> everything OK, reply 1, macAddr:" + macAddr);
                    ObjectMapper map_out = OMManager.getInstance().getObjectMapper();
                    JsonNode jsonNode = map_out.createObjectNode();
                    ((ObjectNode) jsonNode).put("response", 200);
                    ((ObjectNode) jsonNode).put("source", "alive_response");
                    incoming.writeAndFlush(jsonNode + "\n"); // echo KEEPALIVE request
                } else {
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "    =====> HashMap inconsistent, disconnect macAddr " + macAddr + " channel " + incoming);
                    ObjectMapper map_out = OMManager.getInstance().getObjectMapper();
                    JsonNode jsonNode = map_out.createObjectNode();
                    ((ObjectNode) jsonNode).put("response", -300);
                    ((ObjectNode) jsonNode).put("source", "cert_response");
                    incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                    return;
                }
            } else {
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, String.format("=====> macAddr(%s) is null, reply 0", ctx.channel().hashCode()));
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -700);
                ((ObjectNode) jsonNode).put("source", "cert_response");
                incoming.writeAndFlush(jsonNode + "\n");
            }
        }else{
            ObjectMapper jsonMapper = new ObjectMapper();
            JsonReqPack json = null;
            try {
                json = jsonMapper.readValue(msg, JsonReqPack.class);
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, " read channel Read0 error " + ExceptionUtils.getStackTrace(e));
            }
            if (json == null) {
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -400 netty error , channel content" + msg);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -400);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
            if (json.macAddress == null) {
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -500 netty error , channel content" + msg);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -500);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
            if (json.sessionId == null) {
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -600 netty error , channel content" + msg);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -600);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "get msg from telnet " + json.macAddress);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "session id " + json.sessionId);
            String sessionId = json.sessionId;
            String macAddress = json.macAddress;
            SmartGenie session_sg = SGSessionUtils.getSmartGenie(sessionId);
            if(session_sg == null){
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -10005 netty error, session " + sessionId + "mac " + macAddress);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -10005);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
            SmartGenie mac_sg = isValidSmartGenie(macAddress);
            if(mac_sg == null){
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -12001 netty error, session " + sessionId + "mac " + macAddress);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -12001);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
            if (session_sg.id.equals(mac_sg.id)) {
                CallChannelIP.updateMacAddress(macAddress);
                try{
                    if(macAddressToChannel.containsKey(macAddress)){
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " mac already in hash " + macAddress + "channel " + macAddressToChannel.get(macAddress).toString());
                        Channel old_channel = macAddressToChannel.get(macAddress);
                        macAddressToChannel.remove(macAddress);
                        if(old_channel != null && channelToMacAddress.containsKey(old_channel)){
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " channel already in hash " + old_channel.toString() + " macaddress " + channelToMacAddress.get(old_channel));
                            channelToMacAddress.remove(old_channel);
                        }
                        macAddressToChannel.put(macAddress, incoming);
                        channelToMacAddress.put(incoming, macAddress);
                    }else{
                        macAddressToChannel.put(macAddress, incoming);
                        channelToMacAddress.put(incoming, macAddress);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, " write in hash error " + ExceptionUtils.getStackTrace(e));
                }
                ObjectMapper map_success = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_success.createObjectNode();
                ((ObjectNode) jsonNode).put("response", 1);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n");
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "=====> Get Mac Addresss:" + macAddress);
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "  ===> channel.hashCode" + ctx.channel().hashCode());
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "  ===> channel.msg:" + macAddress);
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "  ===> macAddressToChannel.put(" + macAddress + ", " + chid + ")");
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "  ===> channelToMacAddress.put(" + chid + ", " + macAddress + ")");
            }else{
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " -12002 netty error, session " + sessionId + "mac " + macAddress);
                ObjectMapper map_error = OMManager.getInstance().getObjectMapper();
                JsonNode jsonNode = map_error.createObjectNode();
                ((ObjectNode) jsonNode).put("response", -12002);
                ((ObjectNode) jsonNode).put("source","cert_response");
                incoming.writeAndFlush(jsonNode + "\n").addListener(ChannelFutureListener.CLOSE);
                return;
            }
        }
    }

    private boolean isHashMapCorrect(String macAddress, ChannelHandlerContext ctx) {
        Channel incomming = ctx.channel();
        return
                macAddressToChannel.get(macAddress) == incomming &&
                        channelToMacAddress.get(incomming) != null &&
                        channelToMacAddress.get(incomming).equals(macAddress);
    }

    private SmartGenie isValidSmartGenie(String macAddress) {
        if(!GeneralValidator.isValidMacAddr(macAddress)){
            return null;
        }
        SmartGenie sg = SmartGenie.findByMacAddress(macAddress);
        if (sg != null) {
            return sg;
        } else {
            return null;
        }
    }
}
