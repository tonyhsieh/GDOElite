package controllers.socket;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import io.netty.channel.Channel;
import models.SmartGenie;
import models.XGenie;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.Logger;
import play.Logger.ALogger;
import play.libs.Json;
import utils.ExceptionUtil;
import utils.LoggingUtils;
import utils.OMManager;

import java.util.Calendar;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class P2PConnectionManager implements Runnable {
    private SmartGenie sg;
    private XGenie xg;
    private String triggerSource;
    private int doorCtrl;
    private String timeStamp;
    private String action;
    public P2PConnectionManager(SmartGenie smartGenie, XGenie xGenie, String triggerSource, int doorCtrl, String action) {
        Calendar rightNow = Calendar.getInstance();
        String now = String.valueOf(rightNow.getTimeInMillis());
        this.timeStamp = now;
        this.sg = smartGenie;
        this.xg = xGenie;
        this.triggerSource = triggerSource;
        this.doorCtrl = doorCtrl;
        this.action = action;
    }

    @Override
    public void run() {
        String logHeader = "[P2PConnectionManager] run() - ";
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + "Inform Callee");
        Channel channel = (Channel) P2PServerHandler.macAddressToChannel.get(sg.macAddress);
        if (channel != null) {
            ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
            JsonNode jsonNode = mapper.createObjectNode();
            ((ObjectNode) jsonNode).put("source", triggerSource);
            ((ObjectNode) jsonNode).put("macAddr", xg.macAddr);
            ((ObjectNode) jsonNode).put("doorCtrl", doorCtrl);
            ((ObjectNode) jsonNode).put("timeStamp", timeStamp);
            ((ObjectNode) jsonNode).put("action", action);
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, logHeader + " jsonNode " + jsonNode.toString());
            channel.writeAndFlush(jsonNode + "\n");
        } else {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, logHeader + "Socket Channel not found for Callee: " + sg.macAddress + "gdo macaddr " + xg.macAddr);
        }
    }
}
