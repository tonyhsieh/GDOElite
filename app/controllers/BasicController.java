/**
 *
 */
package controllers;

import json.models.web.admin.vo.UserVO;
import frameworks.Constants;

/**
 * @author carloxwang
 *
 */
public class BasicController extends BaseController{

	public static UserVO user(){

		if(getCrtUser() != null){

			return new UserVO(getCrtUser());
		}else{
			session().remove(Constants.SESSION_KEY_USER);
		}


		return null;
	}
}
