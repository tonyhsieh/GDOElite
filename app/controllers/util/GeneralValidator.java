package controllers.util;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class GeneralValidator {
	
	public static boolean isValidMacAddr(String macAddr) {
		String patternMac = "^[0-9A-Fa-f]{12}$";
		Pattern pattern = Pattern.compile(patternMac);

		// Now create matcher object.
		Matcher matcher = pattern.matcher(macAddr);
		if (!matcher.find()) {
			return false;
		}

		return true;
	}
	/*
	public static void validateDeviceJson(Common c, JsonNode node) throws Exception {
		if (c instanceof Callee) {
			JsonNode macAddrNode = node.get("macAddr");
			if (macAddrNode == null) {
				c.setStatusCode(StatusCode.MISSING_MAC_ADDRESS);
				throw new Exception("Missing parameter [macAddr]");
			}
			String macAddr = macAddrNode.asText();
			if (!isValidMacAddr(macAddr)) {
				c.setStatusCode(StatusCode.INVALID_MAC_ADDRESS);
				throw new Exception("Invalid MAC Address: " + macAddr);
			}
		} else if (c instanceof Caller) {
			if (node.get("networkEnv") == null) {
				c.setStatusCode(StatusCode.MISSING_NAT_TYPE);
				throw new Exception("Missing parameter [networkEnv]");
			}
		}
		
		if (node.get("intIp") == null) {
			c.setStatusCode(StatusCode.MISSING_INT_IP);
			throw new Exception("Missing parameter [intIp]");
		}
		if (node.get("intPort") == null) {
			c.setStatusCode(StatusCode.MISSING_INT_PORT);
			throw new Exception("Missing parameter [intPort]");
		}
		if (node.get("extIp") == null) {
			c.setStatusCode(StatusCode.MISSING_EXT_IP);
			throw new Exception("Missing parameter [extIp]");
		}
		if (node.get("extPort") == null) {
			c.setStatusCode(StatusCode.MISSING_EXT_PORT);
			throw new Exception("Missing parameter [extPort]");
		}
		if (node.get("natType") == null) {
			c.setStatusCode(StatusCode.MISSING_NAT_TYPE);
			throw new Exception("Missing parameter [natType]");
		}
	}
	
	public static void validateConnStatJson(Common c, JsonNode node) throws Exception {
		if (node.findPath("sessionKey") == null) {
			c.setStatusCode(StatusCode.MISSING_SESSION_KEY);
			throw new Exception("Missing parameter [sessionKey]");
		}
		String key = node.findPath("sessionKey").textValue();
		SessionKey sessionKey = SessionKey.find(key);
		if (sessionKey == null) {
			c.setStatusCode(StatusCode.SESSION_KEY_NOT_FOUND);
			throw new Exception("Invalid SessionKey: " + key);
		}
		
		if (node.findPath("makeCallId") == null) {
			c.setStatusCode(StatusCode.MISSING_MAKECALL_ID);
			throw new Exception("Missing parameter [makeCallId]");
		}
		
		if (node.findPath("tunnel") == null) {
			c.setStatusCode(StatusCode.MISSING_TUNNEL);
			throw new Exception("Missing parameter [tunnel]");
		}
		if (node.findPath("connected") == null) {
			c.setStatusCode(StatusCode.MISSING_CONNECTED);
			throw new Exception("Missing parameter [connected]");
		}
		
		JsonNode nodeIntIp = node.findPath("intIp");
		JsonNode nodeIntPort = node.findPath("intPort");
		JsonNode nodeExtIp = node.findPath("extIp");
		JsonNode nodeExtPort = node.findPath("extPort");
		if (nodeIntIp == null && nodeIntPort == null && nodeExtIp == null && nodeExtPort == null) {
			c.setStatusCode(StatusCode.GENERAL_ERROR);
			throw new Exception("Missing parameter [ip/port]");
		}
		if (nodeIntIp == null) {
			if (nodeExtIp == null) {
				c.setStatusCode(StatusCode.MISSING_EXT_IP);
				throw new Exception("Missing parameter [extIp]");
			}
			if (nodeExtPort == null) {
				c.setStatusCode(StatusCode.MISSING_EXT_PORT);
				throw new Exception("Missing parameter [extPort]");
			}
		}
		if (nodeExtIp == null) {
			if (nodeIntIp == null) {
				c.setStatusCode(StatusCode.MISSING_INT_IP);
				throw new Exception("Missing parameter [intIp]");
			}
			if (nodeIntPort == null) {
				c.setStatusCode(StatusCode.MISSING_INT_PORT);
				throw new Exception("Missing parameter [intPort]");
			}
		}
	}
	*/
}
