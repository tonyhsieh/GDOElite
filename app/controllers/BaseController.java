package controllers;

import models.User;

import org.apache.commons.lang3.StringUtils;

import play.mvc.Controller;
import play.mvc.Http;

import com.avaje.ebean.Ebean;

import frameworks.Constants;

/**
 * Created with IntelliJ IDEA.
 * User: carloxwang
 * Date: 13/4/24
 * Time: PM3:22
 */
public class BaseController extends Controller{

    protected static void setErrorMessage(String msg){
        flash("errMsg", msg);
    }

    protected static void setWarningMessage(String msg){
        flash("warnMsg", msg);
    }

    protected static void setInfoMessage(String msg){
        flash("infoMsg", msg);
    }

	@SuppressWarnings("unchecked")
	protected static <T> T createInputForm(){
		return (T) Http.Context.current().args.get(Constants.WEB_ADMIN_SESSION_HTTP_ARGS_KEY);
	}


    protected static String buildServerURL(){

    	String scheme = (request().version().contains("HTTPS")) ? "https://" : "http://";

    	return scheme + request().host();
    }

    public static User getCrtUser(){
    	String userId = session().get(Constants.SESSION_KEY_USER);

    	if(StringUtils.isBlank(userId)){
    		return null;
    	}

    	User user = Ebean.find(User.class, userId);

    	return user;
    }

}
