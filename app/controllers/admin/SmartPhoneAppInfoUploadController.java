package controllers.admin;

import com.avaje.ebean.*;
import controllers.api.v1.app.BaseAPIController;
import json.models.web.admin.vo.AppInfoVO;
import json.models.web.admin.vo.PageVO;
import models.AppInfo;
import play.data.DynamicForm;
import play.mvc.Result;
import utils.LoggingUtils;
import views.html.admin.uploadAppInfo;

import java.util.HashMap;
import java.util.Map;
import play.data.FormFactory;
import javax.inject.Inject;
import static play.data.Form.form;

/**
 *
 * @author johnwu
 *
 */
public class SmartPhoneAppInfoUploadController extends BaseAPIController {
	@Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();
	
	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
	}
	
	public Result managerAppInfo(String page) {
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}
		
		PagedList<AppInfo> appInfoPage = null;
		appInfoPage = AppInfo.appInfoPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);

		PageVO<AppInfoVO> appInfoVO = new PageVO<AppInfoVO>(appInfoPage);

		for(AppInfo appInfo : appInfoPage.getList()){
			appInfoVO.currents.add(new AppInfoVO(appInfo));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
        return ok(uploadAppInfo.render(appInfoVO, order, direction, null));
    }

    public Result update() {
        DynamicForm form = formFactory.form().bindFromRequest();
		String version_code = form.get("versionCode");
		String version_name = form.get("versionName");
		String forceUpgrade = form.get("force_upgrade");
        String device_os_name = form.get("class").toLowerCase();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Form : tostring " + form.toString());

        if (!version_code.equals("") && !version_name.equals("")  && !device_os_name.equals("")) {
            AppInfo appInfo = AppInfo.findByOsName(device_os_name);
            if(appInfo == null){
                appInfo = new AppInfo();
            }
        	appInfo.version_code = Integer.parseInt(version_code);
        	appInfo.version_name = version_name;
			if("force_upgrade".equals(forceUpgrade)){
				appInfo.force_upgrade = 1;
			}else{
				appInfo.force_upgrade = 0;
			}
        	appInfo.device_os_name = device_os_name;
        	appInfo.save();

            return redirect(controllers.admin.routes.SmartPhoneAppInfoUploadController.managerAppInfo("0"));
        }
        else {
            return badRequest("AppInfo update error");
        }
    }

}
