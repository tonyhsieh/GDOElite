package controllers.admin;

import static play.data.Form.form;

import models.AdminWhiteList;
import models.User;
import models.UserIdentifier;

import org.apache.commons.lang3.StringUtils;

import play.data.FormFactory;
import play.mvc.Result;
import views.html.admin.admin_login;

import com.avaje.ebean.annotation.Transactional;
import com.restfb.DefaultFacebookClient;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.Version;
import com.restfb.exception.FacebookException;

import controllers.BaseController;
import controllers.admin.annotations.ParameterInject;
import controllers.admin.forms.FBLoginForm;
import frameworks.Constants;
import frameworks.models.UserIdentifierType;

import javax.inject.Inject;

/**
 * @author carloxwang
 */
public class AdminLoginController extends BaseController
{
    @ParameterInject(FBLoginForm.class)
    @Transactional
    public Result doLogin()
    {
        FBLoginForm form = createInputForm();

        String email;
        String nickName;

        try
        {
            FacebookClient facebookClient = new DefaultFacebookClient(form.accessToken, Version.LATEST);
            com.restfb.types.User restFBUser = facebookClient.fetchObject("me", com.restfb.types.User.class, Parameter.with("fields", "email,name"));

            email = restFBUser.getEmail();
            nickName = restFBUser.getName();

            if (email == null)
            {
                return badRequest("failed to get email address");
            }

            if (!restFBUser.getId().equalsIgnoreCase(form.fbUserId))
            {
                return badRequest("Are you trying to do something bad...?!");
            }
        }
        catch (FacebookException e)
        {
            e.printStackTrace();
            return badRequest("Are you trying to do something bad...?!");
        }

        UserIdentifier ui = UserIdentifier.getUserIdentifier(form.fbUserId, UserIdentifierType.FACEBOOK);

        if (ui == null)
        {
            // check whitelist
            AdminWhiteList awl = AdminWhiteList.find(email);

            if (awl == null)
            {
                return badRequest("............ You're not on the invitation list.");
            }

            // bind email
            UserIdentifier emailUI = UserIdentifier.getUserIdentifier(email, UserIdentifierType.EMAIL);

            User _u;

            if (emailUI == null || emailUI.user == null)
            {
                // create
                User user = new User();
                user.email = email;
                user.isAdmin = true;
                user.nickname = nickName;
                user.fbUserId = form.fbUserId;
                user.save();

                _u = user;

                UserIdentifier _ui = new UserIdentifier();
                _ui.identData = email;
                _ui.identType = UserIdentifierType.EMAIL;
                _ui.id = UserIdentifier.genUserIdentifierId(UserIdentifierType.EMAIL, email);
                _ui.user = user;
                _ui.save();
            }
            else
            {
                _u = emailUI.user;
                _u.fbUserId = form.fbUserId;
            }

            UserIdentifier _ui = new UserIdentifier();
            _ui.identData = form.fbUserId;
            _ui.identType = UserIdentifierType.FACEBOOK;
            _ui.id = UserIdentifier.genUserIdentifierId(UserIdentifierType.FACEBOOK, form.fbUserId);
            _ui.user = _u;
            _ui.save();

            session().put(Constants.SESSION_KEY_USER, _u.id.toString());
        }
        else
        {
            if (!ui.user.isAdmin)
            {
                return badRequest("Hey... What are you doing?");
            }

            session().put(Constants.SESSION_KEY_USER, ui.user.id.toString());
        }

        setInfoMessage("Login Success");


        if (StringUtils.isNotBlank(form.from))
        {
            return redirect(form.from);
        }

        return redirect(routes.AdminHomeController.indexPage());
    }

    public Result loginPage()
    {
        return ok(admin_login.render());
    }
    @Inject FormFactory formFactory;
    public Result logout()
    {

        if (getCrtUser() == null)
        {
            // Remove anyway
            session().remove(Constants.SESSION_KEY_USER);

            return redirect(routes.AdminHomeController.indexPage());
        }

        setInfoMessage("You've logged our");
        session().remove(Constants.SESSION_KEY_USER);
        String from = formFactory.form().bindFromRequest().get("from");

        if (StringUtils.isNotBlank(from))
        {
            return redirect(from);
        }

        return redirect(routes.AdminHomeController.indexPage());
    }
}
