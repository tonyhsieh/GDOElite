package controllers.admin;

import play.mvc.Result;
import controllers.BaseController;
import views.html.admin.admin_cloudwatch;

public class AdminCloudWatchController extends BaseController
{
    public Result cloudWatchIndex()
    {
        return ok(admin_cloudwatch.render());
    }

    public Result ec2Alarm()
    {
        return ok(admin_cloudwatch.render());
    }
}
