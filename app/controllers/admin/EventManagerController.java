/**
 * 
 */
package controllers.admin;
import static play.data.Form.form;
import play.data.FormFactory;
import javax.inject.Inject;
import java.util.List;

import json.models.web.admin.vo.DuplicateEventLogVO;
import json.models.web.admin.vo.PageVO;
import json.models.web.admin.vo.SmartGenieVO;
import json.models.web.admin.vo.UserVO;

import org.apache.commons.lang3.StringUtils;

import models.DuplicateEventLog;
import models.Event;
import models.SmartGenie;
import models.User;
import play.data.DynamicForm;
import play.mvc.Result;
import views.html.admin.admin_user;
import views.html.admin.event_ajax;
import views.html.admin.event_manager;
import views.html.admin.admin_duplicate_event_log;

import com.avaje.ebean.*;


import controllers.BaseController;

/**
 * @author johnwu
 * @version 創建時間：2013/8/15 上午11:42:30
 */
public class EventManagerController extends BaseController {
    @Inject FormFactory formFactory;
	public Result index() {
        return ok(event_manager.render());
    }
	
	public Result addNewEvent(){
		
		DynamicForm form = formFactory.form().bindFromRequest();
		
		int eventType = Integer.parseInt(form.get("eventType"));
		String eventMsg = form.get("eventMsg");
		boolean canNotify = "0".equals(form.get("canNotify"))?false:true;
		String comment = form.get("comment");
		
		Event event = Ebean.find(Event.class, eventType);
		
		if(event == null){
			event = new Event();
			event.eventType = eventType;
		}
		
		event.eventMsg = eventMsg;
		event.canNotify = canNotify;
		event.comment = comment;
		
		event.save();
		
		return redirect(controllers.admin.routes.EventManagerController.index());
	}
	
	public Result listEventAjax(){
		List<Event> eventList = Event.list();
		
		return ok(event_ajax.render(eventList));
	}
	
	
	public Result duplicateEventLog(String page, String sgId){

		//DynamicForm df = form().bindFromRequest();
		
		//String orderByProp = "createAt";

		PagedList<DuplicateEventLog> duplicateEventLogPage = null;

		duplicateEventLogPage = DuplicateEventLog.logPageBySg(sgId, page, 20);
		
		PageVO<DuplicateEventLogVO> duplicateEventLogPageVO = new PageVO<DuplicateEventLogVO>(duplicateEventLogPage);

		for(DuplicateEventLog sf : duplicateEventLogPage.getList()){
			
			DuplicateEventLogVO sfvo= new DuplicateEventLogVO(sf);
			
			duplicateEventLogPageVO.currents.add(sfvo);
		}

		return ok(admin_duplicate_event_log.render(duplicateEventLogPageVO, sgId));
	}
	
}
