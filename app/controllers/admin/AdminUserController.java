/**
 *
 */
package controllers.admin;

import static play.data.Form.form;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.PersistenceException;

import json.models.web.admin.vo.PageVO;
import json.models.web.admin.vo.UserVO;
import models.AdminWhiteList;
import models.Configuration;
import models.Contact;
import models.Message;
import models.PnRelationship;
import models.RequestJoin;
import models.SmartGenie;
import models.SmartGenieLog;
import models.User;
import models.UserIdentifier;
import models.UserRelationship;
import models.XGenie;
import models.XGenieRelationship;

import org.apache.commons.lang3.StringUtils;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.DynamicForm;
import play.mvc.Result;
import play.mvc.Security;
import utils.LoggingUtils;
import views.html.admin.admin_user;
import views.html.admin.white_list_user_ajax;

import com.avaje.ebean.*;

import controllers.BaseController;
import frameworks.models.UserIdentifierType;
import play.data.FormFactory;
import javax.inject.Inject;
/**
 * @author carloxwang
 *
 */
@Security.Authenticated(AdminSecured.class)
public class AdminUserController extends BaseController {
    @Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();

	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
		orderByPropMap.put("email", "email");
		orderByPropMap.put("name", "nickname");
	}
	
	public Result manageUsers(String page){

		DynamicForm df =formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String email = df.get("email");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

		PagedList<User> userPage = null;
		
		if(StringUtils.isBlank(email)){
			email = null;
			userPage = User.userPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			userPage = User.userPageWithEmail(email.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}

		PageVO<UserVO> userPageVO = new PageVO<UserVO>(userPage);

		for(User u : userPage.getList()){
			userPageVO.currents.add(new UserVO(u));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}

		return ok(admin_user.render(userPageVO, order, direction, email));
	}

	public Result deleteUser(String userId){
		User user = User.find(userId);

		if(user == null){
			setErrorMessage("User Not Found!!");
			return redirect(routes.AdminUserController.manageUsers("0"));
		}

		// clean all devices
		List<SmartGenie> sgList = SmartGenie.listSmartGenieByOwner(user.id.toString());

		for(SmartGenie sg : sgList){
			List<XGenie> xgList = XGenie.listXGBySG(sg);

			// unpairing all
			for(XGenie xg : xgList){
				xg.smartGenie = null;
				xg.status = -14002;
				xg.save();
				
				//Check  XGenieRelationship
				List<XGenieRelationship> listXGenieRelationship = XGenieRelationship.listXgByParentXgId(xg.macAddr);
				for(int j = 0; j < listXGenieRelationship.size(); j++){
					listXGenieRelationship.get(j).delete();
				}
				
				listXGenieRelationship = XGenieRelationship.listXgByChileXgId(xg.macAddr);
				for(int j = 0; j < listXGenieRelationship.size(); j++){
					listXGenieRelationship.get(j).delete();
				}
				
				//Remove Passive device configuration    2014 06 18 by Jack
				List<Configuration> listConfig = Configuration.listConfigurationByMacAddress(xg.macAddr);{
	                for(int j = 0; j < listConfig.size(); j++){
	                    listConfig.get(j).delete();
	                }
	            }
			}
			
			SmartGenieLog sgLog = SmartGenieLog.find(sg.id);
			
			if(sgLog!=null){
	            sgLog.extIp = null;
	            sgLog.innIp = null;
	            sgLog.netMask = null;
	            sgLog.wifiMacAddress = null;
	            sgLog.wifiIP = null;
	            sgLog.ssidName = null;
	            sgLog.firmwareVer = null;
	            sgLog.lat = null;
	            sgLog.lng = null;
	            sgLog.p2pSuccess = 0;
	            sgLog.p2pFail = 0;
	            
	            sgLog.save();
	        }

			sg.owner = null;
			sg.isPairing = false;
			sg.save();

		}
		
		//remove contact data which user id equal userId
		List<Contact> listContact = Contact.listByUserId(userId);
		
		for(Contact contact : listContact){
			contact.delete();
		}

		// clean all pn relationship
		List<PnRelationship> pnList = PnRelationship.listByUser(user);
		Ebean.delete(pnList);
		
		// clean all pending messages
		List<Message> msgList = Message.listByUser(user.id.toString());
		Ebean.delete(msgList);

		// clean all userIdentifier
		List<UserIdentifier> uiList = UserIdentifier.getListUserIdentifierByUserId(user.id.toString());
		Ebean.delete(uiList);

		// clean all user relationship
		List<UserRelationship> urList = UserRelationship.listUserRelationshipByUserOrOwner(user.id.toString());
		Ebean.delete(urList);

		// clean all request join
		List<RequestJoin> rjList = RequestJoin.listRequestJoinByUserOrOwner(user.id.toString());
		Ebean.delete(rjList);

//		List<UserSession> usList = UserSession.findByUserId(user.id.toString());
//		Ebean.delete(usList);

		user.delete();
		setInfoMessage("Done... RIP...");
		return redirect(routes.AdminUserController.manageUsers("0"));


	}
	
	public Result listWhiteListAjax(){
		List<AdminWhiteList> adminWhiteList = AdminWhiteList.list();
		
		return ok(white_list_user_ajax.render(adminWhiteList));
	}
	
	public Result deleteWhiteUser(){
		DynamicForm df =formFactory.form().bindFromRequest();
		
		String email = df.get("email");
		
		AdminWhiteList whiteUser =  AdminWhiteList.find(email);
		if(whiteUser != null){
			whiteUser.delete();
		}
		
		return redirect(routes.AdminUserController.manageUsers("0"));
	}
	
	public Result insertWhiteUser(){
		DynamicForm df = formFactory.form().bindFromRequest();
		
		String email = df.get("email");
		
		UserIdentifier emailUI = UserIdentifier.getUserIdentifier(email, UserIdentifierType.EMAIL);
        AdminWhiteList whiteList = AdminWhiteList.find(email);
        if(whiteList == null){
            whiteList = new AdminWhiteList();
        }else{
            setErrorMessage("Duplicate email");
            return redirect(routes.AdminUserController.manageUsers("0"));
        }
        whiteList.email = email;
        try{
            whiteList.save();
        }catch(Exception e){
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "white list save error " + ExceptionUtils.getStackTrace(e));
        }
        if(emailUI != null){
            emailUI.user.isAdmin = true;
            try{
                emailUI.user.save();
            }catch (Exception e)
            {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "white list useridentifier error " + ExceptionUtils.getStackTrace(e));
            }
		}

		return redirect(routes.AdminUserController.manageUsers("0"));
	}
	
	public Result exportEmailFile(){
	    response().setHeader("Content-disposition","attachment; filename=email.txt"); 
		
		List<User> user = Ebean.find(User.class).findList();
		
		File file = null;
		try {
			file = new File("email.txt");
			OutputStream os = new FileOutputStream(file);
			BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(os));
			
			for(int i=0; i<user.size(); i++){
				bw.write(user.get(i).email);
				bw.newLine();
			}
			
			bw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

        return ok(file).as("application/x-download");
	}
}
