/**
 *
 */
package controllers.admin.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.mvc.With;
import controllers.admin.ParameterInjectAction;

/**
 * @author carloxwang
 *
 */
@With(ParameterInjectAction.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ParameterInject {

	Class<?> value();

}
