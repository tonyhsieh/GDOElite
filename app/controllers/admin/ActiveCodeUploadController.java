package controllers.admin;

import static play.data.Form.form;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import json.models.web.admin.vo.P2pActiveCodeFileVO;
import json.models.web.admin.vo.PageVO;

import org.apache.commons.lang3.StringUtils;

import models.P2pActiveCodeFile;
import models.P2pActiveCode;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.PagedList;

import controllers.BaseController;
import frameworks.models.FileType;
import play.data.DynamicForm;
import play.mvc.Http;
import play.mvc.Result;
import utils.db.FileEntityManager;
import views.html.admin.uploadActiveCode;
import play.data.FormFactory;
import javax.inject.Inject;

public class ActiveCodeUploadController extends BaseController{
    @Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();
	
	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
		orderByPropMap.put("facName", "name");
		orderByPropMap.put("fileName", "fileName");
	}
	
	public Result managerActiveCode(String page) {
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String facName = df.get("facName");
		
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}
		
		PagedList<P2pActiveCodeFile> facPage = null;
		
		if(StringUtils.isBlank(facName)){
			facName = null;
			facPage = P2pActiveCodeFile.facPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			facPage = P2pActiveCodeFile.facPageWithFacName(facName.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}

		PageVO<P2pActiveCodeFileVO> facPageVO = new PageVO<P2pActiveCodeFileVO>(facPage);

		for(P2pActiveCodeFile fac : facPage.getList()){
			facPageVO.currents.add(new P2pActiveCodeFileVO(fac));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		return ok(uploadActiveCode.render(facPageVO, order, direction, facName));
	}
	
	public Result upload() {
		Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart uploadFilePart = body.getFile("upload");
        
        if (uploadFilePart != null) {	
        	File file = (File) uploadFilePart.getFile();
        	String fileName = uploadFilePart.getFilename();
        	String s3Name = UUID.randomUUID().toString();
        	int count = 0;
        	
        	Ebean.beginTransaction();
			try {
				InputStream input = new FileInputStream(file);
				BufferedReader buffer = new BufferedReader(new InputStreamReader(input));
				String str = null;
				while((str = buffer.readLine()) != null){
					count++;
					if(count > 1){
						P2pActiveCode p2p = new P2pActiveCode();
						if(str.indexOf(",") != -1){
							String[] arr = str.split(",");
							Pattern pattern = Pattern.compile("[a-zA-Z0-9]{12}");
							Matcher matcherMacAdress = pattern.matcher(arr[0]);
							Matcher matcherAtiveCode = pattern.matcher(arr[1]);
							if(matcherMacAdress.find() && matcherAtiveCode.find()){
								p2p.mac_addr = arr[0].trim();
								p2p.active_code = arr[1].trim();
								Ebean.save(p2p);
							}else{
								setErrorMessage((count-1)+"-pen format error!!!");
								return redirect(controllers.admin.routes.ActiveCodeUploadController.managerActiveCode("0"));
							}	
						}else{
							setErrorMessage((count-1)+"-pen format error!!!");
							return redirect(controllers.admin.routes.ActiveCodeUploadController.managerActiveCode("0"));
						}
					}
				}
				Ebean.commitTransaction();
				buffer.close();
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				Ebean.endTransaction();
			}
			
			FileEntityManager feo = new FileEntityManager(file, s3Name, FileType.ACTIVE_CODE);
			
			P2pActiveCodeFile activeCode = new P2pActiveCodeFile();
			
			activeCode.items = count-1;
			activeCode.name = "SYSTEM";
			activeCode.fileName = fileName;
			activeCode.s3_name = s3Name; 
			activeCode.save();
			
			setInfoMessage("File upload success!!!");
        	return redirect(controllers.admin.routes.ActiveCodeUploadController.managerActiveCode("0"));
        }else{
        	setErrorMessage("File upload error!!!");
        	return redirect(controllers.admin.routes.ActiveCodeUploadController.managerActiveCode("0"));
        }	
	}
}
