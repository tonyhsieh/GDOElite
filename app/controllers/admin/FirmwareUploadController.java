package controllers.admin;

import static play.data.Form.form;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.models.web.admin.vo.FirmwareGroupVO;
import json.models.web.admin.vo.PageVO;
import models.FirmwareGroup;
import models.SmartGenie;
import models.XGenie;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.StringUtils;


import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.DynamicForm;

import play.data.FormFactory;
import javax.inject.Inject;
import play.mvc.Http;
import play.mvc.Result;
import utils.LoggingUtils;
import utils.db.FileEntityManager;
import views.html.admin.uploadFile;

import com.avaje.ebean.*;

import controllers.api.v1.app.BaseAPIController;
import controllers.api.v1.sg.DeviceEventLoggerController;
import frameworks.models.EventLogType;
import frameworks.models.FileType;

/**
 *
 * @author johnwu
 *
 */
public class FirmwareUploadController extends BaseAPIController {
    @Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();
	
	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
		orderByPropMap.put("fgName", "name");
	}
	
	public Result managerFirmware(String page) {
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String fgName = df.get("fgName");
		
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}
		
		PagedList<FirmwareGroup> fgPage = null;
		
		if(StringUtils.isBlank(fgName)){
			fgName = null;
			fgPage = FirmwareGroup.fgPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			fgPage = FirmwareGroup.fgPage(fgName.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}

		PageVO<FirmwareGroupVO> fgPageVO = new PageVO<FirmwareGroupVO>(fgPage);

		for(FirmwareGroup fg : fgPage.getList()){
			fgPageVO.currents.add(new FirmwareGroupVO(fg));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
        return ok(uploadFile.render(fgPageVO, order, direction, fgName));
    }

    public Result upload() {
        Http.MultipartFormData body = request().body().asMultipartFormData();
        Http.MultipartFormData.FilePart uploadFilePart = body.getFile("upload");

        DynamicForm form = formFactory.form().bindFromRequest();

        FileType type = FileType.valueOf(form.get("class"));
        String groupName = form.get("groupName");
        String version = form.get("version");
        String forceUpgrade = form.get("force_upgrade");

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, "FirmwareUploadController->upload() groupName : " + groupName);

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Form : tostring " + form.toString());

        
        if (uploadFilePart != null) {

        	File file = (File)uploadFilePart.getFile();
        	String name = uploadFilePart.getFilename();


        	FileEntityManager feo;

            FirmwareGroup new_firmware;
            FirmwareGroup nonforcegroup = FirmwareGroup.findByFwGrpNameForce(groupName, 0);
			FirmwareGroup forcegroup = FirmwareGroup.findByFwGrpNameForce(groupName, 1);

            if("force_upgrade".equals(forceUpgrade)){
                name = FilenameUtils.getBaseName(name) + "_force." + FilenameUtils.getExtension(name);
                feo = new FileEntityManager(file, name, type, null);
                if(forcegroup == null){
                    new_firmware = new FirmwareGroup();
                }else{
                    new_firmware = forcegroup;
                }
            }else{
                feo = new FileEntityManager(file, name, type, null);
                if(nonforcegroup == null){
                    new_firmware = new FirmwareGroup();
                }else{
                    new_firmware = nonforcegroup;
                }
            }

            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "version : " + version);

        	new_firmware.firmwareVersion = version;
        	new_firmware.name = groupName;
        	new_firmware.fileEntity = feo.fileEntity;
        	
        	if("force_upgrade".equals(forceUpgrade)){
        		new_firmware.force_upgrade = 1;
        	}else{
        		new_firmware.force_upgrade = 0;
        	}
            try{
                new_firmware.save();
            }catch (Exception err){
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "new_firmware save error " + ExceptionUtils.getStackTrace(err));
            }


        	if(type == FileType.SG_FIRMWARE){
        		notifyAllSmartGenie(new_firmware);
        	}else if(type == FileType.XG_FIRMWARE){
        		notifyAllXGenie(new_firmware);
        	}
        	
            return redirect(controllers.admin.routes.FirmwareUploadController.managerFirmware("0"));
        }
        else {
            return badRequest("File upload error");
        }
    }
    
    public Result allFirmware(String page){
    	return null;
    }

	private static void notifyAllXGenie(FirmwareGroup fg) {
		List<XGenie> xgList = XGenie.listByFG(fg);
		
		//Logger.info("notifyAllXGenie fg.name : " + fg.name);
        Calendar rightNow = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		for(XGenie xg:xgList){
			if(xg.smartGenie != null && xg.smartGenie.owner != null){
				
				//DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance());
				
				//Logger.info("notifyAllXGenie xg.deviceType : " + xg.deviceType);
				
				//Logger.info("notifyAllXGenie xg.macAddr : " + xg.macAddr);
				
				switch(xg.deviceType) { 
	            case 0: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
	            	//from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
	                break; 
	            case 1:
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
                    //from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
	                break;
	            case 2: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_IRRI_FIRMWARE_NEW");
                    //from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_IRRI_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
	                break;
	            case 3: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
	            	//from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()) );
	                break;
	            case 4: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
	            	
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
	                break;
	            case 100102: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
	            	//from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
	                break;
	            default: 
	            	
	            	//Logger.info("EventLogType : EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW");
	            	//from john to jack
	            	DeviceEventLoggerController.sendMsg(xg.macAddr, xg.smartGenie.id, EventLogType.PASSIVE_DEVICE_GDO_FIRMWARE_NEW.value, Calendar.getInstance(), sdf.format(rightNow.getTime()));
				}
				
				
			}
    	}
	}

	private static void notifyAllSmartGenie(FirmwareGroup fg){
    	//List<SmartGenie> sgList = SmartGenie.listAll();
		Calendar rightNow = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
		List<SmartGenie> sgList = SmartGenie.listByFG(fg);
    	
    	for(SmartGenie sg:sgList){
    		if(sg.owner != null){
    		    //from john to jack
    			DeviceEventLoggerController.sendMsg(null, sg.id, EventLogType.ASANTE_GENIE_FIRMWARE_NEW.value, rightNow, sdf.format(rightNow.getTime()));
    		}
    	}
    }
}
