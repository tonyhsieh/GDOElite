package controllers.admin;

import com.avaje.ebean.*;
import controllers.api.v1.app.BaseAPIController;
import json.models.web.admin.vo.AppInfoVO;
import json.models.web.admin.vo.EmailBlackListVO;
import json.models.web.admin.vo.PageVO;
import models.AppInfo;
import models.EmailBlacklist;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.data.DynamicForm;
import play.mvc.Result;
import utils.LoggingUtils;
import views.html.admin.uploadEmailBlackList;

import java.util.HashMap;
import java.util.Map;
import play.data.FormFactory;
import javax.inject.Inject;
import static play.data.Form.form;

/**
 *
 * @author johnwu
 *
 */
public class EmailBlackListUploadController extends BaseAPIController {
    @Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();
	
	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
	}
	
	public Result managerEmailList(String page) {
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}
		
		PagedList<EmailBlacklist> EmailBlackListPage = null;
		EmailBlackListPage = EmailBlacklist.emailBlacklistPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);

		PageVO<EmailBlackListVO> emailBlackListVO = new PageVO<EmailBlackListVO>(EmailBlackListPage);

		for(EmailBlacklist emailblist : EmailBlackListPage.getList()){
			emailBlackListVO.currents.add(new EmailBlackListVO(emailblist));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
        return ok(uploadEmailBlackList.render(emailBlackListVO, order, direction, null));
    }

    public Result update() {
        DynamicForm form = formFactory.form().bindFromRequest();
		String email = form.get("email");
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Form : tostring " + form.toString());

        if (!email.equals("")) {
            EmailBlacklist ebl = EmailBlacklist.findByEmail(email);
            if(ebl == null){
                ebl = new EmailBlacklist();
                ebl.email = email;
                try{
                    ebl.save();
                }catch (Exception err){
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Email Black List save error " + ExceptionUtils.getStackTrace(err));
                }
                return redirect(controllers.admin.routes.EmailBlackListUploadController.managerEmailList("0"));
            }else{
				return redirect(controllers.admin.routes.EmailBlackListUploadController.managerEmailList("0"));
			}
        }
        else {
            return badRequest("Email black list exist already");
        }
    }
}
