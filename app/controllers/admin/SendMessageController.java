package controllers.admin;

import java.util.ArrayList;
import java.util.List;

import models.*;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.data.DynamicForm;
import play.mvc.Result;
import controllers.BaseController;

import utils.ConfigUtil;
import utils.FirmwareUtils;
import utils.LoggingUtils;
import utils.SQSManager;
import utils.SendMsgUtil;
import utils.notification.DeviceEventQueueMsg;
import views.html.admin.send_message;
import play.data.FormFactory;
import javax.inject.Inject;


public class SendMessageController extends BaseController {
    @Inject FormFactory formFactory;
    static @Inject play.Configuration configuration;
	public Result managerMessage(){
		
		return ok(send_message.render());
	}
	
	public Result sendMessage(){
		DynamicForm form = formFactory.form().bindFromRequest();
		
		int decice = Integer.valueOf(form.get("device"));
		int condition = Integer.valueOf(form.get("condition"));
		String email = form.get("email");
		String notification = form.get("notification");
        String version = form.get("version");
        String title = form.get("title");
        String content = form.get("content");

        
        //Logger.info("compareVer() email : " + email);
        //Logger.info("compareVer() notification : " + notification);
        //Logger.info("compareVer() title : " + title);
        //Logger.info("compareVer() content : " + content);

        List<SmartGenieLog> listSgLog = null;
        
        List<XGenie> listXg = null;
        
        //List<SmartGenie> listSgResult = new ArrayList<SmartGenie>();
        List<User> listUserResult = new ArrayList<User>();
        
        //Condition
        //1. < | 2. = | 3. <=
        
        if(decice == 0){	//AG
        	
        	listSgLog = SmartGenieLog.findAll();
        	
        	for(SmartGenieLog sgLog:listSgLog){
        		
        		try{
        			
        			if(sgLog.firmwareVer!=null){
        			
		        		if(FirmwareUtils.compareVer(version, sgLog.firmwareVer, condition)){
		        			
		        			SmartGenie sg = SmartGenie.find(sgLog.id);
		        			
		        			if(sg.isPairing==true){

		        				//Logger.info("compareVer() scrVer : " + version);
		        				//Logger.info("compareVer() desVer : " + sgLog.firmwareVer);
		        				//Logger.info("Send Msg To : " + sg.owner.email);
		        				
		        				listUserResult.add(sg.owner);
		        			}
		        		}
        			}
        		
        		}catch(Exception err){
					LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "Send Msg AG err : " + err.toString());
        		}
        	}
        	
        }else if(decice == 1 /* IC */ || decice == 2 /* GDO */){
        	
        	if(decice == 1){
        		listXg = XGenie.listByDevType(2);
        	}else if(decice == 2){
        		listXg = XGenie.listByDevType(1);
        	}

        	for(XGenie xg:listXg){
        		
        		if(xg.status==1){
        		
	        		try{
		        		String info = xg.info;
		        		
		        		String[] sections = info.split(";");
		
		        		String value = "";
		        		
		        		for(String section: sections){
		
		    				String[] tmp = section.split(":");
		    				
		    				if("fwVer".equals(tmp[0])){
		    					
		    					value = tmp[1];
		    					
		    					if(FirmwareUtils.compareVer(version, tmp[1], condition)){

			        				SmartGenie sg = SmartGenie.find(xg.smartGenie.id);
			        				
			        				if(sg.owner!=null){
		    						
			        					//Logger.info("compareVer() scrVer : " + version);
				        				//Logger.info("compareVer() desVer : " + tmp[1]);
				        				////Logger.info("Send Msg XG : " + xg.macAddr);
				        				//Logger.info("Send Msg To : " + sg.owner.email);
			        					
			        					//listXgResult.add(xg);
			        					//listSgResult.add(sg);
				        				
			        					listUserResult.add(sg.owner);
			        				}
		    	        		}
		    				}
		        		}
		        	}catch(Exception err){
                        LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "Send Msg XG err : " + err.toString());
					}
        		}
	        }
        }else if(decice == 3 /* iPhone */ || decice == 4 /* Android */){
        	
        	//String version = form.get("version");
        	int iVersion = -1;

        	try{
        		iVersion = Integer.parseInt(version);
        	}catch(Exception err){
        		iVersion = -1;
        	}
        	
        	List<PnRelationship> listPnRelationship = null;
        	
        	if(iVersion>=0){
        	
	        	if(decice==3){
	        		listPnRelationship = PnRelationship.listByOsName("ios");
	        	}else if(decice==4){
	        		listPnRelationship = PnRelationship.listByOsName("android");
	        	}
	        	
	        	List<SmartGenie> listSmartGenie = null;
	        	
	        	for(PnRelationship pnRelationship : listPnRelationship){
	        		
	        		listSmartGenie = SmartGenie.listSmartGenieByOwner(pnRelationship.user.id.toString());
	        		
	        		int appCode = -1;
	        		
	        		if(listSmartGenie.size()!=0 || listSmartGenie!=null){
	        			
	        			try{
	        				appCode = Integer.parseInt(pnRelationship.appCode);
	        			}catch(Exception err){
	        				appCode = -1;
	        			}
	        			
	        			//Condition
	        	        //0. < | 1. = 
	        			if(condition==1 && appCode!=-1){
	        				
	        				if(iVersion==appCode){
	        					listUserResult.add(pnRelationship.user);
	        					
	        					//Logger.info("sendMessage() : " + pnRelationship.user.id);
	        		        	//Logger.info("sendMessage() : " + pnRelationship.pnId);
	        		        	//Logger.info("=====================================");
	        					
	        				}
	        				
	        			}else if(condition==0 && appCode!=-1){
	        				
	        				if(appCode < iVersion){
	        					listUserResult.add(pnRelationship.user);
	        					
	        					//Logger.info("sendMessage() : " + pnRelationship.user.id);
	        		        	//Logger.info("sendMessage() : " + pnRelationship.pnId);
	        		        	//Logger.info("=====================================");
	        					
	        				}
	        			}
	        		}
	        	}
        	}
        }
        
        boolean isEmail = false;
        boolean isPushNotification = false;
        
        if(email!=null){
        	isEmail = true;
        }
        
        if(notification!=null){
        	isPushNotification = true;
        }
        
        DeviceEventQueueMsg queueMsg = new DeviceEventQueueMsg();

        queueMsg.title = title;
        queueMsg.content = content;
        queueMsg.isEmail = isEmail;
        queueMsg.isPushNotification = isPushNotification;
        
        queueMsg.listOwnerEmail = new ArrayList<String>();
        
        for(User user: listUserResult){
        	
        	//Logger.info("Email in Queue : " + user.email);
			if(EmailBlacklist.emailIsValid(user.email)){
				queueMsg.listOwnerEmail.add(user.email);
			}else{
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "email is invalid: " + user.email);
            }
        }

        String url = ConfigUtil.getTransporterUrl();
        if (url != null && url.length() > 0)
        {
            SendMsgUtil.sendMsgByAdminConsole(queueMsg.listOwnerEmail, queueMsg.title,
                    queueMsg.content, queueMsg.isEmail, queueMsg.isPushNotification);
        }
        else
        {
            sendMsg(queueMsg);
        }
        
        //Logger.info("=====================================");
		
		return ok(send_message.render());
	}

	public static void sendMsg(DeviceEventQueueMsg queueMsg){

		String queueRegion = configuration.getString("queue.region");
        String queueName = configuration.getString("queue.name.1");
		SQSManager sqsManager = SQSManager.getInstance(queueRegion);

		String queueUrl;

		ObjectMapper om = new ObjectMapper();
		try {
			queueUrl = sqsManager.getQueue(queueName);
			String sqsMsgId = sqsManager.sendMessage(queueUrl, om.writeValueAsString(queueMsg));
			LoggingUtils.log(LoggingUtils.LogLevel.INFO, "SQS MessageId : " + sqsMsgId);
		} catch (Exception e) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "SQS sendMsg err : " + e.toString());
			e.printStackTrace();
		}
	}
}
