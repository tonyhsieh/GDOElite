/**
 *
 */
package controllers.admin;

import play.mvc.Result;
import play.mvc.Security;
import controllers.BaseController;

/**
 * @author carloxwang
 *
 */
@Security.Authenticated(AdminSecured.class)
public class AdminHomeController extends BaseController {


	public Result indexPage(){
		return redirect(routes.AdminUserController.manageUsers("0"));
	}
}
