/**
 *
 */
package controllers.admin;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;



import play.data.Form;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;
import controllers.admin.annotations.ParameterInject;
import frameworks.Constants;
import utils.LoggingUtils;
import play.data.FormFactory;

/**
 * @author carloxwang
 *
 */
public class ParameterInjectAction extends Action<ParameterInject> {

    @Inject FormFactory formFactory;
	@Override
	public CompletionStage<Result> call(Context ctx) {
		Object pack;

		Form<?> form = null;

		try{
			form = formFactory.form(configuration.value()).bindFromRequest(ctx.request());
			pack = form.get();
			Http.Context.current().args.put(Constants.WEB_ADMIN_SESSION_HTTP_ARGS_KEY, pack);


		} catch (IllegalStateException e){

			if( form != null ){
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");
				for( String key : form.errors().keySet() ){
                    LoggingUtils.log(LoggingUtils.LogLevel.WARN, form.errors().get(key).toString());
				}

                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
			}

			return CompletableFuture.completedFuture(badRequest("Bad Request"));
		}

		return delegate.call(ctx);
	}

}
