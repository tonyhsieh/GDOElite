package controllers.admin;

import play.mvc.Http.Context;
import play.mvc.Result;
import play.mvc.Security;
import frameworks.Constants;

public class AdminSecured extends Security.Authenticator {

    @Override
    public String getUsername(Context ctx) {
        return ctx.session().get(Constants.SESSION_KEY_USER);
    }

    @Override
    public Result onUnauthorized(Context ctx) {
        return redirect(routes.AdminLoginController.loginPage().absoluteURL(ctx.request()) + "?from=" + ctx.request().uri());

    }

}