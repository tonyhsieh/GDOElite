package controllers.admin;

import static play.data.Form.form;

import java.util.HashMap;
import java.util.Map;

import javax.persistence.PersistenceException;

import json.models.web.admin.vo.EmployeeVO;
import json.models.web.admin.vo.PageVO;
import json.models.web.admin.vo.SmartGenieVO;
import models.AdminWhiteList;
import models.Employee;
import models.SmartGenie;
import models.UserIdentifier;

import org.apache.commons.lang3.StringUtils;

import play.data.DynamicForm;
import play.mvc.Result;
import tyrex.services.UUID;
import views.html.admin.employee_list;
import views.html.admin.employee_edit;
import play.data.FormFactory;
import javax.inject.Inject;
import com.avaje.ebean.*;

import controllers.BaseController;
import frameworks.models.UserIdentifierType;

public class EmployeeController extends BaseController {
	@Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();

	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
		orderByPropMap.put("depart", "department");
		orderByPropMap.put("name", "name");
		orderByPropMap.put("email", "email");
	}
	
	public Result listAllEmployee(String page){

		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String email = df.get("email");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

		PagedList<Employee> employeePage = null;

		if(StringUtils.isBlank(email)){
			email = null;
			employeePage = Employee.emPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			employeePage = Employee.emPageWithEmail(email.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}
		
		PageVO<EmployeeVO> employeePageVO = new PageVO<EmployeeVO>(employeePage);

		for(Employee emp : employeePage.getList()){
			employeePageVO.currents.add(new EmployeeVO(emp));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		return ok(employee_list.render(employeePageVO, order, direction, email));

	}
	
	public Result insertEmployee(){
		
		DynamicForm df = formFactory.form().bindFromRequest();
		
		String empId = df.get("empId");
		String name = df.get("name");
		String department = df.get("department");
		String cellPhone = df.get("cellPhone");
		String phoneExtension = df.get("phoneExtension");
		String email = df.get("email");
		
		Employee employee = Employee.find(empId);
		
		if(employee == null){
			employee = new Employee();
			employee.id = UUID.create();
			employee.name = name;
			employee.department = department;
			employee.cellPhone = cellPhone;
			employee.phoneExtension = phoneExtension;
			employee.email = email;
			employee.save();
		}else{
			employee.name = name;
			employee.department = department;
			employee.cellPhone = cellPhone;
			employee.phoneExtension = phoneExtension;
			employee.email = email;
			employee.save();
		}
		
		return redirect(routes.EmployeeController.listAllEmployee("0"));
	}
	
	public Result deleteEmployee(String empId){
		
		Employee employee =  Employee.find(empId);
		
		if(employee != null){
			employee.delete();
		}
		
		return redirect(routes.EmployeeController.listAllEmployee("0"));
	}
	
	public Result editEmployee(String empId) {
		EmployeeVO employeeVO = null;
		
		if(StringUtils.isBlank(empId)){
			employeeVO = new EmployeeVO(null);	
		}else{
			Employee employee = Employee.find(empId);
			employeeVO = new EmployeeVO(employee);
		}	
		
        return ok(employee_edit.render(employeeVO));
    }
}
