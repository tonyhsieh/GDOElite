/**
 *
 */
package controllers.admin;

import static play.data.Form.form;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;

import json.models.web.admin.vo.P2pActiveCodeVO;
import json.models.web.admin.vo.PageVO;
import json.models.web.admin.vo.SmartGenieHistoryVO;
import json.models.web.admin.vo.SmartGenieInfoVO;
import json.models.web.admin.vo.SmartGenieVO;
import json.models.web.admin.vo.XGenieInfoVO;
import json.models.web.admin.vo.XGenieVO;


import models.P2pActiveCode;
import models.SmartGenie;
import models.SmartGenieActionHistory;
import models.SmartGenieLog;
import models.User;
import models.XGenie;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import static play.data.Form.form;

import org.springframework.beans.support.PagedListHolder;
import play.data.FormFactory;
import play.data.DynamicForm;
import play.mvc.Result;
import play.mvc.Security;


import views.html.admin.admin_user_device_detail;
import views.html.admin.admin_user_devices;
import views.html.admin.admin_xg;
import views.html.admin.admin_xg_detail;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.PagedList;

import controllers.BaseController;

/**
 * @author carloxwang
 *
 */
@Security.Authenticated(AdminSecured.class)
public class AdminDeviceController extends BaseController {
    @Inject FormFactory formFactory;
	private final static Map<String, String> orderByPropMap = new HashMap<String, String>();

	static {
		orderByPropMap.put("create", "createAt");
		orderByPropMap.put("update", "updateAt");
		orderByPropMap.put("macAddr", "macAddress");
		orderByPropMap.put("macAddress", "macAddr");
		orderByPropMap.put("devType", "deviceType");
		orderByPropMap.put("name", "name");
	}
	
	public Result listAllSmartGenie(String page){

		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String macAddr = df.get("macAddr");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<SmartGenie> devicePage = null;

		if(StringUtils.isBlank(macAddr)){
			macAddr = null;
			devicePage = SmartGenie.sgPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			devicePage = SmartGenie.sgPageWithMacAddr(macAddr.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}
		
		P2pActiveCodeVO p2pVo = new P2pActiveCodeVO(null);

		if(devicePage.getList().isEmpty()){
			P2pActiveCode p2p = P2pActiveCode.findByMacAddr(macAddr);
			p2pVo = new P2pActiveCodeVO(p2p);
			p2pVo.status = "New";
		}
		
		PageVO<SmartGenieVO> devicePageVO = new PageVO<SmartGenieVO>(devicePage);
		
		for(SmartGenie sf : devicePage.getList()){
			SmartGenieVO sfvo= new SmartGenieVO(sf, null);
			
			if(sf.owner == null){		
				sfvo.status = "Orphan";
			}else{
				sfvo.status = "Binding";
			}
			
			devicePageVO.currents.add(sfvo);						
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		return ok(admin_user_devices.render(devicePageVO, p2pVo, order, direction, macAddr));

	}

	public Result listSmartGenie(String page, String userId){

		User user = User.find(userId);

		if(user == null){
			setErrorMessage("User Not Found!!");
			return redirect(routes.AdminUserController.manageUsers("0"));
		}

		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<SmartGenie> devicePage = SmartGenie.sgPageWithUser(userId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		
		PageVO<SmartGenieVO> devicePageVO = new PageVO<SmartGenieVO>(devicePage);

		for(SmartGenie sf : devicePage.getList()){
			SmartGenieVO sfvo= new SmartGenieVO(sf, null);
			
			if(sf.owner == null){		
				sfvo.status = "Orphan";
			}else{
				sfvo.status = "Binding";
			}
			
			devicePageVO.currents.add(sfvo);
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		P2pActiveCodeVO p2pVO = new P2pActiveCodeVO(null);
		
		return ok(admin_user_devices.render(devicePageVO, p2pVO, order, direction, null));
	}

	public Result getSmartGenie(String page, String smartGenieId){

		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<SmartGenie> devicePage = SmartGenie.sgPageWithSGenieId(smartGenieId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		
		PageVO<SmartGenieVO> devicePageVO = new PageVO<SmartGenieVO>(devicePage);

		for(SmartGenie sf : devicePage.getList()){
			SmartGenieVO sfvo= new SmartGenieVO(sf, null);
			
			if(sf.owner == null){		
				sfvo.status = "Orphan";
			}else{
				sfvo.status = "Binding";
			}
			
			devicePageVO.currents.add(sfvo);
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		P2pActiveCodeVO p2pVO = new P2pActiveCodeVO(null);
		
		return ok(admin_user_devices.render(devicePageVO, p2pVO, order, direction, null));
	}

	
	public Result listAllXGenie(String page){
		
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
		String macAddr = df.get("macAddr");
	
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<XGenie> devicePage = null;
		
		if(StringUtils.isBlank(macAddr)){
			macAddr = null;
			devicePage = XGenie.xgPage(orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}else{
			devicePage = XGenie.xgPageWithMacAddr(macAddr.trim(), orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		}
		
		PageVO<XGenieVO> devicePageVO = new PageVO<XGenieVO>(devicePage);

		for(XGenie xg : devicePage.getList()){
			devicePageVO.currents.add(new XGenieVO(xg));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		return ok(admin_xg.render(devicePageVO, order, direction, macAddr));

	}
	
	/*
	public Result listXGenies(String smartGenieId){
		SmartGenie sg = SmartGenie.find(smartGenieId);

		List<XGenie> xgenies = XGenie.listXGBySG(sg);

		List<XGenieVO> voList = new ArrayList<XGenieVO>(xgenies.size());
		
		for(XGenie xg : xgenies){
			voList.add(new XGenieVO(xg));
		}

		return ok(admin_xg.render(voList));
	}
	*/
	
	public Result listXGenies(String page, String smartGenieId){
		
		SmartGenie sg = SmartGenie.find(smartGenieId);

		if(sg == null){
			setErrorMessage("Device Not Found!!");
			return redirect(routes.AdminUserController.manageUsers("0"));
		}

		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<XGenie> devicePage = XGenie.xgPageWithSGenieId(smartGenieId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		
		PageVO<XGenieVO> devicePageVO = new PageVO<XGenieVO>(devicePage);

		for(XGenie xg : devicePage.getList()){
			devicePageVO.currents.add(new XGenieVO(xg));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}

		return ok(admin_xg.render(devicePageVO, order, direction, null));
	}
	
	public Result showXGInfo(String xgId){
		XGenie xg = Ebean.find(XGenie.class, xgId);
		
		if(xg == null){
			setErrorMessage("Device Not Found!!");
			return redirect(routes.AdminUserController.manageUsers("0"));
		}
		
		//add by Jack
		//For LIST XG info by key / value
		
		List<XGenieInfoVO> listXgInfo = new ArrayList<XGenieInfoVO>();
		
		XGenieInfoVO xgInfo = null;
		String[] strTmp;
		String[] strKeyValue = null;
		
		try{
			strKeyValue = xg.info.split(";"); 
		
			for(int i = 0; i< strKeyValue.length; i++){
				xgInfo = new XGenieInfoVO();
			
				strTmp = strKeyValue[i].split(":");
			
				xgInfo.key = strTmp[0];
				xgInfo.value = strTmp[1];
			
				listXgInfo.add(xgInfo);
			}
		}catch(Exception err){
			
		}
		
		/*
		ObjectMapper om = new ObjectMapper();
		XGenieInfoVO xgInfo = null;
		
		if(StringUtils.isNotBlank(xg.info)){
			try {
				xgInfo = om.readValue(xg.info, XGenieInfoVO.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		*/
		
		XGenieVO xgVO = new XGenieVO(xg, listXgInfo);
		
		return ok(admin_xg_detail.render(xgVO));
	}
	
	public Result showSGInfo(String smartGenieId, String page){
		SmartGenie sg = SmartGenie.find(smartGenieId);
		
		if(sg == null){
			setErrorMessage("Device Not Found!!");
			return redirect(routes.AdminUserController.manageUsers("0"));
		}
		
		SmartGenieLog sgLog = SmartGenieLog.find(smartGenieId);
		
		DynamicForm df = formFactory.form().bindFromRequest();
		String order = df.get("order");
		String direction = df.get("direction");
				
		String orderByProp = orderByPropMap.get(order);

		if(orderByProp == null){
			orderByProp = "createAt";
		}

        PagedList<SmartGenieActionHistory> historyPage = SmartGenieActionHistory.sgHistoryPage(smartGenieId, orderByProp, ("asc".equalsIgnoreCase(direction)), new Integer(page), 20);
		
		PageVO<SmartGenieHistoryVO> historyPageVO = new PageVO<SmartGenieHistoryVO>(historyPage);

		for(SmartGenieActionHistory sf : historyPage.getList()){
			historyPageVO.currents.add(new SmartGenieHistoryVO(sf));
		}

		if(direction == null){
			direction = "desc";
		}

		if(order == null){
			order = "create";
		}
		
		ObjectMapper om = new ObjectMapper();
		SmartGenieInfoVO sgInfo = null;
		if(StringUtils.isNotBlank(sg.info)){
			try {
				sgInfo = om.readValue(sg.info, SmartGenieInfoVO.class);
			} catch (JsonParseException e) {
				e.printStackTrace();
			} catch (JsonMappingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		SmartGenieVO sgVO = new SmartGenieVO(sg, sgLog, sgInfo);
		
		return ok(admin_user_device_detail.render(sgVO, historyPageVO, order, direction));
	}
}
