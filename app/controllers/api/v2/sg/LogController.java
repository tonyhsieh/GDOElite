package controllers.api.v2.sg;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import controllers.api.v1.sg.cache.SGSessionUtils;
import frameworks.models.StatusCode;
import json.models.api.v2.sg.BaseRespPack;
import models.SmartGenie;
import play.mvc.BodyParser;
import play.mvc.Result;
import utils.LogUtil;
import utils.MyUtil;

public class LogController extends BaseAPIController
{
    @BodyParser.Of(BodyParser.Json.class)
    public Result log()
    {
        try
        {
            JsonNode jsonNode = request().body().asJson();
            if (jsonNode == null || !jsonNode.has("type") || !jsonNode.has("logLevel"))
            {
                return createIndicatedJsonStatusResp(StatusCode.FORMAT_ERROR, "requires 'type' and 'logLevel'");
            }

            if (!jsonNode.has("sessionId"))
            {
                return createIndicatedJsonStatusResp(StatusCode.SESSION_ERROR, "no sessionId found");
            }

            String sessionId = jsonNode.get("sessionId").textValue();
            SmartGenie smartGenie = SGSessionUtils.getSmartGenie(sessionId);

            if (smartGenie == null)
            {
                return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "no home extender found");
            }

            ((ObjectNode) jsonNode).put("heMac", smartGenie.macAddress);
            ((ObjectNode) jsonNode).put("heName", smartGenie.name);

            ((ObjectNode) jsonNode).put("extIp", MyUtil.getClientIp(request()));

            LogUtil.log(jsonNode.toString());

            BaseRespPack respPack = new BaseRespPack();
            return createJsonResp(respPack);
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, e.getMessage() + "");
        }
    }
}
