package controllers.api.v2.sg;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.api.v1.app.form.XGenieStatusReqPack;
import controllers.api.v1.app.form.XGenieStatusReqPack.XGenieVO;
import controllers.api.v1.sg.DeviceEventLoggerController;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.cache.SGSessionUtils;
import controllers.api.v1.sg.form.*;
import controllers.api.v2.sg.form.AddDeviceReqPack;
import controllers.api.v2.sg.form.InitialSmartGenieReqPack;
import controllers.api.v2.sg.form.ListRemoveXGenieReqPack;
import controllers.api.v2.sg.form.RemoveXGenieReqPack;
import frameworks.Constants;
import frameworks.models.*;

import json.models.api.v1.sg.vo.UserInfoVO;
import json.models.api.v2.sg.*;
import json.models.api.v2.sg.vo.XGenieBattInfoVO;
import json.models.api.v2.sg.vo.XGenieInfoAllVO;
import json.models.api.v2.sg.vo.XGenieInfoBaseVO;
import json.models.api.v2.sg.vo.XGenieInfoVO;
import models.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.apache.commons.lang3.time.DateFormatUtils;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import services.SmartGenieService;
import services.UserRelationshipService;
import utils.CacheManager;
import utils.ExceptionUtil;
import utils.LoggingUtils;
import utils.generator.CredentialGenerator;
import utils.GeoLocationUtil;

import javax.inject.Inject;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;


//@PerformanceTracker
public class SmartGenieController extends BaseAPIController {
    /*
	隢���https://asante.atlassian.net/wiki/display/AST/API+Behavior+SmartGenie+Initial+API+Flow+Chart
	by Jack Chuang
	*/
    @Inject
    play.Configuration configuration;

    @Transactional
    @ParameterInject(InitialSmartGenieReqPack.class)
    public Result initial() {

        InitialSmartGenieReqPack pack = createInputPack();

        String sessionId = UUID.randomUUID().toString();
        SmartGenie smartGenie = null;
        SmartGenieLog smartGenieLog;

        if (pack.homeExtenderId != null) {
            try {
                smartGenie = Ebean.find(SmartGenie.class, pack.homeExtenderId);
            } catch (IllegalArgumentException err) {
                return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "initial find smartgenie failed " + ExceptionUtil.exceptionStacktraceToString(err));
                return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "unknow error");
            }
        } else if (pack.homeExtenderMac != null) {
            smartGenie = SmartGenie.findByMacAddress(pack.homeExtenderMac);
            if (smartGenie == null) {
                FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(pack.firmwareGroupName);
                if (firmwareGroup == null) {
                    return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND_FWGROUPNAME, "smartgenie not found firmware group name");
                }
                smartGenie = new SmartGenie();
                smartGenie.id = CredentialGenerator.genSmartGenieId(pack.homeExtenderMac);
                smartGenie.macAddress = pack.homeExtenderMac;
                smartGenie.isPairing = false;
                smartGenie.firmwareGroup = firmwareGroup;
                smartGenie.autoUpgradeTime = configuration.getString("autoUpgradeTime");
                smartGenie.autoUpgradeOnceTime = configuration.getString("autoUpgradeOnceTime");
                smartGenie.autoUpgrade = configuration.getBoolean("autoUpgrade");
                smartGenie.autoUpgradeOnce = configuration.getBoolean("autoUpgradeOnce");

                try {
                    smartGenie.devType = Integer.parseInt(pack.devType);
                } catch (Exception err) {
                    smartGenie.devType = 0;
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "initial set devType failed " + ExceptionUtil.exceptionStacktraceToString(err));
                }
            } else {

                FirmwareGroup firmwareGroup = FirmwareGroup.findByFwGrpName(pack.firmwareGroupName);

                if (firmwareGroup == null) {
                    return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND_FWGROUPNAME, "smartgenie not found firmware group name");
                }

                smartGenie.firmwareGroup = firmwareGroup;

                try {
                    smartGenie.devType = Integer.parseInt(pack.devType);
                } catch (Exception err) {
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "initial set devType failed " + ExceptionUtil.exceptionStacktraceToString(err));
                    smartGenie.devType = 0;
                }
            }
        }
        if (smartGenie.autoUpgrade == null && smartGenie.autoUpgradeTime == null && smartGenie.autoUpgradeOnceTime == null && smartGenie.autoUpgradeOnce == null) {
            smartGenie.autoUpgradeTime = configuration.getString("autoUpgradeTime");
            smartGenie.autoUpgradeOnceTime = configuration.getString("autoUpgradeOnceTime");
            smartGenie.autoUpgrade = configuration.getBoolean("autoUpgrade");
            smartGenie.autoUpgradeOnce = configuration.getBoolean("autoUpgradeOnce");
        }

        smartGenieLog = SmartGenieLog.find(smartGenie.id);
        if (smartGenieLog == null) {
            smartGenieLog = new SmartGenieLog();
            smartGenieLog.id = smartGenie.id;
        }

        //Get External IP Address
        Http.RequestHeader httpservletrequest = request();
        if (httpservletrequest == null)
            return null;
        String s = httpservletrequest.getHeader("X-Forwarded-For");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("WL-Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_CLIENT_IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.remoteAddress();
        //s = httpservletrequest.getRemoteAddr();
        if ("127.0.0.1".equals(s) || "0:0:0:0:0:0:0:1".equals(s)) {
            try {
                s = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException unknownhostexception) {
                unknownhostexception.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Initial get smartgenie ip failed " + ExceptionUtil.exceptionStacktraceToString(unknownhostexception));
            }
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Initial Smart genie IP " + s);
        smartGenieLog.extIp = s;

        if (smartGenieLog.timezone == null) {
            try {
                smartGenieLog.timezone = GeoLocationUtil.getTimeZoneOffset(s);
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Initial get smartgenie timezone error" + ExceptionUtil.exceptionStacktraceToString(err));
            }
        }

        if (!SGSessionUtils.login(sessionId, smartGenie.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SESSION_ERROR, "create session failure");
        }

        //Get P2P Active Code
        //by Jack 2013 11 21
        if (smartGenie.activeCode == null) {
            P2pActiveCode p2pActiveCode = P2pActiveCode.find(smartGenie.macAddress);
            if (p2pActiveCode != null) {
                smartGenie.activeCode = p2pActiveCode.active_code;
            }
        }
        //end

        try {
            smartGenieLog.save();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Initial smartgenielog save error" + ExceptionUtil.exceptionStacktraceToString(err));
        }
        try {
            smartGenie.save();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Initial smartgenie save error" + ExceptionUtil.exceptionStacktraceToString(err));
        }


        SmartGenieRespPack respPack = new SmartGenieRespPack();
        respPack.sessionId = sessionId;
        respPack.homeExtenderId = smartGenie.id;
        respPack.timezone = smartGenieLog.timezone;
        respPack.autoUpgrade = smartGenie.autoUpgrade;
        respPack.autoUpgradeTime = smartGenie.autoUpgradeTime;
        respPack.autoUpgradeOnceTime = smartGenie.autoUpgradeOnceTime;
        respPack.autoUpgradeOnce = smartGenie.autoUpgradeOnce;
        respPack.sysTimestamp = Calendar.getInstance();
        respPack.p2p_activecode = smartGenie.activeCode;
        respPack.s3_buckets = configuration.getString("s3.buckets.url");

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(AddDeviceReqPack.class)
    public Result addDevice() {
        AddDeviceReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
        if (xg != null) {
            if (xg.status == 1) {
                if (!xg.smartGenie.equals(smartGenie)) {
                    return createIndicatedJsonStatusResp(StatusCode.XGENIE_DUPLICATED, "add xgenie failure");
                }
            }
        }
        if (!SmartGenieService.addNewDevicev2(pack.xGenieMac, smartGenie, pack.firmwareGroupName, pack.xGenieType, pack.name)) {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_DUPLICATED, "add xgenie failure");
        }

        if (pack.parentXGenieMac != null) {
            if (!pack.parentXGenieMac.equals("null")) {
                XGenie parentXg = XGenie.findByXgMac(pack.parentXGenieMac);
                if (parentXg == null) {
                    return createIndicatedJsonStatusResp(StatusCode.PARENT_XGENIE_NOTFOUND, "add xgenie failure no parent");
                }
            }
        }

        //Does the Xg bind another Xg
        if (pack.parentXGenieMac != null) {
            XGenieRelationship xgr = new XGenieRelationship();
            xgr.smartGenieId = smartGenie.id;
            xgr.parentMacAddr = pack.parentXGenieMac;
            xgr.childMacAddr = pack.xGenieMac;

            if (pack.parentParam != null) {
                if (!pack.parentParam.equals("null")) {
                    xgr.parentParam = pack.parentParam;
                }
            }
            try {
                xgr.save();
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "add xgenie relationship error " + ExceptionUtils.getStackTrace(err));
            }
        }

        DeviceLog el = new DeviceLog();
        el.sgId = smartGenie.id;
        el.xGId = pack.xGenieMac;
        el.eventType = EventLogType.ADD_DEVICE.value;
        el.eventData = Constants.EVENT_LOG_DATA_ADD_DEVICE;

        if (smartGenie.owner != null) {
            el.ownerUserId = smartGenie.owner.id.toString();
        }
        try {
            el.save();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "add device log error " + ExceptionUtils.getStackTrace(err));
        }

        List<? super XGenieInfoBaseVO> listXg = listxg(smartGenie);


        ListXgRespPack respPack = new ListXgRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        respPack.xGenieList = listXg;

        return createJsonResp(respPack);

    }

    @BodyParser.Of(BodyParser.AnyContent.class)
    @Transactional
    @SessionInject(RemoveXGenieReqPack.class)
    public Result removeDevice() {
        RemoveXGenieReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        ObjectMapper jsonMapper = new ObjectMapper();
        ListRemoveXGenieReqPack listRemoveXGenie = null;
        try {
            listRemoveXGenie = jsonMapper.readValue(pack.xGenieInfo, ListRemoveXGenieReqPack.class);
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "readvalue list remove xgenie " + ExceptionUtils.getStackTrace(e));
        }
        for (int i = 0; i < listRemoveXGenie.xGenieList.size(); i++) {
            try {
                //Find the xGenie
                XGenie xGenie = null;

                try {
                    xGenie = XGenie.findByXgMac(listRemoveXGenie.xGenieList.get(i).macAddr);

                    if (xGenie.smartGenie.id.equals(smartGenie.id)) {
                        DeviceLog el = new DeviceLog();
                        el.sgId = smartGenie.id;
                        el.xGId = xGenie.id;

                        el.eventType = EventLogType.REMOVE_DEVICE.value;
                        el.eventData = Constants.EVENT_LOG_DATA_REMOVE_DEVICE;
                        try {
                            el.save();
                        } catch (Exception err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "remove device device log save error " + ExceptionUtils.getStackTrace(err));
                        }
                        xGenie.smartGenie = null;
                        xGenie.name = null;
                        xGenie.status = StatusCode.XGENIE_REMOVED.value;
                        try {
                            xGenie.save();
                        } catch (Exception err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "remove device device log save error " + ExceptionUtils.getStackTrace(err));
                        }
                        //Remove Contact Data
                        List<EventContact> listEc = null;
                        listEc = EventContact.listByMac(xGenie.macAddr);
                        List<Contact> listContact = null;
                        for (EventContact ec : listEc) {
                            listContact = Contact.listByEventContact(ec.id);

                            //remove contact data
                            for (Contact contact : listContact) {
                                contact.delete();
                            }
                        }

                        //Remove XGenieRelationship
                        List<XGenieRelationship> listXGenieRelationship = XGenieRelationship.listXgByParentXgId(xGenie.macAddr);
                        for (int j = 0; j < listXGenieRelationship.size(); j++) {
                            listXGenieRelationship.get(j).delete();
                        }

                        listXGenieRelationship = XGenieRelationship.listXgByChileXgId(xGenie.macAddr);
                        for (int j = 0; j < listXGenieRelationship.size(); j++) {
                            listXGenieRelationship.get(j).delete();
                        }

                        //Remove XGenieConfiguration
                        List<Configuration> listConfig = Configuration.listConfigurationByMacAddress(xGenie.macAddr);
                        for (int j = 0; j < listConfig.size(); j++) {
                            listConfig.get(j).delete();
                        }
                    } else {
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "remove device failure " + "xg mac" + listRemoveXGenie.xGenieList.get(i).macAddr + " status " + SgRemoveXgType.FAILURE.value);
                    }
                } catch (Exception err) {
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "remove device device error " + ExceptionUtils.getStackTrace(err));
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "xGenie = null : " + listRemoveXGenie.xGenieList.get(i).macAddr);
                }
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "remove device begin error " + ExceptionUtils.getStackTrace(e));
            }
        }

        List<? super XGenieInfoBaseVO> listXg = listxg(smartGenie);

        ListRemoveDeviceRespPack listRemoveDeviceRespPack = new ListRemoveDeviceRespPack();
        listRemoveDeviceRespPack.xGenieList = listXg;
        listRemoveDeviceRespPack.sysTimestamp = Calendar.getInstance();

        return createJsonResp(listRemoveDeviceRespPack);
    }


    @SessionInject(BaseReqPack.class)
    public CompletionStage<Result> keepALive() {

        final String sgId = getSmartGenieId();


        Http.RequestHeader httpservletrequest = request();

        if (httpservletrequest == null)
            return null;
        String s = httpservletrequest.getHeader("X-Forwarded-For");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("WL-Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_CLIENT_IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.remoteAddress();
        //s = httpservletrequest.getRemoteAddr();
        if ("127.0.0.1".equals(s) || "0:0:0:0:0:0:0:1".equals(s))
            try {
                s = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException unknownhostexception) {
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "KeepALive UnknownHostException : " + unknownhostexception.toString());
            }

        //Logger.info("keep a live Public IP : " + s);

        try {
            String memPubIp = CacheManager.get(CacheKey.AG_PUBLIC_IP, sgId);

            if (memPubIp != null) {
                if (!memPubIp.equals(s)) {

                    CacheManager.removeWithId(CacheKey.AG_PUBLIC_IP, sgId);

                    SmartGenieLog sgLog = SmartGenieLog.find(sgId);

                    if (sgLog != null) {
                        if (!sgLog.extIp.equals(s)) {
                            sgLog.extIp = s;
                            try {
                                sgLog.save();
                            } catch (Exception err) {
                                err.printStackTrace();
                                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie extIP save error " + ExceptionUtils.getStackTrace(err));
                            }
                        }
                    }
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Different Public IP : memPubIp->" + memPubIp + " | s->" + s);

                    CacheManager.setWithId(CacheKey.AG_PUBLIC_IP, sgId, s, CacheManager.AG_PUBLIC_IP_EXPIRED_TIME);

                } else {
                    //Do Nothing
                }
            } else {

                SmartGenieLog sgLog = SmartGenieLog.find(sgId);

                if (sgLog != null) {
                    if (!sgLog.extIp.equals(s)) {
                        sgLog.extIp = s;
                        try {
                            sgLog.save();
                        } catch (Exception err) {
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie no cache extIP save error " + ExceptionUtils.getStackTrace(err));
                        }
                    }
                }

                CacheManager.setWithId(CacheKey.AG_PUBLIC_IP, sgId, s, CacheManager.AG_PUBLIC_IP_EXPIRED_TIME);
            }
        } catch (Exception err) {
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "keepALive() cache public ip err : " + err.toString());
        }


        return CompletableFuture.supplyAsync(() -> {

                    long start = System.currentTimeMillis();
                    //update SmartGenieLog time
                    int updateCnt = 0;
                    try {
                        updateCnt = SmartGenieLog.updateTimestamp(sgId, DateFormatUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
                    } catch (Exception err) {
                        err.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie keep alive updaate error " + ExceptionUtils.getStackTrace(err));
                    }
                    if (updateCnt == 0) {
                        SmartGenieLog sgLog;
                        sgLog = SmartGenieLog.find(sgId);
                        if (sgLog == null) {
                            sgLog = new SmartGenieLog();
                            sgLog.id = sgId;
                            try {
                                sgLog.save();
                            } catch (Exception err) {
                                err.printStackTrace();
                                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie keep alive save record time error " + ExceptionUtils.getStackTrace(err));
                            }
                        } else {
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie keep alive update 0 row but has smart genie log already ");
                        }
                    }

                    long duration = System.currentTimeMillis() - start;

                    return NOP();
                }

        );
    }

    @Transactional
    @SessionInject(KeepAliveReqPack.class)
    public Result listUserInfoBySgSessionId() {
        KeepAliveReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        List<UserRelationship> urList = UserRelationshipService.listUserRelationshipBySG(smartGenie.id);
        List<UserInfoVO> sgUserVOList = new ArrayList<UserInfoVO>();
        User user = smartGenie.owner;
        if (user == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smart genie ownership empty");
        }
        UserInfoVO userInfoVO = new UserInfoVO();
        userInfoVO.email = user.email;

        sgUserVOList.add(userInfoVO);

        for (UserRelationship item : urList) {
            userInfoVO = new UserInfoVO();

            try {
                User sgUser = Ebean.find(User.class, item.userId);
                userInfoVO.email = sgUser.email;
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "list user info failed " + ExceptionUtil.exceptionStacktraceToString(err));
            }

            sgUserVOList.add(userInfoVO);
        }
        Collections.sort(sgUserVOList, new CompareTool());
        ListUserInfoBySgSessionIdRespPack respPack = new ListUserInfoBySgSessionIdRespPack();
        respPack.userList = sgUserVOList;
        respPack.sysTimestamp = Calendar.getInstance();
        return createJsonResp(respPack);
    }

     class CompareTool implements Comparator<UserInfoVO> {
         @Override
         public int compare(UserInfoVO s1, UserInfoVO s2) {
             return s1.email.compareTo(s2.email);
         }
     }


    @Transactional
    @SessionInject(UpdateNetworkInfoReqPack.class)
    public Result updateNetworkInfo() {
        UpdateNetworkInfoReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        SmartGenieLog smartGenieLog = SmartGenieLog.find(smartGenie.id);
        if (smartGenieLog == null) {
            smartGenieLog = new SmartGenieLog();
            smartGenieLog.id = smartGenie.id;
        }
        if (pack.innIp != null) {
            if (!"null".equals(pack.innIp)) {
                smartGenieLog.innIp = pack.innIp;
            } else {
                smartGenieLog.innIp = null;
            }
        }
        if (pack.netMask != null) {
            if (!"null".equals(pack.netMask)) {
                smartGenieLog.netMask = pack.netMask;
            } else {
                smartGenieLog.netMask = null;
            }
        }
        if (pack.wifiMacAddress != null) {
            if (!"null".equals(pack.wifiMacAddress)) {
                smartGenieLog.wifiMacAddress = pack.wifiMacAddress;
            } else {
                smartGenieLog.wifiMacAddress = null;
            }
        }
        if (pack.wifiIP != null) {
            if (!"null".equals(pack.wifiIP)) {
                smartGenieLog.wifiIP = pack.wifiIP;
            } else {
                smartGenieLog.wifiIP = null;
            }
        }
        if (pack.ssidName != null) {
            if (!"null".equals(pack.ssidName)) {
                smartGenieLog.ssidName = pack.ssidName;
            } else {
                smartGenieLog.ssidName = null;
            }
        }
        if (pack.firmwareVer != null) {
            if (!"null".equals(pack.firmwareVer)) {
                smartGenieLog.firmwareVer = pack.firmwareVer;
            } else {
                smartGenieLog.firmwareVer = null;
            }
        }

        //Get External IP Address
        Http.RequestHeader httpservletrequest = request();
        if (httpservletrequest == null)
            return null;
        String s = httpservletrequest.getHeader("X-Forwarded-For");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("WL-Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_CLIENT_IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.remoteAddress();
        //s = httpservletrequest.getRemoteAddr();
        if ("127.0.0.1".equals(s) || "0:0:0:0:0:0:0:1".equals(s)) {
            try {
                s = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException unknownhostexception) {
                unknownhostexception.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo get smartgenie ip failed " + ExceptionUtil.exceptionStacktraceToString(unknownhostexception));
            }
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "updateNetworkInfo IP " + s);
        smartGenieLog.extIp = s;
        if (pack.timezone != null) {
            if (!"null".equals(pack.timezone)) {
                try {
                    smartGenieLog.timezone = Integer.parseInt(pack.timezone);
                } catch (Exception err) {
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo timezone error " + ExceptionUtil.exceptionStacktraceToString(err));
                    try {
                        smartGenieLog.timezone = GeoLocationUtil.getTimeZoneOffset(s);
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "updateNetwork timezone with request " + smartGenieLog.timezone);
                    } catch (Exception exp) {
                        exp.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo get smartgenie timezone failed" + ExceptionUtil.exceptionStacktraceToString(exp));
                    }
                }
            }
        } else {
            try {
                smartGenieLog.timezone = GeoLocationUtil.getTimeZoneOffset(s);
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "updateNetwork timezone " + smartGenieLog.timezone);
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo get smartgenie timezone failed with no request" + ExceptionUtil.exceptionStacktraceToString(err));
            }
        }
        if (pack.autoUpgrade != null) {
            if (!"null".equals(pack.autoUpgrade)) {
                smartGenie.autoUpgrade = pack.autoUpgrade;
            } else {
                smartGenie.autoUpgrade = configuration.getBoolean("autoUpgrade");
            }
        }
        if (pack.autoUpgradeTime != null) {
            if (!"null".equals(pack.autoUpgradeTime)) {
                smartGenie.autoUpgradeTime = pack.autoUpgradeTime;
            } else {
                smartGenie.autoUpgradeTime = configuration.getString("autoUpgradeTime");
            }
        }
        if (pack.autoUpgradeOnceTime != null) {
            if (!"null".equals(pack.autoUpgradeOnceTime)) {
                smartGenie.autoUpgradeOnceTime = pack.autoUpgradeOnceTime;
            } else {
                smartGenie.autoUpgradeOnceTime = configuration.getString("autoUpgradeOnceTime");
            }
        }
        if (pack.autoUpgradeOnce != null) {
            if (!"null".equals(pack.autoUpgradeOnce)) {
                smartGenie.autoUpgradeOnce = pack.autoUpgradeOnce;
            } else {
                smartGenie.autoUpgradeOnce = configuration.getBoolean("autoUpgradeOnce");
            }
        }

        try {
            smartGenieLog.save();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo smart_genie_log save failed " + ExceptionUtil.exceptionStacktraceToString(err));
        }

        try {
            smartGenie.save();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo smart_genie save failed " + ExceptionUtil.exceptionStacktraceToString(err));
        }

        if (pack.xGenieInfo != null) {
            String msg = pack.xGenieInfo;
            xGenieInfo info = null;
            try {
                ObjectMapper om = new ObjectMapper();
                info = om.readValue(msg, xGenieInfo.class);
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "xgenie info error" + ExceptionUtil.exceptionStacktraceToString(err));
            }
            if (info == null) {
                return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "xgenieInfo not found");
            }
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "xgenie info" + info);
            String xgMac = info.macAddr;
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "xgMac : " + xgMac);
            XGenie xGenie = XGenie.findByXgMac(xgMac);
            if (xGenie == null) {
                return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
            }
            if (xGenie.smartGenie == null) {
                return createIndicatedJsonStatusResp(StatusCode.PARENT_XGENIE_NOTFOUND, "xgenie does not have smart genie ");
            }
            try {
                if (!smartGenie.id.equals(xGenie.smartGenie.id)) {
                    return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smartgenie and xgenie did not match");
                }
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo check smartgenie and xgenie failed " + ExceptionUtil.exceptionStacktraceToString(err));
            }
            if (info.ipaddr != null) {
                xGenie.ipaddr = info.ipaddr;
            }
            if (info.deviceType != null) {
                xGenie.deviceType = Integer.parseInt(info.deviceType);
            }
            if (info.firmwareGroupName != null) {
                FirmwareGroup firmware = FirmwareGroup.findByFwGrpName(info.firmwareGroupName);
                if (firmware != null) {
                    xGenie.firmwareGroup = firmware;
                } else {
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "updateNetworkInfo no such firmware ");
                }
            }
            if (info.fwVer != null) {
                xGenie.fwVer = info.fwVer;
            }
            if (info.alive != null) {
                xGenie.alive = info.alive;
            }
            if (info.name != null) {
                xGenie.name = info.name;
            }
            if (info.light != null) {
                xGenie.light = info.light;
            }
            if (info.fps != null) {
                xGenie.fps = info.fps;
            }
            if (info.doorstatus != null) {
                xGenie.doorstatus = info.doorstatus;
            }
            if (info.lowbatt != null) {
                xGenie.lowBatt = info.lowbatt;
            }
            if (info.battChangeDate != null) {
                xGenie.battChangeDate = info.battChangeDate;
            }
            try {
                xGenie.save();
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtils.getStackTrace(err));
                smartGenie = null;
                smartGenieLog = null;
                xGenie = null;
                return createIndicatedJsonStatusResp(StatusCode.CAN_NOT_WRITE_TO_DATABASE, "xgenie can not store info");
            }
        }

        UpdateNetworkInfoRespPack respPack = new UpdateNetworkInfoRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        return createJsonResp(respPack);
    }

    @BodyParser.Of(BodyParser.AnyContent.class)
    @Transactional
    @SessionInject(ResetSgReqPack.class)
    public Result resetSg() {

        ResetSgReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        //Remove UserRelationship data
        //by Jack
        List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipBySG(smartGenie.id);
        for (int i = 0; i < listUserRelationship.size(); i++) {
            UserRelationship userRelationship = listUserRelationship.get(i);
            userRelationship.delete();
        }

        //Remove XGenie and Asante Genie relationship
        List<XGenie> listXGenie = XGenie.listXGBySG(smartGenie);
        for (int i = 0; i < listXGenie.size(); i++) {
            //閮���device_log
            DeviceLog el = new DeviceLog();
            el.sgId = smartGenie.id;
            el.xGId = listXGenie.get(i).id;

            el.eventType = EventLogType.REMOVE_DEVICE.value;
            el.eventData = Constants.EVENT_LOG_DATA_REMOVE_DEVICE;
            el.save();

            listXGenie.get(i).smartGenie = null;
            listXGenie.get(i).name = null;
            listXGenie.get(i).status = StatusCode.XGENIE_REMOVED.value;
            listXGenie.get(i).save();

            //remove event_contact
            //by Jack Chuang 20140218
            List<EventContact> listEc = EventContact.listByMac(listXGenie.get(i).macAddr);

            for (EventContact ec : listEc) {
                List<Contact> listContact = Contact.listByEventContact(ec.id);

                for (Contact contact : listContact) {
                    contact.delete();
                }
            }
            //end

            //瑼Ｘ XGenieRelationship
            List<XGenieRelationship> listXGenieRelationship = XGenieRelationship.listXgByParentXgId(listXGenie.get(i).macAddr);
            for (int j = 0; j < listXGenieRelationship.size(); j++) {
                listXGenieRelationship.get(j).delete();
            }

            listXGenieRelationship = XGenieRelationship.listXgByChileXgId(listXGenie.get(i).macAddr);
            for (int j = 0; j < listXGenieRelationship.size(); j++) {
                listXGenieRelationship.get(j).delete();
            }

            //Remove Passive device configuration    2014 06 12 by Jack
            List<Configuration> listConfig = Configuration.listConfigurationByMacAddress(listXGenie.get(i).macAddr);
            {
                for (int j = 0; j < listConfig.size(); j++) {
                    listConfig.get(j).delete();
                }
            }
        }

        setLog(smartGenie.macAddress, smartGenie.owner.id.toString(), PairingType.UNPAIRING);

        SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);

        if (sgLog != null) {
            sgLog.extIp = null;
            sgLog.innIp = null;
            sgLog.netMask = null;
            sgLog.wifiMacAddress = null;
            sgLog.wifiIP = null;
            sgLog.ssidName = null;
            sgLog.firmwareVer = null;
            sgLog.lat = null;
            sgLog.lng = null;
            sgLog.p2pSuccess = 0;
            sgLog.p2pFail = 0;

            sgLog.save();
        }

        smartGenie.owner = null;
        smartGenie.name = null;
        smartGenie.isPairing = false;
        smartGenie.autoUpgrade = null;
        smartGenie.autoUpgradeOnce = null;
        smartGenie.autoUpgradeOnceTime = null;
        smartGenie.autoUpgradeTime = null;
        smartGenie.save();

        ResetSgRespPack respPack = new ResetSgRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        return createJsonResp(respPack);
    }

    private static void setLog(String macAddr, String userId, PairingType type) {
        PairingHistoryLog log = new PairingHistoryLog();

        log.macAddr = macAddr;
        log.userId = userId;
        log.type = type;

        log.save();
    }

    @Transactional
    @SessionInject(XGenieStatusReqPack.class)
    public Result updateXGStatus() {
        SmartGenie sg = getSmartGenie();

        XGenieStatusReqPack pack = createInputPack();

        for (XGenieVO vo : pack.xgenieInfo) {
            XGenie xg = XGenie.findByXgMac(vo.macAddr);

            if (xg != null && xg.smartGenie.id.equals(sg.id)) {
                xg.status = vo.status;
                xg.save();
            }
        }

        UpdateNetworkInfoRespPack respPack = new UpdateNetworkInfoRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(KeepAliveReqPack.class)
    public Result getSgLocation() {
        KeepAliveReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        SmartGenieLog sgLog = null;
        if (smartGenie != null) {
            sgLog = SmartGenieLog.findLocation(smartGenie.id);
        } else {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found for getSgLocation");
        }
        GetSgLocation respPack = new GetSgLocation();
        respPack.sysTimestamp = Calendar.getInstance();
        respPack.lat = sgLog.lat;
        respPack.lng = sgLog.lng;

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(SetSgStatusReqPack.class)
    public Result setSgStatus() {

        SetSgStatusReqPack pack = createInputPack();

        SmartGenie smartGenie = getSmartGenie();

        SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);

        if ("NotAlive".equals(pack.status)) {
            sgLog.logDateTime = null;
            sgLog.recordDateTime = null;
            sgLog.save();

            if (pack.isSendNotification != null) {
                if ("1".equals(pack.isSendNotification)) {
                    Calendar rightNow = Calendar.getInstance();
                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss.SSS");
                    //from john to jack
                    DeviceEventLoggerController.sendMsg(null, sgLog.id, EventLogType.ASANTE_GENIE_NOT_WORK.value, rightNow, sdf.format(rightNow.getTime()));
                }
            }

        } else {
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_STATUS, "unknown status");
        }

        SetSgStatusRespPack respPack = new SetSgStatusRespPack();
        respPack.sysTimestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }

    public static List<? super XGenieInfoBaseVO> listxg(SmartGenie smartGenie) {
        List<XGenieInfoBaseVO> listXg = new ArrayList<>();

        List<XGenie> listXGenie = XGenie.listXGBySG(smartGenie);

        for (int i = 0; i < listXGenie.size(); i++) {
            List<XGenieRelationship> listXGenieRelationship = XGenieRelationship.listXgByChileXgId(listXGenie.get(i).macAddr);
            if ((listXGenie.get(i).deviceType == 3) && listXGenieRelationship.size() != 0) {
                XGenieInfoAllVO xginfo = new XGenieInfoAllVO();
                xginfo.battChangeDate = listXGenie.get(i).battChangeDate;
                xginfo.macAddr = listXGenie.get(i).macAddr;
                xginfo.deviceType = listXGenie.get(i).deviceType;
                xginfo.name = listXGenie.get(i).name;
                xginfo.parentMacAddr = listXGenieRelationship.get(0).parentMacAddr;
                xginfo.parentParam = listXGenieRelationship.get(0).parentParam;
                listXg.add(xginfo);
            } else if ((listXGenie.get(i).deviceType == 3) && listXGenieRelationship.size() == 0) {
                XGenieBattInfoVO xginfo = new XGenieBattInfoVO();
                xginfo.macAddr = listXGenie.get(i).macAddr;
                xginfo.deviceType = listXGenie.get(i).deviceType;
                xginfo.name = listXGenie.get(i).name;
                xginfo.battChangeDate = listXGenie.get(i).battChangeDate;
                listXg.add(xginfo);
            } else if ((listXGenie.get(i).deviceType != 3) && listXGenieRelationship.size() != 0) {
                XGenieInfoVO xginfo = new XGenieInfoVO();
                xginfo.macAddr = listXGenie.get(i).macAddr;
                xginfo.deviceType = listXGenie.get(i).deviceType;
                xginfo.name = listXGenie.get(i).name;
                xginfo.parentMacAddr = listXGenieRelationship.get(0).parentMacAddr;
                xginfo.parentParam = listXGenieRelationship.get(0).parentParam;
                listXg.add(xginfo);
            } else {
                XGenieInfoBaseVO xginfo = new XGenieInfoBaseVO();
                xginfo.macAddr = listXGenie.get(i).macAddr;
                xginfo.deviceType = listXGenie.get(i).deviceType;
                xginfo.name = listXGenie.get(i).name;
                listXg.add(xginfo);
            }
        }
        Collections.sort(listXg, new CompareList());

        return listXg;
    }

    static public class CompareList implements Comparator<XGenieInfoBaseVO> {
        @Override
        public int compare(XGenieInfoBaseVO s1, XGenieInfoBaseVO s2) {
            return s1.name.compareTo(s2.name);
        }
    }


}


