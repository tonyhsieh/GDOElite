/**
 *
 */
package controllers.api.v2.sg;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.BaseController;
import controllers.api.v1.sg.form.BaseReqPack;
import frameworks.Constants;
import frameworks.models.StatusCode;
import json.models.api.v2.sg.BaseRespPack;
import json.models.api.v1.sg.JsonStatusCode;
import json.models.api.v2.sg.RespPack;
import models.SmartGenie;
import play.data.Form;
import play.mvc.Http;
import play.mvc.Result;

import java.io.IOException;
import java.util.Calendar;

/**
 * @author carloxwang
 *
 */
public abstract class BaseAPIController extends BaseController {


	@SuppressWarnings("unchecked")
	protected static <T> T createInputPack(){
		return (T) Http.Context.current().args.get(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY);
	}

	protected static BaseReqPack readReqParams(Form<?> formData){
		try{
			return (BaseReqPack) formData.bindFromRequest().get();
		} catch (IllegalStateException e){
			return null;
		}

	}
	protected static Result createJsonResp(BaseRespPack respPack){

		respPack.sysTimestamp = Calendar.getInstance();

		ObjectMapper om = new ObjectMapper();
		String responseString = "";

		try {
			responseString = om.writeValueAsString(respPack);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		return ok(responseString).as("application/json");
	}

	protected static Result createExceptionJsonResp(Exception e){

		RespPack respPack = new RespPack();

		respPack.status = new JsonStatusCode();

		respPack.status.code = StatusCode.UNKNOWN_ERROR.value;
		respPack.status.message = e.getMessage();

		return createJsonResp(respPack);

	}

	protected static Result createIndicatedJsonStatusResp(StatusCode status, String message){

		RespPack respPack = new RespPack();

		respPack.status = new JsonStatusCode();
		respPack.status.code = status.value;
		respPack.status.message = message;

		return createJsonResp(respPack);
	}

	protected static Result NOP(){
		return ok("");
	}

	protected static String getSmartGenieId(){
		return (String)Http.Context.current().args.get(Constants.API_V1_SG_SESSION_SG_ID_KEY);
	}
	protected static SmartGenie getSmartGenie(){
		String sgId = (String)Http.Context.current().args.get(Constants.API_V1_SG_SESSION_SG_ID_KEY);
		return Ebean.find(SmartGenie.class, sgId);

	}
}
