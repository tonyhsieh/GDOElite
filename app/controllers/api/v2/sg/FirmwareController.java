package controllers.api.v2.sg;

import com.avaje.ebean.Ebean;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.ChkAllFwVerReqPack;
import controllers.api.v2.sg.form.ChkFwVerReqPack;
import frameworks.models.FileType;
import frameworks.models.StatusCode;
import json.models.api.v2.sg.ChkAllFwVerRespPack;
import json.models.api.v2.sg.ChkFwVerRespPack;
import json.models.api.v2.sg.vo.SmartGenie4ChkFwVerVO;
import json.models.api.v1.sg.vo.SmartGenieVO;
import json.models.api.v2.sg.vo.XGenieVO;
import json.models.api.v2.sg.vo.XGenieFwVer;
import models.FileEntity;
import models.FirmwareGroup;
import models.SmartGenie;
import models.XGenie;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.Result;
import services.SmartGenieService;
import utils.FirmwareUtils;
import utils.LoggingUtils;
import utils.db.FileEntityManager;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author johnwu
 *
 */
@PerformanceTracker
public class FirmwareController extends BaseAPIController {

	@SessionInject(ChkFwVerReqPack.class)
	public Result checkVer(){
		ChkFwVerReqPack pack = createInputPack();

		//取得firmware group 和 file
		SmartGenie smartGenie = getSmartGenie();
		FirmwareGroup fw = Ebean.find(FirmwareGroup.class, smartGenie.firmwareGroup.id.toString());

		FirmwareGroup fwGroup = FirmwareGroup.findByFwGrpName(fw.name);
		FirmwareGroup forcefwGroup = FirmwareGroup.findByFwGrpNameForce(fw.name, 1);


		FileEntity firmwareSG = Ebean.find(FileEntity.class, fwGroup.fileEntity.id.toString());
		FileEntity forcefirmwareSG = FileEntity.findFileEntityIdByFwGrpNameForce(fwGroup.name, 1);
		SmartGenieVO smartGenieVO = new SmartGenieVO();

		//版本比對
		if(firmwareSG != null ){
			if(!FirmwareUtils.checkVer(fwGroup.firmwareVersion, pack.homeExtenderFwVer)){
				smartGenieVO.newFwVer = fwGroup.firmwareVersion;
				smartGenieVO.forceUpgrade = fwGroup.force_upgrade;
				try {
					smartGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareSG.s3Path, FileType.SG_FIRMWARE, null);
				} catch (Exception e) {
					e.printStackTrace();
					LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl error " + ExceptionUtils.getStackTrace(e));
				}
			}
		}
		if(forcefirmwareSG != null ){
			if(!FirmwareUtils.checkVer(forcefwGroup.firmwareVersion, pack.homeExtenderFwVer)){

				smartGenieVO.forceFwVer = forcefwGroup.firmwareVersion;

				try {
					smartGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareSG.s3Path , FileType.SG_FIRMWARE, null);
				} catch (Exception e) {
					e.printStackTrace();
					LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl force error " + ExceptionUtils.getStackTrace(e));
				}
			}
		}


        List<XGenieFwVer> xgenieInfo = null;
        try {
            xgenieInfo = new ObjectMapper().readValue(pack.xGenieInfo, new TypeReference<List<XGenieFwVer>>() {});
        } catch (Exception e) {
            e.printStackTrace();
        }

        List<XGenieVO> XGenieVOList = new ArrayList<XGenieVO>();
        for(XGenieFwVer xgFw: xgenieInfo){
            XGenieVO xGenieVO = new XGenieVO();
            if(StringUtils.isNotBlank(xgFw.xGenieMac)){
				XGenie xGenie = XGenie.findByXgMac(xgFw.xGenieMac);

                if(xGenie == null)
                    return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xGenie not found");

                //取得 firmware group 和 file

                FirmwareGroup fwXG = Ebean.find(FirmwareGroup.class, xGenie.firmwareGroup.id.toString());
                FirmwareGroup fwXGGroup = FirmwareGroup.findByFwGrpName(fwXG.name);
                FirmwareGroup forcefwXGGroup= FirmwareGroup.findByFwGrpNameForce(fwXGGroup.name, 1);
                FileEntity firmwareXG = Ebean.find(FileEntity.class, fwXGGroup.fileEntity.id.toString());
                FileEntity forcefirmwareXG = FileEntity.findFileEntityIdByFwGrpNameForce(fwXGGroup.name, 1);

                if(!SmartGenieService.hasXGenie(smartGenie.id, xGenie.id)){
                    return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie do not match");
                }


                //版本比對
                if(firmwareXG != null){
                    if(!FirmwareUtils.checkVer(fwXGGroup.firmwareVersion, xgFw.xGenieFwVer)){
                        xGenieVO.id = xGenie.id;
                        xGenieVO.newFwVer = fwXGGroup.firmwareVersion;
                        xGenieVO.macAddr = xGenie.macAddr;
                        xGenieVO.forceUpgrade = fwXGGroup.force_upgrade;
                        try {
                            xGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareXG.s3Path, FileType.XG_FIRMWARE, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl xgenie error " + ExceptionUtils.getStackTrace(e));
                        }
                    }else{
                        xGenieVO.macAddr = xgFw.xGenieMac;
                    }
                }
                if(forcefirmwareXG != null){
                    if(!FirmwareUtils.checkVer(forcefwXGGroup.firmwareVersion, xgFw.xGenieFwVer)){
                        xGenieVO.id = xGenie.id;
                        xGenieVO.forceFwVer = forcefwXGGroup.firmwareVersion;
                        xGenieVO.macAddr = xGenie.macAddr;
                        try {
                            xGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareXG.s3Path , FileType.XG_FIRMWARE, null);
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadforceUrl xgenie  error " + ExceptionUtils.getStackTrace(e));
                        }
                    }else{
                        xGenieVO.macAddr = xgFw.xGenieMac;
                    }
                }
            }
            XGenieVOList.add(xGenieVO);
        }


		ChkFwVerRespPack respPack = new ChkFwVerRespPack();

		respPack.sessionId = pack.sessionId;
		respPack.homeExtender = smartGenieVO;
	//	respPack.xGenie = XGenieVOList;

		return createJsonResp(respPack);
	}

	@SessionInject(ChkAllFwVerReqPack.class)
	public Result checkAllFwVer(){
		ChkAllFwVerReqPack pack = createInputPack();

		//取得firmware group 和 file
		SmartGenie smartGenie = getSmartGenie();
        if(smartGenie == null){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie does not exist");
        }
        if(smartGenie.firmwareGroup == null){
            return createIndicatedJsonStatusResp(StatusCode.NOT_FOUND, "firmwaregroup does not exist");
        }
        if(smartGenie.firmwareGroup.name == null){
            return createIndicatedJsonStatusResp(StatusCode.NOT_FOUND, "firmwaregroup name does not exist");
        }
		FirmwareGroup fwGroup = FirmwareGroup.findByFwGrpName(smartGenie.firmwareGroup.name);
		FirmwareGroup forcefwGroup = FirmwareGroup.findByFwGrpNameForce(smartGenie.firmwareGroup.name, 1);
        if(fwGroup == null){
            return createIndicatedJsonStatusResp(StatusCode.NOT_FOUND, "fwGroup does not exist");
        }
		FileEntity firmwareSG = Ebean.find(FileEntity.class, fwGroup.fileEntity.id.toString());
		FileEntity forcefirmwareSG = FileEntity.findFileEntityIdByFwGrpNameForce(fwGroup.name, 1);
        SmartGenie4ChkFwVerVO smartGenieVO = new SmartGenie4ChkFwVerVO();
        if(firmwareSG != null ){
                smartGenieVO.newFwVer = fwGroup.firmwareVersion;
                smartGenieVO.forceUpgrade = fwGroup.force_upgrade;
                try {
                    smartGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareSG.s3Path, FileType.SG_FIRMWARE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl error " + ExceptionUtils.getStackTrace(e));
                }

        }
        if(forcefirmwareSG != null ){
            smartGenieVO.forceFwVer = forcefwGroup.firmwareVersion;
            try {
                smartGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareSG.s3Path , FileType.SG_FIRMWARE, null);
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl force error " + ExceptionUtils.getStackTrace(e));
            }
        }


        List<XGenieVO> XGenieVOList = new ArrayList<XGenieVO>();
        XGenieVO xGenieVO;
		FirmwareGroup fwXGGroup;
		FirmwareGroup forcefwXGGroup;
		List<XGenie> listXg = XGenie.listXGBySG(smartGenie);

		for(XGenie xg: listXg){
			xGenieVO = new XGenieVO();
            fwXGGroup = FirmwareGroup.findByFwGrpName(xg.firmwareGroup.name);
            forcefwXGGroup = FirmwareGroup.findByFwGrpNameForce(xg.firmwareGroup.name, 1);
            FileEntity firmwareXG = Ebean.find(FileEntity.class, fwXGGroup.fileEntity.id.toString());
            FileEntity forcefirmwareXG = FileEntity.findFileEntityIdByFwGrpNameForce(fwXGGroup.name, 1);
            if(firmwareXG != null){
				xGenieVO.newFwVer = fwXGGroup.firmwareVersion;
				xGenieVO.forceUpgrade = fwXGGroup.force_upgrade;
                try {
                    xGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareXG.s3Path, FileType.XG_FIRMWARE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl xgenie error " + ExceptionUtils.getStackTrace(e));
                }
			}
			if(forcefirmwareXG != null){
				xGenieVO.forceFwVer = forcefwXGGroup.firmwareVersion;
                try {
                    xGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareXG.s3Path , FileType.XG_FIRMWARE, null);
                } catch (Exception e) {
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadforceUrl xgenie  error " + ExceptionUtils.getStackTrace(e));
                }
			}
			xGenieVO.macAddr = xg.macAddr;
            xGenieVO.id = xg.id;
            XGenieVOList.add(xGenieVO);
		}





		ChkAllFwVerRespPack respPack = new ChkAllFwVerRespPack();
		respPack.homeExtender = smartGenieVO;
		respPack.xGenie = XGenieVOList;

		return createJsonResp(respPack);
	}
}
