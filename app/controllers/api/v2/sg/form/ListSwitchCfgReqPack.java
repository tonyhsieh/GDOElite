package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;

import java.util.List;

public class ListSwitchCfgReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6808602219645716401L;

		public String cfgId;
		
		public boolean available;

}
