package controllers.api.v2.sg.form;

import controllers.api.v1.app.form.BaseReqPack;

import java.util.List;

public class ListRemoveXGenieReqPack extends BaseReqPack{


	private static final long serialVersionUID = 5621650813692098856L;

	public List<XGenieList> xGenieList;
	
	public static class XGenieList{
		public String macAddr;
	}
}
