package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class SetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6674196315565101142L;

	@Constraints.Required
	public String cfgId;

	@Constraints.Required
	public String xGenieMac;
	
	@Constraints.Required
	public String cfgClass;
	
	public String data;
	
	public String desc;
}
