package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class RemoveConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6938432366468284787L;
	
	@Constraints.Required
	public String cfgIdList;

    @Constraints.Required
    public String xGenieMac;

    @Constraints.Required
    public String cfgClass;


}
