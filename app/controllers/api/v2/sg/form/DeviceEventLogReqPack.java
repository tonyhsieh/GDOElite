/**
 *
 */
package controllers.api.v2.sg.form;


import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
public class DeviceEventLogReqPack extends BaseReqPack {


	/**
	 *
	 */
	private static final long serialVersionUID = -5346666470282894618L;

	@Constraints.Required
	public String ts;

	public String xGenieMac;

	@Constraints.Required
	public int eventType;

	public String eventData;

	public String attachment;

	public static class Attachment{

		public String contentType;

		public String rawData;
	}
}
