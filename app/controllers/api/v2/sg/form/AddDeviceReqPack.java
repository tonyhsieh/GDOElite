package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class AddDeviceReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -885301291478126778L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String  xGenieMac;

	@Constraints.Required
	public String xGenieType;
	
	@Constraints.Required
	public String firmwareGroupName;

    @Constraints.Required
    public String name;
	
	public String parentXGenieMac;
	
	public String parentParam;
	

}
