package controllers.api.v2.sg.form;


import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

/**
 * 
 * @author johnwu
 *
 */

public class ChkFwVerReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6259406344740581425L;


	@Constraints.Required
	public String xGenieInfo;

    @Constraints.Required
	public String homeExtenderFwVer;
}
