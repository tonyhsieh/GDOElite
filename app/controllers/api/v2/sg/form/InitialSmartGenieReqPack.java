package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class InitialSmartGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5451617983481997392L;
	@Constraints.Required
	public String homeExtenderMac;
	
	public String homeExtenderId;
	
    @Constraints.Required
	public String firmwareGroupName;
	@Constraints.Required
	public String devType;
	

}
