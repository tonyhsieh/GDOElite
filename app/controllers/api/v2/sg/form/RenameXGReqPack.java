package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class RenameXGReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3998278165935806886L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String xGenieMac;
	
	@Constraints.Required
	public String name;
}
