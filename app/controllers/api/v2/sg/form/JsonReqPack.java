package controllers.api.v2.sg.form;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * @author carloxwang
 */
public class JsonReqPack implements Serializable
{
    private static final long serialVersionUID = 2366406825126107126L;

    public String sessionId;

    public String macAddress;


    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }
}
