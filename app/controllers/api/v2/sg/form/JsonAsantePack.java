package controllers.api.v2.sg.form;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * @author carloxwang
 */
public class JsonAsantePack implements Serializable
{
    private static final long serialVersionUID = 2366406825126107124L;

    public String asante;

    @Override
    public String toString() {
        return "JsonAsantePack{" +
                "asante='" + asante + '\'' +
                '}';
    }
}
