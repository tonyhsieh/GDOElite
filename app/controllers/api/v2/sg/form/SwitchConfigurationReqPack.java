package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class SwitchConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2874960518241258353L;

	@Constraints.Required
	public String cfgIdList;

    @Constraints.Required
    public String xGenieMac;


}
