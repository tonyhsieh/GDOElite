package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

public class GetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7430973520139928557L;

	@Constraints.Required
	public String cfgIdList;

	@Constraints.Required
	public String xGenieMac;

	
	@Constraints.Required
	public String cfgClass;
}
