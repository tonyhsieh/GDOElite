package controllers.api.v2.sg.form;

import controllers.api.v1.sg.form.BaseReqPack;
import play.data.validation.Constraints;

import java.util.List;

public class GetMulityConfigurationsReqPack extends BaseReqPack{

    /**
     * 
     */
    private static final long serialVersionUID = -1887148912340338016L;
    
    @Constraints.Required
    public String cfgId;
    

}
