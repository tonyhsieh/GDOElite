/**
 *
 */
package controllers.api.v2.sg;

import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.routes;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.*;
import controllers.api.v2.sg.form.AddConfigurationReqPack;
import controllers.api.v2.sg.form.GetConfigurationReqPack;
import controllers.api.v2.sg.form.GetMulityConfigurationsReqPack;
import controllers.api.v2.sg.form.ListConfigurationReqPack;
import controllers.api.v2.sg.form.ListRemoveConfigurationReqPack;
import controllers.api.v2.sg.form.ListSwitchCfgReqPack;
import controllers.api.v2.sg.form.RemoveConfigurationReqPack;
import controllers.api.v2.sg.form.SetConfigurationReqPack;
import controllers.api.v2.sg.form.SwitchConfigurationReqPack;
import frameworks.models.DataFileType;
import frameworks.models.StatusCode;
import json.models.api.v2.sg.vo.ConfigurationInfoBaseVO;
import json.models.api.v2.sg.SGCfgRespPack;
import json.models.api.v2.sg.vo.ConfigurationInfoVO;
import json.models.api.v2.sg.ListConfigurationRespPack;
import json.models.api.v2.sg.XgConfigurationRespPack;
import json.models.api.v2.sg.GetMuiltyConfigurationsRespPack;
import json.models.api.v2.sg.vo.ICProgramVO;
import json.models.api.v2.sg.SwitchConfigurationRespPack;
import json.models.api.v2.sg.SetConfigurationRespPack;
import models.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.ConfigurationService;
import services.SGConfigurationService;
import utils.LoggingUtils;

import java.io.IOException;
import java.util.*;

/**
 * @author carloxwang
 *
 */
@PerformanceTracker
public class ConfigController extends BaseAPIController {

	@SessionInject(UploadConfigReqPack.class)
	public Result backupConfiguration() throws IOException{

		UploadConfigReqPack pack = createInputPack();

		SGConfigurationService.backupCfg(pack.cfg, getSmartGenie());


		return createIndicatedJsonStatusResp(StatusCode.GENERAL_SUCCESS, "SUCCESS");
	}

	@ParameterInject(RetrieveCfgReqPack.class)
	public Result retreiveConfiguration(){
		RetrieveCfgReqPack pack = createInputPack();

		SmartGenie sg = SmartGenie.findByMacAddress(pack.sgMac);

		if( sg == null ){
			LoggingUtils.log(LoggingUtils.LogLevel.WARN, "Mac Address could NOT be found!!");
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "Invalid Mac Address");
		}

		SmartGenieConfiguration sgc = SmartGenieConfiguration.findLatestCfg(sg);

		if( sgc == null ){
            LoggingUtils.log(LoggingUtils.LogLevel.WARN, "No backup configuration found");
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_CFG_NOTFOUND, "No configuration found");
		}

		SGCfgRespPack respPack = new SGCfgRespPack();
		System.out.println(sgc);

		respPack.downloadUrl = buildServerURL() + routes.FileDownloadController.download(sgc.cfgFile.id.toString(), "sgconf");

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(AddConfigurationReqPack.class)
	public Result addConfiguration(){
		AddConfigurationReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
		if(xg == null ){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
		}
		if(xg.smartGenie == null || !xg.smartGenie.id.equals(smartGenie.id)){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "xgenie not match");
        }
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		Configuration cfg = new Configuration();
		cfg.id = UUID.randomUUID().toString();
		try {
			ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
		} catch (Exception e) {
			e.printStackTrace();
			return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
		}
		cfg.cfg_type = pack.cfgClass.toUpperCase();
		cfg.descript = pack.desc;
		cfg.avilable = false;
		cfg.mac_addr = pack.xGenieMac;
		try{
            cfg.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add config save error" + ExceptionUtils.getStackTrace(err));
        }
		ConfigurationHistory cfgHist = new ConfigurationHistory();
		cfgHist.configuration_id = cfg.id;
		cfgHist.cfg_type = pack.cfgClass.toUpperCase();
		cfgHist.descript = pack.desc;
		cfgHist.avilable = false;
		cfgHist.mac_addr = pack.xGenieMac;
		cfgHist.sg_id = xg.smartGenie.id;
		try{
            cfgHist.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add config history save error" + ExceptionUtils.getStackTrace(err));
        }
        List<Configuration> listCfg = Configuration.listConfigurationByMacAddress(pack.xGenieMac);

        List<? super ConfigurationInfoBaseVO> listRespPackVO = null;
        try{
            listRespPackVO = listconfig(listCfg);
        }catch (Exception e){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
        }
        XgConfigurationRespPack respPack = new XgConfigurationRespPack();
		respPack.sysTimestamp = Calendar.getInstance();
        respPack.configlist = listRespPackVO;
		return createJsonResp(respPack);
	}
	

	
	@Transactional
    @SessionInject(GetConfigurationReqPack.class)
    public Result getConfig(){
        GetConfigurationReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
        if(xg == null ){
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        }
        if(xg.smartGenie == null || !xg.smartGenie.id.equals(smartGenie.id)){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "xgenie not match");
        }
        if(!DataFileType.hasDataFileType(pack.cfgClass)){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
        }
        List<GetMulityConfigurationsReqPack> listCfg = null;
        try {
            listCfg = new ObjectMapper().readValue(pack.cfgIdList, new TypeReference<List<GetMulityConfigurationsReqPack>>() {});
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, " read get config data error" + ExceptionUtils.getStackTrace(e));
        }
        List<ICProgramVO> listICProgramVO = new ArrayList<ICProgramVO>();
        ICProgramVO icProgramVO = null;
        for(int i=0; i < listCfg.size(); i++){
            icProgramVO = new ICProgramVO();
            Configuration cfg = Configuration.find(listCfg.get(i).cfgId);
            if(cfg == null){
                icProgramVO.available = false;
                icProgramVO.cfgType = null;
                icProgramVO.cfgId = listCfg.get(i).cfgId;
                icProgramVO.xGenieMac = null;
                icProgramVO.desc = null;
                icProgramVO.configuration = null;
            }else{
                icProgramVO.available = cfg.avilable;
                icProgramVO.cfgType = cfg.cfg_type;
                icProgramVO.cfgId = cfg.id;
                icProgramVO.xGenieMac = cfg.mac_addr;
                icProgramVO.desc = cfg.descript;
                String strConfiguration = "";
                try {
                    strConfiguration = ConfigurationService.getFile(listCfg.get(i).cfgId, pack.cfgClass);
                } catch (IOException e) {
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "get config file" + ExceptionUtils.getStackTrace(e));
                }
                icProgramVO.configuration = strConfiguration;
            }
            listICProgramVO.add(icProgramVO);
        }

        GetMuiltyConfigurationsRespPack respPack = new GetMuiltyConfigurationsRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        respPack.configlist = listICProgramVO;

        return createJsonResp(respPack);
    }
	
	@Transactional
	@SessionInject(ListConfigurationReqPack.class)
	public Result listConfiguration(){
		ListConfigurationReqPack pack = createInputPack();
		List<Configuration> listCfg = Configuration.listConfigurationByMacAddress(pack.xGenieMac);
		if(listCfg.size() == 0){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}

        List<? super ConfigurationInfoBaseVO> listRespPackVO = null;
        try{
            listRespPackVO = listconfig(listCfg);
        }catch (Exception e){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
        }


        ListConfigurationRespPack listRespPack = new ListConfigurationRespPack();
		listRespPack.configlist = listRespPackVO;
		return createJsonResp(listRespPack);
	}
	
	@Transactional
	@SessionInject(SwitchConfigurationReqPack.class)
	public Result enableConfig(){
		SwitchConfigurationReqPack pack = createInputPack();
		SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
        if(xg == null ){
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        }
        if(xg.smartGenie == null || !xg.smartGenie.id.equals(smartGenie.id)){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "xgenie not match");
        }
		ObjectMapper jsonMapper = new ObjectMapper();
		List<ListSwitchCfgReqPack> listSwitchCfg = null;
		try {
			listSwitchCfg = jsonMapper.readValue(pack.cfgIdList, new TypeReference<List<ListSwitchCfgReqPack>>() {}        );
    	} catch (Exception e) {
    		e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, " read enable config data error" + ExceptionUtils.getStackTrace(e));
    	}
		Configuration configuration = null;
		ConfigurationHistory cfgHist;
		for(int i = 0; i < listSwitchCfg.size(); i++){
			configuration = Configuration.find(listSwitchCfg.get(i).cfgId);
			if(configuration != null){
                if(configuration.mac_addr == null || !configuration.mac_addr.equals(xg.macAddr)){
                    return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_XGENIE_NOTMATCH, "xgenie, configuration not match");
                } else{
                    configuration.avilable = listSwitchCfg.get(i).available;
                    try{
                        configuration.save();
                    }catch (Exception err){
                        err.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, " enable config save error" + ExceptionUtils.getStackTrace(err));
                    }
                    cfgHist = new ConfigurationHistory();
                    cfgHist.configuration_id = configuration.id;
                    cfgHist.cfg_type = configuration.cfg_type;
                    cfgHist.descript = configuration.descript;
                    cfgHist.avilable = configuration.avilable;
                    cfgHist.mac_addr = configuration.mac_addr;
                    xg = XGenie.findByXgMac(configuration.mac_addr);
                    if(xg.smartGenie != null){
                        cfgHist.sg_id = xg.smartGenie.id;
                        try{
                            cfgHist.save();
                        }catch (Exception err){
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.INFO, " enable config history save error" + ExceptionUtils.getStackTrace(err));
                        }
                    }else{
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, " enable config xg macaddress do not have smart genie");
                    }
                }
			} else{
                return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
            }
		}
        xg = XGenie.findByXgMac(configuration.mac_addr);
        List<Configuration> listconfig = Configuration.listConfigurationByMacAddress(xg.macAddr);

        List<? super ConfigurationInfoBaseVO> listRespPackVO = null;
        try{
            listRespPackVO = listconfig(listconfig);
        }catch (Exception e){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
        }

        SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
		respPack.sysTimestamp = Calendar.getInstance();
        respPack.configlist = listRespPackVO;
		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(SetConfigurationReqPack.class)
	public Result editConfig(){
		SetConfigurationReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
        if(xg == null ){
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        }
        if(xg.smartGenie == null || !xg.smartGenie.id.equals(smartGenie.id)){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "xgenie not match");
        }
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		Configuration configuration = Configuration.find(pack.cfgId);
		if(configuration == null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		if(configuration.mac_addr == null || !configuration.mac_addr.equals(xg.macAddr)){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_XGENIE_NOTMATCH, "xgenie, configuration not match");
        }
		if(pack.desc != null && pack.data == null){
			configuration.descript = pack.desc;
			try{
                configuration.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "edit config save config error" + ExceptionUtils.getStackTrace(e));
            }
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = configuration.id;
			cfgHist.cfg_type = configuration.cfg_type;
			cfgHist.descript = configuration.descript;
			cfgHist.avilable = configuration.avilable;
			cfgHist.mac_addr = configuration.mac_addr;
            xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "edit config save config history error" + ExceptionUtils.getStackTrace(e));
            }
		}
		if(pack.desc == null && pack.data != null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "editconfig backup file error" + ExceptionUtils.getStackTrace(e));
			}
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = configuration.descript;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;
			try{
                cfg.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "editconfig save with desc data null error" + ExceptionUtils.getStackTrace(e));
            }

			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
            xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "editconfig config history save with desc data null error" + ExceptionUtils.getStackTrace(e));
            }
            configuration.delete();
		}
		
		if(pack.desc != null && pack.data != null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "listconfig error" + ExceptionUtils.getStackTrace(e));
			}
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = pack.desc;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;

            try{
                cfg.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "editconfig config save with desc, data error" + ExceptionUtils.getStackTrace(e));
            }
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
			xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "editconfig config history save with desc, data error" + ExceptionUtils.getStackTrace(e));
            }
			configuration.delete();
		}
        xg = XGenie.findByXgMac(pack.xGenieMac);
        List<Configuration> listconfig = Configuration.listConfigurationByMacAddress(xg.macAddr);

        List<? super ConfigurationInfoBaseVO> listRespPackVO = null;
        try{
            listRespPackVO = listconfig(listconfig);
        }catch (Exception e){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
        }

        SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		respPack.sysTimestamp = Calendar.getInstance();
        respPack.configlist = listRespPackVO;
		return createJsonResp(respPack);
	}
	@BodyParser.Of(BodyParser.AnyContent.class)
	@Transactional
	@SessionInject(RemoveConfigurationReqPack.class)
	public Result removeConfiguration(){
        RemoveConfigurationReqPack pack = createInputPack();
        SmartGenie smartGenie = getSmartGenie();
        XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
        if(xg == null ){
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        }
        if(xg.smartGenie == null || !xg.smartGenie.id.equals(smartGenie.id)){
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "xgenie not match");
        }
		ObjectMapper jsonMapper = new ObjectMapper();
        List<ListRemoveConfigurationReqPack> listRemoveConfiguration = null;
		try {
			listRemoveConfiguration = jsonMapper.readValue(pack.cfgIdList, new TypeReference<List<ListRemoveConfigurationReqPack>>(){} );
    	} catch (Exception e) {
    		e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "removeconfig config deserialize error" + ExceptionUtils.getStackTrace(e));
    	}
		
		for(int i = 0; i < listRemoveConfiguration.size(); i++){
            Configuration configuration = Configuration.find(listRemoveConfiguration.get(i).cfgId);
            if(configuration != null && configuration.mac_addr.equals(pack.xGenieMac)){
                try{
                    //Remove data from Configuration
                    configuration.delete();
                }catch(Exception err){
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "remove config error" + ExceptionUtils.getStackTrace(err));
                }
            }else{
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "remove config error not found");
            }
		}
        List<Configuration> listconfig = Configuration.listConfigurationByMacAddress(xg.macAddr);
        List<? super ConfigurationInfoBaseVO> listRespPackVO = null;
        try{
            listRespPackVO = listconfig(listconfig);
        }catch (Exception e){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
        }
        SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		respPack.sysTimestamp = Calendar.getInstance();
        respPack.configlist = listRespPackVO;
		return createJsonResp(respPack);
	}
	

public List<? super ConfigurationInfoBaseVO> listconfig (List<Configuration> listCfg){
    List<? super ConfigurationInfoBaseVO> listRespPackVO = new ArrayList<>();
    for(int i=0; i < listCfg.size(); i++){
        Configuration cfg = listCfg.get(i);
        if(cfg.cfg_type.equals("IRRIGATION_CONTROLLER") ){
            ConfigurationInfoVO cfgVO = new ConfigurationInfoVO();
            cfgVO.cfgId = cfg.id;
            cfgVO.desc = cfg.descript;
            cfgVO.update = cfg.updateAt.getTime().toString();
            cfgVO.cfgClass = cfg.cfg_type;
            cfgVO.available = cfg.avilable;
            cfgVO.xGenieMac = cfg.mac_addr;
            String strConfiguration = "";
            try {
                strConfiguration = ConfigurationService.getFile(cfg.id, cfg.cfg_type);
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "listconfig error" + ExceptionUtils.getStackTrace(e));
                throw new RuntimeException(e);
            }
            cfgVO.configuration = strConfiguration;
            listRespPackVO.add(cfgVO);
        }else{
            ConfigurationInfoBaseVO cfgVO = new ConfigurationInfoBaseVO();
            cfgVO.cfgId = cfg.id;
            cfgVO.desc = cfg.descript;
            cfgVO.update = cfg.updateAt.getTime().toString();
            cfgVO.cfgClass = cfg.cfg_type;
            cfgVO.available = cfg.avilable;
            cfgVO.xGenieMac = cfg.mac_addr;
            listRespPackVO.add(cfgVO);
        }
    }




    return listRespPackVO;

}







}
