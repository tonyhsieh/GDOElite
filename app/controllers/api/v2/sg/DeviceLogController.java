package controllers.api.v2.sg;

import com.avaje.ebean.annotation.Transactional;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v2.sg.form.EventLogGDOReqPack;
import controllers.api.v2.sg.form.EventLogSDReqPack;
import frameworks.models.StatusCode;
import json.models.api.v2.sg.SendGDOEventLogRespPack;
import json.models.api.v2.sg.SendSDEventLogRespPack;
import models.*;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.Result;
import utils.LoggingUtils;

import java.sql.Timestamp;
import java.util.*;

/**
 * Created by joseph chou on 4/6/16.
 */

@PerformanceTracker
public class DeviceLogController extends BaseAPIController {

    @Transactional
    @SessionInject(EventLogGDOReqPack.class)
    public Result createGDOLog() {

        EventLogGDOReqPack reqPack = createInputPack();

        // MAC address should be associated with the smart genie
        // TODO refactor this code snippet to become a common utility
        SmartGenie sg = getSmartGenie();
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }

        XGenie xg = XGenie.findByXgMac(reqPack.xGenieMac);
        if (xg == null) {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "device not found");
        } else if (xg.smartGenie == null || !xg.smartGenie.id.equals(sg.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "device mismatched");
        }

        XGenieGDOLog model = new XGenieGDOLog();
        model.xg_mac_addr = reqPack.xGenieMac;
        model.event = Integer.parseInt(reqPack.event);
        model.ts = new Timestamp(Long.parseLong(reqPack.ts));
        model.door = Integer.parseInt(reqPack.door);
        if(reqPack.user != null){
            User u = User.findByEmail(reqPack.user);
            if(u != null){
                if(sg.owner != null && u.id.toString().equals(sg.owner.id.toString())) {
                    model.user_name = sg.owner.nickname;
                }else{
                    List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipByOwnerNUser(
                            sg.owner.id.toString(), u.id.toString());
                    for(UserRelationship us : listUserRelationship){
                        model.user_name = us.nickname;
                    }
                }
            }else{
                model.user_name = reqPack.user;
            }
        }
        try{
            model.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "create GDO log error " + ExceptionUtils.getStackTrace(err));
        }


        SendGDOEventLogRespPack respPack = new SendGDOEventLogRespPack();

        respPack.sysTimestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(EventLogSDReqPack.class)
    public Result createSDLog() {

        EventLogSDReqPack reqPack = createInputPack();

        // MAC address should be associated with the smart genie
        // TODO refactor this code snippet to become a common utility
        SmartGenie sg = getSmartGenie();
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }

        XGenie xg = XGenie.findByXgMac(reqPack.xGenieMac);
        if (xg == null) {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "device not found");
        } else if (xg.smartGenie == null || !xg.smartGenie.id.equals(sg.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "device mismatched");
        }

        XGenieSDLog model = new XGenieSDLog();
        model.xg_mac_addr = reqPack.xGenieMac;
        model.event = Integer.parseInt(reqPack.event);
        model.ts = new Timestamp(Long.parseLong(reqPack.ts));
        model.interrupt = Integer.parseInt(reqPack.interrupt);
        try{
            model.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "create SD log error " + ExceptionUtils.getStackTrace(err));
        }


        SendSDEventLogRespPack respPack = new SendSDEventLogRespPack();

        respPack.sysTimestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }
}