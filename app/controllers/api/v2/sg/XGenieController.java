package controllers.api.v2.sg;

import com.avaje.ebean.annotation.Transactional;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v2.sg.form.RenameXGReqPack;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.KeepAliveReqPack;
import frameworks.models.StatusCode;

import json.models.api.v2.sg.ListXgRespPack;
import json.models.api.v2.sg.RenameXGRespPack;
import json.models.api.v2.sg.vo.XGenieInfoBaseVO;
import models.SmartGenie;
import models.XGenie;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.Result;
import utils.LoggingUtils;

import java.util.Calendar;
import java.util.List;

@PerformanceTracker
public class XGenieController extends BaseAPIController{
	
	@Transactional
	@SessionInject(KeepAliveReqPack.class)
	public Result listXG(){

		KeepAliveReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

        List<? super XGenieInfoBaseVO> listXg = SmartGenieController.listxg(smartGenie);

        ListXgRespPack respPack = new ListXgRespPack();

		respPack.sysTimestamp = Calendar.getInstance();
        respPack.xGenieList = listXg;

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(RenameXGReqPack.class)
	public Result rename(){
		RenameXGReqPack pack = createInputPack();
		SmartGenie smartGenie = getSmartGenie();
		XGenie xg = XGenie.findByXgMac(pack.xGenieMac);
		if(xg==null){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
		}
		if(xg.smartGenie == null || smartGenie == null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie not match");
		}
		
		if(!xg.smartGenie.id.equals(smartGenie.id)){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie not match");
		}
		
		xg.name = pack.name;
		try{
            xg.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "xGenie rename error " + ExceptionUtils.getStackTrace(err));
        }

        List<? super XGenieInfoBaseVO> listXg = SmartGenieController.listxg(smartGenie);
        RenameXGRespPack respPack = new RenameXGRespPack();
        respPack.sysTimestamp = Calendar.getInstance();
        respPack.xGenieList = listXg;
        return createJsonResp(respPack);
	}
}
