/**
 *
 */
package controllers.api.v1.app;

import java.util.Calendar;
import java.util.UUID;

import json.models.api.v1.app.ResetPwdRespPack;
import models.UserIdentifier;

import play.Configuration;
import play.mvc.Result;
import utils.*;
import utils.generator.CredentialGenerator;
import views.html.email.reset_pwd_confirm_mail;
import views.html.mobile.user.reset_pwd;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.cache.PwdReset;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.DoResetPwdReqPack;
import controllers.api.v1.app.form.ResetPwdReqPack;
import frameworks.models.CacheKey;
import frameworks.models.StatusCode;
import frameworks.models.UserIdentifierType;
import javax.inject.Inject;

/**
 * @author carloxwang
 *
 */
@PerformanceTracker
public class PasswordController extends BaseAPIController {
	@Inject
	Configuration configuration;

	@Transactional
	@ParameterInject(ResetPwdReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result resetPwd(){
		ResetPwdReqPack pack = createInputPack();

		// Using Memcached for password reset
		UserIdentifier ui = UserIdentifier.getUserIdentifier(pack.loginId.toLowerCase(), UserIdentifierType.EMAIL);

		if( ui == null ){
			return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "User NOT FOUND");
		}

		PwdReset pr = new PwdReset();
		pr.userIdentifierId = ui.id.toString();
		pr.authCode = CredentialGenerator.genAuthCode();

		Calendar valid = Calendar.getInstance();
		valid.add(Calendar.MINUTE, 30);
		pr.validBefore = valid;

		String cacheId = UUID.randomUUID().toString();

		CacheManager.setWithId(CacheKey.PWD_RESET, cacheId, pr, 30*60);

		ResetPwdRespPack respPack = new ResetPwdRespPack();
		respPack.targetEmail = pack.loginId;
		respPack.validBefore = valid;

		String url =  "http://" + request().host() + "/m/user/reset_pwd/" + cacheId + "/" + pr.authCode;
		
		String email = ConfigUtil.getEmailSender();

		MailUtil.instance().sendMail(pack.loginId, "Password Reset Confirm Email", reset_pwd_confirm_mail.render(url, email).toString());

		return createJsonResp(respPack);

	}


	public Result confirmAuthCode(String ai, String ac){

		PwdReset pr = (PwdReset) CacheManager.get(CacheKey.PWD_RESET, ai);

        if( pr == null ){
        	setWarningMessage("Your session has expired, Please go back to your App to resend the confirm email.");
        	flash("showBackToAppBtn", "true");
        	return ok(reset_pwd.render("Reset Password", ai, ac));
        }

		if(!pr.authCode.equals(ac) ){
			// 送個來亂的訊息回去
			// Add by Carlos, 2013/4/23 下午4:36:05

			return notFound("404 Error");
		}

		UserIdentifier ui = Ebean.find(UserIdentifier.class, pr.userIdentifierId);

		if( ui == null ){
			//Logger.warn("Oops... ");
			return notFound("404 Error");
		}

		return ok(reset_pwd.render("Reset Password", ai, ac));

	}

	@ParameterInject(DoResetPwdReqPack.class)
	@MobileLogInject(BaseReqPack.class)

    public Result doResetPwd(String ai, String ac){

		DoResetPwdReqPack pack = createInputPack();

        PwdReset pr = CacheManager.get(CacheKey.PWD_RESET, ai);

        if( pr == null ){
        	return createIndicatedJsonStatusResp(StatusCode.RESET_PWD_SESSION_EXPIRED, "Session Expired");
        }

        if( !pr.authCode.equals(ac) ){
        	// 也許來亂的，所以也送個來亂的訊息回去
        	// Add by Carlos, 2013/4/25 上午11:52:48
        	return notFound();
        }

        UserIdentifier ui = Ebean.find(UserIdentifier.class, pr.userIdentifierId);

        if( ui == null){
        	//Logger.warn("Oops...");
        	return notFound();
        }

        try{
            CacheManager.removeWithId(CacheKey.PWD_RESET, ai);
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "reset password remove cache error" + ExceptionUtil.exceptionStacktraceToString(err));
        }

        ui.setPassword(UserIdentifier.pwdHash(pack.pwd));
        try{
            ui.save();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "reset password save error" + ExceptionUtil.exceptionStacktraceToString(err));
        }





        return createIndicatedJsonStatusResp(StatusCode.GENERAL_SUCCESS, "SUCCESS");
    }
}
