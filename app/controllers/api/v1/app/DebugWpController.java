package controllers.api.v1.app;

import java.util.UUID;

import models.Checkout;
import play.mvc.Result;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.form.DebugWpUpdateReqPack;

@Transactional
@ParameterInject(DebugWpUpdateReqPack.class)
public class DebugWpController extends BaseAPIController{

	@Transactional
	public Result update(){

		DebugWpUpdateReqPack pack = createInputPack();

		Checkout checkout = new Checkout();

		checkout.id = UUID.randomUUID().toString();
		
		checkout.no = pack.no;
		
		checkout.pay = pack.pay;
		
		checkout.price = pack.price;

		checkout.table_no = pack.table;
		
		checkout.tax = pack.tax;
		
		checkout.total = pack.total;
		
		checkout.save();

		return NOP();
	}
}
