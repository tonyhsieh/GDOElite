package controllers.api.v1.app;

import controllers.api.v1.app.form.IcWateringTime;
import models.XGenie;

public interface IIcEventStatsStrategy {
	
	public IcWateringTime[] summarizeEventStats(XGenie xGenie, byte zoneId, short offset);

}
