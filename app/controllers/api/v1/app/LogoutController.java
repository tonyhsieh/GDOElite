package controllers.api.v1.app;

import json.models.api.v1.app.AuthRespPack;
import models.PnRelationship;
import play.mvc.BodyParser;
import play.mvc.Result;
import utils.CacheManager;

import com.avaje.ebean.Ebean;

import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.cache.UserSessionCache;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.app.form.BaseReqPack;
import frameworks.exceptions.FormDataValidationException;
import frameworks.models.CacheKey;
import frameworks.models.StatusCode;

/**
 * @author john
 */
//@PerformanceTracker
public class LogoutController extends BaseAPIController {
    @BodyParser.Of(BodyParser.AnyContent.class)
	@MobileLogInject(BaseReqPack.class)
	@SessionInject(BaseReqPack.class)
	public Result doLogout() throws FormDataValidationException{

		BaseReqPack pack = createInputPack();

		//user session data delete

		UserSessionCache usc = CacheManager.get(CacheKey.USER_SESSION, pack.sessionId);
		PnRelationship pnR = Ebean.find(PnRelationship.class, usc.pnId);
		
		try{
			if(pnR!=null){
				Ebean.delete(pnR);
			}
		}catch(Exception err){
			
		}
		
		if(usc == null || pnR == null){
			return createIndicatedJsonStatusResp(StatusCode.AUTH_LOGOUT_ERROR, "usc or pnR empty");
		}
		
		boolean result = UserSessionUtils.logout(pack.sessionId);

		if(!result)
			return createIndicatedJsonStatusResp(StatusCode.AUTH_LOGOUT_ERROR, "logout failure");

		AuthRespPack respPack = new AuthRespPack();
		respPack.sessionId = pack.sessionId;

		return createJsonResp(respPack);
	}
}
