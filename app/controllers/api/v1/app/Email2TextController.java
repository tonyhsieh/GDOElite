package controllers.api.v1.app;

import com.avaje.ebean.annotation.Transactional;

import org.apache.commons.lang3.exception.ExceptionUtils;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Random;

import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.cache.Email2TextPin;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.Email2TextSetReqPack;
import controllers.api.v1.app.form.Email2TextValidateReqPack;
import frameworks.models.CacheKey;
import frameworks.models.StatusCode;
import json.models.api.v1.app.Email2TextSetRespPack;
import json.models.api.v1.app.Email2TextValidateRespPack;
import json.models.api.v1.app.GetGatewayListRespPack;
import json.models.api.v1.app.vo.GatewayVO;
import models.Gateway;
import models.User;

import play.mvc.Result;
import utils.CacheManager;
import utils.LoggingUtils;
import utils.PhoneNumberValidationUtil;
import utils.MailUtil;

import javax.inject.Inject;

@Transactional
public class Email2TextController extends BaseAPIController
{
    @Inject
    private play.Configuration configuration;

    @MobileLogInject(BaseReqPack.class)
    @SessionInject(BaseReqPack.class)
    public Result getGatewayList()
    {
        BaseReqPack pack = createInputPack();

        GetGatewayListRespPack respPack = new GetGatewayListRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.listGateway = getGatewayVOs();

        return createJsonResp(respPack);
    }

    @Transactional
    @MobileLogInject(BaseReqPack.class)
    @SessionInject(Email2TextSetReqPack.class)
    public Result setEmail2Text()
    {
        final Integer waitingSeconds = configuration.getInt("email2text.pin.waitingTime", 60);
        final int expiration = configuration.getInt("email2text.pin.expiration", 30 * 60);
        final boolean isSendingEmail = configuration.getBoolean("email2text.pin.isSendingEmail", true);
        final boolean isValidatingPhoneNumber = configuration.getBoolean("email2text.isValidatingPhoneNumber", true);

        Email2TextSetReqPack pack = createInputPack();

        if (pack.phoneNumber.length() != 10)
        {
            return createIndicatedJsonStatusResp(StatusCode.WRONG_PARAMETER, "length of phone number should be 10");
        }

        User user = getUser();

        final String cacheId = user.id.toString();
        Email2TextPin email2TextPin = CacheManager.get(CacheKey.EMAIL2TEXT_PIN, cacheId);
        if (email2TextPin != null)
        {
            if (!getBooleanValueForObject(pack.isResend))
            {
                // if a user requests with same phone number and NOT-isResend, just return original information
                for (Email2TextPin.Email2TextPinItem item : email2TextPin.list)
                {
                    if (item != null && item.gatewayId.equals(pack.gatewayId) && item.phoneNumber.equals(pack.phoneNumber))
                    {
                        Email2TextSetRespPack respPack = new Email2TextSetRespPack();
                        respPack.timestamp = Calendar.getInstance();
                        respPack.pin = item.pin;
                        respPack.smsEmail = getEmail2Text(pack.phoneNumber, Gateway.getById(item.gatewayId));
                        long requestTS = item.requestTime.getTimeInMillis();
                        long currentTS = System.currentTimeMillis();

                        if (currentTS - requestTS > waitingSeconds * 1000)
                        {
                            respPack.waitingTime = 0L;
                        }
                        else
                        {
                            respPack.waitingTime = waitingSeconds - ((currentTS - requestTS) / 1000);
                        }
                        return createJsonResp(respPack);
                    }
                }
            }
        }

        final Gateway gateway = Gateway.getById(pack.gatewayId);
        if (gateway == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.WRONG_PARAMETER, "invalid gateway id");
        }

        if (email2TextPin == null)
        {
            email2TextPin = new Email2TextPin();
        }
        email2TextPin.userId = user.id.toString();
        email2TextPin.sessionId = pack.sessionId;

        // remove old entries
        Iterator<Email2TextPin.Email2TextPinItem> iterator = email2TextPin.list.iterator();
        while (iterator.hasNext())
        {
            Email2TextPin.Email2TextPinItem item = iterator.next();

            if (item != null && (!item.gatewayId.equals(pack.gatewayId) || !item.phoneNumber.equals(pack.phoneNumber))
                    && !item.isValidated)
            {
                iterator.remove();

                LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "removed: " + item.toString());
            }
        }

        Email2TextPin.Email2TextPinItem email2TextPinItem = new Email2TextPin.Email2TextPinItem();
        email2TextPinItem.pin = String.valueOf(new Random().nextInt(9999 - 1000) + 1000);
        email2TextPinItem.countryCode = (pack.countryCode == null) ? "1" : pack.countryCode;
        email2TextPinItem.gatewayId = pack.gatewayId;
        email2TextPinItem.phoneNumber = pack.phoneNumber;

        // and add new entry
        email2TextPin.list.add(email2TextPinItem);

        if (isValidatingPhoneNumber)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, pack.phoneNumber + "....");
            email2TextPinItem.isPhoneNumberValid = isPhoneNumberValid(pack.phoneNumber);
        }
        else
        {
            email2TextPinItem.isPhoneNumberValid = true;
        }

        if (email2TextPinItem.isPhoneNumberValid == null)
        {
            // ignore if phone number validation service is unavailable
            email2TextPinItem.isPhoneNumberValid = true;
        }
        else if (!email2TextPinItem.isPhoneNumberValid)
        {
            return createIndicatedJsonStatusResp(StatusCode.PHONE_NUMBER_VALIDATION_FAILED, "invalid phone number");
        }

        email2TextPinItem.requestTime = Calendar.getInstance();
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.SECOND, expiration);
        email2TextPinItem.validBefore = calendar;
        email2TextPinItem.isValidated = false;

        CacheManager.setWithId(CacheKey.EMAIL2TEXT_PIN, cacheId, email2TextPin, expiration);

        final String smsEmail = getEmail2Text(pack.phoneNumber, gateway);

        if (isSendingEmail)
        {
            if (email2TextPinItem.isPhoneNumberValid == null || !email2TextPinItem.isPhoneNumberValid)
            {
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "No PIN validation email is sent to " + smsEmail + " (invalid phone number)");
            }
            else
            {
                StringBuilder sb = new StringBuilder();
                sb.append("Your PIN code is ");
                sb.append(email2TextPinItem.pin);
                sb.append(" for phone number ");
                sb.append(email2TextPinItem.phoneNumber);
                sb.append(". This code will be expired after ");
                sb.append(email2TextPinItem.validBefore.getTime().toString());

                MailUtil.instance().sendMail(smsEmail, "Asante PIN validation", sb.toString());
            }
        }
        else
        {
            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "No PIN validation email is sent to " + smsEmail);
        }

        Email2TextSetRespPack respPack = new Email2TextSetRespPack();
        respPack.timestamp = Calendar.getInstance();
        respPack.pin = email2TextPinItem.pin;
        respPack.smsEmail = smsEmail;
        respPack.waitingTime = (long) waitingSeconds;

        return createJsonResp(respPack);
    }

    private static boolean getBooleanValueForObject(Object obj)
    {
        if (obj == null)
        {
            return false;
        }

        if (obj instanceof Boolean)
        {
            return (Boolean) obj;
        }

        if (obj instanceof String)
        {
            String s = (String) obj;
            if (s.equalsIgnoreCase("T") || s.equalsIgnoreCase("TRUE")
                    || s.equals("1"))
            {
                return true;
            }
        }

        return false;
    }

    @Transactional
    @MobileLogInject(BaseReqPack.class)
    @SessionInject(Email2TextValidateReqPack.class)
    public Result validatePin()
    {
        final int expiration = configuration.getInt("email2text.pin.expiration", 30 * 60);

        Email2TextValidateReqPack pack = createInputPack();

        User user = getUser();

        final String cacheId = user.id.toString();
        Email2TextPin email2TextPin = CacheManager.get(CacheKey.EMAIL2TEXT_PIN, cacheId);
        if (email2TextPin == null || email2TextPin.list.size() == 0)
        {
            return createIndicatedJsonStatusResp(StatusCode.EMAIL2TEXT_PIN_EXPIRED, "PIN session not found");
        }

        Email2TextPin.Email2TextPinItem email2TextPinItem = null;
        for (Email2TextPin.Email2TextPinItem item : email2TextPin.list)
        {
            if (item != null && item.pin.equals(pack.pin))
            {
                if (item.validBefore.getTimeInMillis() < System.currentTimeMillis())
                {
                    return createIndicatedJsonStatusResp(StatusCode.EMAIL2TEXT_PIN_EXPIRED, "PIN session expired");
                }

                email2TextPinItem = item;
            }
        }

        if (email2TextPinItem == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.EMAIL2TEXT_WRONG_PIN, "wrong PIN");
        }

        if (email2TextPinItem.isValidated)
        {
            return createIndicatedJsonStatusResp(StatusCode.EMAIL2TEXT_WRONG_PIN, "PIN already validated");
        }

        email2TextPinItem.isValidated = true;
        CacheManager.setWithId(CacheKey.EMAIL2TEXT_PIN, cacheId, email2TextPin, expiration);

        Gateway gateway = Gateway.getById(email2TextPinItem.gatewayId);
        if (gateway == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.WRONG_PARAMETER, "invalid gateway id: " + email2TextPinItem.gatewayId);
        }

        final String smsEmail = getEmail2Text(email2TextPinItem.phoneNumber, gateway);

        Email2TextValidateRespPack respPack = new Email2TextValidateRespPack();
        respPack.timestamp = Calendar.getInstance();
        respPack.smsEmail = smsEmail;

        return createJsonResp(respPack);
    }

    public static List<GatewayVO> getGatewayVOs()
    {
        List<GatewayVO> gatewayVOs = new ArrayList<>();
        for (Gateway g : Gateway.list())
        {
            GatewayVO gatewayVO = new GatewayVO();
            gatewayVO.id = g.id;
            gatewayVO.countryCode = g.countryCode;
            gatewayVO.name = g.carrier;
            gatewayVOs.add(gatewayVO);
        }

        Collections.sort(gatewayVOs, (g1, g2) ->
        {
            return g1.name.compareTo(g2.name);
        });
        return gatewayVOs;
    }

    public static String getEmail2Text(final String phoneNumber, final Gateway gateway)
    {
        final String gate = gateway.gateway;
        int idx = gate.indexOf("@");
        if (idx == -1)
        {
            return phoneNumber + gate;
        }
        return phoneNumber + gate.substring(idx);
    }

    private Boolean isPhoneNumberValid(final String phoneNumber)
    {
        final int phoneNumberValidationResultCacheTime = configuration.getInt("email2text.phoneNumberValidationResultCacheTime", 86400);

        Boolean result = CacheManager.get(CacheKey.EMAIL2TEXT_PIN, phoneNumber);
        if (result != null)
        {
            return result;
        }

        try
        {
            Boolean isValid = PhoneNumberValidationUtil.validate(phoneNumber, "us");
            CacheManager.setWithId(CacheKey.EMAIL2TEXT_PIN, phoneNumber, isValid, phoneNumberValidationResultCacheTime);
            return isValid;
        }
        catch (Exception e)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtils.getStackTrace(e));
            return null;
        }
    }
}
