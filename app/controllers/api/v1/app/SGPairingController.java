package controllers.api.v1.app;

import java.util.List;

import json.models.api.v1.app.SessionRespPack;
import models.Contact;
import models.DeviceLog;
import models.EventContact;
import models.PairingHistoryLog;
import models.SmartGenie;
import models.User;
import models.UserRelationship;
import models.UserRelationshipKeeper;
import models.XGenie;
import org.springframework.beans.factory.annotation.Autowired;
import play.mvc.Result;
import services.SmartGenieService;
import utils.generator.CredentialGenerator;

import com.avaje.ebean.Ebean;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.PairingReqPack;
import frameworks.Constants;
import frameworks.models.EventLogType;
import frameworks.models.PairingType;
import frameworks.models.StatusCode;

@PerformanceTracker
public class SGPairingController extends BaseAPIController {

	@Autowired
	private SmartGenieService smartGenieService;

	@SessionInject(PairingReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result doPairing(){
		PairingReqPack pack = createInputPack();

		User user = getUser();

		SmartGenie sg = Ebean.find(SmartGenie.class, CredentialGenerator.genSmartGenieId(pack.macAddr));

		if(sg == null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "no such smart genie");

		if(sg.isPairing)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_IS_PAIRING, "smart genie was paired");
		
		sg.owner = user;
		sg.name = "My Home";
		sg.isPairing = true;
		sg.save();

		//if the owner has other smart genie and has users, all users must be added under the new smart genie.
		//by Jack
		//Is the owner has other smart genie?
		List<SmartGenie> listSmartGenie = SmartGenie.listSmartGenieByOwner(user.id.toString());
		
		if(!listSmartGenie.isEmpty()){
			
			int index = 0;

			List<UserRelationship> listUserRelationship;
			
			//判斷是否是user relationship的資料
			for(int i = 0; i < listSmartGenie.size(); i++){
				listUserRelationship = UserRelationship.listUserRelationshipBySG(listSmartGenie.get(i).id);
				
				for(int j = 0; j < listUserRelationship.size(); j++){
					if(j > 0){
						index = i;
						break;
					}
				}
				
				if(index > 0){
					break;
				}
			}
			
			listUserRelationship = UserRelationship.listUserRelationshipBySG(listSmartGenie.get(index).id);

			if(listUserRelationship.size()==0){
				//If without Asante Genie, check UserRelationShipKeeper.
				//If UserRelationShipKeeper has the Owner's data(user relationship), add all user relationship with the Asante Genie.
				
				List<UserRelationshipKeeper> listUrk = UserRelationshipKeeper.listByOwner(user.id.toString());
				if(listUrk!=null){
					for(UserRelationshipKeeper urk: listUrk){
						UserRelationship userRelationship = new UserRelationship();
						
						userRelationship.id = UserRelationship.genUserRelationshipId(sg.id, urk.ownerId, urk.userId);
						userRelationship.ownerId = urk.ownerId;
						userRelationship.userId = urk.userId;
						userRelationship.nickname = urk.nickname;
						userRelationship.countryCode = urk.countryCode;
						userRelationship.cellPhone = urk.cellPhone;
						userRelationship.email = urk.email;
						userRelationship.smartGenieId = sg.id;
						userRelationship.save();
					}
				}
			}else{
			
				for(int i = 0; i < listUserRelationship.size(); i++){
					UserRelationship userRelationship = new UserRelationship();

					userRelationship.countryCode = listUserRelationship.get(i).countryCode;
					userRelationship.cellPhone = listUserRelationship.get(i).cellPhone;
					userRelationship.nickname = listUserRelationship.get(i).nickname;
					userRelationship.ownerId = listUserRelationship.get(i).ownerId;
					userRelationship.userId = listUserRelationship.get(i).userId;
					userRelationship.email = listUserRelationship.get(i).email;
					userRelationship.smartGenieId = sg.id;

					userRelationship.id = UserRelationship.genUserRelationshipId(
        								userRelationship.smartGenieId,
        								userRelationship.ownerId,
        								userRelationship.userId);
					userRelationship.save();
				}
			}
		}
		
		setLog(pack.macAddr, user.id.toString(), PairingType.PAIRING);

		SessionRespPack respPack = new SessionRespPack();
		respPack.sessionId = pack.sessionId;

		return createJsonResp(respPack);
	}

	@SessionInject(PairingReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result doUnPairing(){
		PairingReqPack pack = createInputPack();

		SmartGenie sg = Ebean.find(SmartGenie.class, CredentialGenerator.genSmartGenieId(pack.macAddr));

		if(sg == null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "no such smart genie");
		}

		User user = getUser();

		try{
			if(!sg.owner.id.toString().equals(user.id.toString())){
				return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "sg ownership error");
			}
		}
		catch(Exception err){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "sg ownership empty");
		}
		
		//Remove XGenie and Asante Genie relationship
		//by Jack
		List<XGenie> listXGenie = XGenie.listXGBySG(sg);
		for(int i = 0; i < listXGenie.size(); i++){
			//記錄至 device_log
			DeviceLog el = new DeviceLog();
			el.sgId = sg.id;
			el.xGId = listXGenie.get(i).id;

			el.eventType = EventLogType.REMOVE_DEVICE.value;
			el.eventData = Constants.EVENT_LOG_DATA_REMOVE_DEVICE;
			el.save();

			listXGenie.get(i).smartGenie = null;
			listXGenie.get(i).name = null;
			listXGenie.get(i).status = StatusCode.XGENIE_REMOVED.value;
			listXGenie.get(i).save();
			
			//remove event_contact
			//by Jack Chuang 20140217
			List<EventContact> listEc = EventContact.listByMac(listXGenie.get(i).macAddr);
			
			for(EventContact ec: listEc){
				List<Contact> listContact = Contact.listByEventContact(ec.id);
				
				for(Contact contact: listContact){
					contact.delete();
				}
			}
			//end
		}	

		//Remove UserRelationship data
		//by Jack
		List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipBySG(sg.id);
		for(int i = 0; i < listUserRelationship.size(); i++){
			UserRelationship userRelationship = listUserRelationship.get(i);
			userRelationship.delete();
		}

		sg.owner = null;
		sg.name = null;
		sg.isPairing = false;
		sg.save();

		setLog(pack.macAddr, user.id.toString(), PairingType.UNPAIRING);

		SessionRespPack respPack = new SessionRespPack();
		respPack.sessionId = pack.sessionId;

		return createJsonResp(respPack);
	}

	private static void setLog(String macAddr, String userId, PairingType type){
		PairingHistoryLog log = new PairingHistoryLog();

		log.macAddr = macAddr;
		log.userId = userId;
		log.type = type;

		log.save();
	}
}
