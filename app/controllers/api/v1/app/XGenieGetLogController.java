package controllers.api.v1.app;

import com.avaje.ebean.annotation.Transactional;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.DeviceLogReqPack;
import frameworks.models.StatusCode;
import json.models.api.v1.app.XGenieLogRespPack;
import models.SmartGenieLog;
import models.XGenie;
import models.XGenieGDOLog;
import models.XGenieSDLog;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.Result;
import utils.LoggingUtils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.TimeZone;

public class XGenieGetLogController extends BaseAPIController
{
    @Transactional
    @SessionInject(DeviceLogReqPack.class)
    public Result getGDOEventLog()
    {
        DeviceLogReqPack deviceLogReqPack = createInputPack();

        XGenie xGenie = XGenie.findByXgMac(deviceLogReqPack.xgMAC);
        if (xGenie == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "no such xGenie");
        }

        List<Object> logItems = new ArrayList<>();
        SimpleDateFormat sdf = getSimpleDateFormatFromSmartGenieTimezoneByXGenieMAC(deviceLogReqPack.xgMAC);

        List<XGenieGDOLog> xGenieGDOLogs = XGenieGDOLog.findByXgMac(deviceLogReqPack.xgMAC, deviceLogReqPack.begin,
                deviceLogReqPack.end, deviceLogReqPack.order);
        for (XGenieGDOLog xGenieGDOLog : xGenieGDOLogs)
        {
            XGenieLogRespPack.XGenieGDOEventLogItem logItem = new XGenieLogRespPack.XGenieGDOEventLogItem();
            logItem.time = sdf.format(xGenieGDOLog.ts);
            logItem.user = xGenieGDOLog.user_name;
            logItem.door = xGenieGDOLog.door;
            logItem.action = xGenieGDOLog.event;
            logItems.add(logItem);
        }

        XGenieLogRespPack respPack = new XGenieLogRespPack();
        respPack.sessionId = deviceLogReqPack.sessionId;
        respPack.log = logItems;

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(DeviceLogReqPack.class)
    public Result getSDEventLog()
    {
        DeviceLogReqPack deviceLogReqPack = createInputPack();

        XGenie xGenie = XGenie.findByXgMac(deviceLogReqPack.xgMAC);
        if (xGenie == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "no such xGenie");
        }

        List<Object> logItems = new ArrayList<>();
        SimpleDateFormat sdf = getSimpleDateFormatFromSmartGenieTimezoneByXGenieMAC(deviceLogReqPack.xgMAC);

        List<XGenieSDLog> xGenieGSDLogs = XGenieSDLog.findByXgMac(deviceLogReqPack.xgMAC, deviceLogReqPack.begin,
                deviceLogReqPack.end, deviceLogReqPack.order);
        for (XGenieSDLog xGenieGSDLog : xGenieGSDLogs)
        {
            XGenieLogRespPack.XGenieSDEventLogItem logItem = new XGenieLogRespPack.XGenieSDEventLogItem();
            logItem.time = sdf.format(xGenieGSDLog.ts);
            logItem.event = xGenieGSDLog.event;
            logItem.interrupt = xGenieGSDLog.interrupt;
            logItems.add(logItem);
        }

        XGenieLogRespPack respPack = new XGenieLogRespPack();
        respPack.sessionId = deviceLogReqPack.sessionId;
        respPack.log = logItems;

        return createJsonResp(respPack);
    }

    private static SimpleDateFormat getSimpleDateFormatFromSmartGenieTimezoneByXGenieMAC(final String macAddr)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy:MM:dd:HH:mm");

        try
        {
            XGenie xGenie1 = XGenie.findByXgMac(macAddr);
            SmartGenieLog smartGenieLog = SmartGenieLog.find(xGenie1.smartGenie.id);
            int rawOffset = smartGenieLog.timezone * 1000 * 3600;

            String [] timezones = TimeZone.getAvailableIDs(rawOffset);
            if (timezones.length > 0)
            {
                sdf.setTimeZone(TimeZone.getTimeZone(timezones[0]));
            }
            else
            {
                sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            }
        }
        catch (Exception e)
        {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "get date format error " + ExceptionUtils.getStackTrace(e));
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        }
        return sdf;
    }
}
