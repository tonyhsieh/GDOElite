package controllers.api.v1.app;

import com.avaje.ebean.*;

import java.util.LinkedList;
import java.util.List;

import json.models.api.v1.app.UserRelationRespPack;
import json.models.api.v1.app.vo.PageVO;
import json.models.api.v1.app.vo.UserVO;
import models.User;

import play.mvc.Result;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.SearchUserReqPack;

/**
 * @author carloxwang
 */
@PerformanceTracker
public class UserRelationController extends BaseAPIController
{
    @SessionInject(SearchUserReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result findUser()
    {
        // SearchUserReqPack consist of search text, page(current  display page), page size
        //page : result output current page, each page has how many user
        SearchUserReqPack pack = createInputPack();
        Query<User> query =
                Ebean.find(User.class)
                        .where()
                        .disjunction()
                        .like("nickname", pack.searchText)
                        .like("id", pack.searchText)
                        .like("email", pack.searchText + "%").order().desc("create_at");
        PagedList<User> paging_list = query.findPagedList(pack.page - 1, pack.pageSize);
        List<User> user_page_list = paging_list.getList();
        List<UserVO> userList = new LinkedList<>();
        // find the user
        for (User u : user_page_list)
        {
            //LoggingUtils.log(LoggingUtils.LogLevel.DEBUG,"user in the user relation" + u.toString());
            userList.add(new UserVO(u));
        }

        UserRelationRespPack respPack = new UserRelationRespPack();
        respPack.userList = new PageVO<UserVO>(pack.page.longValue(), pack.pageSize.longValue(),
                (long) paging_list.getTotalPageCount(), (long) paging_list.getTotalRowCount());
        respPack.userList.elements = userList;

        return createJsonResp(respPack);
    }
}
