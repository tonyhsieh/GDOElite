package controllers.api.v1.app;
import java.util.*;

import models.EmailBlacklist;

import com.fasterxml.jackson.databind.ObjectMapper;


import play.data.DynamicForm;
import play.mvc.Result;
import utils.ConfigUtil;
import utils.LoggingUtils;
import utils.SQSManager;
import utils.SendMsgUtil;
import utils.notification.DeviceEventQueueMsg;

import javax.inject.Inject;
import play.Configuration;
import static play.mvc.Results.ok;
import play.data.FormFactory;

/**
 * Created by tony_hsieh on 2016/6/22.
 */
public class DebugQueueController {
    static Configuration configuration ;
    FormFactory formFactory;
    @Inject
    public DebugQueueController(Configuration configuration, FormFactory formFactory) {
        this.configuration = configuration;
        this.formFactory = formFactory;
    }

    public Result sendDebugMsg(){
        DynamicForm form = formFactory.form().bindFromRequest();
        final String sgId = form.get("sgId");
        final String strTimeStamp = form.get("TimeStamp");
        final String mac_Address = form.get("macAddress");
        final Integer event_Type = Integer.parseInt(form.get("eventType"));
        final String title = form.get("title");
        final String content = form.get("content");
        final String email = form.get("email");
        final Boolean isEmail = !email.isEmpty();
        final Boolean isPushNotification = Boolean.parseBoolean(form.get("isPushNotification"));
        final Integer count = Integer.parseInt(form.get("count"));

        new Thread(new Runnable() {
            @Override
            public void run() {
                DebugQueueRunnable(sgId, strTimeStamp, mac_Address, event_Type, title, content, email, isEmail, isPushNotification, count);
            }
        }).start();
        return ok("Done with sending "+ count.toString() + " " + "message");
    }

    public static DeviceEventQueueMsg createQueueMsg(String sgId, String strMsgId, String title, String content,
                                                     String email, Boolean isEmail, Boolean isPushNotification,
                                                     Integer event_Type){
        DeviceEventQueueMsg queueMsg = new DeviceEventQueueMsg();
        queueMsg.sgId = sgId;
        queueMsg.id = strMsgId;
        queueMsg.title = title;
        queueMsg.content = content;
        queueMsg.listOwnerEmail = new ArrayList<>();
        queueMsg.listOwnerEmail.add(email);
        queueMsg.isEmail = isEmail;
        queueMsg.isPushNotification = isPushNotification;
        queueMsg.eventType = event_Type;
        return queueMsg;
    }

    public static void DebugQueueRunnable(String sgId, String strTimeStamp, String mac_Address, Integer eventType,
                                          String title, String content, String email, Boolean isEmail,
                                          Boolean isPushNotification, Integer count) {

        for (Integer i = 1; i <= count; i++) {
            if(email != null && !EmailBlacklist.emailIsValid(email)){
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "email is invalid: " + email);
                return;
            }

            String url = ConfigUtil.getTransporterUrl();
            if (url != null && url.length() > 0) {
                List<String> emailList = new ArrayList<>();
                emailList.add(email);
                SendMsgUtil.sendMsgByAdminConsole(emailList, title, content, isEmail, isPushNotification);
                continue;
            }


            String strMsgTmp = strTimeStamp.substring(0, 19);
            char[] chMsgTmp = strMsgTmp.toCharArray();
            StringBuffer strbufMsgId = new StringBuffer();
            for(int j = 0; j < chMsgTmp.length; j++){
                if(chMsgTmp[j] != '-' && chMsgTmp[j] != ':' && chMsgTmp[j] != ' ' && chMsgTmp[j] != '/'){
                    strbufMsgId.append(chMsgTmp[j]);
                }
            }
            String strMsgId = "";
            strMsgId = mac_Address.substring(6) + strbufMsgId.toString();
            strMsgId += String.valueOf(eventType);

            LoggingUtils.log(LoggingUtils.LogLevel.DEBUG, "Msg queue count " + count + " " + "number" + i);

            DeviceEventQueueMsg queueMsg = createQueueMsg(sgId, strMsgId, title, content , email, isEmail, isPushNotification, eventType);

            String queueRegion = configuration.getString("queue.region");
            String queueName = configuration.getString("queue.name.1");
            SQSManager sqsManager = SQSManager.getInstance(queueRegion);
            String queueUrl;

            ObjectMapper om = new ObjectMapper();

            try {
                queueUrl = sqsManager.getQueue(queueName);
                String sqsMsgId = sqsManager.sendMessage(queueUrl, om.writeValueAsString(queueMsg));
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "SQS MessageId : " + sqsMsgId);

            } catch (Exception e) {
                // TODO Auto-generated catch block
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "SQS sendMsg err : " + e.toString());
                e.printStackTrace();
            }
        }
    }
}
