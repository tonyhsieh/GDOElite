package controllers.api.v1.app;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import controllers.api.v1.app.form.IcWateringTime;
import frameworks.models.EventLogType;
import frameworks.models.IcDataId;
import models.IcEventStats;
import models.XGenie;

public class IcEventStatsMonthly extends AbstractIcEventStats {
	
	public IcEventStatsMonthly() {}
	
	public IcEventStatsMonthly(Calendar c) {
		c.set(Calendar.DAY_OF_MONTH,
				c.getActualMinimum(Calendar.DAY_OF_MONTH));
    	setTimeToBeginOfDay(c);
    	startTime = c.getTime();
        c.set(Calendar.DAY_OF_MONTH, 
        		c.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndOfDay(c);
        endTime = c.getTime();
	}

	@Override
	public IcWateringTime[] summarizeEventStats(XGenie xGenie, byte zoneId, short offset) {
		currTime = Calendar.getInstance();
		currTime.set(Calendar.MONTH, currTime.get(Calendar.MONTH) + offset);
		
		currTime.set(Calendar.DAY_OF_MONTH,
				currTime.getActualMinimum(Calendar.DAY_OF_MONTH));
        setTimeToBeginOfDay(currTime);
        startTime = currTime.getTime();
        
        final int days = currTime.getActualMaximum(Calendar.DAY_OF_MONTH);
        currTime.set(Calendar.DAY_OF_MONTH, days);
        setTimeToEndOfDay(currTime);
        endTime = currTime.getTime();
        
        icDataId = IcDataId.DAILY;

        List<IcEventStats> lstStatsW = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERING.value, startTime, endTime);
        List<IcEventStats> lstStatsS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_STOPPED.value, startTime, endTime);
		List<IcEventStats> lstStatsWS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERSAVING.value, startTime, endTime);
		
		IcWateringTime[] log = new IcWateringTime[days];
		
		for (int i = 0; i < days; i++) {
			int watering = 0;
			int stopByManually = 0;
			int stopByWaterSaving = 0;
			
			for (Iterator<IcEventStats> itW = lstStatsW.iterator(); itW.hasNext();) {
				IcEventStats stats = itW.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_MONTH) == i + 1) {
					watering = stats.getCumulativeMinutes();
					itW.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itS = lstStatsS.iterator(); itS.hasNext();) {
				IcEventStats stats = itS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_MONTH) == i + 1) {
					stopByManually = stats.getCumulativeMinutes();
					itS.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itWS = lstStatsWS.iterator(); itWS.hasNext();) {
				IcEventStats stats = itWS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_MONTH) == i + 1) {
					stopByWaterSaving = stats.getCumulativeMinutes();
					itWS.remove();
					break;
				}
			}
			
			IcWateringTime icWateringTime = new IcWateringTime(watering, stopByManually, stopByWaterSaving);
			log[i] = icWateringTime;
		}
		
		if (offset == 0) {
			int dayOfMonth = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
			IcWateringTime icw = new IcEventStatsDaily().getCurrDateStats(xGenie, zoneId);
			IcWateringTime logIcw = log[dayOfMonth - 1];
			logIcw.setWatering(logIcw.getWatering() + icw.getWatering());
			logIcw.setStopByManually(logIcw.getStopByManually() + icw.getStopByManually());
			logIcw.setStopByWaterSaving(logIcw.getStopByWaterSaving() + icw.getStopByWaterSaving());
			log[dayOfMonth - 1] = logIcw;
		}
		
		return log;
	}
	
	public void insertMonthlyStats(String xGenieId, byte zoneId, int eventType) {
		icDataId = IcDataId.HOURLY;
		
		List<IcEventStats> lstStats = IcEventStats.find(IcDataId.toValue(IcDataId.DAILY), xGenieId, zoneId, eventType, startTime, endTime);
		
		int cumulativeMinutes = 0;
		for (IcEventStats stats : lstStats) {
			cumulativeMinutes += stats.getCumulativeMinutes();
		}
		
		if (cumulativeMinutes > 0) {
			IcEventStats icEventStatsMonthly = new IcEventStats(IcDataId.toValue(IcDataId.MONTHLY), xGenieId, zoneId, eventType, startTime, cumulativeMinutes);
			icEventStatsMonthly.save();
		}
	}
	
	public IcWateringTime getCurrMonthStats(XGenie xGenie, byte zoneId) {
		int watering = 0;
		int stopM = 0;
		int stopWS = 0;
		IcWateringTime[] icWateringTimes = summarizeEventStats(xGenie, zoneId, (short) 0);
		for (int i = 0; i < icWateringTimes.length; i++) {
			IcWateringTime icWateringTime = icWateringTimes[i];
			watering += icWateringTime.getWatering();
			stopM += icWateringTime.getStopByManually();
			stopWS += icWateringTime.getStopByWaterSaving();
		}
		
		IcWateringTime icWateringTime = new IcEventStatsDaily().getCurrDateStats(xGenie, zoneId);
		watering += icWateringTime.getWatering();
		stopM += icWateringTime.getStopByManually();
		stopWS += icWateringTime.getStopByWaterSaving();
		
		return new IcWateringTime(watering, stopM, stopWS);
	}

}
