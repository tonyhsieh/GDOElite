package controllers.api.v1.app;

import java.util.Calendar;

import controllers.api.v1.app.form.GetIftttStatusReqPack;
import controllers.api.v1.app.form.IftttStatusReqPack;
import json.models.api.v1.app.IftttRespPack;
import json.models.api.v1.app.UserRespPack;
import models.DeviceLog;
import models.SmartGenie;
import models.User;
import models.XGenie;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.BodyParser;
import play.mvc.Result;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.RemoveXGenieReqPack;
import frameworks.Constants;
import frameworks.models.EventLogType;
import frameworks.models.StatusCode;
import utils.LoggingUtils;

@PerformanceTracker
public class XGenieController extends BaseAPIController{
	@BodyParser.Of(BodyParser.AnyContent.class)
	@Transactional
	@SessionInject(RemoveXGenieReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result removeDevice(String smartGenieId, String xGenieMac){

		RemoveXGenieReqPack pack = createInputPack();

		//查詢是否有SmartGenie ID
		SmartGenie smartGenie = SmartGenie.find(smartGenieId);

		if(smartGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");

		//Logger.info("SmartGenie ID : " + smartGenie.id);
		//Logger.info("SmartGenie MAC : " + smartGenie.macAddress);

		//查詢XGenie ID是否正確
		XGenie xGenie = XGenie.findByXgMac(xGenieMac);

		if(xGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");

		//查詢SmartGenie是否相同
		if(!smartGenie.id.equals(xGenie.smartGenie.id))
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smartgenie and xgenie not march");

		//查詢Owner是否正確
		if(smartGenie.owner==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");

        User user;
        try{
             user = UserSessionUtils.getUser(pack.sessionId);
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "removeDevice session user not found " + ExceptionUtils.getStackTrace(err));
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "session user not found");
        }
        if (user != null){
            if(!smartGenie.owner.id.equals(user.id)){
                return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "smartgenie ownership error");
            }
        }




		//記錄至 device_log
		DeviceLog el = new DeviceLog();
		el.sgId = smartGenie.id;
		el.xGId = xGenie.id;

		el.eventType = EventLogType.REMOVE_DEVICE.value;

		el.eventData = Constants.EVENT_LOG_DATA_REMOVE_DEVICE;

		if(smartGenie.owner != null){
			el.ownerUserId = smartGenie.owner.id.toString();
		}
		el.save();

		xGenie.smartGenie = null;
		xGenie.name = null;
		xGenie.status = StatusCode.XGENIE_REMOVED.value;
		xGenie.save();

		UserRespPack respPack = new UserRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}

    @BodyParser.Of(BodyParser.AnyContent.class)
    @Transactional
    @SessionInject(IftttStatusReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result iftttStatus(){
        IftttStatusReqPack pack = createInputPack();
        User user = getUser();
        //查詢XGenie ID是否正確
        XGenie xGenie = XGenie.findByXgMac(pack.xGenieMac);
        if(xGenie == null)
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        //查詢是否有SmartGenie ID
        SmartGenie smartGenie = xGenie.smartGenie;
        if(smartGenie == null)
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");
        //查詢Owner是否正確
        if(smartGenie.owner == null)
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");
        if(user != null){
            if(!smartGenie.owner.id.equals(user.id))
                return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "smartgenie ownership error");
        }else{
            return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "user not found error");
        }
        if(pack.iFTTTEnabled != null){
            xGenie.iftttStatus = pack.iFTTTEnabled;
            try{
                xGenie.update();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "update ifttt status error " + ExceptionUtils.getStackTrace(e));
            }
        }

        IftttRespPack respPack = new IftttRespPack();
        respPack.iFTTTEnabled = xGenie.iftttStatus;
        return createJsonResp(respPack);
    }


    @Transactional
    @BodyParser.Of(BodyParser.AnyContent.class)
    @SessionInject(GetIftttStatusReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result getIftttStatus(){
        GetIftttStatusReqPack pack = createInputPack();
        //查詢XGenie ID是否正確
        XGenie xGenie = XGenie.findByXgMac(pack.xGenieMac);
        if(xGenie == null)
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
        //查詢是否有SmartGenie ID
        SmartGenie smartGenie = xGenie.smartGenie;
        if(smartGenie == null)
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");

        IftttRespPack respPack = new IftttRespPack();
        respPack.iFTTTEnabled = xGenie.iftttStatus;
        return createJsonResp(respPack);
    }






}
