package controllers.api.v1.app;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Calendar;
import java.util.UUID;
import java.util.regex.Pattern;

import frameworks.models.FileType;
import json.models.api.v1.app.GetSgOwnershipByMacRespPack;
import json.models.api.v1.app.SmartGenieNetworkRespPack;
import json.models.api.v1.app.UpdateNetworkInfoRespPack;
import json.models.api.v1.app.UserRespPack;
import json.models.api.v1.app.SessionRespPack;
import json.models.api.v1.app.vo.SmartGenieNetworkInfoVO;
import models.*;


import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.Http;
import play.mvc.Result;
import services.UserRelationshipService;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.GetNetworkInfoReqPack;
import controllers.api.v1.app.form.GetSgOwnershipByMacReqPack;
import controllers.api.v1.app.form.SetSgLocationReqPack;
import controllers.api.v1.app.form.SetSmartGenieReqPack;
import controllers.api.v1.app.form.UpdatNetworkInfo;
import frameworks.models.StatusCode;
import utils.ConfigUtil;
import utils.S3Manager;
import utils.db.FileEntityManager;
import utils.ExceptionUtil;
import utils.LoggingUtils;

@PerformanceTracker
public class SmartGenieInfoController extends BaseAPIController{

	@SessionInject(GetSgOwnershipByMacReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result getSgOwnershipByMac(String macAddr){
		GetSgOwnershipByMacReqPack pack = createInputPack();

		String userId = getUser().id.toString();

		//是否有MacAddr的記錄
		SmartGenie smartGenie = SmartGenie.findByMacAddress(macAddr);
		if(smartGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");

		GetSgOwnershipByMacRespPack respPack = new GetSgOwnershipByMacRespPack();

		//是否有Owner
		UserRelationship userRelationship = UserRelationshipService.findByMemberIdNSmartGenie(userId, smartGenie);

		if(userRelationship==null){

			//判斷此SmartGenie是否已經被Pairing過,並且已經有Owner了
			User user = smartGenie.owner;
			if(user==null){
				respPack.hasOwner = false;
			}else{
				respPack.hasOwner = true;
			}

			respPack.ownership = false;
			respPack.usership = false;
		}else{
			//user和sg關係
			respPack.hasOwner = true;

			if(userRelationship.ownerId.equals(userId)){
				respPack.ownership = true;
				respPack.usership = false;
			}
			else if(userRelationship.userId.equals(userId)){
				respPack.ownership = false;
				respPack.usership = true;
			}
			else{
				respPack.ownership = false;
				respPack.usership = false;
			}
		}

		//如果userRelationship==null,有可能是還沒有和user建立關係,但是已經有owner了
		try{
			if(smartGenie.owner.id.toString().equals(userId)){
				respPack.hasOwner = true;
				respPack.ownership = true;
				respPack.usership = false;
			}
		}
		catch(NullPointerException err){}

		respPack.sessionId = pack.sessionId;
		
		respPack.macAddr = macAddr;

		return createJsonResp(respPack);
	}

	@Transactional
	@SessionInject(SetSmartGenieReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result setSmartGenieInfo(String smartGenieId){

		SetSmartGenieReqPack pack = createInputPack();

		User user = getUser();

		//檢查smartGenieId是否正確
		SmartGenie smartGenie = SmartGenie.find(smartGenieId);

		if(smartGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");

		//檢查smartGenie 是否有 owner
		if(smartGenie.owner==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");

		//簡查smartGenie Owner是否正確
		if(!smartGenie.owner.id.equals(user.id))
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "smartgenie ownership error");

		//檢查是否有設定xGenie
		if(!"".equals(pack.xGenieId)){

			//檢查xGenie是否正確
			XGenie xGenie = XGenie.findByXgMac(pack.xGenieId);

			if(xGenie==null)
				return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");

			//若有,檢查smartGenie及xGenie是否相配
			if(!smartGenie.id.equals(xGenie.smartGenie.id))
				return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smartgenie and xgenie not march");

			//xGenieName是否為null,否->寫入
			try{
				pack.xGenieName = pack.xGenieName.trim();
			}catch(Exception err){
				
			}
			
			if(!"".equals(pack.xGenieName)){
				xGenie.name = pack.xGenieName;
				xGenie.save();
			}
		}

		//smartGenieName是否為null,否->寫入
		try{
			pack.smartGenieName = pack.smartGenieName.trim();
		}catch(Exception err){
			
		}
		
		if(!"".equals(pack.smartGenieName)){
			smartGenie.name = pack.smartGenieName;
			smartGenie.save();
		}

		UserRespPack respPack = new UserRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}

    @Transactional
    @SessionInject(SetSmartGenieReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result uploadPhoto(String sgMAC) {

        SetSmartGenieReqPack pack = createInputPack();

        User user = getUser();

        // make sure the smart genie exists
        SmartGenie smartGenie = SmartGenie.findByMacAddress(sgMAC);

        if (smartGenie == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");
        }

        // make sure the smart genie has owner
        if (smartGenie.owner == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");
        }

        // the current user should be the owner of the smart genie
        if (!smartGenie.owner.id.equals(user.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "smartgenie ownership error");
        }

        if (request().body().asRaw().size() == 0) {
            return createIndicatedJsonStatusResp(StatusCode.WRONG_PARAMETER, "file not found");
        }
        java.io.File file = request().body().asRaw().asFile();
        if(smartGenie.photoFileId != null){
            try {
                FileEntityManager.deleteS3file(smartGenie.photoFileId.s3Path, FileType.SG_PHOTO);
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie delete s3 error " + ExceptionUtils.getStackTrace(e));
            }
            try {
                String filename = UUID.randomUUID().toString();
                FileEntityManager feo = new FileEntityManager(file, filename, FileType.SG_PHOTO, null);
                smartGenie.photoFileId = feo.fileEntity;
                smartGenie.update();
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie delete s3 error " + ExceptionUtils.getStackTrace(e));
            } finally {
                file.delete();
            }
        }else{
            try {
                String filename = UUID.randomUUID().toString();
                FileEntityManager feo = new FileEntityManager(file, filename, FileType.SG_PHOTO, null);
                smartGenie.photoFileId = feo.fileEntity;
                smartGenie.update();
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie delete s3 error " + ExceptionUtils.getStackTrace(e));
            } finally {
                file.delete();
            }
        }

        SessionRespPack respPack = new SessionRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }


    @Transactional
    @SessionInject(SetSmartGenieReqPack.class)
    @MobileLogInject(BaseReqPack.class)
    public Result deletePhoto(String homeExtenderMac) {

        SetSmartGenieReqPack pack = createInputPack();

        User user = getUser();

        // make sure the smart genie exists
        SmartGenie smartGenie = SmartGenie.findByMacAddress(homeExtenderMac);

        if (smartGenie == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smartgenie not found");
        }

        // make sure the smart genie has owner
        if (smartGenie.owner == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");
        }

        // the current user should be the owner of the smart genie
        if (!smartGenie.owner.id.equals(user.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_ERROR, "smartgenie ownership error");
        }
        if(smartGenie.photoFileId == null){
            return createIndicatedJsonStatusResp(StatusCode.FILE_NOTFOUND, "smartgenie no snapshot error");
        }
        try {
            FileEntityManager.deleteS3file(smartGenie.photoFileId.s3Path, FileType.SG_PHOTO );
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie delete s3 error " + ExceptionUtils.getStackTrace(e));
            return createIndicatedJsonStatusResp(StatusCode.FILE_DELETE_ERROR, "file delete from s3 error");
        }
        try{
            smartGenie.photoFileId = null;
            smartGenie.update();
        }catch (Exception err){
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smart genie delete save error " + ExceptionUtils.getStackTrace(err));
        }

        SessionRespPack respPack = new SessionRespPack();
        respPack.sessionId = pack.sessionId;
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }








































	@SessionInject(GetNetworkInfoReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result getNetworkInfo(){
		GetNetworkInfoReqPack pack = createInputPack();
		//Logger.info(pack.toString());

		boolean isAtLan;

		User user = getUser();

		//是否有MacAddr的記錄
		SmartGenie smartGenie = SmartGenie.findByMacAddress(pack.sgMac);
		if(smartGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");

		SmartGenieLog smartGenieLog = SmartGenieLog.find(smartGenie.id);
		if(smartGenieLog==null)
			smartGenieLog = new SmartGenieLog();

		//Get Mobile external IP address
		Http.RequestHeader httpservletrequest = request();

		if (httpservletrequest == null)
	        return null;
	    String extIP = httpservletrequest.getHeader("X-Forwarded-For");
	    if (extIP == null || extIP.length() == 0 || "unknown".equalsIgnoreCase(extIP))
	    	extIP = httpservletrequest.getHeader("Proxy-Client-IP");
	    if (extIP == null || extIP.length() == 0 || "unknown".equalsIgnoreCase(extIP))
	    	extIP = httpservletrequest.getHeader("WL-Proxy-Client-IP");
	    if (extIP == null || extIP.length() == 0 || "unknown".equalsIgnoreCase(extIP))
	    	extIP = httpservletrequest.getHeader("HTTP_CLIENT_IP");
	    if (extIP == null || extIP.length() == 0 || "unknown".equalsIgnoreCase(extIP))
	    	extIP = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
	    if (extIP == null || extIP.length() == 0 || "unknown".equalsIgnoreCase(extIP))
	    	extIP = httpservletrequest.remoteAddress();
	    if ("127.0.0.1".equals(extIP) || "0:0:0:0:0:0:0:1".equals(extIP))
	        try {
	        	extIP = InetAddress.getLocalHost().getHostAddress();
	        }
	        catch (UnknownHostException unknownhostexception) {
	        }

	    //compare Smart Genie and Mobile external IP address
	    if(extIP.equals(smartGenieLog.extIp)){
	    	isAtLan = true;
	    }else{
	    	isAtLan = false;
	    }
	    
	    
		//AND mobile and Smart Genie networking information
		if(isAtLan){
			Pattern pat;
			pat = Pattern.compile("[.]");
			String[] strSgIP = pat.split(smartGenieLog.innIp);
			String[] strSgNetmask = pat.split(smartGenieLog.netMask);
			String[] strMobileIP = pat.split(pack.innIp);
			String[] strMobileNetmask = pat.split(pack.netMask);

			int[] iSgIP = new int[strSgIP.length];
			int[] iSgNetmask = new int[strSgNetmask.length];
			int[] iMobileIP = new int[strMobileIP.length];
			int[] iMobileNetmask = new int[strMobileNetmask.length];

			for(int i=0; i<strSgIP.length; i++){
				iSgIP[i] = Integer.parseInt(strSgIP[i]);
				iSgNetmask[i] = Integer.parseInt(strSgNetmask[i]);
				iMobileIP[i] = Integer.parseInt(strMobileIP[i]);
				iMobileNetmask[i] = Integer.parseInt(strMobileNetmask[i]);
			}

			//compare
			for(int i=0; i<strSgIP.length; i++){
				if((iSgIP[i] & iSgNetmask[i]) != (iMobileIP[i] & iMobileNetmask[i])){
					isAtLan = false;
					break;
				}
			}
		}
		
		
		/*
		Logger.error("***********************************************************");
		Logger.error("***********************************************************");
	    Logger.error("user : " + user.email);
	    Logger.error("extIp : " + extIP);
	    Logger.error("innIp : " + pack.innIp);
	    Logger.error("MobileNetmask : " + pack.netMask);
	    
	    Logger.error("***********************************************************");
	    
	    Logger.error("MAC : " + pack.sgMac);
	    Logger.error("extIp : " + smartGenieLog.extIp);
	    Logger.error("innerIp : " + smartGenieLog.innIp);
	    Logger.error("netMask : " + smartGenieLog.netMask);
	    Logger.error("***********************************************************");
	    Logger.error("***********************************************************");
	    */
		

		SmartGenieNetworkRespPack respPack = new SmartGenieNetworkRespPack();

		SmartGenieNetworkInfoVO infoVO = new SmartGenieNetworkInfoVO();

		infoVO.isAtLan = isAtLan;

		infoVO.smartGenieInnerIP = smartGenieLog.innIp;

		infoVO.smartGenieNetmask = smartGenieLog.netMask;

		infoVO.wifiMacAddress = smartGenieLog.wifiMacAddress;

		infoVO.sgMac = pack.sgMac;

		respPack.sessionId = pack.sessionId;

		respPack.networkInfo = infoVO;

		return createJsonResp(respPack);
	}
	
	@SessionInject(UpdatNetworkInfo.class)
	@MobileLogInject(BaseReqPack.class)
	public Result updateNetworkInfo(){
		UpdatNetworkInfo pack = createInputPack();

		String userId = getUser().id.toString();

		//是否有MacAddr的記錄
		SmartGenie smartGenie = SmartGenie.findByMacAddress(pack.sgMac);
		if(smartGenie==null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");

		SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);
		
		if(pack.p2pSuccess){
			
			int iRet = 0;
			 
			iRet = sgLog.p2pSuccess;
			
			iRet++;
			sgLog.p2pSuccess = iRet;
			
		}else{
			int iRet = 0; 

			iRet = sgLog.p2pFail;
			
			iRet++;
			sgLog.p2pFail = iRet;
		}
		
		sgLog.save();
		
		UpdateNetworkInfoRespPack respPack = new UpdateNetworkInfoRespPack();

		respPack.sessionId = pack.sessionId;

		return createJsonResp(respPack);
	}
	
	@SessionInject(SetSgLocationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public Result setSgLocation(){
		SetSgLocationReqPack pack = createInputPack();
		//Coordinate sgCord = null;
		User user = getUser();
		//是否有MacAddr的記錄
		SmartGenie smartGenie = SmartGenie.findByMacAddress(pack.macAddr);
		
		if(smartGenie == null)
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");

        /*
        //Get External IP Address
        Http.RequestHeader httpservletrequest = request();
        if (httpservletrequest == null)
           return null;
        String s = httpservletrequest.getHeader("X-Forwarded-For");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("WL-Proxy-Client-IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_CLIENT_IP");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.getHeader("HTTP_X_FORWARDED_FOR");
        if (s == null || s.length() == 0 || "unknown".equalsIgnoreCase(s))
            s = httpservletrequest.remoteAddress();
        if ("127.0.0.1".equals(s) || "0:0:0:0:0:0:0:1".equals(s)) {
            try {
              s = InetAddress.getLocalHost().getHostAddress();
            } catch (UnknownHostException unknownhostexception) {
                unknownhostexception.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "setsglocation IP failed " + ExceptionUtil.exceptionStacktraceToString(unknownhostexception));
            }
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "setsglocation IP " + s);

		SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);
        if(pack.loclat != null && pack.loclng != null){
            if(!pack.loclat.equals("null") && !pack.loclng.equals("null") && !pack.loclat.equals(0) && !pack.loclng.equals(0) ){
                sgLog.lat = pack.loclat;
                sgLog.lng = pack.loclng;
            }else{
                sgCord = GeoLocationUtil.getCoordinate(s);
                sgLog.lat = String.valueOf(sgCord.lat);
                sgLog.lng = String.valueOf(sgCord.lng);
            }
        }else{
			sgCord = GeoLocationUtil.getCoordinate(s);
			sgLog.lat = String.valueOf(sgCord.lat);
			sgLog.lng = String.valueOf(sgCord.lng);
        }
          */

        SmartGenieLog sgLog = SmartGenieLog.find(smartGenie.id);
        if(pack.loclat != null && pack.loclng != null)
        {
			sgLog.lat = pack.loclat;
			sgLog.lng = pack.loclng;
			try{
				sgLog.save();
			}catch(Exception err){
				err.printStackTrace();
				LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "setsglocation save failed " + ExceptionUtil.exceptionStacktraceToString(err));
				sgLog = null;
				smartGenie = null;
				user = null;
				return createIndicatedJsonStatusResp(StatusCode.CAN_NOT_WRITE_TO_DATABASE, "can not write to db");
			}
        }else{
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "setsglocation request is null ");
		}

		UpdateNetworkInfoRespPack respPack = new UpdateNetworkInfoRespPack();
		respPack.sessionId = pack.sessionId;
		return createJsonResp(respPack);
	}
}
