package controllers.api.v1.app;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import controllers.api.v1.app.form.IcWateringTime;
import frameworks.models.EventLogType;
import frameworks.models.IcDataId;
import models.IcEventStats;
import models.XGenie;

public class IcEventStatsYearly extends AbstractIcEventStats {

	private final int logNum = 12;
	
	@Override
	public IcWateringTime[] summarizeEventStats(XGenie xGenie, byte zoneId, short offset) {
		currTime = Calendar.getInstance();
		currTime.set(Calendar.YEAR, currTime.get(Calendar.YEAR) + offset);
		
		currTime.set(Calendar.MONTH, Calendar.JANUARY);
		currTime.set(Calendar.DAY_OF_MONTH,
				currTime.getActualMinimum(Calendar.DAY_OF_MONTH));	
        setTimeToBeginOfDay(currTime);
        startTime = currTime.getTime();
        
        currTime.set(Calendar.MONTH, Calendar.DECEMBER);
        currTime.set(Calendar.DAY_OF_MONTH,
        		currTime.getActualMaximum(Calendar.DAY_OF_MONTH));
        setTimeToEndOfDay(currTime);
        endTime = currTime.getTime();
        
        icDataId = IcDataId.MONTHLY;

        List<IcEventStats> lstStatsW = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERING.value, startTime, endTime);
        List<IcEventStats> lstStatsS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_STOPPED.value, startTime, endTime);
		List<IcEventStats> lstStatsWS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERSAVING.value, startTime, endTime);
		
		IcWateringTime[] log = new IcWateringTime[logNum];
		
		for (int i = 0; i < logNum; i++) {
			int watering = 0;
			int stopByManually = 0;
			int stopByWaterSaving = 0;
			
			for (Iterator<IcEventStats> itW = lstStatsW.iterator(); itW.hasNext();) {
				IcEventStats stats = itW.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.MONTH) == i) {
					watering = stats.getCumulativeMinutes();
					itW.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itS = lstStatsS.iterator(); itS.hasNext();) {
				IcEventStats stats = itS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.MONTH) == i) {
					stopByManually = stats.getCumulativeMinutes();
					itS.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itWS = lstStatsWS.iterator(); itWS.hasNext();) {
				IcEventStats stats = itWS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.MONTH) == i) {
					stopByWaterSaving = stats.getCumulativeMinutes();
					itWS.remove();
					break;
				}
			}
			
			IcWateringTime icWateringTime = new IcWateringTime(watering, stopByManually, stopByWaterSaving);
			log[i] = icWateringTime;
		}
		
		if (offset == 0) {
			int month = Calendar.getInstance().get(Calendar.MONTH);
			IcWateringTime icw = new IcEventStatsMonthly().getCurrMonthStats(xGenie, zoneId);
			IcWateringTime logIcw = log[month];
			logIcw.setWatering(logIcw.getWatering() + icw.getWatering());
			logIcw.setStopByManually(logIcw.getStopByManually() + icw.getStopByManually());
			logIcw.setStopByWaterSaving(logIcw.getStopByWaterSaving() + icw.getStopByWaterSaving());
			log[month] = logIcw;
		}
		
		return log;
	}

}
