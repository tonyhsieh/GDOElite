package controllers.api.v1.app;

import java.util.UUID;

import json.models.api.v1.app.AuthRespPack;
import models.PnRelationship;
import models.User;
import models.UserIdentifier;

import org.apache.commons.lang3.exception.ExceptionUtils;

import play.mvc.Result;
import services.UserDevLogService;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.app.form.LoginReqPack;
import frameworks.exceptions.FormDataValidationException;
import frameworks.models.StatusCode;
import frameworks.models.UserIdentifierType;
import utils.LoggingUtils;

/**
 * @author john
 */

@PerformanceTracker
public class LoginController extends BaseAPIController
{
    @Transactional
    @ParameterInject(LoginReqPack.class)
    public Result doLogin() throws FormDataValidationException
    {
        LoginReqPack pack = createInputPack();

        //identifierType value check
        UserIdentifierType identType;

        try
        {
            identType = UserIdentifierType.valueOf(pack.loginType.toUpperCase());
        }
        catch (IllegalArgumentException e)
        {
            return badRequest();
        }

        //檢查 ui 是否存在
        UserIdentifier ui = UserIdentifier.getUserIdentifier(pack.loginId.toLowerCase(), identType);

        if (ui == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "no user found");
        }

        //密碼比對
        if (needCheckedPwd(identType))
        {
            if (!ui.password.equals(UserIdentifier.pwdHash(pack.loginPwd)))
            {
                return createIndicatedJsonStatusResp(StatusCode.USER_PASSWORD_ERROR, "authenticated error");
            }
        }

        //UserDevLogService.recordUserDevLog(pack);

        // create sessionId
        String sessionId = loginSession(pack, ui.user);
        if (sessionId == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.SESSION_ERROR, "create session failure");
        }

        //檢查是否有 device Id (Apple ID or IMEI)
        if (pack.imei != null && !"".equals(pack.imei))
        {
            for (PnRelationship pn : PnRelationship.listByDevId(pack.imei))
            {
                try
                {
                    pn.delete();
                }
                catch (Exception err)
                {
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Log in delete PnRelationship " + ExceptionUtils.getStackTrace(err));
                }
            }
        }

        //如果是沒有 device id的資料
        PnRelationship oldPn = null;
        try
        {
            oldPn = Ebean.find(PnRelationship.class).where().eq("pnId", pack.pnId).findUnique();
        }
        catch (Exception err)
        {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Log in find pnId in PnRelationship " + ExceptionUtils.getStackTrace(err));
        }

        if (oldPn == null)
        {
            oldPn = new PnRelationship();
        }
        oldPn.pn = pack.pnId;
        oldPn.user = ui.user;
        oldPn.pnId = pack.pnId;
        oldPn.devId = pack.imei;
        oldPn.deviceOsName = pack.deviceOsName.toLowerCase();
        try
        {
            oldPn.appCode = pack.appCode;
        }
        catch (Exception err)
        {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Log in addCode in PnRelationship " + ExceptionUtils.getStackTrace(err));
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "Log in addCode in PnRelationship save pn" + oldPn.toString());

        try
        {
            if (pack.pnId == null || pack.pnId.equals("11111111") || pack.pnId.equals("1234567890"))
            {
                // 不應該是預設值
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Invalid pnId: " + pack.pnId);
            }
            else
            {
                oldPn.save();
            }
        }
        catch (Exception err)
        {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Log in save in PnRelationship " + ExceptionUtils.getStackTrace(err));
        }

        //寫入 UserDevLog,因為這裡才會有 session id產生
        UserDevLogService.recordUserDevLog(ui.user, pack, sessionId);

        AuthRespPack respPack = new AuthRespPack();
        respPack.sessionId = sessionId;

        return createJsonResp(respPack);
    }

    private static String loginSession(LoginReqPack pack, User user)
    {
        //sessionId initial
        String sessionId = UUID.randomUUID().toString();

        //insert session in cache
        return UserSessionUtils.login(sessionId, pack.pnId, user.id.toString()) ? sessionId : null;
    }

    private static boolean needCheckedPwd(UserIdentifierType identType)
    {
        if (identType.ordinal() == UserIdentifierType.APPLE.ordinal())
            return false;
        if (identType.ordinal() == UserIdentifierType.FACEBOOK.ordinal())
            return false;
        if (identType.ordinal() == UserIdentifierType.GOOGLE.ordinal())
            return false;
        if (identType.ordinal() == UserIdentifierType.TWITTER.ordinal())
            return false;
        return true;
    }
}
