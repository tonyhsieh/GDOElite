package controllers.api.v1.app;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;

import play.data.FormFactory;

import play.data.Form;
import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;
import services.UserDevLogService;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.form.BaseReqPack;
import utils.LoggingUtils;

public class MobileLogAction extends Action<MobileLogInject>{
/*
 * (non-Javadoc)
 * @see play.mvc.Action#call(play.mvc.Http.Context)
 * By Jack Chuang
 * 2013 05 24
 */

    @Inject FormFactory formFactory;
	@Override
	public CompletionStage<Result> call(Context ctx) {

		BaseReqPack pack;

		Form<? extends BaseReqPack> form = null;

		try{
			form = formFactory.form(configuration.value()).bindFromRequest(ctx.request());
			pack = form.get();
			//Http.Context.current().args.put(Constants.API_V1_APP_REQ_LOG_HTTP_ARGS_KEY, pack);

			//UserDevLogService.recordUserDevLog(pack);
		} catch (IllegalStateException e){

			if( form != null ){
				LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");

				for( String key : form.errors().keySet() ){
					LoggingUtils.log(LoggingUtils.LogLevel.WARN, form.errors().get(key).toString());
				}
				LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
			}

			return CompletableFuture.completedFuture(badRequest("Bad Request"));
		}

		return delegate.call(ctx);
	}

}
