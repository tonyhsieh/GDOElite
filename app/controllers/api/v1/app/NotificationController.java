package controllers.api.v1.app;

import java.util.ArrayList;
import java.util.List;

import json.models.api.v1.app.NotificationRespPack;
import json.models.api.v1.app.vo.MsgVO;
import models.Message;
import models.User;
import play.mvc.Result;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.SessionReqPack;

@PerformanceTracker
public class NotificationController extends BaseAPIController {

	// /api/app/msg/unread
	@SessionInject(SessionReqPack.class)
	public Result getNotification(){

		SessionReqPack pack = createInputPack();

		User user = getUser();

		List<Message> messageList = Message.listByUser(user.id.toString(), false);

		List<MsgVO> listMsg = new ArrayList<MsgVO>(messageList.size());

		for(Message msg:messageList){
			MsgVO msgVO = new MsgVO();
			msgVO.message = msg.content;
			msgVO.messageTimeStamp = msg.sendAt;

			listMsg.add(msgVO);
		}

		NotificationRespPack respPack = new NotificationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.listMsg = listMsg;

		return createJsonResp(respPack);
	}
}
