/**
 *
 */
package controllers.api.v1.app;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;



import play.data.Form;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;
import play.data.FormFactory;


import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.form.BaseReqPack;
import frameworks.Constants;
import utils.ExceptionUtil;
import utils.LoggingUtils;

/**
 * @author carloxwang
 *
 */
public class ParameterInjectAction extends Action<ParameterInject> {

	@Inject FormFactory formFactory;
	@Override
	public  CompletionStage<Result> call(Context ctx)  {
		BaseReqPack pack;

		Form<? extends BaseReqPack> form = null;

		try{
			form = formFactory.form(configuration.value()).bindFromRequest(ctx.request());
			pack = form.get();
			Http.Context.current().args.put(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY, pack);
			LoggingUtils.log(LoggingUtils.LogLevel.INFO, "app parameter inject form " + form.toString() + "configuration" + configuration.value() + "ctx " + ctx.request());

		} catch (Exception e){
			e.printStackTrace();
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "app Parameter inject failed " + ExceptionUtil.exceptionStacktraceToString(e));
			if( form != null ){
				LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");
				for( String key : form.errors().keySet() ){
                    LoggingUtils.log(LoggingUtils.LogLevel.WARN, form.errors().get(key).toString());
				}
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
			}

			return CompletableFuture.completedFuture(badRequest("Bad Request"));
		}

		return delegate.call(ctx);
	}

}
