package controllers.api.v1.app;

import com.avaje.ebean.Ebean;
import com.avaje.ebean.annotation.Transactional;
import com.fasterxml.jackson.core.type.TypeReference;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.*;
import controllers.api.v2.sg.form.ListRemoveConfigurationReqPack;
import controllers.api.v2.sg.form.ListRemoveXGenieReqPack;
import frameworks.models.StatusCode;
import json.models.api.v1.app.AuthRespPack;
import json.models.api.v1.app.vo.AlertSettingVO;
import json.models.api.v1.app.vo.EventContactData;
import models.*;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;
import play.mvc.Result;
import utils.LoggingUtils;
import utils.generator.CredentialGenerator;

import java.util.*;

public class AlertSettingController extends BaseAPIController
{
    private final static Integer[] GDOEventCodes = {10100, 10101, -10101};
    private final static Integer[] SDEventCodes = {-10150, -10151, -10152};

    @SessionInject(AlertReqPack.class)
    public Result getAlertSetting(final String userId) {
        AlertReqPack alertReqPack = createInputPack();
        SmartGenie sg = Ebean.find(SmartGenie.class, CredentialGenerator.genSmartGenieId(alertReqPack.homeExtenderMac));
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "no such smart genie");
        }
        List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipByOwnerNUser(sg.owner.id.toString(), userId);
        if (listUserRelationship.isEmpty()) {
            if(!userId.equals(getUser().id.toString())){
                return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "user not found");
            }
        }

        // to sort by xGenie name
        Comparator<XGenie> nameComparator = new Comparator<XGenie>()
        {
            @Override
            public int compare(XGenie x1, XGenie x2)
            {
                return x1.name.compareTo(x2.name);
            }
        };

        List<XGenie> xGenieList = XGenie.listXGBySG(sg);
        Collections.sort(xGenieList, nameComparator);

        AlertResultRespPack alertResultRespPack = new AlertResultRespPack();
        alertResultRespPack.sessionId = alertReqPack.sessionId;

        // for GDO
        List<XGenie> xgGDOList = new ArrayList<>();
        int[] xgGDOTypes = {1, 100102};
        for (XGenie xg : xGenieList)
        {
            for (int type : xgGDOTypes)
            {
                if (xg.deviceType == type)
                {
                    xgGDOList.add(xg);
                }
            }
        }
        alertResultRespPack.eventGDO = getAlertResult(userId, GDOEventCodes, xgGDOList);

        // for SD
        List<XGenie> xgSDList = new ArrayList<>();
        int[] xgSDTypes = {3};
        for (XGenie xg : xGenieList)
        {
            for (int type : xgSDTypes)
            {
                if (xg.deviceType == type)
                {
                    xgSDList.add(xg);
                }
            }
        }
        alertResultRespPack.eventSD = getAlertResult(userId, SDEventCodes, xgSDList);

        return createJsonResp(alertResultRespPack);
    }

    /**
     * get the list with all status codes and all XGenies
     *
     * @param userId     userId
     * @param eventCodes event  codes array
     * @param xGenieList XGenie list
     */
    private static List<AlertResultRespPack.AlertEventItem> getAlertResult(final String userId, final Integer[] eventCodes,
                                                                           final List<XGenie> xGenieList)
    {
        List<AlertResultRespPack.AlertEventItem> result = new ArrayList<>();

        for  (XGenie xg : xGenieList)
        {
            AlertResultRespPack.AlertEventItem alertEventItem = new AlertResultRespPack.AlertEventItem();
            alertEventItem.event = new ArrayList<>();
            alertEventItem.name = xg.name;
            alertEventItem.xGenieMac= xg.macAddr;
            for (int code : eventCodes)
            {
                AlertResultRespPack.AlertDevMacItem alertDevMacItem = new AlertResultRespPack.AlertDevMacItem();
                alertDevMacItem.type = code;
                alertDevMacItem.email= false;
                alertDevMacItem.text = false;
                alertDevMacItem.push = false;

                EventContactData ecd = getEventContact(userId, xg.macAddr, code);
                if(ecd != null){
                    alertDevMacItem.email = ecd.checkEmail;
                    alertDevMacItem.text = ecd.checkSMS;
                    alertDevMacItem.push = ecd.checkNotification;
                }
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "alert dev mac item " + alertDevMacItem.toString() + "alerteventitem" + alertEventItem.toString() );
                alertEventItem.event.add(alertDevMacItem);
            }

            result.add(alertEventItem);
        }

        return result;
    }

    // from -> ContactController.getUserContactEvent(userId)
    private static EventContactData getEventContact(final String userId, final String xGenieMAC, int eventType)
    {
        EventContactData ecData = null;
        EventContact ec = EventContact.findByMacAndEventType(xGenieMAC, eventType);
        if(ec != null){
            Contact c = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, userId));
            if (c != null)
            {
                ecData = new EventContactData();
                ecData.eventType = ec.eventType;
                ecData.checkEmail = c.notifyEmail;
                ecData.checkNotification = c.notifyPushNotification;
                ecData.checkSMS = c.notifySMS;
            }
        }
        return ecData;
    }

    @Transactional
    @SessionInject(AlertReqPack.class)
    public Result setAlertSetting(final String homeExtenderMac) {
        AlertReqPack alertReqPack = createInputPack();
        SmartGenie sg = SmartGenie.findByMacAddress(homeExtenderMac);
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "no such smart genie");
        }
        List<UserRelationship> listUserRelationship = UserRelationship.listUserRelationshipByOwnerNUser(sg.owner.id.toString(), alertReqPack.userId);
        if (listUserRelationship.isEmpty()) {
            if(!alertReqPack.userId.equals(sg.owner.id.toString())){
                return createIndicatedJsonStatusResp(StatusCode.USER_NOTFOUND, "user not found");
            }
        }
        List<AlertSettingVO> alertSettingVO;
        try
        {
            alertSettingVO = new ObjectMapper().readValue(alertReqPack.params, new TypeReference<List<AlertSettingVO>>(){});
        } catch (Exception e)
        {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "set alert setting deserialize error" + ExceptionUtils.getStackTrace(e));
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "json decoding error");
        }
        if (alertSettingVO == null)
        {
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "json content decoding error");
        }
        List<XGenie> xGenieList = XGenie.listXGBySG(sg);
        if(xGenieList == null){
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "sg has no xg error");
        }
        List<String> macList = new ArrayList<>();
        for(XGenie xg: xGenieList){
            macList.add(xg.macAddr);
        }
        List<EventContactData> ecDataList = new ArrayList<>();
        for (AlertSettingVO aei : alertSettingVO) {
            try{
                for (AlertResultRespPack.AlertDevMacItem admi : aei.event)
                {
                    if (macList.contains(aei.xGenieMac) && (Arrays.asList(GDOEventCodes).contains(admi.type) || Arrays.asList(SDEventCodes).contains(admi.type) ))
                    {
                        EventContactData ecd = new EventContactData();
                        ecd.eventType = admi.type;
                        ecd.checkEmail = admi.email;
                        ecd.checkSMS = admi.text;
                        ecd.checkNotification = admi.push;
                        ecDataList.add(ecd);

                    }else{
                        return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, " xgenie or alert setting not match");
                    }
                }
                addEventContact(alertReqPack.userId, aei.xGenieMac, ecDataList);
            } catch (Exception e) {
                try {
                    StatusCode sc = StatusCode.valueOf(e.getMessage());
                    switch (sc) {
                        case SMARTGENIE_XGENIE_NOTMARCH:
                            return createIndicatedJsonStatusResp(sc, aei.xGenieMac + " -> ownership error");
                        case XGENIE_NOTFOUND:
                            return createIndicatedJsonStatusResp(sc, aei.xGenieMac + " -> not found error");
                        case USER_RELATIONSHIP_ERROR:
                            return createIndicatedJsonStatusResp(sc, aei.xGenieMac + " -> relationship error");
                    }
                } catch (Exception ex) {
                    ex.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "set alert setting response error" + ExceptionUtils.getStackTrace(ex));
                    return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "unknown error");
                }

            }
        }

        AuthRespPack respPack = new AuthRespPack();
        respPack.sessionId = alertReqPack.sessionId;

        return createJsonResp(respPack);
    }

    // from -> ContactController.addEventContact(macAddr)
    private static void addEventContact(final String userId, final String macAddr, final List<EventContactData> ecDataList) throws Exception
    {
        String _userId = userId;
        User appUser = getUser();

        SmartGenie sg = null;
        XGenie xGenie = null;
        String macAddress = "";

        try
        {
            sg = SmartGenie.findByMacAddress(macAddr);
        } catch (Exception e)
        {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact sg find error" + ExceptionUtils.getStackTrace(e));
        }

        try
        {
            xGenie = XGenie.findByXgMac(macAddr);
        } catch (Exception e)
        {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact xg find error" + ExceptionUtils.getStackTrace(e));
        }

        if (xGenie != null) {
            macAddress = xGenie.macAddr;
            sg = xGenie.smartGenie;

            //xGenie's owner check
            if (!sg.owner.id.toString().equals(appUser.id.toString()))
            {
                throw new Exception(StatusCode.SMARTGENIE_XGENIE_NOTMARCH.name());
            }
        } else if (sg != null)
        {
            macAddress = sg.macAddress;
        }

        if (xGenie == null && sg == null)
        {
            throw new Exception(StatusCode.XGENIE_NOTFOUND.name());
        }

        //the invitees is owner or user?
        if (StringUtils.isNotBlank(_userId)) {
            //is User
            UserRelationship urs;

            try
            {
                urs =
                        Ebean.find(
                                UserRelationship.class,
                                UserRelationship.genUserRelationshipId(
                                        sg.id,
                                        appUser.id.toString(),
                                        _userId));
            } catch (Exception e)
            {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact find user error" + ExceptionUtils.getStackTrace(e));
                throw new Exception(StatusCode.USER_RELATIONSHIP_ERROR.name());
            }

            if (urs == null)
            {
                //is Owner
                _userId = appUser.id.toString();
            }
        } else
        {
            //is Owner
            _userId = appUser.id.toString();
        }

        //access all contact method
        for (EventContactData ecData : ecDataList)
        {
            EventContact ec = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, ecData.eventType));

            //if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
            //Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
            //by Jack Chuang 2014 02 07
            EventContact ecTmp = null;
            if (ecData.eventType == 10100)
            {
                ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, 10102));
            }

            if (ecData.eventType == 10101)
            {
                ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, 10103));
            }

            //if ecType = -10101(Door 1 Sensor Battery Low) or -10102(Door 1 Sensor not responding)
            //Add ecType -10103(Door 2 Sensor Battery Low) or -10104(Door 2 Sensor not responding) Automatic
            //by Jack Chuang 2014 02 07
            if (ecData.eventType == -10101)
            {
                ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, -10103));
            }

            if (ecData.eventType == -10102)
            {
                ecTmp = Ebean.find(EventContact.class, CredentialGenerator.genEventContactId(macAddress, -10104));
            }
            //end

            if (ec == null) {
                ec = new EventContact();
                ec.id = CredentialGenerator.genEventContactId(macAddress, ecData.eventType);
                ec.eventType = ecData.eventType;
                ec.deviceMac = macAddress;
                try{
                    ec.save();
                }catch (Exception e){
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save ec error" + ExceptionUtils.getStackTrace(e));
                }

            }

            //if ecType = 10100(Door 1 Opened) or 10101(Door 1 Closed)
            //Add ecType 10102(Door 2 Opened) or 10103(Door 2 Closed) Automatic
            //by Jack Chuang 2014 02 07
            if (ecTmp == null) {
                if (ecData.eventType == 10100)
                {
                    ecTmp = new EventContact();
                    ecTmp.id = CredentialGenerator.genEventContactId(macAddress, 10102);
                    ecTmp.eventType = 10102;
                    ecTmp.deviceMac = macAddress;
                    try{
                        ecTmp.save();
                    }catch (Exception e){
                        e.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save ec 10100 error" + ExceptionUtils.getStackTrace(e));
                    }
                }
                if (ecData.eventType == 10101)
                {
                    ecTmp = new EventContact();
                    ecTmp.id = CredentialGenerator.genEventContactId(macAddress, 10103);
                    ecTmp.eventType = 10103;
                    ecTmp.deviceMac = macAddress;
                    try{
                        ecTmp.save();
                    }catch (Exception e){
                        e.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save ectmp 10101 error" + ExceptionUtils.getStackTrace(e));
                    }
                }

                if (ecData.eventType == -10101)
                {
                    ecTmp = new EventContact();
                    ecTmp.id = CredentialGenerator.genEventContactId(macAddress, -10103);
                    ecTmp.eventType = -10103;
                    ecTmp.deviceMac = macAddress;
                    try{
                        ecTmp.save();
                    }catch (Exception e){
                        e.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save ectmp error -10101" + ExceptionUtils.getStackTrace(e));
                    }
                }

                if (ecData.eventType == -10102)
                {
                    ecTmp = new EventContact();
                    ecTmp.id = CredentialGenerator.genEventContactId(macAddress, -10104);
                    ecTmp.eventType = -10104;
                    ecTmp.deviceMac = macAddress;
                    try{
                        ecTmp.save();
                    }catch (Exception e){
                        e.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save ectmp error -10102" + ExceptionUtils.getStackTrace(e));
                    }
                }
            }
            Contact contact = Ebean.find(Contact.class, CredentialGenerator.genContactId(ec.id, _userId));

            if (contact == null)
            {
                contact = new Contact();
                contact.id = CredentialGenerator.genContactId(ec.id, _userId);
                contact.userId = _userId;
                contact.eventContact = ec;
            }

            contact.notifyEmail = ecData.checkEmail;
            contact.notifyPushNotification = ecData.checkNotification;
            contact.notifySMS = ecData.checkSMS;

			/*
             * If passive device event type != 10002, 10003, -10150, -10151
			 * contact.notifySMS MUST = false
			 * by Jack 2015 09 04
			*/
            if (ecData.eventType != 10002 && ecData.eventType != 10003 &&
                    ecData.eventType != 10100 && ecData.eventType != 10101 &&
                    ecData.eventType != 10102 && ecData.eventType != 10103 &&
                    ecData.eventType != -10150 && ecData.eventType != -10151)
            {
                contact.notifySMS = false;
            }
            try{
                contact.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save contact error " + ExceptionUtils.getStackTrace(e));
            }


            Contact contactTmp;

            if (ecData.eventType == 10100 || ecData.eventType == 10101 ||
                    ecData.eventType == -10101 || ecData.eventType == -10102)
            {
                if (ecTmp == null)
                {
                    throw new Exception(StatusCode.UNKNOWN_ERROR.name());
                }

                contactTmp = Ebean.find(Contact.class, CredentialGenerator.genContactId(ecTmp.id, _userId));

                if (contactTmp == null)
                {
                    contactTmp = new Contact();
                    contactTmp.id = CredentialGenerator.genContactId(ecTmp.id, _userId);
                    contactTmp.userId = _userId;
                    contactTmp.eventContact = ecTmp;
                }
                contactTmp.notifyEmail = ecData.checkEmail;
                contactTmp.notifyPushNotification = ecData.checkNotification;
                contactTmp.notifySMS = ecData.checkSMS;
                try{
                    contactTmp.save();
                }catch (Exception e){
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "add event contact save contact tmp error " + ExceptionUtils.getStackTrace(e));
                }
            }
        }
    }
}
