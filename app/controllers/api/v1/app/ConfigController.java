package controllers.api.v1.app;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.exception.ExceptionUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import json.models.api.v1.app.GetAppInfoRespPack;
import json.models.api.v1.app.GetConfigurationRespPack;
import json.models.api.v1.app.ListConfigurationRespPack;
import json.models.api.v1.app.SetConfigurationRespPack;
import json.models.api.v1.app.SwitchConfigurationRespPack;
import json.models.api.v1.app.XgConfigurationRespPack;
import json.models.api.v1.app.vo.ConfigurationInfoVO;
import models.AppInfo;
import models.Configuration;
import models.ConfigurationHistory;
import models.User;
import models.XGenie;


import play.mvc.BodyParser;
import play.mvc.Result;
import services.ConfigurationService;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.app.annotations.MobileLogInject;
import controllers.api.v1.app.annotations.ParameterInject;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.form.AddConfigurationReqPack;
import controllers.api.v1.app.form.BaseReqPack;
import controllers.api.v1.app.form.GetAppVersionCodeReqPack;
import controllers.api.v1.app.form.GetConfigurationReqPack;
import controllers.api.v1.app.form.LoginReqPack;
import controllers.api.v1.app.form.SetConfigurationReqPack;
import controllers.api.v1.app.form.ListConfigurationReqPack;
import controllers.api.v1.app.form.ListSwitchCfgReqPack;
import controllers.api.v1.app.form.SwitchConfigurationReqPack;
import frameworks.models.DataFileType;
import frameworks.models.StatusCode;
import utils.ConfigUtil;
import utils.LoggingUtils;

@PerformanceTracker
public class ConfigController extends BaseAPIController {
	
	@Transactional
	@SessionInject(AddConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result addConfiguration(){

		AddConfigurationReqPack pack = createInputPack();

		User user = getUser();

		XGenie xg = XGenie.findByXgMac(pack.xGenieId);
		
		if(xg==null){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
		}
		
		if(xg.smartGenie==null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");
		}
		
		if(!xg.smartGenie.owner.equals(user)){
			return createIndicatedJsonStatusResp(StatusCode.USER_RELATIONSHIP_ERROR, "relationship error");
		}
		
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration cfg = new Configuration();
		cfg.id = UUID.randomUUID().toString();
		
		try {
			ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config backup file error " + ExceptionUtils.getStackTrace(e));
			return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
		}
		
		cfg.cfg_type = pack.cfgClass.toUpperCase();

		cfg.descript = pack.desc;
		cfg.avilable = false;
		cfg.mac_addr = pack.xGenieId;
		
		cfg.save();
		
		ConfigurationHistory cfgHist = new ConfigurationHistory();
		cfgHist.configuration_id = cfg.id;

		cfgHist.cfg_type = pack.cfgClass.toUpperCase();
		
		cfgHist.descript = pack.desc;
		cfgHist.avilable = false;
		cfgHist.mac_addr = pack.xGenieId;
		cfgHist.sg_id = xg.smartGenie.id;
		
		cfgHist.save();

		XgConfigurationRespPack respPack = new XgConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.configurationId = cfg.id;

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(GetConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result getConfiguration(){

		GetConfigurationReqPack pack = createInputPack();

		User user = getUser();

		Configuration cfg = Configuration.find(pack.configId);
		
		if(cfg==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		List<Configuration> listCfg = Configuration.listConfigurationByCfgType(pack.cfgClass);
		if(listCfg.size()==0){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration type not found");
		}
		
		String strConfiguration = "";
		try {
			strConfiguration = ConfigurationService.getFile(pack.configId, pack.cfgClass);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GetConfigurationRespPack respPack = new GetConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.avilable = cfg.avilable;
		respPack.cfgType = cfg.cfg_type;
		respPack.desc = cfg.descript;
		respPack.xgId = cfg.mac_addr;
		respPack.configuration = strConfiguration;

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(ListConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result listConfiguration(){

		ListConfigurationReqPack pack = createInputPack();

		User user = getUser();

		List<Configuration> listCfg = Configuration.listConfigurationByMacAddress(pack.xGenieId);
		
		if(listCfg.size()==0){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		ListConfigurationRespPack listRespPack = new ListConfigurationRespPack();
		
		List<ConfigurationInfoVO> listRespPackVO = new ArrayList<ConfigurationInfoVO>();
		
		Configuration cfg = null;
		
		ConfigurationInfoVO cfgVO = null;
		
		for(int i=0; i<listCfg.size(); i++){
			cfg = new Configuration();
			cfgVO = new ConfigurationInfoVO();
			
			cfg = listCfg.get(i);
			
			cfgVO.cfgId = cfg.id;
			cfgVO.desc = cfg.descript;
			cfgVO.update = cfg.updateAt.getTime().toString();
			cfgVO.cfgClass = cfg.cfg_type;
			cfgVO.avilable = cfg.avilable;
			
			listRespPackVO.add(cfgVO);
		}
		
		listRespPack.listCfg = listRespPackVO;
		listRespPack.sessionId = pack.sessionId;
		return createJsonResp(listRespPack);
	}
	
	@Transactional
	@SessionInject(SwitchConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result switchConfiguration(){

		SwitchConfigurationReqPack pack = createInputPack();

		User user = getUser();

		//Logger.info("data : " + pack.data);
		
		ObjectMapper jsonMapper = new ObjectMapper();
		
		ListSwitchCfgReqPack listSwitchCfg = null;
		
		try {
			listSwitchCfg = jsonMapper.readValue(pack.data, ListSwitchCfgReqPack.class);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		
		Configuration configuration = null;
		
		ConfigurationHistory cfgHist = null;
		
		for(int i = 0; i < listSwitchCfg.switchData.size(); i++){
			
			configuration = Configuration.find(listSwitchCfg.switchData.get(i).cfgId);
			
			if(configuration!=null){
				configuration.avilable = listSwitchCfg.switchData.get(i).avilable;
				configuration.save();
				
				//write to ConfigurationHistory
				cfgHist = new ConfigurationHistory();

				cfgHist.configuration_id = configuration.id;
				cfgHist.cfg_type = configuration.cfg_type;
				cfgHist.descript = configuration.descript;
				cfgHist.avilable = configuration.avilable;
				cfgHist.mac_addr = configuration.mac_addr;
				
				XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
				cfgHist.sg_id = xg.smartGenie.id;
				
				cfgHist.save();
			}
		}
		
		SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(SetConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result setConfiguration(){

		SetConfigurationReqPack pack = createInputPack();

		User user = getUser();

		//Logger.info("data : " + pack.data);
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration configuration = Configuration.find(pack.configId);
		if(configuration==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		if(pack.desc!=null && pack.data==null){
			configuration.descript = pack.desc;
			configuration.save();
			
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			
			cfgHist.configuration_id = configuration.id;
			cfgHist.cfg_type = configuration.cfg_type;
			cfgHist.descript = configuration.descript;
			cfgHist.avilable = configuration.avilable;
			cfgHist.mac_addr = configuration.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			
			cfgHist.save();
		}
		
		SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		
		if(pack.desc==null && pack.data!=null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = configuration.descript;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;
			cfg.save();
			
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			cfgHist.save();
			
			configuration.delete();
			
			respPack.configurationId = cfg.id;
		}
		
		if(pack.desc!=null && pack.data!=null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = pack.desc;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;
			cfg.save();
			
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			cfgHist.save();
			
			configuration.delete();
			
			respPack.configurationId = cfg.id;
		}
		
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	@Transactional
    @BodyParser.Of(BodyParser.AnyContent.class)
	@SessionInject(GetConfigurationReqPack.class)
	@MobileLogInject(BaseReqPack.class)
	public  Result removeConfiguration(){

		GetConfigurationReqPack pack = createInputPack();

		User user = getUser();

		//Logger.info("data : " + pack.data);
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration configuration = Configuration.find(pack.configId);
		if(configuration==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		//Remove data from Configuration
		configuration.delete();
		
		SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	@Transactional
	@ParameterInject(GetAppVersionCodeReqPack.class)
	public  Result getAppVersionCode(){

		GetAppVersionCodeReqPack pack = createInputPack();

		AppInfo appInfo = AppInfo.find("1");
		
		GetAppInfoRespPack respPack = new GetAppInfoRespPack();
		respPack.timestamp = Calendar.getInstance();
		respPack.versionCode = appInfo.version_code;
		respPack.versionName = appInfo.version_name;

		return createJsonResp(respPack);
	}

	@Transactional
	@ParameterInject(GetAppVersionCodeReqPack.class)
	public  Result getV2AppVersionCode(){
		GetAppVersionCodeReqPack pack = createInputPack();
		int os_version = 9;
        if(pack.deviceOsName.equals("ios") && pack.deviceOsVersion != null){
            if(pack.deviceOsVersion.contains(".")){
                try{
                    os_version = Integer.parseInt(pack.deviceOsVersion.substring(0, pack.deviceOsVersion.indexOf(".", 0)));
                    LoggingUtils.log(LoggingUtils.LogLevel.INFO, "os version " + os_version );
                }catch(Exception e)
                {
                    e.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "os version exception" + ExceptionUtils.getStackTrace(e) );
                }
            }else{
                try{
                    os_version = Integer.parseInt(pack.deviceOsVersion);
                }catch (Exception err){
                    err.printStackTrace();
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "device os version parse exception" + ExceptionUtils.getStackTrace(err) );
                }
            }
        }
		if("amazon".equalsIgnoreCase(pack.deviceOsName)){
			pack.deviceOsName = "android";
		}
		AppInfo appInfo = AppInfo.findByOsName(pack.deviceOsName);
		GetAppInfoRespPack respPack = new GetAppInfoRespPack();
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "os version " + os_version );
        if(pack.deviceOsName.equals("ios")){
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "device ios version" + os_version);
            if(os_version >= 8){
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "device ios version >8 " + os_version);
                respPack.versionCode = appInfo.version_code;
                respPack.versionName = appInfo.version_name;
                respPack.forceUpgrade = appInfo.force_upgrade;
            }else{
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "device ios version <8 " + os_version);
                respPack.versionCode = ConfigUtil.getVersionCodeForIosUnder8();
                respPack.versionName = ConfigUtil.getVersionNameIosUnder8();
                respPack.forceUpgrade = ConfigUtil.getForceUpgradeIosUnder8();
            }
		}else{
            respPack.versionCode = appInfo.version_code;
            respPack.versionName = appInfo.version_name;
            respPack.forceUpgrade = appInfo.force_upgrade;
		}
        respPack.timestamp = Calendar.getInstance();
        respPack.sessionId = pack.sessionId;
		return createJsonResp(respPack);
	}

}
