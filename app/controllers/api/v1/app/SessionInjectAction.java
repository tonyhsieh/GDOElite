package controllers.api.v1.app;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;
import javax.inject.Inject;

import play.data.FormFactory;


import json.models.api.v1.sg.JsonStatusCode;
import json.models.api.v1.sg.RespPack;

import models.AppActionHistory;
import models.AppActionRecord;
import models.User;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;


import play.data.Form;
import play.mvc.Action;
import play.mvc.*;
import play.mvc.Http.Context;
import play.mvc.Result;
import controllers.api.v1.app.annotations.SessionInject;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.app.form.BaseReqPack;
import frameworks.Constants;
import frameworks.models.StatusCode;
import utils.ExceptionUtil;
import utils.LoggingUtils;

/**
 *
 * @author johnwu
 *
 */
public class SessionInjectAction extends Action<SessionInject>{
	@Inject FormFactory formFactory;
    @BodyParser.Of(BodyParser.AnyContent.class)
	@Override
	public CompletionStage<Result> call(Context ctx) {
		BaseReqPack pack;
        String sessionId = null;

		Form<? extends BaseReqPack> form = null;

		try{
            Http.Request request = ctx.request();
            form = formFactory.form(configuration.value()).bindFromRequest(request);
            pack = form.get();
			Http.Context.current().args.put(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY, pack);
            if(StringUtils.isNotBlank(pack.sessionId)){
				User user = UserSessionUtils.getUser(pack.sessionId);

				if(user != null){
					Http.Context.current().args.put(Constants.API_V1_APP_SESSION_HTTP_ARGS_KEY, user);

					recordActionHistory(ctx, user);
					
				}else{
					return CompletableFuture.completedFuture(ok(createExceptionJsonResp(StatusCode.SESSION_FAILURE, "no such session found")));
				}
			}else{
				return CompletableFuture.completedFuture(ok(createExceptionJsonResp(StatusCode.SESSION_FAILURE, "session is blanked")));
			}
		} catch (Exception e){
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Session inject failed " + ExceptionUtil.exceptionStacktraceToString(e));
			if( form != null ){
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");
				for( String key : form.errors().keySet() ){
                    LoggingUtils.log(LoggingUtils.LogLevel.WARN, form.errors().get(key).toString());
				}
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
			}

			return CompletableFuture.completedFuture(badRequest(createExceptionJsonResp(StatusCode.FORM_DATA_ERROR, "Bad Request")));
		}

		return delegate.call(ctx);
	}
	
	private void recordActionHistory(Context ctx, User user){
		String strForm = "";
		
		Map<String, String[]> map = null;
		
		try{
			map = ctx.request().body().asMultipartFormData().asFormUrlEncoded();
		}catch(Exception err){
			
		}
		
		if(map!=null){
			Set keys = map.keySet( );
			if(keys != null) {

				Iterator iterator = keys.iterator( );
				while(iterator.hasNext( )) {
					Object key = iterator.next();
					Object[] value = map.get(key);
					strForm += key + "=" + value[0] + ";";
					//Logger.info("App form : " + key + "/value : " + value[0]);
				}
			}
		}
		
		//Logger.info("catch form : " + strForm);

		String strAction;
		if(ctx.toString().contains("?")){
			strAction = ctx.toString().substring(
					ctx.toString().indexOf("("), 
					ctx.toString().indexOf("?"));
			strAction += ")";
			
			strForm = ctx.toString().substring(
					ctx.toString().indexOf("?"), 
					ctx.toString().length() - 1
					);
			
		}else{
			strAction = ctx.toString().substring(ctx.toString().indexOf("("));

			/*
			//Add by Jack 2013 10 16  -- CAN NOT catch App form record
			//如果攔截不到form的參數,就暫存內容
			if("".equals(strForm)){
				
				strForm = ctx.request().body().toString();
				
				Logger.info("catch form before : " + strForm);
				
				strForm = strForm.substring(strForm.indexOf("Map"));
				
				Logger.info("catch form after : " + strForm);
			}
			//////////////////////////////////////////////////////////////
			 */
		}
		
		AppActionRecord appActionRecord = AppActionRecord.find(user.id.toString());
		
		if(appActionRecord==null){
			appActionRecord = new AppActionRecord();
			appActionRecord.id = user.id.toString();
		}else{
			//For EBean record update time
			appActionRecord.action = strForm + " ";
		}
		
		//如果記錄Form的內容會大於255, 就進行裁剪避免exception
		if(strForm.length() > 255){
			strForm = strForm.substring(0, 255);
		}
		
		///////////////////////////////////////////////////////////////////////////////////

		try{
			appActionRecord.action = strAction;
			appActionRecord.form = strForm;
			appActionRecord.save();
		}catch(Exception err){}
		
		AppActionHistory appActionHistory = new AppActionHistory();
		appActionHistory.updateUserId = user.id.toString();
		appActionHistory.userId = user.id.toString();
		appActionHistory.action = strAction;
		appActionHistory.form = strForm;
		appActionHistory.save();
	}
	
	public JsonNode createExceptionJsonResp(StatusCode status, String errorMsg){

		RespPack respPack = new RespPack();

		respPack.status = new JsonStatusCode();

		respPack.status.code = status.value;
		respPack.status.message = errorMsg;
		respPack.timestamp = Calendar.getInstance();

		ObjectMapper om = new ObjectMapper();

		return om.convertValue(respPack, JsonNode.class);

	}
 }

