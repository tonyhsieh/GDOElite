/**
 *
 */
package controllers.api.v1.app.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import play.mvc.With;
import controllers.api.v1.app.ParameterInjectAction;
import controllers.api.v1.app.form.BaseReqPack;

/**
 * @author carloxwang
 *
 */
@With(ParameterInjectAction.class)
@Target({ElementType.TYPE, ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
public @interface ParameterInject {

	Class<? extends BaseReqPack> value();

}
