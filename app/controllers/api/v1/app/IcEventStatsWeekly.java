package controllers.api.v1.app;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import controllers.api.v1.app.form.IcWateringTime;
import frameworks.models.EventLogType;
import frameworks.models.IcDataId;
import models.IcEventStats;
import models.XGenie;

public class IcEventStatsWeekly extends AbstractIcEventStats {

	private final int logNum = 7;
	
	@Override
	public IcWateringTime[] summarizeEventStats(XGenie xGenie, byte zoneId, short offset) {
		currTime = Calendar.getInstance();
		currTime.set(Calendar.DATE, currTime.get(Calendar.DATE) + 7 * offset);
		currTime.setFirstDayOfWeek(Calendar.SUNDAY);
		
		currTime.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		setTimeToBeginOfDay(currTime);
		startTime = currTime.getTime();
		
		currTime.set(Calendar.DAY_OF_WEEK, Calendar.SATURDAY);
		setTimeToEndOfDay(currTime);
        endTime = currTime.getTime();
        
        icDataId = IcDataId.DAILY;

        List<IcEventStats> lstStatsW = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERING.value, startTime, endTime);
        List<IcEventStats> lstStatsS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_STOPPED.value, startTime, endTime);
		List<IcEventStats> lstStatsWS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERSAVING.value, startTime, endTime);
		
		IcWateringTime[] log = new IcWateringTime[logNum];
		
		for (int i = 0; i < logNum; i++) {
			int watering = 0;
			int stopByManually = 0;
			int stopByWaterSaving = 0;
			
			for (Iterator<IcEventStats> itW = lstStatsW.iterator(); itW.hasNext();) {
				IcEventStats stats = itW.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_WEEK) == i + 1) {
					watering = stats.getCumulativeMinutes();
					itW.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itS = lstStatsS.iterator(); itS.hasNext();) {
				IcEventStats stats = itS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_WEEK) == i + 1) {
					stopByManually = stats.getCumulativeMinutes();
					itS.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itWS = lstStatsWS.iterator(); itWS.hasNext();) {
				IcEventStats stats = itWS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.DAY_OF_WEEK) == i + 1) {
					stopByWaterSaving = stats.getCumulativeMinutes();
					itWS.remove();
					break;
				}
			}
			
			IcWateringTime icWateringTime = new IcWateringTime(watering, stopByManually, stopByWaterSaving);
			log[i] = icWateringTime;
		}
		
		if (offset == 0) {
			int dayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
			IcWateringTime icw = new IcEventStatsDaily().getCurrDateStats(xGenie, zoneId);
			IcWateringTime logIcw = log[dayOfWeek - 1];
			logIcw.setWatering(logIcw.getWatering() + icw.getWatering());
			logIcw.setStopByManually(logIcw.getStopByManually() + icw.getStopByManually());
			logIcw.setStopByWaterSaving(logIcw.getStopByWaterSaving() + icw.getStopByWaterSaving());
			log[dayOfWeek - 1] = logIcw;
		}
		
		return log;
	}

}
