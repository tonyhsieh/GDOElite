package controllers.api.v1.app;

import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

import controllers.api.v1.app.form.IcWateringTime;
import frameworks.models.EventLogType;
import frameworks.models.IcDataId;
import models.IcEventStats;
import models.XGenie;

public class IcEventStatsDaily extends AbstractIcEventStats {

	private final int logNum = 24;
	
	public IcEventStatsDaily() {}
	
	public IcEventStatsDaily(Calendar c) {
    	setTimeToBeginOfDay(c);
    	startTime = c.getTime();
    	setTimeToEndOfDay(c);
    	endTime = c.getTime();
	}
	
	@Override
	public IcWateringTime[] summarizeEventStats(XGenie xGenie, byte zoneId, short offset) {
		currTime = Calendar.getInstance();
    	icDataId = IcDataId.DAILY;
    	
    	currTime.set(Calendar.DATE, currTime.get(Calendar.DATE) + offset);
		
		setTimeToBeginOfDay(currTime);
		startTime = currTime.getTime();
		
		setTimeToEndOfDay(currTime);
		endTime = currTime.getTime();
		
		icDataId = IcDataId.HOURLY;

		List<IcEventStats> lstStatsW = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERING.value, startTime, endTime);
		List<IcEventStats> lstStatsS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_STOPPED.value, startTime, endTime);
		List<IcEventStats> lstStatsWS = IcEventStats.find(IcDataId.toValue(icDataId), xGenie.id, zoneId, EventLogType.PASSIVE_DEVICE_IRRI_WATERSAVING.value, startTime, endTime);
		
		IcWateringTime[] log = new IcWateringTime[logNum];
		
		for (int i = 0; i < logNum; i++) {
			int watering = 0;
			int stopByManually = 0;
			int stopByWaterSaving = 0;
			
			for (Iterator<IcEventStats> itW = lstStatsW.iterator(); itW.hasNext();) {
				IcEventStats stats = itW.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.HOUR_OF_DAY) == i) {
					watering = stats.getCumulativeMinutes();
					itW.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itS = lstStatsS.iterator(); itS.hasNext();) {
				IcEventStats stats = itS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.HOUR_OF_DAY) == i) {
					stopByManually = stats.getCumulativeMinutes();
					itS.remove();
					break;
				}
			}
			
			for (Iterator<IcEventStats> itWS = lstStatsWS.iterator(); itWS.hasNext();) {
				IcEventStats stats = itWS.next();
				currTime.setTime(stats.getDate());
				if (currTime.get(Calendar.HOUR_OF_DAY) == i) {
					stopByWaterSaving = stats.getCumulativeMinutes();
					itWS.remove();
					break;
				}
			}
			
			IcWateringTime icWateringTime = new IcWateringTime(watering, stopByManually, stopByWaterSaving);
			log[i] = icWateringTime;
		}
		
		return log;
	}
	
	public void insertDailyStats(String xGenieId, byte zoneId, int eventType) {
		icDataId = IcDataId.HOURLY;
		
		List<IcEventStats> lstStats = IcEventStats.find(IcDataId.toValue(IcDataId.HOURLY), xGenieId, zoneId, eventType, startTime, endTime);
		
		int cumulativeMinutes = 0;
		for (IcEventStats stats : lstStats) {
			cumulativeMinutes += stats.getCumulativeMinutes();
		}
		
		if (cumulativeMinutes > 0) {
			IcEventStats icEventStatsDaily = new IcEventStats(IcDataId.toValue(IcDataId.DAILY), xGenieId, zoneId, eventType, startTime, cumulativeMinutes);
			icEventStatsDaily.save();
		}
	}
	
	public IcWateringTime getCurrDateStats(XGenie xGenie, byte zoneId) {
		int watering = 0;
		int stopM = 0;
		int stopWS = 0;
		IcWateringTime[] icWateringTimes = summarizeEventStats(xGenie, zoneId, (short) 0);
		for (int i = 0; i < logNum; i++) {
			IcWateringTime icWateringTime = icWateringTimes[i];
			watering += icWateringTime.getWatering();
			stopM += icWateringTime.getStopByManually();
			stopWS += icWateringTime.getStopByWaterSaving();
		}
		
		return new IcWateringTime(watering, stopM, stopWS);
	}
	
}
