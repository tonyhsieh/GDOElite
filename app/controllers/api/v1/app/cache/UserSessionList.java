package controllers.api.v1.app.cache;

import java.io.Serializable;
import java.util.List;

public class UserSessionList implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -5726724555955199589L;

	public List<UserSessionData> currentSessionList;
}
