package controllers.api.v1.app.cache;

import java.util.ArrayList;
import java.util.List;

import models.User;
import utils.CacheManager;

import com.avaje.ebean.Ebean;

import frameworks.models.CacheKey;

/**
 * @author johnwu
 * @version 創建時間：2013/8/9 下午4:06:33
 */

public class UserSessionUtils {
	
	public static synchronized boolean login(String sessionId, String pnId, String userId){
		UserSessionCache usc = new UserSessionCache();
		usc.pnId = pnId;
		usc.userId = userId;
		CacheManager.setWithId(CacheKey.USER_SESSION, sessionId, usc, CacheManager.USERSESSION_EXPIRED_TIME);
		
		UserSessionList sessionList = CacheManager.get(CacheKey.USER_SESSION_LIST);
		
		if(sessionList == null){
			sessionList = new UserSessionList();
		}
		
		if(sessionList.currentSessionList == null){
			sessionList.currentSessionList = new ArrayList<UserSessionData>();
		}
		
		UserSessionData sd = new UserSessionData(sessionId, userId);
		sessionList.currentSessionList.add(sd);
		CacheManager.set(CacheKey.USER_SESSION_LIST, sessionList);
		
		return (CacheManager.get(CacheKey.USER_SESSION, sessionId) != null);
	}
	
	public static synchronized boolean logout(String sessionId){
		CacheManager.removeWithId(CacheKey.USER_SESSION, sessionId);
		
		UserSessionList sessionList = CacheManager.get(CacheKey.USER_SESSION_LIST);
		
		if(sessionList != null){
			if(sessionList.currentSessionList != null){
				List<UserSessionData> userSessionData = sessionList.currentSessionList;
				for(UserSessionData sessionData:userSessionData){
					if(sessionData.sessionId.equals(sessionId)){
						sessionList.currentSessionList.remove(sessionData);
						CacheManager.set(CacheKey.USER_SESSION_LIST, sessionList);
						break;
					}
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
		
		return (CacheManager.get(CacheKey.USER_SESSION, sessionId) == null);
	}
	
	public static synchronized List<String> listSession(String userId){
		List<String> mList = new ArrayList<String>();
		
		UserSessionList sessionList = CacheManager.get(CacheKey.USER_SESSION_LIST);
		for(UserSessionData sessionData:sessionList.currentSessionList){
			if(sessionData.userId.equals(userId)){
				mList.add(sessionData.sessionId);
			}
		}
	
		return mList;
	}
	
	public static User getUser(String sessionId){
		UserSessionCache usc = CacheManager.get(CacheKey.USER_SESSION, sessionId);
		
		if(usc == null){
			return null;
		}
		
		return Ebean.find(User.class, usc.userId);
	}
}
