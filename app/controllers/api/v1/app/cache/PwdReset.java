/**
 *
 */
package controllers.api.v1.app.cache;

import java.io.Serializable;
import java.util.Calendar;

/**
 * @author carloxwang
 *
 */
public class PwdReset implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -3739364866269379150L;

	public String userIdentifierId;

	public String authCode;

	public Calendar validBefore;

	@Override
	public String toString() {
		return "PwdReset ["
				+ (userIdentifierId != null ? "userIdentifierId="
						+ userIdentifierId + ", " : "")
				+ (authCode != null ? "authCode=" + authCode + ", " : "")
				+ (validBefore != null ? "validBefore=" + validBefore : "")
				+ "]";
	}


}
