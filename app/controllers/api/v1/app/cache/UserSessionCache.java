package controllers.api.v1.app.cache;

import java.io.Serializable;

public class UserSessionCache implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2835461666723644301L;

	public String userId;
	
	public String pnId;
	
	@Override
	public String toString() {
		return "Session ["
			+ (userId != null ? "userId="	+ userId + ", " : "")
			+ (pnId != null ? "pnId=" + pnId + ", " : "")
			+ "]";
	}
}
