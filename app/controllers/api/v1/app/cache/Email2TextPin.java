package controllers.api.v1.app.cache;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class Email2TextPin implements Serializable
{
    private static final long serialVersionUID = -4067498244427797251L;

    public String userId;
    public String sessionId;

    public List<Email2TextPinItem> list = new ArrayList<>();

    public static class Email2TextPinItem implements Serializable
    {
        private static final long serialVersionUID = -272518962169067077L;

        public String countryCode;
        public Integer gatewayId;
        public String phoneNumber;
        public Boolean isPhoneNumberValid;
        public String pin;
        public Calendar validBefore;
        public Calendar requestTime;
        public Boolean isValidated;

        @Override
        public String toString()
        {
            return "Email2TextPinItem{" +
                    "countryCode='" + countryCode + '\'' +
                    ", gatewayId=" + gatewayId +
                    ", phoneNumber='" + phoneNumber + '\'' +
                    ", isPhoneNumberValid=" + isPhoneNumberValid +
                    ", pin='" + pin + '\'' +
                    ", validBefore=" + validBefore.getTimeInMillis() +
                    ", requestTime=" + requestTime.getTimeInMillis() +
                    ", isValidated=" + isValidated +
                    '}';
        }
    }

    @Override
    public String toString()
    {
        return "Email2TextPin{" +
                "userId='" + userId + '\'' +
                ", sessionId='" + sessionId + '\'' +
                ", list=" + list +
                '}';
    }
}
