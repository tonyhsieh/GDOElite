package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class GetNetworkInfoReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7053972479296459310L;
	
	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String innIp;
	
	@Constraints.Required
	public String netMask;
	
	@Constraints.Required
	public String wifiMacAddress;
	
	@Constraints.Required
	public String sgMac;
}
