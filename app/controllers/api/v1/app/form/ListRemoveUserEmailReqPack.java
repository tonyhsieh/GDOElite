package controllers.api.v1.app.form;

import java.util.List;

import json.models.api.v1.app.vo.SmartGenieUserVO;

public class ListRemoveUserEmailReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6449840565750443314L;
	
	public List<ListEmail> AccountEmail;
	
	public static class ListEmail{
		public String email;
	}
}
