package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class Email2TextSetReqPack extends BaseReqPack
{
    private static final long serialVersionUID = -406390838491880301L;

    public String countryCode;

    @Constraints.Required
    public Integer gatewayId;

    @Constraints.Required
    public String phoneNumber;

    public Object isResend;
}
