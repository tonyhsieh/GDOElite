package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class AddConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1803494606237984320L;

	@Constraints.Required
	public String xGenieId;
	
	@Constraints.Required
	public String data;
	
	@Constraints.Required
	public String desc;
	
	@Constraints.Required
	public String cfgClass;
}
