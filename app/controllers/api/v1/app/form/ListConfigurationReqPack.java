package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class ListConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6472896151275080089L;

	@Constraints.Required
	public String xGenieId;
}
