package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class SetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7518269500279891973L;

	@Constraints.Required
	public String configId;
	
	@Constraints.Required
	public String cfgClass;
	
	public String data;
	
	public String desc;
}
