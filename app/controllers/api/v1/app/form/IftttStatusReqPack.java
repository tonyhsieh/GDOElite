package controllers.api.v1.app.form;


import play.data.validation.Constraints;

public class IftttStatusReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6262388763457568135L;

    @Constraints.Required
    public String xGenieMac;

    @Constraints.Required
    public Boolean iFTTTEnabled;


}
