package controllers.api.v1.app.form;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

import play.data.validation.Constraints;

public class GetWateringHistoryReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -435130420221638669L;
	
	@Constraints.Required
	@Min(1)
	@Max(6)
	private byte zoneId;
	
	@Constraints.Required
	@Min(0)
	@Max(3)
	private byte dataId;
	
	@Constraints.Required
	@Min(-32768)
	@Max(0)
	private short offset;

	public byte getZoneId() {
		return zoneId;
	}

	public void setZoneId(byte zoneId) {
		this.zoneId = zoneId;
	}

	public byte getDataId() {
		return dataId;
	}

	public void setDataId(byte dataId) {
		this.dataId = dataId;
	}

	public short getOffset() {
		return offset;
	}

	public void setOffset(short offset) {
		this.offset = offset;
	}
	
}
