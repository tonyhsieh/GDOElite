package controllers.api.v1.app.form;

import java.io.Serializable;

public class IcWateringTime implements Serializable {

	private int watering;
	private int stopByManually;
	private int stopByWaterSaving;
	
	public IcWateringTime(int watering, int stopByManually, int stopByWaterSaving) {
		this.watering = watering;
		this.stopByManually = stopByManually;
		this.stopByWaterSaving = stopByWaterSaving;
	}

	public int getWatering() {
		return watering;
	}

	public void setWatering(int watering) {
		this.watering = watering;
	}

	public int getStopByManually() {
		return stopByManually;
	}

	public void setStopByManually(int stopByManually) {
		this.stopByManually = stopByManually;
	}

	public int getStopByWaterSaving() {
		return stopByWaterSaving;
	}

	public void setStopByWaterSaving(int stopByWaterSaving) {
		this.stopByWaterSaving = stopByWaterSaving;
	}
	
}
