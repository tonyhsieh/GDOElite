package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * 
 * @author johnwu
 *
 */

public class ReqJoinSGReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5205431149369451287L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String targetUserId;
}
