package controllers.api.v1.app.form;

import json.models.api.v1.app.BaseRespPack;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;
import java.util.List;

public class AlertResultRespPack extends BaseRespPack
{
    private static final long serialVersionUID = -6648398471596915954L;

    public String sessionId;

    public List<AlertEventItem> eventGDO;
    public List<AlertEventItem> eventSD;

    @Override
    public String toString() {
        return "AlertResultRespPack{" +
                "sessionId='" + sessionId + '\'' +
                ", eventGDO=" + eventGDO +
                ", eventSD=" + eventSD +
                '}';
    }

    public static class AlertEventItem implements Serializable
    {
        private static final long serialVersionUID = 9130362233107649166L;

        public String name;
        public String xGenieMac;
        public List<AlertDevMacItem> event;

        @Override
        public String toString() {
            return "AlertEventItem{" +
                    "name='" + name + '\'' +
                    ", xgMAC='" + xGenieMac + '\'' +
                    ", event=" + event +
                    '}';
        }
    }
    public static class AlertSetItem implements Serializable
    {
        private static final long serialVersionUID = 9130362233107649136L;

        public String xgMAC;
        public List<AlertDevMacItem> event;

        @Override
        public String toString() {
            return "AlertEventItem{" +
                    ", xgMAC='" + xgMAC + '\'' +
                    ", event=" + event +
                    '}';
        }
    }


    public static class AlertDevMacItem implements Serializable
    {
        private static final long serialVersionUID = -7491873788116536902L;

        public int type;
        public boolean email;
        public boolean text;
        public boolean push;

        @Override
        public String toString() {
            return "AlertDevMacItem{" +
                    "type=" + type +
                    ", email=" + email +
                    ", text=" + text +
                    ", push=" + push +
                    '}';
        }
    }
}
