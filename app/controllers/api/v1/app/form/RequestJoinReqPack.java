package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class RequestJoinReqPack extends BaseReqPack {

        /**
         * 
         */
        private static final long serialVersionUID = 5553264639050162582L;
        
        @Constraints.Required
        public String requestJoinType;
}