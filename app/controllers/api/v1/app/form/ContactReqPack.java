package controllers.api.v1.app.form;

public class ContactReqPack extends BaseReqPack{

        /**
         * 
         */
        private static final long serialVersionUID = 2661877981518481614L;

        //public String contactGroupId;
        
        public String contactType;
        
        public String contactData;
        
        public String relationship;
        
        public String contactMsgType;
        
        public String name;
        
        public String smartGenieId;
        
        public String contactId;
        
        public String srcContactGroupId;
        
        public String destContactGroupId;
        
        public String userId;
}
