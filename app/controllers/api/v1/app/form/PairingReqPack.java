package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class PairingReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4882446153194880840L;

	@Constraints.Required
	public String macAddr;
}
