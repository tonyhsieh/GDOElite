package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class SwitchConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5791468975022363147L;

	@Constraints.Required
	public String data;
}
