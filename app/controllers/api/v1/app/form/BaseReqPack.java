/**
 *
 */
package controllers.api.v1.app.form;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * @author carloxwang
 *
 */
public class BaseReqPack implements Serializable {

	private static final long serialVersionUID = 2366406825126107129L;

	public String sessionId;

	/**
	 * Android : IMEI
	 * iPhone  : UUID
	 */
	public String imei;
	
	public String appVer;
	
	/**
	 * Device-related Information
	 */
	public String deviceName;
	
	public String deviceOsName;
	
	public String deviceOsVersion;
	
	public String deviceManufacturer;
	
	public String deviceModule;
	
	
	
	/**
	 * Cell tower information
	 */
	public String mcc;
	public String mnc;
	public String lac;
	public String cellId;
	
	
	/**
	 * GP inforation
	 */
	public Double lat;
	public Double lng;

	
	/**
	 * test case
	 */
	public boolean isTest;

	@Override
	public String toString()
	{
		return ReflectionToStringBuilder.toString(this);
	}
}
