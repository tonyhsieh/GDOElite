/**
 *
 */
package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
public class DoResetPwdReqPack extends BaseReqPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 9041342102626873802L;

	@Constraints.Required
	public String pwd;


}
