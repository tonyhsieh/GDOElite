package controllers.api.v1.app.form;


public class SetSmartGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 237274668037338645L;

	public String smartGenieName;
	
	public String xGenieId;
	
	public String xGenieName;
}
