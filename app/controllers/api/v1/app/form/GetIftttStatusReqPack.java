package controllers.api.v1.app.form;


import play.data.validation.Constraints;

public class GetIftttStatusReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6262388763457568124L;

    @Constraints.Required
    public String xGenieMac;


}
