package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class SetUserInfoReqPack extends BaseReqPack{
	/**
	 *
	 */
	private static final long serialVersionUID = 2834990912734950539L;

	@Constraints.Required
	public String sessionId;
	
	public String loginPwd;

	public String nickname;

	/*
	@Override
	public String toString() {
		return "SetUserInfoReqPack ["
				+ (loginPwd != null ? "loginPwd=" + loginPwd + ", " : "")
				+ (nickname != null ? "nickname=" + nickname + ", " : "")
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (imei != null ? "imei=" + imei + ", " : "")
				+ (appVer != null ? "appVer=" + appVer + ", " : "")
				+ (deviceName != null ? "deviceName=" + deviceName + ", " : "")
				+ (deviceOsName != null ? "deviceOsName=" + deviceOsName + ", "
						: "")
				+ (deviceOsVersion != null ? "deviceOsVersion="
						+ deviceOsVersion + ", " : "")
				+ (deviceManufacturer != null ? "deviceManufacturer="
						+ deviceManufacturer + ", " : "")
				+ (deviceModule != null ? "deviceModule=" + deviceModule + ", "
						: "") + (mcc != null ? "mcc=" + mcc + ", " : "")
				+ (mnc != null ? "mnc=" + mnc + ", " : "")
				+ (lac != null ? "lac=" + lac + ", " : "")
				+ (cellId != null ? "cellId=" + cellId + ", " : "")
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lng != null ? "lng=" + lng : "") + "]";
	}
	*/
}
