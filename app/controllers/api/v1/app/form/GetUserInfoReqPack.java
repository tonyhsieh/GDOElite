package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class GetUserInfoReqPack extends BaseReqPack{
	/**
	 *
	 */
	private static final long serialVersionUID = 2390406837261738490L;
	
	@Constraints.Required
	public String sessionId;
	
	/*
	@Override
	public String toString() {
		
		return "GetUserInfoReqPack ["
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (imei != null ? "imei=" + imei + ", " : "")
				+ (appVer != null ? "appVer=" + appVer + ", " : "")
				+ (deviceName != null ? "deviceName=" + deviceName + ", " : "")
				+ (deviceOsName != null ? "deviceOsName=" + deviceOsName + ", "
						: "")
				+ (deviceOsVersion != null ? "deviceOsVersion="
						+ deviceOsVersion + ", " : "")
				+ (deviceManufacturer != null ? "deviceManufacturer="
						+ deviceManufacturer + ", " : "")
				+ (deviceModule != null ? "deviceModule=" + deviceModule + ", "
						: "") + (mcc != null ? "mcc=" + mcc + ", " : "")
				+ (mnc != null ? "mnc=" + mnc + ", " : "")
				+ (lac != null ? "lac=" + lac + ", " : "")
				+ (cellId != null ? "cellId=" + cellId + ", " : "")
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lng != null ? "lng=" + lng : "") + "]";
	}
	*/
}