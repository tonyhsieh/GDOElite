package controllers.api.v1.app.form;

public class ContactGroupReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1498852584398392557L;

	public String contactGroupId;
	
	public String name;
	
	public String msgType;
}
