/**
 *
 */
package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
public class SearchUserReqPack extends BaseReqPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 8002751402677307497L;

	@Constraints.Required
	public String searchText;

	public Integer page = 1;

	public Integer pageSize = 10;
}
