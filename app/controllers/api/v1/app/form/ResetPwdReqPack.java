/**
 *
 */
package controllers.api.v1.app.form;

import play.data.validation.Constraints;

/**
 * @author carloxwang
 *
 */
public class ResetPwdReqPack extends BaseReqPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 946224294560195156L;

	@Constraints.Required
	public String loginId;

	public String sgMac;

	/*
	@Override
	public String toString() {
		return "ResetPwdReqPack ["
				+ (loginId != null ? "loginId=" + loginId + ", " : "")
				+ (sgMac != null ? "sgMac=" + sgMac + ", " : "")
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (imei != null ? "imei=" + imei + ", " : "")
				+ (appVer != null ? "appVer=" + appVer + ", " : "")
				+ (deviceName != null ? "deviceName=" + deviceName + ", " : "")
				+ (deviceOsName != null ? "deviceOsName=" + deviceOsName + ", "
						: "")
				+ (deviceOsVersion != null ? "deviceOsVersion="
						+ deviceOsVersion + ", " : "")
				+ (deviceManufacturer != null ? "deviceManufacturer="
						+ deviceManufacturer + ", " : "")
				+ (deviceModule != null ? "deviceModule=" + deviceModule + ", "
						: "") + (mcc != null ? "mcc=" + mcc + ", " : "")
				+ (mnc != null ? "mnc=" + mnc + ", " : "")
				+ (lac != null ? "lac=" + lac + ", " : "")
				+ (cellId != null ? "cellId=" + cellId + ", " : "")
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lng != null ? "lng=" + lng : "") + "]";
	}
	*/
}
