/**
 * 
 */
package controllers.api.v1.app.form;

import java.util.List;

import controllers.api.v1.sg.form.BaseReqPack;

/**
 * @author johnwu
 * @version 創建時間：2013/10/8 下午4:37:24
 */
public class XGenieStatusReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2766803712621380987L;

	public List<XGenieVO> xgenieInfo;
	
	public static class XGenieVO{
		public String macAddr;
		public Integer status;
	}
}
