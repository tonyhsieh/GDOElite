package controllers.api.v1.app.form;

import play.data.validation.Constraints;

public class SetSgLocationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2299486136176548551L;
	
	@Constraints.Required
	public String loclat;
	
	@Constraints.Required
	public String loclng;
	
	@Constraints.Required
	public String macAddr;
}
