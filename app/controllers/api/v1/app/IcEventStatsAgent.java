package controllers.api.v1.app;

import controllers.api.v1.app.form.IcWateringTime;
import models.XGenie;

public class IcEventStatsAgent {

	public IcWateringTime[] getIcWateringTime(IIcEventStatsStrategy strategy, XGenie xGenie, byte zoneId, short offset) {
		return strategy.summarizeEventStats(xGenie, zoneId, offset);
	}
	
}
