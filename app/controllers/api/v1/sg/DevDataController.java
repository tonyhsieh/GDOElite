/**
 *
 */
package controllers.api.v1.sg;

/**
 * @author carloxwang
 *
 */
/*
public class DevDataController extends BaseAPIController {

	@Transactional
	public Result build() throws IOException{
		if(Play.isDev()){

			// Cleanup

			SmartGenie sg = SmartGenie.findByMacAddress("00-11-AA-22-BB-11");

			if(sg != null){
				sg.delete();
			}


			List<User> userList = Ebean.find(User.class)
				.where()
					.in("email", "hsiangyu.wang@gmail.com", "holajohn9@gmail.com", "chuang4j@gmail.com")
				.findList();

			Ebean.delete(userList);

			// Cleanup
			List<FirmwareGroup> fgList = Ebean.find(FirmwareGroup.class)
												.where()
													.eq("name", "sg.general")
												.findList();

			List<FileEntity> feList = new LinkedList<FileEntity>();
			for(FirmwareGroup fg : fgList){
				feList.add(fg.fileEntity);
			}
			Ebean.delete(fgList);

			Ebean.delete(feList);
			// End of Cleanup

			User user1 = new User();
			user1.email = "hsiangyu.wang@gmail.com";
			user1.nickname = "Carlos Wang";
			user1.save();

			User user2 = new User();
			user2.email = "holajohn9@gmail.com";
			user2.nickname = "John Wu";
			user2.save();

			User user3 = new User();
			user3.email = "chuang4j@gmail.com";
			user3.nickname = "Jack Chuang";
			user3.save();

			// Firmware

			FileEntityManager feo = new FileEntityManager("sg-1.0.bin", "<Yo><WTF></WTF></Yo", FileType.SG_FIRMWARE);

			FirmwareGroup fg = new FirmwareGroup();
			fg.fileEntity = feo.fileEntity;
			fg.name = "sg.general";
			fg.save();


			// SG
			sg = new SmartGenie();
			sg.firmwareGroup = fg;
			sg.isPairing = true;
			sg.macAddress = "00-11-AA-22-BB-11";
			sg.name = "My Stupid Genie";
			sg.owner = user1;
			sg.id = CredentialGenerator.genSmartGenieId(sg.macAddress);

			sg.save();


			// SG Session
			String sgSessionId = UUID.randomUUID().toString();
			SGSessionUtils.login(sgSessionId, sg.id);


			return ok(sgSessionId);
		}else{
			return notFound();
		}
	}
}
*/