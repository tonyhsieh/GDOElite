package controllers.api.v1.sg.cache;

public class SGSessionData{
	
	public String sessionId;

	public String sgId;

	public SGSessionData(String sessionId, String sgId){
		this.sessionId = sessionId;
		this.sgId = sgId;
	}
	
	@Override
	public String toString() {
		return "Session ["
				+ (sessionId != null ? "sessionId="	+ sessionId + ", " : "")
				+ (sgId != null ? "sgId" + sgId + ", " : "")
				+ "]";
	}
}