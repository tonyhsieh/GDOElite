package controllers.api.v1.sg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import json.models.api.v1.sg.DataFileResultRespPack;
import json.models.api.v1.sg.DownloadDataFileRespPack;
import json.models.api.v1.sg.ListDataFileRespPack;
import json.models.api.v1.sg.vo.DataFileVO;
import models.DataFile;
import models.DataFileHistory;
import models.SmartGenie;

import play.mvc.BodyParser;
import play.mvc.Result;
import services.ConfigurationService;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.DownloadDataFileReqPack;
import controllers.api.v1.sg.form.RemoveDataFileReqPack;
import controllers.api.v1.sg.form.RetrieveCfgReqPack;
import controllers.api.v1.sg.form.UploadDataFileReqPack;
import controllers.api.v1.sg.form.listDataFileReqPack;
import frameworks.models.DataFileType;
import frameworks.models.StatusCode;

@PerformanceTracker
public class DataFileController extends BaseAPIController{
	
	@Transactional
	@SessionInject(UploadDataFileReqPack.class)
	public Result uploadDataFile(){

		UploadDataFileReqPack pack = createInputPack();

		//Logger.info(pack.toString());

		SmartGenie smartGenie = getSmartGenie();
		
		if(!DataFileType.hasDataFileType(pack.fileType)){
			return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "data type not found");
		}

		DataFile df = DataFile.findByTypeAndNameAndSgId(pack.fileType, pack.fileName, smartGenie.id);
		
		if(df!=null){	//the file is EXIST
			
			df.file_id = UUID.randomUUID().toString();	//file name;
			
			try {
				ConfigurationService.backupFile(pack.data, df.file_id, pack.fileType);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			df.save();
			
			DataFileHistory dfHist = new DataFileHistory();
			dfHist.data_file_id = df.id;
			dfHist.content_type = df.content_type;
			dfHist.data_name = df.data_name;
			dfHist.data_type = df.data_type;
			dfHist.sg_id = df.sg_id;
			dfHist.file_id = df.file_id;
			dfHist.save();
			
		}else{			//the file needs to create
			df = new DataFile();
			df.file_id = UUID.randomUUID().toString();	//file name;
			
			try {
				ConfigurationService.backupFile(pack.data, df.file_id, pack.fileType);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			
			df.id = UUID.randomUUID().toString();
			df.content_type = pack.contentType;
			df.data_name = pack.fileName;
			df.data_type = pack.fileType;
			df.sg_id = smartGenie.id;
			df.save();
			
			DataFileHistory dfHist = new DataFileHistory();
			dfHist.data_file_id = df.id;
			dfHist.content_type = df.content_type;
			dfHist.data_name 	= df.data_name;
			dfHist.data_type 	= df.data_type;
			dfHist.sg_id 		= df.sg_id;
			dfHist.file_id 		= df.file_id;
			dfHist.save();
		}
		
		DataFileResultRespPack respPack = new DataFileResultRespPack();
        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		
		return createJsonResp(respPack);
	}
	
	@Transactional
	@ParameterInject(DownloadDataFileReqPack.class)
	public Result downloadDataFile(){
		
		DownloadDataFileReqPack pack = createInputPack();
		
		//Logger.info(pack.toString());

		SmartGenie smartGenie = SmartGenie.findByMacAddress(pack.sgMac);
		
		if(!DataFileType.hasDataFileType(pack.fileType)){
			return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "data type not found");
		}
		
		DataFile df = DataFile.findByTypeAndNameAndSgId(pack.fileType, pack.fileName, smartGenie.id);
		
		if(df==null){
			return createIndicatedJsonStatusResp(StatusCode.FILE_NOTFOUND, "the file not found");
		}
		
		String result = "";
		try {
			result = ConfigurationService.getFile(df.file_id, pack.fileType, pack.downloadType);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		DownloadDataFileRespPack respPack = new DownloadDataFileRespPack();
        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.contentType = df.content_type;
		respPack.result = result;
		
		return createJsonResp(respPack);
	}
	
	@Transactional
	@ParameterInject(listDataFileReqPack.class)
	public Result listDataFile(){
		
		listDataFileReqPack pack = createInputPack();
		
		//Logger.info(pack.toString());

		SmartGenie smartGenie = SmartGenie.findByMacAddress(pack.sgMac);
		
		if(smartGenie==null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "Invalid Mac Address");
		}
		
		if(!DataFileType.hasDataFileType(pack.fileType)){
			return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "data type not found");
		}
		
		List<DataFile> listDataFile = DataFile.listByTypeAndSgId(pack.fileType, smartGenie.id);
		
		ListDataFileRespPack listDataFileRespPack = new ListDataFileRespPack();
		
		List<DataFileVO> listDataFileVO = new ArrayList<DataFileVO>();
		DataFileVO dataFileVO = null;
		
		for(DataFile dataFile: listDataFile){
			dataFileVO = new DataFileVO();
			
			dataFileVO.update = dataFile.updateAt.getTime().toString();
			dataFileVO.contentType = dataFile.content_type;
			dataFileVO.fileType = dataFile.data_type;
			dataFileVO.fileName = dataFile.data_name;
			
			listDataFileVO.add(dataFileVO);
		}
		
		ListDataFileRespPack respPack = new ListDataFileRespPack();
        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.listFile = listDataFileVO;
		
		return createJsonResp(respPack);
	}
	@BodyParser.Of(BodyParser.AnyContent.class)
	@Transactional
	@SessionInject(RemoveDataFileReqPack.class)
	public Result removeDataFile(){

		RemoveDataFileReqPack pack = createInputPack();

		//Logger.info(pack.toString());

		SmartGenie smartGenie = getSmartGenie();
		
		if(!DataFileType.hasDataFileType(pack.fileType)){
			return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "data type not found");
		}

		if(!DataFileType.hasDataFileType(pack.fileType)){
			return createIndicatedJsonStatusResp(StatusCode.DATA_TYPE_NOTFOUND, "data type not found");
		}
		
		DataFile df = DataFile.findByTypeAndNameAndSgId(pack.fileType, pack.fileName, smartGenie.id);
		
		if(df==null){
			return createIndicatedJsonStatusResp(StatusCode.FILE_NOTFOUND, "the file not found");
		}
		
		df.delete();
		
		DataFileResultRespPack respPack = new DataFileResultRespPack();
        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		
		return createJsonResp(respPack);
	}
}
