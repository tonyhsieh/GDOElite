package controllers.api.v1.sg;

import java.util.ArrayList;
import java.util.List;

import json.models.api.v1.sg.ChkAllFwVerRespPack;
import json.models.api.v1.sg.ChkFwVerRespPack;
import json.models.api.v1.sg.vo.SmartGenie4ChkFwVerVO;
import json.models.api.v1.sg.vo.SmartGenieVO;
import json.models.api.v1.sg.vo.XGenie4ChkFwVerVO;
import json.models.api.v1.sg.vo.XGenieVO;
import models.FileEntity;
import models.FirmwareGroup;
import models.SmartGenie;
import models.XGenie;

import org.apache.commons.lang3.StringUtils;


import org.apache.commons.lang3.exception.ExceptionUtils;

import play.mvc.Result;
import services.SmartGenieService;
import utils.FirmwareUtils;
import utils.LoggingUtils;
import utils.db.FileEntityManager;

import com.avaje.ebean.Ebean;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.ChkAllFwVerReqPack;
import controllers.api.v1.sg.form.ChkFwVerReqPack;
import frameworks.models.FileType;
import frameworks.models.StatusCode;

/**
 *
 * @author johnwu
 *
 */
@PerformanceTracker
public class FirmwareController extends BaseAPIController {

	@SessionInject(ChkFwVerReqPack.class)
	public Result checkVer(){
		ChkFwVerReqPack pack = createInputPack();

		//取得firmware group 和 file
		SmartGenie smartGenie = getSmartGenie();
		FirmwareGroup fw = Ebean.find(FirmwareGroup.class, smartGenie.firmwareGroup.id.toString());

		FirmwareGroup fwGroup = FirmwareGroup.findByFwGrpName(fw.name);
		FirmwareGroup forcefwGroup = FirmwareGroup.findByFwGrpNameForce(fw.name, 1);


		FileEntity firmwareSG = Ebean.find(FileEntity.class, fwGroup.fileEntity.id.toString());
		FileEntity forcefirmwareSG = FileEntity.findFileEntityIdByFwGrpNameForce(fwGroup.name, 1);
		SmartGenieVO smartGenieVO = new SmartGenieVO();

		//版本比對
		if(firmwareSG != null ){
			if(!FirmwareUtils.checkVer(fwGroup.firmwareVersion, pack.smartGenieFwVer)){
				smartGenieVO.newFwVer = fwGroup.firmwareVersion;
				smartGenieVO.forceUpgrade = fwGroup.force_upgrade;
				try {
					smartGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareSG.s3Path, FileType.SG_FIRMWARE, null);
				} catch (Exception e) {
					e.printStackTrace();
					LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl error " + ExceptionUtils.getStackTrace(e));
				}
			}
		}
		if(forcefirmwareSG != null ){
			if(!FirmwareUtils.checkVer(forcefwGroup.firmwareVersion, pack.smartGenieFwVer)){

				smartGenieVO.forceFwVer = forcefwGroup.firmwareVersion;

				try {
					smartGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareSG.s3Path , FileType.SG_FIRMWARE, null);
				} catch (Exception e) {
					e.printStackTrace();
					LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl force error " + ExceptionUtils.getStackTrace(e));
				}
			}
		}

		XGenieVO xGenieVO = new XGenieVO();
		if(StringUtils.isNotBlank(pack.xGenieId) && StringUtils.isNotBlank(pack.xGenieFwVer)){
			XGenie xGenie = XGenie.findByXgMac(pack.xGenieId);

			if(xGenie == null)
				return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xGenie not found");

			//取得 firmware group 和 file

			FirmwareGroup fwXG = Ebean.find(FirmwareGroup.class, xGenie.firmwareGroup.id.toString());
            FirmwareGroup fwXGGroup = FirmwareGroup.findByFwGrpName(fwXG.name);
            FirmwareGroup forcefwXGGroup= FirmwareGroup.findByFwGrpNameForce(fwXGGroup.name, 1);
			FileEntity firmwareXG = Ebean.find(FileEntity.class, fwXGGroup.fileEntity.id.toString());
			FileEntity forcefirmwareXG = FileEntity.findFileEntityIdByFwGrpNameForce(fwXGGroup.name, 1);


			if(!SmartGenieService.hasXGenie(smartGenie.id, xGenie.id))
				return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie do not match");


			//版本比對
			if(firmwareXG != null){
				if(!FirmwareUtils.checkVer(fwXGGroup.firmwareVersion, pack.xGenieFwVer)){
					xGenieVO.id = xGenie.id;
					xGenieVO.newFwVer = fwXGGroup.firmwareVersion;
					xGenieVO.macAddr = xGenie.macAddr;
					xGenieVO.forceUpgrade = fwXGGroup.force_upgrade;
					try {
						xGenieVO.downloadUrl = FileEntityManager.getDownloadUrl(firmwareXG.s3Path, FileType.XG_FIRMWARE, null);
					} catch (Exception e) {
						e.printStackTrace();
						LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadUrl xgenie error " + ExceptionUtils.getStackTrace(e));
					}
				}else{
					xGenieVO.macAddr = pack.xGenieId;
				}
			}
			if(forcefirmwareXG != null){
				if(!FirmwareUtils.checkVer(forcefwXGGroup.firmwareVersion, pack.xGenieFwVer)){
					xGenieVO.id = xGenie.id;
					xGenieVO.forceFwVer = forcefwXGGroup.firmwareVersion;
					xGenieVO.macAddr = xGenie.macAddr;
					try {
						xGenieVO.downloadForceUrl = FileEntityManager.getDownloadUrl(forcefirmwareXG.s3Path , FileType.XG_FIRMWARE, null);
					} catch (Exception e) {
						e.printStackTrace();
						LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getDownloadforceUrl xgenie  error " + ExceptionUtils.getStackTrace(e));
					}
				}else{
					xGenieVO.macAddr = pack.xGenieId;
				}
			}
		}

		ChkFwVerRespPack respPack = new ChkFwVerRespPack();

		respPack.sessionId = pack.sessionId;
		respPack.smartGenie = smartGenieVO;
		respPack.xgenie = xGenieVO;

		return createJsonResp(respPack);
	}

	@SessionInject(ChkAllFwVerReqPack.class)
	public Result checkAllFwVer(){
		ChkAllFwVerReqPack pack = createInputPack();

		//取得firmware group 和 file
		SmartGenie smartGenie = getSmartGenie();
		FirmwareGroup fw = Ebean.find(FirmwareGroup.class, smartGenie.firmwareGroup.id.toString());
		FirmwareGroup fwGroup;
		FirmwareGroup forcefwGroup;
        fwGroup = FirmwareGroup.findByFwGrpName(fw.name);
        forcefwGroup = FirmwareGroup.findByFwGrpNameForce(fw.name, 1);
		SmartGenie4ChkFwVerVO smartGenieVO = new SmartGenie4ChkFwVerVO();
		if(fwGroup != null){
			smartGenieVO.newFwVer = fwGroup.firmwareVersion;
			smartGenieVO.forceUpgrade = fwGroup.force_upgrade;
		}
		if(forcefwGroup != null){

			smartGenieVO.forceFwVer = forcefwGroup.firmwareVersion;

		}

		List<XGenie4ChkFwVerVO> xGenieVOList = new ArrayList<XGenie4ChkFwVerVO>();
		XGenie4ChkFwVerVO xGenieVO;
		FirmwareGroup fwXGGroup;
		FirmwareGroup forcefwXGGroup;
		List<XGenie> listXg = XGenie.listXGBySG(smartGenie);

		for(XGenie xg: listXg){

			xGenieVO = new XGenie4ChkFwVerVO();

			FirmwareGroup fwXG = Ebean.find(FirmwareGroup.class, xg.firmwareGroup.id.toString());

            fwXGGroup = FirmwareGroup.findByFwGrpName(fwXG.name);
            forcefwXGGroup = FirmwareGroup.findByFwGrpNameForce(fwXG.name, 1);


			if(fwXGGroup != null){
				xGenieVO.newFwVer = fwXGGroup.firmwareVersion;
				xGenieVO.forceUpgrade = fwXGGroup.force_upgrade;
			}
			if(forcefwXGGroup != null){

				xGenieVO.forceFwVer = forcefwXGGroup.firmwareVersion;

			}
			xGenieVO.macAddr = xg.macAddr;
			xGenieVOList.add(xGenieVO);
		}

		ChkAllFwVerRespPack respPack = new ChkAllFwVerRespPack();

		respPack.sessionId = pack.sessionId;
		respPack.smartGenie = smartGenieVO;
		respPack.xgenie = xGenieVOList;

		return createJsonResp(respPack);
	}
}
