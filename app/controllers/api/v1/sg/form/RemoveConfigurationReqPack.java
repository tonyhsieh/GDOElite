package controllers.api.v1.sg.form;

import java.util.List;

import controllers.api.v1.sg.form.ListRemoveXGenieReqPack.XGenie;
import play.data.validation.Constraints;

public class RemoveConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6938432366468284787L;
	
	@Constraints.Required
	public String data;
}
