package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class UpdateNetworkInfoReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -641750202198162260L;

	@Constraints.Required
	public String sessionId;
	
	public String innIp;
	
	public String netMask;
	
	public String wifiMacAddress;
	
	public String wifiIP;
	
	public String ssidName;
	
	public String firmwareVer;
	
	public String xGenieInfo;
	
	public String timezone;

	public Boolean autoUpgrade;

	public String autoUpgradeTime;

	public String autoUpgradeOnceTime;

	public Boolean autoUpgradeOnce;

}
