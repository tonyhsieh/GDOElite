package controllers.api.v1.sg.form;


/**
 * 
 * @author johnwu
 *
 */

public class ChkFwVerReqPack extends BaseReqPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6259406344740581425L;
	
	public String smartGenieFwVer;
	
	public String xGenieId;
	
	public String xGenieFwVer;
}
