package controllers.api.v1.sg.form;

import java.util.List;

import controllers.api.v1.app.form.BaseReqPack;

public class ListRemoveConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -8765908080646157862L;

	public List<XGenie> data;
	
	public static class XGenie{
		public String cfgClass;
		
		public String configId;
	}
}
