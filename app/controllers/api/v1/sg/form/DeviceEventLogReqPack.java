/**
 *
 */
package controllers.api.v1.sg.form;


/**
 * @author carloxwang
 *
 */
public class DeviceEventLogReqPack extends BaseReqPack {


	/**
	 *
	 */
	private static final long serialVersionUID = -5346666470282894618L;

	public String smartGenieTimestamp;

	public String xGenieId;

	//public String eventType;
	public int eventType;

	public String eventData;

	public String attachment;

	public static class Attachment{

		public String contentType;

		public String rawData;
	}
}
