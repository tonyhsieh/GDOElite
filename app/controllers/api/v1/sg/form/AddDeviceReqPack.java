package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class AddDeviceReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -885301291478126778L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String xGenieId;
	
	public String xGenieStatusCode;
	
	public String xGenieType;
	
	@Constraints.Required
	public String firmwareGroupName;
	
	public String parentXGenieId;
	
	public String parentParam;
	
	/*
	@Override
	public String toString() {
		return "AddDeviceReqPack ["
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (xGenieId != null ? "xGenieId=" + xGenieId + ", " : "")
				+ (firmwareGroupName != null ? "firmwareGroupName=" + firmwareGroupName + ", " : "")
				+ (xGenieStatusCode != null ? "xGenieStatusCode=" + xGenieStatusCode + ", " : "")
				+ (parentXGenieId != null ? "parentXGenieId=" + parentXGenieId + ", " : "")
				+ "]";
	}
	*/
}
