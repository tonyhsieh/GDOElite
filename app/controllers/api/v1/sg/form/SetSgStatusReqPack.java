package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class SetSgStatusReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2992129962050121638L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String status;
	
	public String isSendNotification;
}
