package controllers.api.v1.sg.form;

import play.data.validation.Constraints.Max;
import play.data.validation.Constraints.Min;
import play.data.validation.Constraints;

public class IcEventLogReqPack extends BaseReqPack {

	@Constraints.Required
	private String macAddr;
	
	@Min(1)
	@Max(6)
	private byte zoneId;
	
	@Min(10052)
	@Max(10054)
	private int eventType;
	
	@Constraints.Required
	private String startTime;
	
	@Min(1)
	@Max(720)
	private int duration;
	
	public String getMacAddr() {
		return macAddr;
	}
	
	public void setMacAddr(String macAddr) {
		this.macAddr = macAddr;
	}
	
	public byte getZoneId() {
		return zoneId;
	}
	
	public void setZoneId(byte zoneId) {
		this.zoneId = zoneId;
	}
	
	public int getEventType() {
		return eventType;
	}
	
	public void setEventType(int eventType) {
		this.eventType = eventType;
	}
	
	public String getStartTime() {
		return startTime;
	}
	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}
	
	public int getDuration() {
		return duration;
	}
	
	public void setDuration(int duration) {
		this.duration = duration;
	}
	
}
