/**
 *
 */
package controllers.api.v1.sg.form;

import play.data.validation.Constraints;


/**
 * @author carloxwang
 *
 */
public class RetrieveCfgReqPack extends BaseReqPack {

	/**
	 *
	 */
	private static final long serialVersionUID = -627492151832570380L;

	@Constraints.Required
	public String sgMac;
}
