package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class DownloadDataFileReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -749975128012903634L;

	@Constraints.Required
	public String sgMac;
	
	@Constraints.Required
	public String fileType;
	
	@Constraints.Required
	public String fileName;
	
	public String downloadType;
	
	/*
	@Override
	public String toString() {
		return "DownloadDataFileReqPack ["
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (fileType != null ? "fileType=" + fileType + ", " : "")
				+ (fileName != null ? "fileName=" + fileName + ", " : "")
				+ (downloadType != null ? "downloadType=" + downloadType + ", " : "")
				+ "]";
	}
	*/
}
