package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class ResetSgReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2074017327996382118L;

	@Constraints.Required
	public String sessionId;
}
