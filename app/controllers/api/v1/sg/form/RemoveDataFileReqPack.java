package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class RemoveDataFileReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7799248403249211863L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String fileType;
	
	@Constraints.Required
	public String fileName;
}
