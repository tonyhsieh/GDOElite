package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class GetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7430973520139928557L;

	@Constraints.Required
	public String configId;
	
	@Constraints.Required
	public String cfgClass;
}
