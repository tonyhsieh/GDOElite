package controllers.api.v1.sg.form;

import java.util.List;

import play.data.validation.Constraints;

public class GetMulityConfigurationsReqPack extends BaseReqPack{

    /**
     * 
     */
    private static final long serialVersionUID = -1887148912340338016L;
    
    @Constraints.Required
    public List<CfgClass> configId;
    
    public static class CfgClass{
        public String id;
    }
}
