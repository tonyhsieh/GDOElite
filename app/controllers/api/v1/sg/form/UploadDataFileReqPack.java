package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class UploadDataFileReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6469910739835872597L;

	@Constraints.Required
	public String sessionId;
	
	@Constraints.Required
	public String fileType;
	
	@Constraints.Required
	public String fileName;
	
	@Constraints.Required
	public String contentType;
	
	@Constraints.Required
	public String data;
	
	/*
	@Override
	public String toString() {
		return "UploadDataFileReqPack ["
				+ (sessionId != null ? "sessionId=" + sessionId + ", " : "")
				+ (fileType != null ? "fileType=" + fileType + ", " : "")
				+ (fileName != null ? "fileName=" + fileName + ", " : "")
				+ (contentType != null ? "contentType=" + contentType + ", " : "")
				+ (data != null ? "data=" + data + ", " : "")
				+ "]";
	}
	*/
}
