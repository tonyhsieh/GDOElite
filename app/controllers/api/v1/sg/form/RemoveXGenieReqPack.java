package controllers.api.v1.sg.form;

import play.data.validation.Constraints;
import controllers.api.v1.sg.form.BaseReqPack;

public class RemoveXGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3126847284086581886L;

	@Constraints.Required
	public String xGenieId;
}
