package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class SetConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6674196315565101142L;

	@Constraints.Required
	public String configId;
	
	@Constraints.Required
	public String cfgClass;
	
	public String data;
	
	public String desc;
}
