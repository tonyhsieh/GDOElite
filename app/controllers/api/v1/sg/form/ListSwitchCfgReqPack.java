package controllers.api.v1.sg.form;

import java.util.List;

import controllers.api.v1.sg.form.ListSwitchCfgReqPack.SwitchCfg;

public class ListSwitchCfgReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 6808602219645716406L;

	public List<SwitchCfg> switchData;
	
	public static class SwitchCfg{
		
		public String cfgId;
		
		public boolean avilable;
	}
}
