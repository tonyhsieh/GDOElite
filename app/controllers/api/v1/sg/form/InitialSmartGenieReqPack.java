package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class InitialSmartGenieReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5451617983481997392L;
	@Constraints.Required
	public String sgMac;
	
	public String smartGenieId;
	
    @Constraints.Required
	public String firmwareGroupName;

	public String devType;
	
	/*
	@Override
	public String toString() {
		return "InitialSmartGenieReqPack ["
				+ (sgMac != null ? "sgMac=" + sgMac + ", " : "")
				+ (firmwareGroupName != null ? "firmwareGroupName=" + firmwareGroupName + ", " : "")
				+ (smartGenieId != null ? "smartGenieId=" + smartGenieId + ", " : "")
				+ "]";
	}
	*/
}
