package controllers.api.v1.sg.form;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

/**
 * @author carloxwang
 */
public class BaseReqPack implements Serializable
{
    private static final long serialVersionUID = 2366406825126107129L;

    public String sessionId;

    public String sgVer;

    /**
     * test case
     */
    public boolean isTest;

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }
}
