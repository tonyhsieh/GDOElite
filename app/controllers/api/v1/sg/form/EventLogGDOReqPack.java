package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

/**
 * Created by joseph chou on 4/6/16.
 */
public class EventLogGDOReqPack extends BaseReqPack {

    @Constraints.Required
    public String macAddr;

    @Constraints.Required
    public String ts;

    @Constraints.Required
    public String door;

    @Constraints.Required
    public String event;

    @Constraints.Required
    public String user;
}
