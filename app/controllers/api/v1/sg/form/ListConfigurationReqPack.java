package controllers.api.v1.sg.form;

import play.data.validation.Constraints;

public class ListConfigurationReqPack extends BaseReqPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -3247881919621668358L;

	@Constraints.Required
	public String xGenieId;
}
