package controllers.api.v1.sg;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

import javax.inject.Inject;

import play.data.Form;
import play.mvc.Action;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.form.BaseReqPack;
import frameworks.Constants;
import utils.ExceptionUtil;
import utils.LoggingUtils;

import play.data.FormFactory;

/**
 * @author carloxwang
 */
public class ParameterInjectAction extends Action<ParameterInject>
{
    @Inject
    private FormFactory formFactory;

    @Override
    public CompletionStage<Result> call(Context ctx)
    {
        BaseReqPack pack;

        Form<? extends BaseReqPack> form = null;

        try
        {
            Http.Request request = ctx.request();

            form = formFactory.form(configuration.value()).bindFromRequest(request);
            pack = form.get();
            Http.Context.current().args.put(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY, pack);

            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sg parameter inject form " + form.toString() + "configuration" + configuration.value() + "ctx " + ctx.request());
        }
        catch (Exception err)
        {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "sg Parameter inject failed " + ExceptionUtil.exceptionStacktraceToString(err));

            if (form != null)
            {
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");
                for (String key : form.errors().keySet())
                {
                    LoggingUtils.log(LoggingUtils.LogLevel.WARN, form.errors().get(key).toString());
                }

                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
            }
            else
            {
                LoggingUtils.log(LoggingUtils.LogLevel.INFO, "form is null");
            }

            return CompletableFuture.completedFuture(badRequest("Bad Request"));
        }

        return delegate.call(ctx);
    }
}
