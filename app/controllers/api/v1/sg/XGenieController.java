package controllers.api.v1.sg;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.models.api.v1.sg.ListXgRespPack;
import json.models.api.v1.sg.RenameXGRespPack;
import json.models.api.v1.sg.vo.XGenieInfoVO;
import models.SmartGenie;
import models.XGenie;
import models.XGenieRelationship;

import play.mvc.Result;

import com.avaje.ebean.annotation.Transactional;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.AddDeviceReqPack;
import controllers.api.v1.sg.form.KeepAliveReqPack;
import controllers.api.v1.sg.form.RenameXGReqPack;
import frameworks.models.StatusCode;

@PerformanceTracker
public class XGenieController extends BaseAPIController{
	
	@Transactional
	@SessionInject(KeepAliveReqPack.class)
	public Result listXG(){

		KeepAliveReqPack pack = createInputPack();

		//Logger.info(pack.toString());

		SmartGenie smartGenie = getSmartGenie();

		XGenieInfoVO xg = null;
		
		List<XGenieInfoVO> listXg = new ArrayList<XGenieInfoVO>();
		
		ListXgRespPack respPack = new ListXgRespPack();

        List<XGenie> listXGenie = XGenie.listXGBySG(smartGenie);
        
        for(int i = 0; i < listXGenie.size(); i++){
        	xg = new XGenieInfoVO();
        	
        	xg.macAddr = listXGenie.get(i).macAddr;
        	xg.deviceType = listXGenie.get(i).deviceType;
        	xg.name = listXGenie.get(i).name;
        	
        	List<XGenieRelationship> listXGenieRelationship = XGenieRelationship.listXgByChileXgId(listXGenie.get(i).macAddr);
        	
        	if(listXGenieRelationship.size()!=0){
        		xg.parentMacAddr = listXGenieRelationship.get(0).parentMacAddr;
        		xg.parentParam = listXGenieRelationship.get(0).parentParam;
        	}

        	listXg.add(xg);
        }

        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.listXgenie = listXg;
		
		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(RenameXGReqPack.class)
	public Result rename(){

		RenameXGReqPack pack = createInputPack();

		//Logger.info(pack.toString());

		SmartGenie smartGenie = getSmartGenie();

		XGenie xg = XGenie.findByXgMac(pack.xGenieId);
		
		if(xg==null){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
		}
		if(xg.smartGenie == null || smartGenie == null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie not match");
		}
		
		if(!xg.smartGenie.id.equals(smartGenie.id)){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smart genie and xgenie not match");
		}
		
		xg.name = pack.name;
		
		xg.save();
		
		RenameXGRespPack respPack = new RenameXGRespPack();
        respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		
		return createJsonResp(respPack);
	}
}
