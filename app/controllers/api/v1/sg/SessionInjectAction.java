package controllers.api.v1.sg;

import java.util.Calendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import java.util.concurrent.CompletionStage;
import java.util.concurrent.CompletableFuture;

import javax.inject.Inject;

import json.models.api.v1.sg.JsonStatusCode;
import json.models.api.v1.sg.RespPack;
import models.SmartGenieActionHistory;
import models.SmartGenie;

import org.apache.commons.lang3.StringUtils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import play.data.Form;
import play.mvc.Action;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Http.Context;
import play.mvc.Result;
import utils.CacheManager;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.cache.SGSessionCache;
import controllers.api.v1.sg.form.BaseReqPack;
import frameworks.Constants;
import frameworks.models.CacheKey;
import frameworks.models.StatusCode;
import utils.ExceptionUtil;
import utils.LoggingUtils;

import play.data.FormFactory;

/**
 * @author johnwu
 */

public class SessionInjectAction extends Action<SessionInject>
{
    @Inject
    private FormFactory formFactory;

    @BodyParser.Of(BodyParser.AnyContent.class)
    public CompletionStage<Result> call(Context ctx) {
        BaseReqPack pack;

        Form<? extends BaseReqPack> form = null;

        try {
            Http.Request request = ctx.request();
            form = formFactory.form(configuration.value()).bindFromRequest(request);
            pack = form.get();
            Http.Context.current().args.put(Constants.API_V1_APP_REQ_PACK_HTTP_ARGS_KEY, pack);
            if (StringUtils.isNotBlank(pack.sessionId)) {
                final SGSessionCache session = CacheManager.get(CacheKey.SG_SESSION, pack.sessionId);
                if (session != null) {
                    Http.Context.current().args.put(Constants.API_V1_SG_SESSION_SG_ID_KEY, session.sgId);
                    final SmartGenie smartGenie = SmartGenie.find(session.sgId);
                    if (!"(POST /api/sg/route)".equals(ctx.toString().substring(ctx.toString().indexOf("("))) && !"(POST /api/v2/sg/route)".equals(ctx.toString().substring(ctx.toString().indexOf("(")))) {
                        String strForm = "";
                        Map<String, String[]> map = null;
                        if (ctx.request().body().asMultipartFormData() != null || ctx.request().body().asFormUrlEncoded() != null) {
                            if (ctx.request().body().asMultipartFormData() != null) {
                                try {
                                    map = ctx.request().body().asMultipartFormData().asFormUrlEncoded();
                                }catch (Exception err)
                                {
                                    err.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "session inject get request body form data failed " + ExceptionUtil.exceptionStacktraceToString(err));
                                }
                            } else {
                                try {
                                    map = ctx.request().body().asFormUrlEncoded();
                                }catch (Exception err)
                                {
                                    err.printStackTrace();
                                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "session inject get request body form url failed " + ExceptionUtil.exceptionStacktraceToString(err));
                                }
                            }
                        }
                        if (map != null) {
                            Set keys = map.keySet();
                            Iterator iterator = keys.iterator();
                            while (iterator.hasNext())
                            {
                                Object key = iterator.next();
                                Object[] value = map.get(key);
                                strForm += key + "=" + value[0] + ";";
                            }
                        }
                        String strAction;
                        if (ctx.toString().contains("?")) {
                            strAction = ctx.toString().substring(
                                    ctx.toString().indexOf("("),
                                    ctx.toString().indexOf("?"));
                            strAction += ")";

                            strForm = ctx.toString().substring(
                                    ctx.toString().indexOf("?"),
                                    ctx.toString().length() - 1
                            );
                        } else {
                            strAction = ctx.toString().substring(ctx.toString().indexOf("("));
                            if ("".equals(strForm)) {
                                strForm = ctx.request().body().toString();
                                strForm = strForm.substring(strForm.indexOf("Map"));
                            }
                        }
                        if (strForm.length() > 255)
                        {
                            strForm = strForm.substring(0, 254);
                        }
                        SmartGenieActionHistory smartGenieActionHistory = new SmartGenieActionHistory();
                        smartGenieActionHistory.smartGenieId = smartGenie.id;
                        smartGenieActionHistory.action = strAction;
                        smartGenieActionHistory.form = strForm;
                        try{
                            smartGenieActionHistory.save();
                        }catch (Exception err){
                            err.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "smartGenieActionHistory save failed " + ExceptionUtil.exceptionStacktraceToString(err));
                        }
                    }
                }
                else
                {
                    return CompletableFuture.completedFuture(badRequest(createExceptionJsonResp(StatusCode.SESSION_FAILURE, "no such session found")));
                }
            }
            else
            {
                return CompletableFuture.completedFuture(badRequest(createExceptionJsonResp(StatusCode.SESSION_FAILURE, "session is blanked")));
            }
        }
        catch (IllegalStateException e)
        {

            if (form != null)
            {
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "=====Validation Failed=====");
                for (String key : form.errors().keySet())
                {
                    LoggingUtils.log(LoggingUtils.LogLevel.WARN, "EventLog IllegalStateException : " + form.errors().get(key).toString());
                }
                LoggingUtils.log(LoggingUtils.LogLevel.WARN, "===========================");
            }

            return CompletableFuture.completedFuture(badRequest(createExceptionJsonResp(StatusCode.FORM_DATA_ERROR, "Bad Request")));
        }
        catch (Exception e)
        {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "EventLog e : " + e.toString());
        }

        return delegate.call(ctx);
    }

    private JsonNode createExceptionJsonResp(StatusCode status, String errorMsg)
    {
        RespPack respPack = new RespPack();

        respPack.status = new JsonStatusCode();

        respPack.status.code = status.value;
        respPack.status.message = errorMsg;
        respPack.timestamp = Calendar.getInstance();

        ObjectMapper om = new ObjectMapper();

        return om.convertValue(respPack, JsonNode.class);
    }
}
