/**
 *
 */
package controllers.api.v1.sg;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import com.fasterxml.jackson.databind.ObjectMapper;

import com.avaje.ebean.annotation.Transactional;

import json.models.api.v1.sg.GetMuiltyConfigurationsRespPack;
import json.models.api.v1.sg.SetConfigurationRespPack;
import json.models.api.v1.sg.SwitchConfigurationRespPack;
import json.models.api.v1.sg.ListConfigurationRespPack;
import json.models.api.v1.sg.vo.ConfigurationInfoVO;
import json.models.api.v1.sg.vo.ICProgramVO;
import json.models.api.v1.sg.vo.RemoveDeviceVO;
import json.models.api.v1.sg.GetConfigurationRespPack;
import json.models.api.v1.sg.XgConfigurationRespPack;
import json.models.api.v1.sg.SGCfgRespPack;
import models.Configuration;
import models.ConfigurationHistory;
import models.SmartGenie;
import models.SmartGenieConfiguration;
import models.XGenie;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.BodyParser;
import play.mvc.Result;
import services.ConfigurationService;
import services.SGConfigurationService;
import controllers.api.v1.routes;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.form.GetMulityConfigurationsReqPack;
import controllers.api.v1.sg.form.ListRemoveConfigurationReqPack;
import controllers.api.v1.sg.form.ListRemoveXGenieReqPack;
import controllers.api.v1.sg.form.RemoveConfigurationReqPack;
import controllers.api.v1.sg.form.SetConfigurationReqPack;
import controllers.api.v1.sg.form.ListSwitchCfgReqPack;
import controllers.api.v1.sg.form.SwitchConfigurationReqPack;
import controllers.api.v1.sg.form.ListConfigurationReqPack;
import controllers.api.v1.sg.form.GetConfigurationReqPack;
import controllers.api.v1.sg.form.AddConfigurationReqPack;
import controllers.api.v1.sg.annotations.ParameterInject;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.RetrieveCfgReqPack;
import controllers.api.v1.sg.form.UploadConfigReqPack;
import frameworks.models.DataFileType;
import frameworks.models.StatusCode;
import utils.LoggingUtils;

/**
 * @author carloxwang
 *
 */
@PerformanceTracker
public class ConfigController extends BaseAPIController {

	@SessionInject(UploadConfigReqPack.class)
	public Result backupConfiguration() throws IOException{

		UploadConfigReqPack pack = createInputPack();

		SGConfigurationService.backupCfg(pack.cfg, getSmartGenie());


		return createIndicatedJsonStatusResp(StatusCode.GENERAL_SUCCESS, "SUCCESS");
	}

	@ParameterInject(RetrieveCfgReqPack.class)
	public Result retreiveConfiguration(){
		RetrieveCfgReqPack pack = createInputPack();

		SmartGenie sg = SmartGenie.findByMacAddress(pack.sgMac);

		if( sg == null ){
			LoggingUtils.log(LoggingUtils.LogLevel.WARN, "Mac Address could NOT be found!!");
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "Invalid Mac Address");
		}

		SmartGenieConfiguration sgc = SmartGenieConfiguration.findLatestCfg(sg);

		if( sgc == null ){
            LoggingUtils.log(LoggingUtils.LogLevel.WARN, "No backup configuration found");
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_CFG_NOTFOUND, "No configuration found");
		}

		SGCfgRespPack respPack = new SGCfgRespPack();
		System.out.println(sgc);

		respPack.downloadUrl = buildServerURL() + routes.FileDownloadController.download(sgc.cfgFile.id.toString(), "sgconf");

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(AddConfigurationReqPack.class)
	public Result addConfiguration(){

		AddConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		XGenie xg = XGenie.findByXgMac(pack.xGenieId);
		
		if(xg==null){
			return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "xgenie not found");
		}
		
		/*
		if(xg.smartGenie==null){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_OWNERSHIP_EMPTY, "smartgenie ownership empty");
		}
		*/
		
		/*
		if(!xg.smartGenie.equals(smartGenie)){
			return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "smartgenie and xgenie not march");
		}
		*/
		
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration cfg = new Configuration();
		cfg.id = UUID.randomUUID().toString();
		
		try {
			ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
		}
		
		cfg.cfg_type = pack.cfgClass.toUpperCase();

		cfg.descript = pack.desc;
		cfg.avilable = false;
		cfg.mac_addr = pack.xGenieId;
		
		cfg.save();
		
		ConfigurationHistory cfgHist = new ConfigurationHistory();
		cfgHist.configuration_id = cfg.id;

		cfgHist.cfg_type = pack.cfgClass.toUpperCase();
		
		cfgHist.descript = pack.desc;
		cfgHist.avilable = false;
		cfgHist.mac_addr = pack.xGenieId;
		cfgHist.sg_id = xg.smartGenie.id;
		
		cfgHist.save();

		XgConfigurationRespPack respPack = new XgConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.configurationId = cfg.id;

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(GetConfigurationReqPack.class)
	public Result getConfiguration(){

		GetConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		Configuration cfg = Configuration.find(pack.configId);
		
		if(cfg==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		List<Configuration> listCfg = Configuration.listConfigurationByCfgType(pack.cfgClass);
		if(listCfg.size()==0){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration type not found");
		}
		
		String strConfiguration = "";
		try {
			strConfiguration = ConfigurationService.getFile(pack.configId, pack.cfgClass);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		GetConfigurationRespPack respPack = new GetConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();
		respPack.avilable = cfg.avilable;
		respPack.cfgType = cfg.cfg_type;
		respPack.desc = cfg.descript;
		respPack.xgId = cfg.mac_addr;
		respPack.configuration = strConfiguration;

		return createJsonResp(respPack);
	}
	
	@Transactional
    @SessionInject(GetConfigurationReqPack.class)
    public Result getMulityConfiguration(){

        GetConfigurationReqPack pack = createInputPack();

        SmartGenie smartGenie = getSmartGenie();

        ObjectMapper jsonMapper = new ObjectMapper();
        
        GetMulityConfigurationsReqPack listCfg = null;
        
        GetMuiltyConfigurationsRespPack respPack = new GetMuiltyConfigurationsRespPack();
        List<ICProgramVO> listICProgramVO = new ArrayList<ICProgramVO>();
        ICProgramVO icProgramVO = null;
        
        try {
            listCfg = jsonMapper.readValue(pack.configId, GetMulityConfigurationsReqPack.class);
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        for(int i=0; i<listCfg.configId.size(); i++){
            
            icProgramVO = new ICProgramVO();
            
            Configuration cfg = Configuration.find(listCfg.configId.get(i).id);
            
            if(cfg==null){
                icProgramVO.avilable = false;
                icProgramVO.cfgType = null;
                icProgramVO.configurationId = listCfg.configId.get(i).id;
                icProgramVO.xgId = null;
                icProgramVO.desc = null;
                icProgramVO.configuration = null;
            }else{
                icProgramVO.avilable = cfg.avilable;
                icProgramVO.cfgType = cfg.cfg_type;
                icProgramVO.configurationId = cfg.id;
                icProgramVO.xgId = cfg.mac_addr;
                icProgramVO.desc = cfg.descript;
                //icProgramVO.configuration = null;
                
                String strConfiguration = "";
                try {
                    strConfiguration = ConfigurationService.getFile(listCfg.configId.get(i).id, pack.cfgClass);
                } catch (IOException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                
                icProgramVO.configuration = strConfiguration;
            }
            
            listICProgramVO.add(icProgramVO);
        }
        

        /*
        List<Configuration> listCfg = Configuration.listConfigurationByCfgType(pack.cfgClass);
        if(listCfg.size()==0){
            return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration type not found");
        }
        */
        
        respPack.sessionId = pack.sessionId;
        respPack.timestamp = Calendar.getInstance();
        respPack.result = listICProgramVO;

        return createJsonResp(respPack);
    }
	
	@Transactional
	@SessionInject(ListConfigurationReqPack.class)
	public Result listConfiguration(){

		ListConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		List<Configuration> listCfg = Configuration.listConfigurationByMacAddress(pack.xGenieId);
		
		if(listCfg.size()==0){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		ListConfigurationRespPack listRespPack = new ListConfigurationRespPack();
		
		List<ConfigurationInfoVO> listRespPackVO = new ArrayList<ConfigurationInfoVO>();
		
		Configuration cfg = null;
		
		ConfigurationInfoVO cfgVO = null;
		
		for(int i=0; i<listCfg.size(); i++){
			cfg = new Configuration();
			cfgVO = new ConfigurationInfoVO();
			
			cfg = listCfg.get(i);
			
			cfgVO.cfgId = cfg.id;
			cfgVO.desc = cfg.descript;
			cfgVO.update = cfg.updateAt.getTime().toString();
			cfgVO.cfgClass = cfg.cfg_type;
			cfgVO.avilable = cfg.avilable;
			
			listRespPackVO.add(cfgVO);
		}
		
		listRespPack.listCfg = listRespPackVO;
		listRespPack.sessionId = pack.sessionId;
		return createJsonResp(listRespPack);
	}
	
	@Transactional
	@SessionInject(SwitchConfigurationReqPack.class)
	public Result switchConfiguration(){

		SwitchConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		//Logger.info("data : " + pack.data);

		ObjectMapper jsonMapper = new ObjectMapper();
		
		ListSwitchCfgReqPack listSwitchCfg = null;

		try {
			listSwitchCfg = jsonMapper.readValue(pack.data, ListSwitchCfgReqPack.class);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		Configuration configuration = null;
		
		ConfigurationHistory cfgHist = null;
		
		for(int i = 0; i < listSwitchCfg.switchData.size(); i++){
			
			configuration = Configuration.find(listSwitchCfg.switchData.get(i).cfgId);
			
			if(configuration!=null){
				configuration.avilable = listSwitchCfg.switchData.get(i).avilable;
				configuration.save();
				
				//write to ConfigurationHistory
				cfgHist = new ConfigurationHistory();

				cfgHist.configuration_id = configuration.id;
				cfgHist.cfg_type = configuration.cfg_type;
				cfgHist.descript = configuration.descript;
				cfgHist.avilable = configuration.avilable;
				cfgHist.mac_addr = configuration.mac_addr;
				
				XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
				cfgHist.sg_id = xg.smartGenie.id;
				
				cfgHist.save();
			}
		}
		
		SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	@Transactional
	@SessionInject(SetConfigurationReqPack.class)
	public Result setConfiguration(){

		SetConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		//Logger.info("data : " + pack.data);
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration configuration = Configuration.find(pack.configId);
		if(configuration==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		if(pack.desc!=null && pack.data==null){
			configuration.descript = pack.desc;
			try{
                configuration.save();
            }catch (Exception e ){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config save error " + ExceptionUtils.getStackTrace(e));
                return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_CANNOTSAVE, "config save error");
            }
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = configuration.id;
			cfgHist.cfg_type = configuration.cfg_type;
			cfgHist.descript = configuration.descript;
			cfgHist.avilable = configuration.avilable;
			cfgHist.mac_addr = configuration.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config history save error " + ExceptionUtils.getStackTrace(e));
            }
		}
		
		SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		
		if(pack.desc==null && pack.data!=null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = configuration.descript;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;
			try{
                cfg.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config history save error " + ExceptionUtils.getStackTrace(e));
            }

			
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config history save error " + ExceptionUtils.getStackTrace(e));
            }
            try{
                configuration.delete();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config delete error " + ExceptionUtils.getStackTrace(e));
            }
			respPack.configurationId = cfg.id;
		}
		
		if(pack.desc!=null && pack.data!=null){
			Configuration cfg = new Configuration();
			cfg.id = UUID.randomUUID().toString();
			
			try {
				ConfigurationService.backupFile(pack.data, cfg.id, pack.cfgClass);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return createIndicatedJsonStatusResp(StatusCode.AWS_S3_OPERATION_ERROR, "aws s3 operation error");
			}
			
			cfg.cfg_type = configuration.cfg_type;
			cfg.descript = pack.desc;
			cfg.avilable = configuration.avilable;
			cfg.mac_addr = configuration.mac_addr;
			try{
                cfg.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config save error " + ExceptionUtils.getStackTrace(e));
            }

			
			ConfigurationHistory cfgHist = new ConfigurationHistory();
			cfgHist.configuration_id = cfg.id;
			cfgHist.cfg_type = cfg.cfg_type;
			cfgHist.descript = cfg.descript;
			cfgHist.avilable = cfg.avilable;
			cfgHist.mac_addr = cfg.mac_addr;
			
			XGenie xg = XGenie.findByXgMac(configuration.mac_addr);
			cfgHist.sg_id = xg.smartGenie.id;
			try{
                cfgHist.save();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config history save error " + ExceptionUtils.getStackTrace(e));
            }
            try{
                configuration.delete();
            }catch (Exception e){
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "Config delete error " + ExceptionUtils.getStackTrace(e));
            }
			respPack.configurationId = cfg.id;
		}
		
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	@BodyParser.Of(BodyParser.AnyContent.class)
	@Transactional
	@SessionInject(RemoveConfigurationReqPack.class)
	public Result removeConfiguration(){

		RemoveConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		ObjectMapper jsonMapper = new ObjectMapper();
		
		ListRemoveConfigurationReqPack listRemoveConfiguration = null;
		
		try {
			listRemoveConfiguration = jsonMapper.readValue(pack.data, ListRemoveConfigurationReqPack.class);
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
		
		for(int i = 0; i < listRemoveConfiguration.data.size(); i++){
			
			/*
			//Logger.info("data : " + pack.data);
			if(!DataFileType.hasDataFileType(listRemoveConfiguration.data.get(i).cfgClass)){
				return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
			}
			
			Configuration configuration = Configuration.find(listRemoveConfiguration.data.get(i).configId);
			if(configuration==null){
				return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
			}
			*/
			
			try{
			Configuration configuration = Configuration.find(listRemoveConfiguration.data.get(i).configId);
			
			//Remove data from Configuration
			configuration.delete();
			}catch(Exception err){
				
			}
		}
		
		SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	
	/*
	@Transactional
	@SessionInject(RemoveConfigurationReqPack.class)
	public Result removeConfiguration(){

		RemoveConfigurationReqPack pack = createInputPack();

		SmartGenie smartGenie = getSmartGenie();

		
		
		
		//Logger.info("data : " + pack.data);
		if(!DataFileType.hasDataFileType(pack.cfgClass)){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_TYPE_NOTFOUND, "configuration class not found");
		}
		
		Configuration configuration = Configuration.find(pack.configId);
		if(configuration==null){
			return createIndicatedJsonStatusResp(StatusCode.CONFIGURATION_NOTFOUND, "configuration not found");
		}
		
		//Remove data from Configuration
		configuration.delete();
		
		SetConfigurationRespPack respPack = new SetConfigurationRespPack();
		respPack.sessionId = pack.sessionId;
		respPack.timestamp = Calendar.getInstance();

		return createJsonResp(respPack);
	}
	*/
}
