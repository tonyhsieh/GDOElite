package controllers.api.v1.sg;

import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import akka.actor.ActorSystem;
import controllers.api.IftttController;
import json.models.api.v1.sg.RespPack;
import models.DeviceLog;
import models.Event;
import models.SmartGenie;

import org.apache.commons.lang3.StringUtils;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.apache.commons.lang3.exception.ExceptionUtils;
import play.Configuration;

import play.mvc.Result;
import scala.concurrent.duration.Duration;

import utils.ExceptionUtil;
import utils.LoggingUtils;
import utils.SQSManager;
import utils.ConfigUtil;
import utils.SendMsgUtil;
import utils.db.FileEntityManager;
import utils.notification.DeviceEventQueueMsg;

import com.avaje.ebean.Ebean;

import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.DeviceEventLogReqPack;
import controllers.api.v1.sg.form.DeviceEventLogReqPack.Attachment;
import frameworks.Constants;
import frameworks.models.FileType;
import frameworks.models.MimeType;

import javax.inject.Inject;

import static controllers.api.IftttController.notifyIfttt;

/**
 * @author carloxwang
 */
@PerformanceTracker
public class DeviceEventLoggerController extends BaseAPIController {
    @Inject
    ActorSystem actorSystem;

    @Inject
    static Configuration configuration;

    @SessionInject(DeviceEventLogReqPack.class)
    public Result logEvent() {
        final DeviceEventLogReqPack pack = createInputPack();

        final SmartGenie sg = getSmartGenie();


        notifyIfttt(sg, pack.eventType);

        // 取隨機亂數做等待時間，避免突然塞進Akka造成系統往生
        // Add by Carlos, 2013/5/2 下午4:45:24
        Random rnd = new Random();
        int waitSec = rnd.nextInt(5);


        actorSystem.scheduler().scheduleOnce(Duration.create(waitSec, TimeUnit.SECONDS), () -> {
                    DeviceLog el = new DeviceLog();
                    el.eventType = pack.eventType;
                    el.eventData = pack.eventData;
                    if (sg.owner != null) {
                        el.ownerUserId = sg.owner.id.toString();
                    }
                    el.sgId = sg.id;
                    if (StringUtils.isNotBlank(pack.xGenieId)) {
                        el.xGId = pack.xGenieId;
                    }
                    if (StringUtils.isNotBlank(pack.attachment)) {
                        ObjectMapper om = new ObjectMapper();
                        try {
                            Attachment attachment = om.readValue(new StringReader(pack.attachment), DeviceEventLogReqPack.Attachment.class);
                            FileEntityManager feo = new FileEntityManager(UUID.randomUUID().toString(), attachment.rawData, FileType.EVENT_ATTACHMENT, MimeType.parse(attachment.contentType));
                            el.attachment = feo.fileEntity;
                        } catch (Exception e) {
                            e.printStackTrace();
                            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtil.exceptionStacktraceToString(e));
                        }
                    }
                    try {
                        el.save();
                    } catch (Exception err) {
                        err.printStackTrace();
                        LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "device log save error " + ExceptionUtils.getStackTrace(err));
                    }
                    Event event = Ebean.find(Event.class, pack.eventType);
                    //sendMsg by johnwu
                    if (event != null && event.canNotify) {
                        if (StringUtils.isNotBlank(pack.xGenieId)) {
                            sendMsg(pack.xGenieId, sg.id, event.eventType, null, pack.smartGenieTimestamp);
                        } else {
                            sendMsg(null, sg.id, event.eventType, null, pack.smartGenieTimestamp);
                        }
                    }

                }, actorSystem.dispatcher()
        );


        return createJsonResp(new RespPack());
    }

    //Add by Jack
    public static void sendMsg(String xGenieMac, String sgId, int eventType, Calendar sgTimeStamp, String strTimeStamp) {
        if (eventType == 1000 || eventType == -1000 || eventType == 1001 || eventType == -1001 ||
                eventType == -10050 || eventType == 10050 || eventType == 10051 || eventType == -10051 ||
                eventType == -10000 || eventType == 10000 || eventType == 10001 || eventType == -10001) {
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "send message blocked : " + eventType);
            return;
        }

        String url = ConfigUtil.getTransporterUrl();
        if (url != null && url.length() > 0) {
            SendMsgUtil.sendMsg(xGenieMac, sgId, eventType, strTimeStamp);
            return;
        }

        String queueRegion = configuration.getString("queue.region");
        SQSManager sqsManager = SQSManager.getInstance(queueRegion);
        String queueName = configuration.getString("queue.name.1");
        String queueUrl;

        String strMsgTmp = strTimeStamp.substring(0, 19);

        char[] chMsgTmp = strMsgTmp.toCharArray();

        StringBuilder strbufMsgId = new StringBuilder();

        for (int i = 0; i < chMsgTmp.length; i++) {
            if (chMsgTmp[i] != '-' && chMsgTmp[i] != ':' && chMsgTmp[i] != ' ' && chMsgTmp[i] != '/') {
                strbufMsgId.append(chMsgTmp[i]);
            }
        }

        SmartGenie sg = SmartGenie.find(sgId);

        String strMsgId;

        if ("".equals(xGenieMac) || xGenieMac == null) {
            strMsgId = sg.macAddress.substring(6) + strbufMsgId.toString();
        } else {
            strMsgId = xGenieMac.substring(6) + strbufMsgId.toString();
        }

        strMsgId += String.valueOf(eventType);

        DeviceEventQueueMsg queueMsg = new DeviceEventQueueMsg();
        queueMsg.xGenieMac = xGenieMac;
        queueMsg.sgId = sgId;
        queueMsg.eventType = eventType;
        queueMsg.messageTimeStamp = sgTimeStamp;
        queueMsg.id = strMsgId;

        ObjectMapper om = new ObjectMapper();
        try {
            queueUrl = sqsManager.getQueue(queueName);

            String msgId = sqsManager.sendMessage(queueUrl, om.writeValueAsString(queueMsg));
            LoggingUtils.log(LoggingUtils.LogLevel.INFO, "SQS MessageId : " + msgId);
        } catch (Exception e) {
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, ExceptionUtil.exceptionStacktraceToString(e));
        }
    }
}
