package controllers.api.v1.sg;

import com.avaje.ebean.annotation.Transactional;
import controllers.api.v1.annotations.PerformanceTracker;
import controllers.api.v1.sg.annotations.SessionInject;
import controllers.api.v1.sg.form.EventLogGDOReqPack;
import controllers.api.v1.sg.form.EventLogSDReqPack;
import frameworks.models.StatusCode;
import json.models.api.v1.sg.SwitchConfigurationRespPack;
import models.XGenieGDOLog;
import models.XGenieSDLog;
import models.SmartGenie;
import models.XGenie;
import play.mvc.Result;
import java.util.Calendar;
import java.sql.Timestamp;

/**
 * Created by joseph chou on 4/6/16.
 */

@PerformanceTracker
public class DeviceLogController extends BaseAPIController {

    @Transactional
    @SessionInject(EventLogGDOReqPack.class)
    public Result createGDOLog() {

        EventLogGDOReqPack reqPack = createInputPack();

        // MAC address should be associated with the smart genie
        // TODO refactor this code snippet to become a common utility
        SmartGenie sg = getSmartGenie();
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }

        XGenie xg = XGenie.findByXgMac(reqPack.macAddr);
        if (xg == null) {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "device not found");
        } else if (xg.smartGenie == null || !xg.smartGenie.id.equals(sg.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "device mismatched");
        }

        XGenieGDOLog model = new XGenieGDOLog();
        model.xg_mac_addr = reqPack.macAddr;
        model.event = Integer.parseInt(reqPack.event);
        model.ts = new Timestamp(Long.parseLong(reqPack.ts));
        model.door = Integer.parseInt(reqPack.door);
        model.user_name = reqPack.user;
        model.save();

        SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
        respPack.sessionId = reqPack.sessionId;
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }

    @Transactional
    @SessionInject(EventLogSDReqPack.class)
    public Result createSDLog() {

        EventLogSDReqPack reqPack = createInputPack();

        // MAC address should be associated with the smart genie
        // TODO refactor this code snippet to become a common utility
        SmartGenie sg = getSmartGenie();
        if (sg == null) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_NOTFOUND, "smart genie not found");
        }

        XGenie xg = XGenie.findByXgMac(reqPack.macAddr);
        if (xg == null) {
            return createIndicatedJsonStatusResp(StatusCode.XGENIE_NOTFOUND, "device not found");
        } else if (xg.smartGenie == null || !xg.smartGenie.id.equals(sg.id)) {
            return createIndicatedJsonStatusResp(StatusCode.SMARTGENIE_XGENIE_NOTMARCH, "device mismatched");
        }

        XGenieSDLog model = new XGenieSDLog();
        model.xg_mac_addr = reqPack.macAddr;
        model.event = Integer.parseInt(reqPack.event);
        model.ts = new Timestamp(Long.parseLong(reqPack.ts));
        model.interrupt = Integer.parseInt(reqPack.interrupt);
        model.save();

        SwitchConfigurationRespPack respPack = new SwitchConfigurationRespPack();
        respPack.sessionId = reqPack.sessionId;
        respPack.timestamp = Calendar.getInstance();

        return createJsonResp(respPack);
    }
}