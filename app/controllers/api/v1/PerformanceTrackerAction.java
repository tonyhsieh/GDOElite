/**
 *
 */
package controllers.api.v1;


import play.mvc.Action;
import play.mvc.Http.Context;
import play.mvc.Result;
import controllers.api.v1.annotations.PerformanceTracker;
import play.libs.ws.*;
import java.util.concurrent.CompletionStage;
/**
 * @author carloxwang
 *
 */
public class PerformanceTrackerAction extends Action<PerformanceTracker> {

    @Override
    public CompletionStage<Result> call(Context ctx) {

        long start = System.currentTimeMillis();
        long duration = System.currentTimeMillis() - start;
        //Logger.info("Duration: " + duration + " ms");

        return delegate.call(ctx);
    }



}





