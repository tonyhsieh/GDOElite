package controllers.api;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.v1.app.cache.UserSessionUtils;
import controllers.api.v1.sg.cache.SGSessionCache;
import controllers.api.v2.sg.BaseAPIController;
import frameworks.models.CacheKey;
import frameworks.models.StatusCode;
import models.SmartGenie;
import models.User;
import org.apache.commons.lang3.exception.ExceptionUtils;
import play.mvc.BodyParser;
import play.mvc.Result;
import utils.CacheManager;
import utils.LogUtil;
import utils.LoggingUtils;
import utils.MyUtil;


public class LogController extends BaseAPIController
{
    @BodyParser.Of(BodyParser.Json.class)
    public Result log()
    {
        try
        {
            JsonNode jsonNode = request().body().asJson();
            if (jsonNode == null || !jsonNode.has("type") || !jsonNode.has("logLevel") || !jsonNode.has("sessionId") )
            {
                return createIndicatedJsonStatusResp(StatusCode.FORMAT_ERROR, "format error");
            }
            SmartGenie smartGenie = null;
            User user;
            String sessionId = jsonNode.get("sessionId").toString().replace("\"", "");
            SGSessionCache session = CacheManager.get(CacheKey.SG_SESSION, sessionId);
            if (session != null) {
                smartGenie = SmartGenie.find(session.sgId);
            }
            user = UserSessionUtils.getUser(sessionId);
            if (user == null && smartGenie == null)
            {
                return createIndicatedJsonStatusResp(StatusCode.SESSION_ERROR, "session error");
            }

            if (user != null) {
                ((ObjectNode) jsonNode).put("user", user.email);
            }
            else {
                ((ObjectNode) jsonNode).put("smartGenie", smartGenie.macAddress);
            }

            ((ObjectNode) jsonNode).put("extIp", MyUtil.getClientIp(request()));

            LogUtil.log2Http(jsonNode.toString());

            return createIndicatedJsonStatusResp(StatusCode.GENERAL_SUCCESS, "send to log");
        }
        catch (Exception e)
        {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "send log to kibana error " + ExceptionUtils.getStackTrace(e));
            return createIndicatedJsonStatusResp(StatusCode.UNKNOWN_ERROR, "log error");
        }
    }
}
