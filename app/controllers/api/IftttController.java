package controllers.api;

import com.amazonaws.services.s3.model.S3Object;
import com.avaje.ebean.Ebean;
import com.avaje.ebean.Expr;
import com.avaje.ebean.ExpressionList;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import controllers.api.v1.sg.BaseAPIController;
import controllers.socket.P2PConnectionManager;
import controllers.socket.P2PServerHandler;
import frameworks.models.StatusCode;
import frameworks.models.UserIdentifierType;
import io.netty.channel.Channel;
import models.*;
import models.p2p.CallChannelIP;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.json.JSONObject;
import play.api.Play;
import play.libs.ws.WSClient;
import play.libs.ws.WSResponse;
import play.mvc.BodyParser;
import play.mvc.Http;
import play.mvc.Result;
import services.SmartGenieService;
import services.UserRelationshipService;
import utils.*;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class IftttController extends BaseAPIController {
    private final String SERVICE_API_KEY;
    private final static String DELIMITER = "_";

    private final static String IFTTT_CHANNEL_KEY = "IFTTT-Channel-Key";
    private final static String ACTION_FIELDS = "actionFields";
    private final static String TRIGGER_FIELDS = "triggerFields";
    private final static String CREATED_AT = "created_at";

    private final String PATTERN_DOOR_NAME = "door(\\d+)\\s+\\(([^)$]+)\\)"; // door1 (Jack GDO) --> ["1", "Jack GDO"]
    private final int GROUP_COUNT_DOOR_NAME = 2;

    @Inject
    public IftttController(play.Configuration configuration) {
        SERVICE_API_KEY = configuration.getString("ifttt.service.api.key");
    }

    public enum TriggerSlugs {
        DOOR_OPENED("door_closed"),
        DOOR_CLOSED("door_opened");

        private String value;

        TriggerSlugs(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static TriggerSlugs fromValue(String value) {
            if (value != null) {
                for (TriggerSlugs type : TriggerSlugs.values()) {
                    if (type.getValue().equalsIgnoreCase(value)) {
                        return type;
                    }
                }
            }
            throw new IllegalArgumentException("invalid value");
        }
    }

    public enum TriggerFieldSlugs {
        DOOR_NAME("door_name");

        private String value;

        TriggerFieldSlugs(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static TriggerFieldSlugs fromValue(String value) {
            if (value != null) {
                for (TriggerFieldSlugs type : TriggerFieldSlugs.values()) {
                    if (type.getValue().equalsIgnoreCase(value)) {
                        return type;
                    }
                }
            }
            throw new IllegalArgumentException("invalid value");
        }
    }

    public enum ActionSlugs {
        OPEN_DOOR("open_door"),
        CLOSE_DOOR("close_door");

        private String value;

        ActionSlugs(String value) {
            this.value = value;
        }

        public String getValue() {
            return this.value;
        }

        public static ActionSlugs fromValue(String value) {
            if (value != null) {
                for (ActionSlugs type : ActionSlugs.values()) {
                    if (type.getValue().equalsIgnoreCase(value)) {
                        return type;
                    }
                }
            }
            throw new IllegalArgumentException("invalid value");
        }
    }

    public enum ActionFieldSlugs {
        DOOR_NAME("door_name");

        private String value;

        ActionFieldSlugs(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public static ActionFieldSlugs fromValue(String value) {
            if (value != null) {
                for (ActionFieldSlugs type : ActionFieldSlugs.values()) {
                    if (type.getValue().equalsIgnoreCase(value)) {
                        return type;
                    }
                }
            }
            throw new IllegalArgumentException("invalid value");
        }
    }

    public enum DoorCtrl {
        LEFT(1), RIGHT(2);

        private int value;

        public int getValue() {
            return value;
        }

        DoorCtrl(int value) {
            this.value = value;
        }
    }

    @Inject
    WSClient wsClient;

    /* ========== Called by OAuth2 server ========== */
    @BodyParser.Of(BodyParser.Json.class)
    public Result auth() {
        try {
            JsonNode jsonNode = request().body().asJson();
            if (jsonNode == null || !jsonNode.has("username") || !jsonNode.has("password")) {
                return badRequest();
            }

            String username = jsonNode.get("username").asText();
            String password = jsonNode.get("password").asText();
            UserIdentifier ui = UserIdentifier.getUserIdentifier(username, UserIdentifierType.EMAIL);
            if (ui == null) {
                return badRequest(createErrorBody("user not found"));
            }

            if (!ui.password.equals(UserIdentifier.pwdHash(password))) {
                return badRequest(createErrorBody("password not correct"));
            }
            try {
                ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
                JsonNode result = mapper.createObjectNode();
                ((ObjectNode) result).put("id", ui.user.id.toString());
                ((ObjectNode) result).put("username", username);
                return ok(result);
            } catch (Exception e) {
                e.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "ifttt send result failed " + ExceptionUtil.exceptionStacktraceToString(e));
                return internalServerError();
            }
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "ifttt auth failed " + ExceptionUtil.exceptionStacktraceToString(e));
            return internalServerError();
        }
    }


    @BodyParser.Of(BodyParser.Json.class)
    public Result find() {
        JsonNode jsonNode = request().body().asJson();
        if (jsonNode == null || !jsonNode.has("id")) {
            return badRequest(createErrorBody("format error"));

        }
        User u = User.find(jsonNode.get("id").asText());
        if (u != null) {
            ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
            JsonNode result = mapper.createObjectNode();
            ((ObjectNode) result).put("id", u.id.toString());
            ((ObjectNode) result).put("username", u.email);
            return ok(result);
        } else {
            return badRequest(createErrorBody("user not correct"));
        }
    }

    /* ========== APIs defined by IFTTT ========== */
    public Result userInfo() {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        try {
            ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
            JsonNode result = mapper.createObjectNode();
            ((ObjectNode) result).with("data").put("id", user.email);
            ((ObjectNode) result).with("data").put("name", user.nickname);
            return ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result trigger(String trigger_slug) {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        TriggerSlugs triggerSlug;
        try {
            triggerSlug = TriggerSlugs.fromValue(trigger_slug);
        } catch (RuntimeException e) {
            return badRequest(createErrorBody("invalid trigger slug"));
        }

        final JsonNode jsonNode = request().body().asJson();

        final String doorName;
        try {
            doorName = jsonNode.get(TRIGGER_FIELDS).get(ActionFieldSlugs.DOOR_NAME.getValue()).asText();
        } catch (Exception e) {
            return badRequest(createErrorBody("invalid request body"));
        }

        ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
        ObjectNode result = mapper.createObjectNode();

        try {
            int limit = 50;
            if (jsonNode.has("limit")) {
                limit = jsonNode.get("limit").intValue();
            }

            // return zero items while limit is zero
            if (limit == 0) {
                result.withArray("data");
                return ok(result);
            }

            // get gdo name and door number from door_name
            Pattern pattern = Pattern.compile(PATTERN_DOOR_NAME);
            Matcher matcher = pattern.matcher(doorName);
            if (!matcher.matches() || matcher.groupCount() != GROUP_COUNT_DOOR_NAME) {
                return badRequest(createErrorBody("invalid door name"));
            }
            String gdoName = matcher.group(2);

            // get gdo mac from gdo name
            String gdoMac = getGDOMac(user, gdoName);
            if (gdoMac == null) {
                return badRequest(createErrorBody("invalid gdo name"));
            }

            // query door event by gdo mac and door number, and order by timestamp
            ExpressionList<DeviceLog> expressionList = Ebean.find(DeviceLog.class).where();

            switch (triggerSlug) {
                case DOOR_OPENED:
                    expressionList.or(Expr.eq("eventType", 10101), Expr.eq("eventType", 10103));
                    break;
                case DOOR_CLOSED:
                    expressionList.or(Expr.eq("eventType", 10100), Expr.eq("eventType", 10102));
                    break;
                default:
                    return badRequest(createErrorBody("not supported trigger slug"));
            }

            expressionList.between("eventType", 10100, 10103);
            expressionList.eq("xGId", gdoMac);
            List<DeviceLog> deviceLogList = expressionList.order()
                    .desc("createAt")
                    .setMaxRows(limit).findList();

            XGenie xGenie = XGenie.findByXgMac(gdoMac);
            for (DeviceLog record : deviceLogList) {
                ObjectNode node = mapper.createObjectNode();

                node.put("name", doorName);
                if (xGenie.smartGenie != null) {
                    node.put("homeExtender", xGenie.smartGenie.name);
                }

                node.put("eventType", Event.findByType(record.eventType).eventMsg);

                TimeZone tz = TimeZone.getTimeZone("UTC");
                DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
                df.setTimeZone(tz);
                String createdAt = df.format(new Date(record.createAt.getTimeInMillis()));
                node.put(CREATED_AT, createdAt);

                node.with("meta").put("id", record.id.toString());
                node.with("meta").put("timestamp", record.createAt.getTimeInMillis() / 1000); // in seconds by spec

                result.withArray("data").add(node);
            }
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }

        return ok(result);
    }

    public Result triggerIdentity(String trigger_slug, String trigger_identity) {
        // https://platform.ifttt.com/docs/api_reference#trigger-identity
        // Please note that your implementation of this endpoint is completely optional.
        // If you are not interested in these notifications, you can simply ignore these requests and return a 404.
        return notFound();
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result triggerFieldsDynamicOptions(String trigger_slug, String trigger_field_slug) {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        TriggerSlugs triggerSlug;
        try {
            triggerSlug = TriggerSlugs.fromValue(trigger_slug);
        } catch (RuntimeException e) {
            return badRequest(createErrorBody("invalid trigger slug"));
        }

        TriggerFieldSlugs triggerFieldSlug;
        try {
            triggerFieldSlug = TriggerFieldSlugs.fromValue(trigger_field_slug);
        } catch (RuntimeException e) {
            return badRequest(createErrorBody("invalid trigger field slug"));
        }

        ObjectNode result = OMManager.getInstance().getObjectMapper().createObjectNode();
        try {
            switch (triggerSlug) {
                case DOOR_OPENED:
                case DOOR_CLOSED:
                    if (triggerFieldSlug == TriggerFieldSlugs.DOOR_NAME) {
                        putTriggerOptions(user, result);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }

        return ok(result);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result triggerFieldDynamicValidation(String trigger_slug, String trigger_field_slug) {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        TriggerSlugs triggerSlug;
        try {
            triggerSlug = TriggerSlugs.fromValue(trigger_slug);
        } catch (RuntimeException e) {
            return badRequest(createErrorBody("invalid trigger slug"));
        }

        TriggerFieldSlugs triggerFieldSlug;
        try {
            triggerFieldSlug = TriggerFieldSlugs.fromValue(trigger_field_slug);
        } catch (RuntimeException e) {
            return badRequest(createErrorBody("invalid trigger field slug"));
        }

        final ObjectNode result = OMManager.getInstance().getObjectMapper().createObjectNode();

        try {
            JsonNode bodyNode = request().body().asJson();
            String value = bodyNode.get("value").asText();

            switch (triggerSlug) {
                case DOOR_OPENED:
                case DOOR_CLOSED:
                    if (triggerFieldSlug == TriggerFieldSlugs.DOOR_NAME) {

                        Map<String, String> doorSensors = getDoorSensors(user);
                        if (!doorSensors.containsValue(value)) {
                            result.with("data").put("valid", false);
                            result.with("data").put("message", "Invalid input");
                        } else {
                            result.with("data").put("valid", true);
                        }
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }

        return ok(result);
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result actions(String action_slug) {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
        ObjectNode result = mapper.createObjectNode();

        ActionSlugs actionSlug;
        try {
            actionSlug = ActionSlugs.fromValue(action_slug);
        } catch (IllegalArgumentException e) {
            String message = String.format("invalid action slug: %s", action_slug);
            return badRequest(createErrorBody(message));
        }

        final String ACTION_FIELD_VALUE_DOOR = ActionFieldSlugs.DOOR_NAME.getValue();
        final JsonNode bodyNode = request().body().asJson();

        if (!bodyNode.has(ACTION_FIELDS)) {
            String message = String.format("missing action fields");
            return badRequest(createErrorBody(message));
        }

        if (!bodyNode.get(ACTION_FIELDS).has(ACTION_FIELD_VALUE_DOOR)) {
            ObjectNode extra = mapper.createObjectNode();
            extra.put("value", bodyNode.toString());
            String message = "missing action fields key";
            return badRequest(createErrorBody(message, extra));
        }

        try {
            final String doorName = bodyNode.get(ACTION_FIELDS).get(ACTION_FIELD_VALUE_DOOR).asText();
            Pattern pattern = Pattern.compile(PATTERN_DOOR_NAME);
            Matcher matcher = pattern.matcher(doorName);

            if (!matcher.matches() || matcher.groupCount() != GROUP_COUNT_DOOR_NAME) {
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", doorName);
                String message = "invalid action fields value";
                return badRequest(createErrorBody(message, extra));
            }

            final int doorNumber = Integer.parseInt(matcher.group(1));
            final String gdoName = matcher.group(2);

            final String gdoMac = getGDOMac(user, gdoName);
            if (gdoMac == null) {
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", gdoName);
                String message = "invalid action fields value with invalid GDO";
                return badRequest(createErrorBody(message, extra));
            }

            XGenie xGenie = XGenie.findByXgMac(gdoMac);
            if (xGenie == null) {
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", gdoMac);
                String message = "GDO not found";
                return badRequest(createErrorBody(message, extra));
            }
            if (!xGenie.iftttStatus) {
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", gdoMac);
                String message = "GDO ifttt status is false";
                return badRequest(createErrorBody(message, extra));
            }
            SmartGenie smartGenie = xGenie.smartGenie;
            if (smartGenie == null || smartGenie.macAddress == null) {
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", xGenie.macAddr);
                String message = "GDO do not have Smart Genie";
                return badRequest(createErrorBody(message, extra));
            }

            CallChannelIP channel = CallChannelIP.find(smartGenie.macAddress);
            if(channel == null){
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", smartGenie.macAddress);
                String message = " Smart Genie netty connection not registered";
                return badRequest(createErrorBody(message, extra));
            }

            Channel channel_map = (Channel) P2PServerHandler.macAddressToChannel.get(smartGenie.macAddress);
            if(channel_map == null){
                ObjectNode extra = mapper.createObjectNode();
                extra.put("value", smartGenie.macAddress);
                String message = " Smart Genie does not have netty connection";
                return badRequest(createErrorBody(message, extra));
            }

            if (!operateGDO(smartGenie, xGenie, doorNumber, actionSlug)) {
                return badRequest(createErrorBody("IFTTT is disabled or XGenie is disconnected"));
            }

            ObjectNode node = mapper.createObjectNode();
            node.put("id", System.currentTimeMillis() + DELIMITER + actionSlug.getValue() + DELIMITER + gdoName + DELIMITER + doorNumber);
            result.withArray("data").add(node);

            return ok(result);
        } catch (Exception e) {
            e.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "ifttt action error " + ExceptionUtils.getStackTrace(e));
            return internalServerError();
        }
    }

    @BodyParser.Of(BodyParser.Json.class)
    public Result actionDynamicOptions(String action_slug, String action_field_slug) {
        User user;
        try {
            user = getUserFromRequest(request());
        } catch (RuntimeException e) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        ActionSlugs actionSlug;
        ActionFieldSlugs actionFieldSlug;

        try {
            actionSlug = ActionSlugs.fromValue(action_slug);
            actionFieldSlug = ActionFieldSlugs.fromValue(action_field_slug);
        } catch (IllegalArgumentException e) {
            return badRequest(createErrorBody("invalid action or action fields"));
        }

        JsonNode result = OMManager.getInstance().getObjectMapper().createObjectNode();

        try {
            // TODO: re-factor with switch-case if more options is needed
            if (actionSlug == ActionSlugs.OPEN_DOOR || actionSlug == ActionSlugs.CLOSE_DOOR) {
                if (actionFieldSlug == ActionFieldSlugs.DOOR_NAME) {
                    putActionOptions(user, (ObjectNode) result);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }

        return ok(result);
    }

    public Result status() {
        final String serviceApiKey = request().getHeader(IFTTT_CHANNEL_KEY);

        if (!SERVICE_API_KEY.equals(serviceApiKey)) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        return ok();
    }

    @BodyParser.Of(BodyParser.Empty.class)
    public Result setup() {
        final String serviceApiKey = request().getHeader(IFTTT_CHANNEL_KEY);

        if (!SERVICE_API_KEY.equals(serviceApiKey)) {
            return unauthorized(createErrorBody("invalid authorization"));
        }

        String bucket;

        try {
            String ENV = ConfigUtil.getApplicationEnvironment();
            switch (ENV) {
                case "PROD":
                    bucket = "prod.asante.misc";
                    break;
                case "STAG":
                    bucket = "stg.asante.misc";
                    break;
                case "DEV":
                    bucket = "dev.asante.misc";
                    break;
                default:
                    bucket = "dev.asante.misc";
                    break;
            }

            final String CONFIG_FILENAME = "setup_for_ifttt.json";

            // get object content from S3
            S3Manager s3bucket = S3Manager.getInstance(bucket);
            S3Object s3object = s3bucket.getFile(CONFIG_FILENAME);

            BufferedReader reader = new BufferedReader(new InputStreamReader(s3object.getObjectContent()));
            ObjectMapper mapper = new ObjectMapper();
            JsonNode jsonNode = mapper.readTree(reader);

            return ok(jsonNode);
        } catch (Exception e) {
            e.printStackTrace();
            return internalServerError();
        }
    }

    /* ========== Internal functions ========== */
    private static void putTriggerOptions(User user, ObjectNode result) {
        final ObjectMapper mapper = OMManager.getInstance().getObjectMapper();

        for (Map.Entry<String, String> doorSensor : getDoorSensors(user).entrySet()) {
            String doorSensorName = doorSensor.getValue();
            String doorSensorMac = doorSensor.getKey();

            ObjectNode node = mapper.createObjectNode();
            node.put("label", doorSensorName);
            node.put("value", doorSensorMac);
            result.withArray("data").add(node);
        }
    }

    private static void putActionOptions(User user, ObjectNode result) {
        final ObjectMapper mapper = OMManager.getInstance().getObjectMapper();

        for (Map.Entry<String, String> doorSensor : getDoorSensors(user).entrySet()) {
            String doorSensorName = doorSensor.getValue();
            String doorSensorMac = doorSensor.getKey();

            ObjectNode node = mapper.createObjectNode();
            node.put("label", doorSensorName);
            node.put("value", doorSensorMac);
            result.withArray("data").add(node);
        }
    }

    private static Map<String, String> getDoorSensors(String gdoMac) {
        Map<String, String> doorSensors = new HashMap<>();

        XGenie gdo = XGenie.findByXgMac(gdoMac);
        if (gdo == null) {
            return doorSensors;
        }

        final String gdoName = gdo.name;

        final String PATTERN_STRING = "^(door\\d+).*"; // door2_ds_id --> door2
        Pattern pattern = Pattern.compile(PATTERN_STRING);

        for (XGenieRelationship xg : XGenieRelationship.listXgByParentXgId(gdoMac)) {
            String doorSensorName = xg.parentParam;
            String doorSensorMac = xg.childMacAddr;

            if (doorSensorName == null) {
                continue;
            }

            Matcher matcher = pattern.matcher(doorSensorName);
            if (!matcher.matches()) {
                continue;
            }

            doorSensors.put(doorSensorMac, String.format("%s (%s)", matcher.group(1), gdoName));
        }

        return doorSensors;
    }

    private static Map<String, String> getDoorSensors(User user) {
        Map<String, String> doorSensors = new HashMap<>();

        for (Map.Entry<String, String> gdo : getGDOs(user).entrySet()) {
            String gdoMac = gdo.getKey();

            doorSensors.putAll(getDoorSensors(gdoMac));
        }

        return doorSensors;
    }

    private static Map<String, String> getGDOs(User user) {
        Map<String, String> gdos = new HashMap<>();

        for (SmartGenie smartGenie : SmartGenieService.listSmartGenieByOwner(user.id.toString())) {
            for (XGenie xGenie : XGenie.listXGBySG(smartGenie)) {
                if (xGenie.deviceType == 1 || xGenie.deviceType == 100102) {
                    String name = (xGenie.name == null || xGenie.name.length() == 0) ? xGenie.macAddr : xGenie.name;
                    gdos.put(xGenie.macAddr, name);
                }
            }
        }

        for (UserRelationship ui : UserRelationshipService.listUserRelationshipByUser(user.id.toString())) {
            SmartGenie smartGenie = Ebean.find(SmartGenie.class, ui.smartGenieId);
            for (XGenie xGenie : XGenie.listXGBySG(smartGenie)) {
                if (xGenie.deviceType == 1 || xGenie.deviceType == 100102) {
                    String name = (xGenie.name == null || xGenie.name.length() == 0)
                            ? xGenie.macAddr : xGenie.name;
                    gdos.put(xGenie.macAddr, name);
                }
            }
        }

        return gdos;
    }

    private static String getGDOMac(User user, String gdoName) {
        for (Map.Entry<String, String> entry : getGDOs(user).entrySet()) {
            if (entry.getValue().equalsIgnoreCase(gdoName)) {
                return entry.getKey();
            }
        }
        return null;
    }

    // TODO send commands to GDO over HTTP persistent connections

    private static boolean operateGDO(SmartGenie smartGenie, XGenie xGenie, int doorCtrl, ActionSlugs actionSlugs) {
            try {
                JSONObject node = new JSONObject();
                node.put("user", smartGenie.owner.email);
                node.put("heMac", smartGenie.macAddress);
                node.put("xgMac", xGenie.macAddr);
                node.put("type", "IFTTTToHE");
                node.put("door", Integer.toString(doorCtrl));
                node.put("action", actionSlugs.getValue());
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "ifttt action send log string " + node.toString());
                LogUtil.log(node.toString());
            } catch (Exception err) {
                err.printStackTrace();
                LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "ifttt action send log error " + ExceptionUtils.getStackTrace(err));
            }
            P2PConnectionManager p2pConnMgr = new P2PConnectionManager(smartGenie, xGenie, "ifttt", doorCtrl, actionSlugs.getValue());
            new Thread(p2pConnMgr).start();
            System.out.println("Sending command to " + xGenie.macAddr
                    + "(" + xGenie.fwVer + ") with operation: " + actionSlugs.getValue() + " for door " + doorCtrl);
            return true;


        }

    public static void notifyIfttt(SmartGenie smartGenie, int eventType) {
        try {
            if (eventType >= 10100 && eventType <= 10103) {
                List<User> users = new ArrayList<>();
                User owner = smartGenie.owner;
                for (UserRelationshipKeeper urk : UserRelationshipKeeper.listByOwner(owner.id.toString())) {
                    if (User.find(urk.userId) != null) {
                        users.add(User.find(urk.userId));
                    }
                }
                boolean isAdded = false;
                if (!users.isEmpty()) {
                    for (User user : users) {
                        if (user.email != null && owner.email != null && user.email.equals(owner.email)) {
                            isAdded = true;
                            break;
                        }
                    }
                    if (!isAdded) {
                        users.add(owner);
                    }
                    callIftttRealtimeAPI(users);
                } else {
                    LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "callIftttRealtimeAPI user is null ");
                }

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    static boolean callIftttRealtimeAPI(List<User> users)
            throws InterruptedException, ExecutionException {

        ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
        JsonNode jsonNode = mapper.createObjectNode();

        for (User user : users) {
            JsonNode userId = mapper.createObjectNode();
            ((ObjectNode) userId).put("user_id", user.email);
            ((ObjectNode) jsonNode).withArray("data").add(userId);
        }
        WSResponse wsResponse = null;
        try {

            WSClient ws = Play.current().injector().instanceOf(WSClient.class);
            wsResponse = ws.url("https://realtime.ifttt.com/v1/notifications")
                    .setRequestTimeout(10 * 1000)
                    .setHeader(IFTTT_CHANNEL_KEY, ConfigUtil.getIftttChannelKey())
                    .setHeader("X-Request-ID", UUID.randomUUID().toString())
                    .post(jsonNode)
                    .toCompletableFuture()
                    .get();

        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "callIftttRealtimeAPI error " + ExceptionUtils.getStackTrace(err));
        }
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "callIftttRealtimeAPI with data " + jsonNode.toString());
        return isHttpStatusValid(wsResponse.getStatus());
    }

    private User getUserFromRequest(final Http.Request request) {
        try {
            String auth = request.getHeader("Authorization");
            if (!auth.contains("Bearer")) {
                throw new RuntimeException("invalid authorization");
            }

            String patternMac = "Bearer\\s+";
            String token = auth.replaceAll(patternMac, "");

            User user = getUserFromToken(token);
            if (user == null) {
                throw new RuntimeException("invalid authorization");
            }

            return user;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private User getUserFromToken(final String accessToken)
            throws InterruptedException, ExecutionException {
        WSResponse wsResponse = null;
        try {
            wsResponse = wsClient.url(ConfigUtil.getOauth2ServerEndPoint())
                    .setRequestTimeout(10 * 1000)
                    .setHeader("token", accessToken)
                    .get()
                    .toCompletableFuture()
                    .get();
        } catch (Exception err) {
            err.printStackTrace();
            LoggingUtils.log(LoggingUtils.LogLevel.ERROR, "getUserFromToken error " + ExceptionUtils.getStackTrace(err));
        }
        if (isHttpStatusValid(wsResponse.getStatus())) {
            JsonNode jsonNode = wsResponse.asJson();
            if (jsonNode.has("validity") && jsonNode.get("validity").asBoolean()) {
                String userId = jsonNode.get("userId").asText();
                return getUserFromDB(userId);
            }
            return null;
        } else {
            return null;
        }
    }

    private static User getUserFromDB(final String userId) {
        return User.find(userId);
    }

    private static boolean isHttpStatusValid(int status) {
        return (status >= 200 && status < 400);
    }

    private static ObjectNode createErrorBody(String message, ObjectNode extraNode) {
        final String ROOT_KEY = "errors";
        final String MESSAGE_KEY = "message";

//        {
//            "errors": [
//            {
//                "message": "Something went wrong!"
//            }
//          ]
//        }

        final ObjectMapper mapper = OMManager.getInstance().getObjectMapper();
        final ObjectNode root = mapper.createObjectNode();

        if (extraNode != null) {
            root.setAll(extraNode);
        }

        // it will override any existing values for the same key
        root.put(MESSAGE_KEY, message);

        ObjectNode result = mapper.createObjectNode();
        result.putArray(ROOT_KEY).add(root);

        return result;
    }

    private static ObjectNode createErrorBody(String message) {
        return createErrorBody(message, null);
    }
}
