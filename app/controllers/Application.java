package controllers;

import com.fasterxml.jackson.databind.ObjectMapper;

import play.mvc.Controller;
import play.mvc.Http;
import play.mvc.Result;
import utils.LoggingUtils;
import utils.MyUtil;

import java.io.File;
import java.io.IOException;
import java.util.Map;

public class Application extends Controller
{
    public Result index()
    {
        return ok("ok");
    }

    public Result assets(String dir, String file)
    {
        try
        {
            String type = "image";
            if (file.endsWith(".css"))
            {
                type = "text/css";
            }
            else if (file.endsWith("js"))
            {
                type = "text/javascript";
            }

            File f = new File("public/" + dir + "/" + file);
            return ok(f)
                    .withHeader("Content-type", type)
                    .withHeader("Content-Length", String.valueOf(f.length()));
        }
        catch (Exception e)
        {
            return notFound();
        }
    }

    public Result fake()
    {
        Http.Request httprequest = request();
        String requestBodyString = "";
        if (httprequest.body().asMultipartFormData() != null
                || httprequest.body().asFormUrlEncoded() != null)
        {
            Map<String, String[]> params;
            if (httprequest.body().asMultipartFormData() != null)
            {
                params = httprequest.body().asMultipartFormData().asFormUrlEncoded();
            }
            else
            {
                params = httprequest.body().asFormUrlEncoded();
            }
            ObjectMapper om = new ObjectMapper();
            try
            {
                requestBodyString = om.writeValueAsString(params);
            }
            catch (IOException e)
            {
                e.printStackTrace();
            }
        }

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, httprequest.method() + " "
                + httprequest.uri() + " request IP: " + MyUtil.getClientIp(httprequest)
                + " request body : " + requestBodyString + "request : " + request());
        return ok();
    }
}
