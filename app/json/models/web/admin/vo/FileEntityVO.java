/**
 * 
 */
package json.models.web.admin.vo;

import models.FileEntity;

/**
 * @author johnwu
 * @version 創建時間：2013/10/3 下午1:39:26
 */
public class FileEntityVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 2567201125883705856L;

	public String fileName;
	
	protected FileEntityVO(FileEntity entity) {
		super(entity);
		// TODO Auto-generated constructor stub
		
		this.fileName = entity.fileName + "." + entity.fileExt;
	}

}
