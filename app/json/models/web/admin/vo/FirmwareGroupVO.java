/**
 * 
 */
package json.models.web.admin.vo;

import models.FirmwareGroup;

/**
 * @author johnwu
 * @version 創建時間：2013/10/3 上午10:48:53
 */

public class FirmwareGroupVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3493080282306533134L;

	public String id;
	public String name;
	public String firmwareVersion;
	public FileEntityVO fileEntity;
	public int forceUpgrade;
	
	public FirmwareGroupVO(FirmwareGroup fg) {
		super(fg);

		this.id = fg.id.toString();
		this.name = fg.name;
		this.firmwareVersion = fg.firmwareVersion;
		this.forceUpgrade = fg.force_upgrade;
        this.fileEntity = new FileEntityVO(fg.fileEntity);
		
	}
}
