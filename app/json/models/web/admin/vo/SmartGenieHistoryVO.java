/**
 * 
 */
package json.models.web.admin.vo;

import models.SmartGenieActionHistory;

/**
 * @author johnwu
 * @version 創建時間：2013/10/7 下午3:10:49
 */
public class SmartGenieHistoryVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -202950919576951420L;

	public String action;
	public String form;
	
	
	/**
	 * @param entity
	 */
	public SmartGenieHistoryVO(SmartGenieActionHistory entity) {
		super(entity);
		// TODO Auto-generated constructor stub
		this.action = entity.action;
		this.form = entity.form;
	}

}
