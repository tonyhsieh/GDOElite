package json.models.web.admin.vo;

import models.DuplicateEventLog;
import models.User;

public class DuplicateEventLogVO extends BaseVO{

	/**
	 * 
	 */
	private static final long serialVersionUID = 717604107023151291L;

	public String sgMac;
	public int eventType;
	public String msgId;
	
	public DuplicateEventLogVO(DuplicateEventLog duplicateEventLog) {
		super(duplicateEventLog);
		
		if(duplicateEventLog != null){
			this.sgMac = duplicateEventLog.smartGenie.macAddress;
			this.eventType = duplicateEventLog.eventType;
			this.msgId = duplicateEventLog.msgId;
		}
	}
}
