package json.models.web.admin.vo;

import models.P2pActiveCodeFile;

public class P2pActiveCodeFileVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6413157280161193009L;

	public String id;
	public String name;
	public int items;
	public String fileName;
	public String s3Name;
	
	public P2pActiveCodeFileVO(P2pActiveCodeFile fac) {
		super(fac);
		
		this.id = fac.id.toString();
		this.name = fac.name;
		this.items = fac.items;
		this.fileName = fac.fileName;
		this.s3Name = fac.s3_name;
	}
	
}
