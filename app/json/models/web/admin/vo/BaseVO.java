/**
 *
 */
package json.models.web.admin.vo;

import java.io.Serializable;
import java.util.Calendar;

import json.models.web.CalendarSerializer;
import models.BaseEntity;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author carloxwang
 *
 */
public class BaseVO implements Serializable {

    /**
	 *
	 */
	private static final long serialVersionUID = -9185599038085543664L;

	/**
     * 記錄本筆資料建立時間
     */
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar createAt;

    /**
     * 記錄本筆資料最後一次更新時間
     */
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar updateAt;

	/*
	protected BaseVO(BaseEntity entity){
		this.createAt = entity.createAt;
		this.updateAt = entity.updateAt;
	}
	*/
	
	//Convert TimeZone from GMT to PST
	protected BaseVO(BaseEntity entity){
		if(entity != null){
			this.createAt = entity.createAt;
			this.updateAt = entity.updateAt;
			
			this.createAt.add(Calendar.HOUR, -8);
			this.updateAt.add(Calendar.HOUR, -8);
		}
	}
}
