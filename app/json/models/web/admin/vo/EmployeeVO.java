package json.models.web.admin.vo;

import models.Employee;

public class EmployeeVO extends BaseVO {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1030325510294313227L;
	
	public String empId;
	public String name;
	public String department;
	public String cellPhone;
	public String phoneExtension;
	public String email;
	
	public EmployeeVO(Employee entity) {
		super(entity);
		
		if(entity != null){			
			this.empId = entity.id.toString();
			this.name = entity.name;
			this.department = entity.department;
			this.cellPhone = entity.cellPhone;
			this.phoneExtension = entity.phoneExtension;
			this.email = entity.email;
		}
	}

}
