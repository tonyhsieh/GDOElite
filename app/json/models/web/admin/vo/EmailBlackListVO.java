/**
 * 
 */
package json.models.web.admin.vo;

import models.AppInfo;
import models.EmailBlacklist;

/**
 * @author johnwu
 * @version 創建時間：2013/10/3 上午10:48:53
 */

public class EmailBlackListVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = -3493080282306533134L;

	public String id;
	public String email;



	public EmailBlackListVO(EmailBlacklist emaillist) {
		super(emaillist);

		this.id = emaillist.id.toString();
		this.email = emaillist.email;

	}
}
