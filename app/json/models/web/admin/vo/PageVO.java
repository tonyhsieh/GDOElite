/**
 *
 */
package json.models.web.admin.vo;

import java.util.ArrayList;
import java.util.List;

import com.avaje.ebean.*;

/**
 * @author carloxwang
 *
 */
public class PageVO<T> {

	public Boolean hasNext;
	public Boolean hasPrev;

	public Integer totalPages;
	public Integer totalElements;

	public Integer page;

	public List<T> currents;

	public PageVO(PagedList<?> page){
		this.hasNext = page.hasNext();
		this.hasPrev = page.hasPrev();

		this.totalPages = page.getTotalPageCount();
		this.totalElements = page.getTotalRowCount();

		this.page = page.getPageIndex();

		this.currents = new ArrayList<T>(page.getList().size());
	}

}
