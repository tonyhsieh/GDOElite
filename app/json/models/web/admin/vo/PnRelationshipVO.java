package json.models.web.admin.vo;

import models.PnRelationship;

/**
 * Created by admin on 2015/11/5.
 */
public class PnRelationshipVO extends BaseVO {
    private static final long serialVersionUID = 1773228575048147131L;

    public String PnId;
    public String DevId;
    public String status;


    public  PnRelationshipVO(PnRelationship pn) {
        super(pn);

        this.PnId = pn.pnId;
        this.DevId = pn.devId;
        this.status = new String();
    }
}
