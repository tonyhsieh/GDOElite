package json.models.web.admin.vo;

import models.P2pActiveCode;

public class P2pActiveCodeVO extends BaseVO{
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8531212084968818005L;
	//public String pnid;
	public String macAddr;
	public String activeCode;
	public String status;
	
	public P2pActiveCodeVO(P2pActiveCode p2p){
		super(p2p);
		
		if(p2p != null){
			//add new
			//this.pnid = p2p.pn_id;
			
			this.macAddr = p2p.mac_addr;
			this.activeCode = p2p.active_code;
		}
		
		this.status = new String();
	}
}
