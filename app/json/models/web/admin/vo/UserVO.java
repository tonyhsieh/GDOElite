/**
 *
 */
package json.models.web.admin.vo;

import models.User;

/**
 * @author carloxwang
 *
 */
public class UserVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = -8841819449606147467L;

	public String userId;
	public String email;
	public String nickname;
	public String avatarImageUrl;


	public UserVO(User user) {
		super(user);
		
		if(user != null){
			this.userId = user.id.toString();
			this.email = user.email;
			this.nickname = user.nickname;
	
			this.avatarImageUrl = "https://graph.facebook.com/" + user.fbUserId + "/picture?type=large";
		}
	}
}
