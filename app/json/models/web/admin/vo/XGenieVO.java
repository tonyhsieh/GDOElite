/**
 *
 */
package json.models.web.admin.vo;

import java.util.List;

import models.XGenie;

/**
 * @author carloxwang
 *
 */
public class XGenieVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = 6758851580465187834L;
	public String xGenieId;
	public String name;
	public String macAddr;
	public String sgId;
	public int status;
	public int deviceType;
	
	public List<XGenieInfoVO> listXgInfo;

	public XGenieVO(XGenie entity) {
		super(entity);

		this.xGenieId = entity.id.toString();
		this.name = entity.name;
		this.macAddr = entity.macAddr;
		if(entity.smartGenie == null){
			this.sgId = "null";
		}else{
			this.sgId = entity.smartGenie.id.toString();
		}
		this.status = entity.status;
		this.deviceType = entity.deviceType;
	}
	
	public XGenieVO(XGenie entity, List<XGenieInfoVO> listXgInfo) {
		super(entity);

		this.xGenieId = entity.id.toString();
		this.name = entity.name;
		this.macAddr = entity.macAddr;
		if(entity.smartGenie == null){
			this.sgId = "null";
		}else{
			this.sgId = entity.smartGenie.id.toString();
		}
		this.status = entity.status;
		//this.xgInfo = xgInfo;	
		this.deviceType = entity.deviceType;
		
		this.listXgInfo = listXgInfo;
	}

}
