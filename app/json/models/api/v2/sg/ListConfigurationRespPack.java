package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.ConfigurationInfoBaseVO;

import java.util.List;

public class ListConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4250779519971285690L;

	public List<? super ConfigurationInfoBaseVO> configlist;
}
