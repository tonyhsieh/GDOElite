package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.SmartGenie4ChkFwVerVO;
import json.models.api.v2.sg.vo.XGenieVO;

import java.util.List;

public class ChkAllFwVerRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2270608738187634109L;

	
	public SmartGenie4ChkFwVerVO homeExtender;
	
	public List<XGenieVO> xGenie;
}
