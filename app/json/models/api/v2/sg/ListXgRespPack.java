package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.XGenieInfoBaseVO;

import java.util.List;

public class ListXgRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4528927659070428458L;

	public List<? super XGenieInfoBaseVO> xGenieList;
}
