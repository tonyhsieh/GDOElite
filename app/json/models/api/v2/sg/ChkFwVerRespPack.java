package json.models.api.v2.sg;

import json.models.api.v1.sg.vo.SmartGenieVO;
import json.models.api.v1.sg.vo.XGenieVO;

import java.util.List;

/**
 * 
 * @author johnwu
 *
 */

public class ChkFwVerRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3442853318959431223L;

	public String sessionId;
	
	public SmartGenieVO homeExtender;

	public List<XGenieVO> xGenie;
}
