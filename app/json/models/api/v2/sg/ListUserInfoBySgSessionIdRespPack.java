package json.models.api.v2.sg;

import json.models.api.v1.sg.vo.UserInfoVO;

import java.util.List;

public class ListUserInfoBySgSessionIdRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7297111453683001133L;

	public List<UserInfoVO> userList;
}
