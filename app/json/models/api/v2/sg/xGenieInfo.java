package json.models.api.v2.sg;

public class xGenieInfo{

	private static final long serialVersionUID = -4728927659070428458L;

	public String macAddr;
	
	public String ipaddr;

    public String deviceType;

    public String firmwareGroupName;

    public String fwVer;

    public Boolean alive;

    public String name;

    public Boolean light;

    public Integer fps;

    public Boolean doorstatus;

    public Boolean lowbatt;

    public String battChangeDate;

}
