/**
 *
 */
package json.models.api.v2.sg;

/**
 * @author carloxwang
 *
 */
public class SGCfgRespPack extends BaseRespPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 6522030933240949408L;

	public String downloadUrl;
}
