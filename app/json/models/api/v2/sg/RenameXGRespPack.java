package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.XGenieInfoBaseVO;

import java.util.List;



public class RenameXGRespPack extends BaseRespPack{

    private static final long serialVersionUID = -11;

    public List<? super XGenieInfoBaseVO> xGenieList;

}
