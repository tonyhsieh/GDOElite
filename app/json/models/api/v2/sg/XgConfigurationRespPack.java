package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.ConfigurationInfoBaseVO;

import java.util.List;

public class XgConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5245112996042161157L;

	public List<? super ConfigurationInfoBaseVO> configlist;
}
