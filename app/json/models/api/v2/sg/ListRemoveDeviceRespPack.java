package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.XGenieInfoBaseVO;

import java.util.List;

public class ListRemoveDeviceRespPack extends BaseRespPack{

	private static final long serialVersionUID = -5042750073280983892L;


	public List<? super XGenieInfoBaseVO> xGenieList;
}
