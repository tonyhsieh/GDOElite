package json.models.api.v2.sg.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tony_hsieh on 2017/4/28.
 */
public class XGenieFwVerVO implements Serializable {

    private static final long serialVersionUID = -5121411818806095232L;

    public List<XGenieFwVer> xGenieFwList;

}
