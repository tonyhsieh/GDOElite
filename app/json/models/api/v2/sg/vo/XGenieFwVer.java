package json.models.api.v2.sg.vo;

import java.io.Serializable;

/**
 * Created by tony_hsieh on 2017/4/28.
 */
public class XGenieFwVer implements Serializable {
    private static final long serialVersionUID = -5506964679889016611L;

    public String xGenieMac;
    public String xGenieFwVer;

}
