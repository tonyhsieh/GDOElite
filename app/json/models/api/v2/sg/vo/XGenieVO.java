package json.models.api.v2.sg.vo;

/**
 *
 * @author johnwu
 *
 */

public class XGenieVO {

	public String id;
	public String macAddr;

	public String newFwVer;

	public String downloadUrl;
	public int forceUpgrade;

	public String forceFwVer;
	public String downloadForceUrl;

}
