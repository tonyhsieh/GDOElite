package json.models.api.v2.sg.vo;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

/**
 * Created by tony_hsieh on 2017/5/4.
 */

@JsonAutoDetect( fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE )
public class ConfigurationInfoBaseVO {
    public String cfgId;

    public String desc;

    public String update;

    public String cfgClass;

    public boolean available;

    public String xGenieMac;
}
