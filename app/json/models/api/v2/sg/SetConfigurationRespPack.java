package json.models.api.v2.sg;

import json.models.api.v2.sg.vo.ConfigurationInfoBaseVO;

import java.util.List;

public class SetConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 849538828945262738L;

	public List<? super ConfigurationInfoBaseVO> configlist;
}
