package json.models.api.v2.app;

import json.models.api.v1.app.BaseRespPack;
import json.models.api.v2.app.vo.SmartGenieInfoVO;

import java.util.List;

public class ListSmartGenieNetworkInfoRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3536774770855824827L;

	public String sessionId;

	public SmartGenieInfoVO smartGenieInfo;
}
