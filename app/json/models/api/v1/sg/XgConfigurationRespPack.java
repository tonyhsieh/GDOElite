package json.models.api.v1.sg;

public class XgConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 5245112996042161157L;

	public String sessionId;
	
	public String configurationId;
}
