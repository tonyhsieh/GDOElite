package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.ICProgramVO;

public class GetMuiltyConfigurationsRespPack extends BaseRespPack{

    /**
     * 
     */
    private static final long serialVersionUID = 2276451628126500388L;
    
    public String sessionId;
    
    public List<ICProgramVO> result;

}
