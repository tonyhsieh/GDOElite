package json.models.api.v1.sg;

import json.models.api.v1.sg.BaseRespPack;

public class RenameXGRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7749265226598238865L;

	public String sessionId;
}
