package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.ConfigurationInfoVO;

public class ListConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4250779519971285690L;

	public String sessionId;
	
	public List<ConfigurationInfoVO> listCfg;
}
