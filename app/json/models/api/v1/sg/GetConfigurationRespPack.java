package json.models.api.v1.sg;

public class GetConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8581480768102072863L;

	public String sessionId;
	
	public String desc;
	
	public String xgId;
	
	public boolean avilable;
	
	public String cfgType;
	
	public String configuration;
}
