package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.SmartGenieVO;
import json.models.api.v1.sg.vo.XGenieVO;

/**
 * 
 * @author johnwu
 *
 */

public class ChkFwVerRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3442853318959431223L;

	public String sessionId;
	
	public SmartGenieVO smartGenie;
	
	//public List<XGenieVO> xgenie;
	
	public XGenieVO xgenie;
}
