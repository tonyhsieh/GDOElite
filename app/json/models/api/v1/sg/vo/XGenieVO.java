package json.models.api.v1.sg.vo;

/**
 *
 * @author johnwu
 *
 */

public class XGenieVO{

	public String id;
	public String macAddr;

	public String newFwVer;

	public String downloadUrl;
	public int forceUpgrade;

	public String forceFwVer;
	public String downloadForceUrl;

	@Override
	public String toString() {
		return "XGenieVO{" +
				"id='" + id + '\'' +
				", macAddr='" + macAddr + '\'' +
				", newFwVer='" + newFwVer + '\'' +
				", downloadUrl='" + downloadUrl + '\'' +
				", forceUpgrade=" + forceUpgrade +
				", forceFwVer='" + forceFwVer + '\'' +
				", downloadForceUrl='" + downloadForceUrl + '\'' +
				'}';
	}
}
