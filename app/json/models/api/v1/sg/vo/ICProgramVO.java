package json.models.api.v1.sg.vo;

public class ICProgramVO {
    
    public String desc;
    
    public String xgId;
    
    public boolean avilable;
    
    public String cfgType;
    
    public String configurationId;
    
    public String configuration;
}
