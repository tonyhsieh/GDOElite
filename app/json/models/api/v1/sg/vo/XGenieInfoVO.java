package json.models.api.v1.sg.vo;

public class XGenieInfoVO {
	public String macAddr;
	public int deviceType;
	public String name;
	public String parentMacAddr;
	public String parentParam;
}
