/**
 *
 */
package json.models.api.v1.sg;

import java.io.Serializable;

/**
 * @author carloxwang
 *
 */
public class JsonStatusCode implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8299874814440684740L;

	public Integer code = 1;

	public String message = "SUCCESS";
}
