package json.models.api.v1.sg;

import java.util.List;

import json.models.api.v1.sg.vo.XGenieInfoVO;

public class ListXgRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4728927659070428458L;

	public String sessionId;
	
	public List<XGenieInfoVO> listXgenie;
}
