package json.models.api.v1.app;

import java.util.List;

import json.models.api.v1.app.vo.MsgVO;

public class NotificationRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 5454852729303747267L;

	public String sessionId;
	
	public List<MsgVO> listMsg;
}
