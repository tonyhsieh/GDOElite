/**
 *
 */
package json.models.api.v1.app;

import java.util.Calendar;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author carloxwang
 *
 */
public class ResetPwdRespPack extends BaseRespPack {

	/**
	 *
	 */
	private static final long serialVersionUID = 8252931170381074782L;

	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar validBefore;

	public String targetEmail;
}
