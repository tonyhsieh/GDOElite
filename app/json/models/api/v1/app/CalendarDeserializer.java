/**
 *
 */
package json.models.api.v1.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

import frameworks.Constants;

/**
 * @author carloxwang
 *
 */
public class CalendarDeserializer extends JsonDeserializer<Calendar> {

	@Override
	public Calendar deserialize(JsonParser jsonParser, DeserializationContext ctx) throws IOException, JsonProcessingException {

		SimpleDateFormat sdf = new SimpleDateFormat(Constants.JSON_CALENDAR_PATTERN);

		Calendar c = new GregorianCalendar();
		sdf.format(c.getTime());

		return c;
	}

}
