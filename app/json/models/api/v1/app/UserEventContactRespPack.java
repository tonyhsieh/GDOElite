/**
 * 
 */
package json.models.api.v1.app;

import java.util.ArrayList;

import json.models.api.v1.app.vo.EventContactData;

/**
 * @author johnwu
 * @version 創建時間：2013/8/19 下午5:22:59
 */

public class UserEventContactRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8577891529930688068L;
	
	public String sessionId;
	
	public ArrayList<EventContactData> ecDataList;

	
}
