package json.models.api.v1.app;

public class XgConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 448864567565752789L;

	public String sessionId;
	
	public String configurationId;
}
