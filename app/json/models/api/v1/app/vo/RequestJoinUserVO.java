package json.models.api.v1.app.vo;

import java.util.Calendar;

import json.models.api.v1.app.CalendarSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class RequestJoinUserVO{
	
	public String reqJoinId;
	
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar reqTimeStamp;
	
	public String nickName;
	
	public String email;
}
