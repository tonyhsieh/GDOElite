package json.models.api.v1.app.vo;

import java.io.Serializable;

public class EventContactData implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -5506964679889016619L;
	
	public int eventType;
	public boolean checkEmail;
	public boolean checkSMS;
	public boolean checkNotification;
}