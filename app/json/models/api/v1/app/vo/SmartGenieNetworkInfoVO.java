package json.models.api.v1.app.vo;

public class SmartGenieNetworkInfoVO {

	public boolean isAtLan;

	public String smartGenieInnerIP;
	
	public String smartGenieNetmask;
	
	public String wifiMacAddress;
	
	public String sgMac;
}
