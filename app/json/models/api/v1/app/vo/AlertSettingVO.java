package json.models.api.v1.app.vo;

import controllers.api.v1.app.form.AlertResultRespPack;

import java.io.Serializable;
import java.lang.reflect.Array;
import java.util.List;

public class AlertSettingVO implements Serializable
{
    private static final long serialVersionUID = -8633027390416616941L;

    public String xGenieMac;
    public List<AlertResultRespPack.AlertDevMacItem> event;

    @Override
    public String toString() {
        return "AlertSettingVO{" +
                "xgMAC='" + xGenieMac + '\'' +
                ", event=" + event +
                '}';
    }
}
