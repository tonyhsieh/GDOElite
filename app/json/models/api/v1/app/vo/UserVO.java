/**
 *
 */
package json.models.api.v1.app.vo;

import models.User;

/**
 * @author carloxwang
 *
 */
public class UserVO extends BaseVO {

	/**
	 *
	 */
	private static final long serialVersionUID = -6607125287387350151L;

	public String id;

	public String nickname;

	public String ownerUserId;

	public String email;

	public UserVO(User user){
		super.createAt = user.createAt;
		super.updateAt = user.updateAt;
		this.id = user.id.toString();
		this.nickname = user.nickname;
		this.email = user.email;
	}

}
