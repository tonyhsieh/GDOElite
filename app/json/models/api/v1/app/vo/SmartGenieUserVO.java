package json.models.api.v1.app.vo;

import java.io.Serializable;

public class SmartGenieUserVO implements Serializable
{
    private static final long serialVersionUID = 8085437253610681069L;

    public String userId;
    public String nickName;
    public Integer userType;
    public String email;
    public String countryCode;
    public String cellPhone;
    public Integer gatewayId;
    public String gatewayName;
}
