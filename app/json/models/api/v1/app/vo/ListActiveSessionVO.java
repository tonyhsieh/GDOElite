package json.models.api.v1.app.vo;

public class ListActiveSessionVO {

	public String sessionId;
	
	public String loginTimeStamp;
	
	public String city;
	
	public String deviceName;
	
	public String deviceManufacturer;
	
	public String deviceOsName;
	
	public String deviceOsVersion;
}
