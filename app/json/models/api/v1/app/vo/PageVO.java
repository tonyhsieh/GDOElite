package json.models.api.v1.app.vo;

import java.io.Serializable;
import java.util.List;

/**
 * Created by tony_hsieh on 2016/8/11.
 */
public class PageVO<T> implements Serializable {
    private static final long serialVersionUID = -7402518230467456884L;

    public Long page;

    public Long pageSize;

    public Long totalPage;

    public Long totalCount;

    public Boolean isFirst;

    public Boolean isLast;

    public List<T> elements;

    public PageVO(){

    }


    public PageVO(Long pageCurrent, Long pageSize, Long totalPageCount, Long totalCount) {
        this.page = pageCurrent;
        this.pageSize = pageSize;
        this.totalPage = totalPageCount;
        this.totalCount = totalCount;

        this.isFirst = ( pageCurrent == 1 );
        this.isLast = ( pageCurrent == totalPageCount);

    }
}
