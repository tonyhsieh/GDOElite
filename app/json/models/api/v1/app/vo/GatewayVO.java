package json.models.api.v1.app.vo;

import java.io.Serializable;

public class GatewayVO implements Serializable
{
    private static final long serialVersionUID = -597161432360299263L;

    public Integer id;
    public String countryCode;
    public String name;

    @Override
    public String toString()
    {
        return "Gateway{" +
                "id=" + id +
                ", countryCode='" + countryCode + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
