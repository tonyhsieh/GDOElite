package json.models.api.v1.app.vo;

import java.util.Calendar;

import json.models.api.v1.app.CalendarSerializer;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

public class MsgVO extends BaseVO {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4398779067657245665L;

	public String message;
	
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar messageTimeStamp;
}
