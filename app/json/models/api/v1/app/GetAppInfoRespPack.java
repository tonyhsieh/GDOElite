package json.models.api.v1.app;

public class GetAppInfoRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -7865062284478552958L;
	
	public String sessionId;
	
	public int versionCode;
	
	public String versionName;
	
	public int forceUpgrade;
	
	//Add int forceUpgrade 0(no) Or 1(yes)
}
