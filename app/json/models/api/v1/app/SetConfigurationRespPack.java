package json.models.api.v1.app;

public class SetConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -6909235418194373633L;

	public String sessionId;
	
	public String configurationId;
}
