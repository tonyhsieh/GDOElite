/**
 *
 */
package json.models.api.v1.app;

import java.io.IOException;
import java.util.List;

import json.models.api.v1.app.vo.RequestJoinUserVO;
import json.models.api.v1.app.vo.UserVO;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * @author carloxwang
 *
 */
public class UserRespPack extends BaseRespPack {

	/**
	 *
	 */
	private static final long serialVersionUID = -4155849335988504924L;

	public String sessionId;
	
	public UserVO myInfo;
	
	public UserRespPack(){}
	
	public void getMyInfo(UserVO myInfo, String sessionId){
		
		this.myInfo = myInfo; 
		
		this.sessionId = sessionId;
		
		ObjectMapper om = new ObjectMapper();
		String responseString = "";
		
		try {
			responseString = om.writeValueAsString(myInfo);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
	
	public void setMyInfo(String sessionId){
		this.sessionId = sessionId;
	}
	
	public List<RequestJoinUserVO> listUser;
}
