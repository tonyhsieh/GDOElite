package json.models.api.v1.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.models.api.v1.app.vo.ListActiveSessionVO;
import models.UserDevLog;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ListActiveSessionRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2002931418335422031L;

	public String sessionId;
	
	public ListActiveSessionRespPack(){}
	
	public List<ListActiveSessionVO> listSession = new ArrayList<ListActiveSessionVO>();
	
	public void getListActiveSession(UserDevLog userDevLog, String city){
		
		ListActiveSessionVO listActiveSessionVO = new ListActiveSessionVO();
		
		listActiveSessionVO.city = city;
		listActiveSessionVO.deviceManufacturer = userDevLog.deviceManufacturer;
		listActiveSessionVO.deviceName = userDevLog.deviceName;
		listActiveSessionVO.deviceOsVersion = userDevLog.deviceOsVersion;
		listActiveSessionVO.deviceOsName = userDevLog.deviceOsName;
		listActiveSessionVO.sessionId = userDevLog.userSessionId.toString();
		
		Calendar createDate = userDevLog.createAt;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		listActiveSessionVO.loginTimeStamp = sdf.format(createDate.getTime());
		
		listSession.add(listActiveSessionVO);
		
		ObjectMapper om = new ObjectMapper();
		String responseString = "";
		
		try {
			responseString = om.writeValueAsString(listSession);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
