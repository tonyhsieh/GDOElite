package json.models.api.v1.app;

import java.util.List;

import json.models.api.v1.app.vo.UserSmartGenieVO;

/**
 * 
 * @author johnwu
 *
 */
public class SmartGenieRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6959466290295491685L;

	public String sessionId;
	
	public String smartGenieId;
	
	public List<UserSmartGenieVO> listSmartGenie;
}
