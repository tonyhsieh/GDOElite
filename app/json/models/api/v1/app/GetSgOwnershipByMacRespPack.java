package json.models.api.v1.app;

public class GetSgOwnershipByMacRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1038087362539113065L;

	public String sessionId;
	
	public boolean hasOwner;
	
	public boolean ownership;
	
	public boolean usership;
	
	public String macAddr;
}
