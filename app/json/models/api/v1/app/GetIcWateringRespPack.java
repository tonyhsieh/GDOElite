package json.models.api.v1.app;

import controllers.api.v1.app.form.IcWateringTime;

public class GetIcWateringRespPack extends BaseRespPack {

	private String sessionId;
	IcWateringTime[] log;
	String startTime;
	String endTime;
	
	public GetIcWateringRespPack(IcWateringTime[] log) {
		this.log = log;
	}

	public String getSessionId() {
		return sessionId;
	}

	public void setSessionId(String sessionId) {
		this.sessionId = sessionId;
	}

	public IcWateringTime[] getLog() {
		return log;
	}

	public void setLog(IcWateringTime[] log) {
		this.log = log;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}
	
}
