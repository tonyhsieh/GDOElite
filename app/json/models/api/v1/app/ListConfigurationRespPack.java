package json.models.api.v1.app;

import java.util.List;

import json.models.api.v1.app.vo.ConfigurationInfoVO;

public class ListConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1363743249970487085L;

	public String sessionId;
	
	public List<ConfigurationInfoVO> listCfg;
}
