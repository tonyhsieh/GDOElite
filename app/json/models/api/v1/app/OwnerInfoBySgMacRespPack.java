package json.models.api.v1.app;

public class OwnerInfoBySgMacRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 9063302762564325295L;

	public String sessionId;
	
	public String nickname;
	
	public String email;
	
	public String userId;
}
