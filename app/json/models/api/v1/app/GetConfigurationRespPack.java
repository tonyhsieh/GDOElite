package json.models.api.v1.app;

public class GetConfigurationRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1957004243215650846L;

	public String sessionId;
	
	public String desc;
	
	public String xgId;
	
	public boolean avilable;
	
	public String cfgType;
	
	public String configuration;
}
