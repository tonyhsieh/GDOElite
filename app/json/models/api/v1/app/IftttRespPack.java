package json.models.api.v1.app;


/**
 * 
 * @author johnwu
 *
 */

public class IftttRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4226637508730463539L;

	public Boolean iFTTTEnabled;
}
