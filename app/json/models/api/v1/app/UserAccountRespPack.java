package json.models.api.v1.app;

public class UserAccountRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7255807705975345353L;

	public String sessionId;
	
	public boolean availableCountryCode;
}
