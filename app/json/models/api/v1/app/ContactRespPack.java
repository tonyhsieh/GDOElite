package json.models.api.v1.app;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import json.models.api.v1.app.vo.ContactVO;
import models.ContactGroup;

import com.fasterxml.jackson.databind.ObjectMapper;

public class ContactRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = -4516976900544240168L;

	public String sessionId;
	
	public ContactRespPack(){}
	
	public List<ContactVO> listContactGroup = new ArrayList<ContactVO>();
	
	public void getListContactGroup(ContactGroup contactGroup){
		
		ContactVO contactVO = new ContactVO();

		contactVO.name = contactGroup.name;
		contactVO.msgType = contactGroup.msgType;
		contactVO.contactGroupId = contactGroup.id.toString();
		
		Calendar createDate = contactGroup.createAt;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		contactVO.createAt = sdf.format(createDate.getTime());
		
		listContactGroup.add(contactVO);
		
		ObjectMapper om = new ObjectMapper();
		String responseString = "";
		
		try {
			responseString = om.writeValueAsString(listContactGroup);
		} catch (IOException e) {

			e.printStackTrace();
		}
	}
}
