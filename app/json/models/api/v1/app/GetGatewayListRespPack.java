package json.models.api.v1.app;

import java.util.ArrayList;
import java.util.List;

import json.models.api.v1.app.vo.GatewayVO;

public class GetGatewayListRespPack extends BaseRespPack
{
    private static final long serialVersionUID = 1362067321568076851L;

    public String sessionId;
    public List<GatewayVO> listGateway = new ArrayList<>();
}
