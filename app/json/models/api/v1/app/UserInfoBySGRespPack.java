package json.models.api.v1.app;

import java.util.ArrayList;
import java.util.List;

import json.models.api.v1.app.vo.GatewayVO;
import json.models.api.v1.app.vo.SmartGenieUserVO;

public class UserInfoBySGRespPack extends BaseRespPack
{
    private static final long serialVersionUID = 1815566581694277892L;

    public String sessionId;

    public List<SmartGenieUserVO> listUser = new ArrayList<>();

    public List<GatewayVO> listGateway = new ArrayList<>();
}
