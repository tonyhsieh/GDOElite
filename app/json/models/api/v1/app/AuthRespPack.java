package json.models.api.v1.app;

/**
 * @author john
 */

public class AuthRespPack extends BaseRespPack {

	/**
	 * 
	 */
	private static final long serialVersionUID = 6419872343502339949L;

	public String sessionId;
}
