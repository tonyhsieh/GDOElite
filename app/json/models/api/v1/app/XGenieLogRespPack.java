package json.models.api.v1.app;

import java.io.Serializable;
import java.util.List;

public class XGenieLogRespPack extends BaseRespPack
{
    public String sessionId;

    public List<Object> log;

    public static class XGenieGDOEventLogItem implements Serializable
    {
        private static final long serialVersionUID = -7687554752693886791L;

        public String time;
        public String user;
        public int door;
        public int action;
    }

    public static class XGenieSDEventLogItem implements Serializable
    {
        private static final long serialVersionUID = 8733915412687628234L;

        public String time;
        public int event;
        public int interrupt;
    }
}
