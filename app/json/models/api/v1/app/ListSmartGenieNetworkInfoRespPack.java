package json.models.api.v1.app;

import java.util.List;

import json.models.api.v1.app.vo.SmartGenieInfoVO;
import json.models.api.v1.app.vo.UserSmartGenieVO;

public class ListSmartGenieNetworkInfoRespPack extends BaseRespPack{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3536774770855824827L;

	public String sessionId;
	
	public String smartGenieId;
	
	public List<SmartGenieInfoVO> listSmartGenie;
}
