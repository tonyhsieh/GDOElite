/**
 *
 */
package json.models.api.v1.app;

import java.io.Serializable;
import java.util.Calendar;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 * @author carloxwang
 *
 */

@JsonAutoDetect( fieldVisibility = JsonAutoDetect.Visibility.ANY, getterVisibility = JsonAutoDetect.Visibility.NONE, setterVisibility = JsonAutoDetect.Visibility.NONE, creatorVisibility = JsonAutoDetect.Visibility.NONE )
public class BaseRespPack implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = -6530855634561555817L;

	public JsonStatusCode status = new JsonStatusCode();
	
	@JsonSerialize(using=CalendarSerializer.class)
	public Calendar timestamp;

}
