package json.models.api.v1;

import org.apache.commons.lang3.builder.ReflectionToStringBuilder;

import java.io.Serializable;

import frameworks.models.StatusCode;
import json.models.api.v1.app.JsonStatusCode;

/**
 * basic response pack
 */
public class BasicRespPack implements Serializable
{
    private static final long serialVersionUID = -2310396167458222815L;

    public BasicRespPack()
    {
        status = new JsonStatusCode();
        status.code = StatusCode.GENERAL_SUCCESS.value;
        status.message = StatusCode.GENERAL_SUCCESS.name();
    }

    public JsonStatusCode status;

    @Override
    public String toString()
    {
        return ReflectionToStringBuilder.toString(this);
    }
}
