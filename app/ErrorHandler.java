import org.apache.commons.lang3.exception.ExceptionUtils;

import frameworks.models.StatusCode;
import play.*;
import play.api.OptionalSourceMapper;
import play.api.routing.Router;
import play.http.DefaultHttpErrorHandler;
import play.mvc.Http.*;
import play.mvc.*;
import utils.MyUtil;

import javax.inject.*;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.CompletionStage;

public class ErrorHandler extends DefaultHttpErrorHandler
{
    @Inject
    public ErrorHandler(Configuration configuration, Environment environment,
                        OptionalSourceMapper sourceMapper, Provider<Router> routes)
    {
        super(configuration, environment, sourceMapper, routes);
    }

    @Override
    protected CompletionStage<Result> onBadRequest(RequestHeader request, String message)
    {
        Logger.error("onBadRequest: " + message);
        return CompletableFuture.completedFuture(
                Results.badRequest(MyUtil.getBasicResponse(request, StatusCode.BAD_REQUEST, message))
        );
    }

    @Override
    protected CompletionStage<Result> onForbidden(RequestHeader request, String message)
    {
        Logger.error("onForbidden: " + message);
        return CompletableFuture.completedFuture(
                Results.forbidden(MyUtil.getBasicResponse(request, StatusCode.FORBIDDEN, message))
        );
    }

    @Override
    protected CompletionStage<Result> onNotFound(RequestHeader request, String message)
    {
        Logger.error("onNotFound: " + message);
        return CompletableFuture.completedFuture(
                Results.notFound(MyUtil.getBasicResponse(request, StatusCode.NOT_FOUND, message))
        );
    }

    @Override
    public CompletionStage<Result> onServerError(RequestHeader request, Throwable exception)
    {
        Logger.error("onServerError: ", ExceptionUtils.getStackTrace(exception));
        exception.printStackTrace();
        return CompletableFuture.completedFuture(
                Results.internalServerError(MyUtil.getBasicResponse(request, StatusCode.INTERNAL_SERVER_ERROR, ExceptionUtils.getStackTrace(exception)))
        );
    }
}