CREATE TABLE IF NOT EXISTS `call_channel_ip` (
  `mac_address` varchar(255) NOT NULL,
  `server_ip` varchar(255) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`mac_address`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 ;
