ALTER TABLE smart_genie ADD photo_file_id VARCHAR(255) NULL DEFAULT NULL;

UPDATE `event` SET `event_msg`='Smoke Detector Alert On' WHERE `event_type`='-10150';
UPDATE `event` SET `event_msg`='Smoke Detector Alert Off' WHERE `event_type`='-10151';
UPDATE `event` SET `event_msg`='Smoke Detector Malfunction' WHERE `event_type`='-10152';

INSERT INTO `event` (`event_type`, `create_at`, `update_at`, `update_user_id`, `status`, `create_user_id`, `event_msg`, `can_notify`, `comment`, `opt_lock`) VALUES ('20300', '2016-03-30 11:00:00', '2016-03-30 11:00:00', 'SYSTEM', '1', 'SYSTEM', 'Keypad Battery Low', '1', '', '1');
INSERT INTO `event` (`event_type`, `create_at`, `update_at`, `update_user_id`, `status`, `create_user_id`, `event_msg`, `can_notify`, `comment`, `opt_lock`) VALUES ('20301', '2016-03-30 11:00:00', '2016-03-30 11:00:00', 'SYSTEM', '1', 'SYSTEM', 'Keypad Alert On', '1', '', '1');
INSERT INTO `event` (`event_type`, `create_at`, `update_at`, `update_user_id`, `status`, `create_user_id`, `event_msg`, `can_notify`, `comment`, `opt_lock`) VALUES ('20302', '2016-03-30 11:00:00', '2016-03-30 11:00:00', 'SYSTEM', '1', 'SYSTEM', 'Keypad Alert Off', '1', '', '1');

CREATE TABLE IF NOT EXISTS ic_event_log (
  xgenie_id                 varchar(64),
  zone_id                   tinyint not null,
  event_type                smallint not null,
  start_time                datetime not null,
  duration                  smallint not null,
  end_time                  datetime not null,
  create_at                 datetime not null,
  update_at                 datetime,
  update_user_id            varchar(64),
  status                    integer not null,
  create_user_id            varchar(64) not null,
  opt_lock                  bigint not null,
  constraint ck_ic_usage_log_zone_id check (zone_id in (0,1,2,3,4,5,6)),
  constraint pk_ic_usage_log primary key (xgenie_id,zone_id,event_type,start_time)
);

CREATE TABLE IF NOT EXISTS ic_event_stats (
  id                        integer not null AUTO_INCREMENT,
  data_id                   tinyint not null,
  xgenie_id                 varchar(64),
  zone_id                   tinyint not null,
  event_type                smallint not null,
  date                      datetime not null,
  cum_mins                  int not null,
  create_at                 datetime not null,
  update_at                 datetime,
  update_user_id            varchar(64),
  status                    integer not null,
  create_user_id            varchar(64) not null,
  opt_lock                  bigint not null,
  constraint ck_ic_usage_stats_data_id check (data_id in (-1,0,1,2,3)),
  constraint ck_ic_usage_stats_zone_id check (zone_id in (0,1,2,3,4,5,6)),
  constraint pk_ic_usage_stats primary key (id)
);

CREATE TABLE IF NOT EXISTS `xgenie_gdo_log` (
  `id` VARCHAR(64) NOT NULL,
  `create_at` DATETIME NULL,
  `update_at` DATETIME NULL,
  `update_user_id` VARCHAR(64) NULL,
  `status` INT NULL,
  `create_user_id` VARCHAR(64) NULL,
  `user_name` VARCHAR(255) NULL,
  `sg_mac_addr` CHAR(20) NULL,
  `xg_mac_addr` CHAR(20) NULL,
  `door` INT NULL,
  `event` INT NULL,
  `by_kp` TINYINT(1) NULL,
  `ts` DATETIME(3) NOT NULL,
  `opt_lock` BIGINT(20) NULL,
  PRIMARY KEY (`id`),
  INDEX(`ts`));

CREATE TABLE IF NOT EXISTS `xgenie_sd_log` (
  `id` VARCHAR(64) NOT NULL,
  `create_at` DATETIME NULL,
  `update_at` DATETIME NULL,
  `update_user_id` VARCHAR(64) NULL,
  `status` INT NULL,
  `create_user_id` VARCHAR(64) NULL,
  `user_name` VARCHAR(255) NULL,
  `sg_mac_addr` CHAR(20) NULL,
  `xg_mac_addr` CHAR(20) NULL,
  `event` INT NULL,
  `interrupt` INT NULL,
  `ts` DATETIME(3) NOT NULL,
  `opt_lock` BIGINT(20) NULL,
  PRIMARY KEY (`id`),
  INDEX(`ts`));


