CREATE TABLE IF NOT EXISTS `gateway` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(10) DEFAULT NULL,
  `carrier` varchar(64) DEFAULT NULL,
  `gateway` varchar(64) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

INSERT INTO `gateway` (`id`, `country_code`, `carrier`, `gateway`, `create_at`, `update_at`, `update_user_id`, `status`, `create_user_id`, `opt_lock`) VALUES
(1, '1', 'AllTel', 'number@text.wireless.alltel.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(2, '1', 'AT&T', 'number@txt.att.net', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(3, '1', 'Boost Mobile', 'number@myboostmobile.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(4, '1', 'Cricket', 'number@sms.mycricket.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(5, '1', 'Sprint', 'number@messaging.sprintpcs.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(6, '1', 'T-Mobile', 'number@tmomail.net', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(7, '1', 'US Cellular', 'number@email.uscc.net', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(8, '1', 'Verizon', 'number@vtext.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1),
(9, '1', 'Virgin Mobile', 'number@vmobl.com', '2017-02-22 22:22:22', '2017-02-22 22:22:22', 'SYSTEM', 1, 'SYSTEM', 1);

ALTER TABLE `user` ADD COLUMN `gateway_id` int(11) DEFAULT NULL;
ALTER TABLE `user` ADD COLUMN `pin_validated` tinyint(1) DEFAULT NULL;
ALTER TABLE `user` ADD FOREIGN KEY (`gateway_id`) REFERENCES `gateway`(`id`);

ALTER TABLE `user_relationship` ADD COLUMN `gateway_id` int(11) DEFAULT NULL;
ALTER TABLE `user_relationship` ADD COLUMN `pin_validated` tinyint(1) DEFAULT NULL;
ALTER TABLE `user_relationship` ADD FOREIGN KEY (`gateway_id`) REFERENCES `gateway`(`id`);

ALTER TABLE `user_relationship_keeper` ADD COLUMN `gateway_id` int(11) DEFAULT NULL;
ALTER TABLE `user_relationship_keeper` ADD COLUMN `pin_validated` tinyint(1) DEFAULT NULL;
ALTER TABLE `user_relationship_keeper` ADD FOREIGN KEY (`gateway_id`) REFERENCES `gateway`(`id`);
