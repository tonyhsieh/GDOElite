ALTER TABLE smart_genie ADD COLUMN auto_upgrade boolean NULL;
ALTER TABLE smart_genie ADD COLUMN auto_upgrade_time VARCHAR(40) NULL;
ALTER TABLE smart_genie ADD COLUMN auto_upgrade_once boolean NULL;
