-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: localhost    Database: asante
-- ------------------------------------------------------
-- Server version	5.7.12-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_white_list`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `admin_white_list` (
  `email` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_action_history`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `app_action_history` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_id` varchar(40) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `form` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_app_action_history_user_id_28` (`user_id`),
  KEY `ix_app_action_history_user_id_action_29` (`user_id`,`action`),
  KEY `ix_app_action_history_user_id_create_at_43` (`user_id`,`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_action_record`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `app_action_record` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `form` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `app_info`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `app_info` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `version_code` int(11) NOT NULL,
  `version_name` varchar(64) DEFAULT NULL,
  `device_os_name` varchar(64) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `force_upgrade` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `checkout`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `checkout` (

  `id` varchar(40) NOT NULL,
  `no` varchar(40) DEFAULT NULL,
  `price` varchar(40) DEFAULT NULL,
  `tax` varchar(40) DEFAULT NULL,
  `total` varchar(40) DEFAULT NULL,
  `pay` varchar(40) DEFAULT NULL,
  `table_no` varchar(40) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `configuration` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `mac_addr` varchar(40) NOT NULL,
  `descript` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avilable` tinyint(1) DEFAULT '0',
  `cfg_type` varchar(40) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_configuration_20` (`mac_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `configuration_history`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `configuration_history` (

  `id` varchar(40) NOT NULL,
  `configuration_id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `mac_addr` varchar(40) NOT NULL,
  `descript` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `avilable` tinyint(1) DEFAULT '0',
  `cfg_type` varchar(40) DEFAULT NULL,
  `sg_id` varchar(40) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_configuration_history_21` (`mac_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `contact` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `event_contact_id` varchar(255) DEFAULT NULL,
  `notify_email` tinyint(1) DEFAULT '0',
  `notify_sms` tinyint(1) DEFAULT '0',
  `notify_push_notification` tinyint(1) DEFAULT '0',
  `user_id` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_contact_eventContact_3` (`event_contact_id`),
  KEY `ix_contact_user_id_32` (`user_id`),
  CONSTRAINT `fk_contact_eventContact_3` FOREIGN KEY (`event_contact_id`) REFERENCES `event_contact` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `contact_group`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `contact_group` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `sg_id` varchar(255) DEFAULT NULL,
  `msg_type` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data_file`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `data_file` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `file_id` varchar(40) NOT NULL,
  `content_type` varchar(40) NOT NULL,
  `descript` varchar(40) DEFAULT NULL,
  `data_type` varchar(40) NOT NULL,
  `data_name` varchar(40) NOT NULL,
  `sg_id` varchar(40) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_data_file_22` (`sg_id`),
  KEY `ix_data_file_24` (`data_type`,`data_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `data_file_history`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `data_file_history` (

  `id` varchar(40) NOT NULL,
  `data_file_id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `file_id` varchar(40) NOT NULL,
  `content_type` varchar(40) NOT NULL,
  `descript` varchar(40) DEFAULT NULL,
  `data_type` varchar(40) NOT NULL,
  `data_name` varchar(40) NOT NULL,
  `sg_id` varchar(40) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_data_file_history_23` (`sg_id`),
  KEY `ix_data_file_history_25` (`data_type`,`data_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `device_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `owner_user_id` varchar(255) DEFAULT NULL,
  `sg_id` varchar(255) DEFAULT NULL,
  `x_gid` varchar(255) DEFAULT NULL,
  `event_type` int(11) DEFAULT NULL,
  `event_data` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `device_position_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `device_position_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_dev_log_id` varchar(40) DEFAULT NULL,
  `mcc` varchar(255) DEFAULT NULL,
  `mnc` varchar(255) DEFAULT NULL,
  `lac` varchar(255) DEFAULT NULL,
  `cell_id` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `lat` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `duplicate_event_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `duplicate_event_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `msg_id` varchar(255) DEFAULT NULL,
  `event_type` int(11) DEFAULT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  KEY `ix_duplicate_event_log_smart_genie_id_44` (`smart_genie_id`,`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `employee`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `employee` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `department` varchar(255) DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `phone_extension` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `event` (

  `event_type` int(11) NOT NULL AUTO_INCREMENT,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `event_msg` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `can_notify` tinyint(1) DEFAULT '0',
  `comment` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`event_type`)
) ENGINE=InnoDB AUTO_INCREMENT=10104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `event_contact`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `event_contact` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `event_type` int(11) DEFAULT NULL,
  `device_mac` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_event_contact_device_mac_33` (`device_mac`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `file_entity`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `file_entity` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `file_ext` varchar(255) DEFAULT NULL,
  `file_size` bigint(20) DEFAULT NULL,
  `s3path` varchar(255) DEFAULT NULL,
  `content_type` int(11) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `firmware_group`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `firmware_group` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `firmware_version` varchar(255) DEFAULT NULL,
  `file_entity_id` varchar(40) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `force_upgrade` int(11) DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `ix_firmware_group_fileEntity_4` (`file_entity_id`),
  CONSTRAINT `fk_firmware_group_fileEntity_4` FOREIGN KEY (`file_entity_id`) REFERENCES `file_entity` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `message`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `message` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `receiver_Id` varchar(40) DEFAULT NULL,
  `sender_type` int(11) DEFAULT NULL,
  `sender_id` varchar(255) DEFAULT NULL,
  `content` varchar(255) DEFAULT NULL,
  `deliver_status` tinyint(1) DEFAULT '0',
  `send_at` datetime NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_message_receiver_5` (`receiver_Id`),
  CONSTRAINT `fk_message_receiver_5` FOREIGN KEY (`receiver_Id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `network_info_history_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `network_info_history_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `inn_ip` varchar(255) DEFAULT NULL,
  `net_mask` varchar(255) DEFAULT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `ext_ip` varchar(255) DEFAULT NULL,
  `wifi_mac_address` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_network_info_history_log_smartGenie_6` (`smart_genie_id`),
  CONSTRAINT `fk_network_info_history_log_smartGenie_6` FOREIGN KEY (`smart_genie_id`) REFERENCES `smart_genie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `notification`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `notification` (

  `id` varchar(64) NOT NULL,
  `sender_id` varchar(64) DEFAULT NULL,
  `receiver_id` varchar(64) DEFAULT NULL,
  `smart_genie_id` varchar(64) DEFAULT NULL,
  `xgenie_id` varchar(64) DEFAULT NULL,
  `event_type` int(11) DEFAULT NULL,
  `time_stamp` datetime DEFAULT NULL,
  `event_contact_id` varchar(64) DEFAULT NULL,
  `notification_type` varchar(40) DEFAULT NULL,
  `notification_to` varchar(255) DEFAULT NULL,
  `msg` varchar(255) DEFAULT NULL,
  `response` varchar(255) DEFAULT NULL,
  `memo` varchar(255) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_notification_sender_id_34` (`sender_id`),
  KEY `ix_notification_sender_id_receiver_id_35` (`sender_id`,`receiver_id`),
  KEY `ix_notification_sender_id_receiver_id_event_contact_id_36` (`sender_id`,`receiver_id`,`event_contact_id`),
  KEY `ix_notification_input_form_37` (`smart_genie_id`,`xgenie_id`,`event_type`,`time_stamp`),
  KEY `ix_notification_smart_genie_id_xgenie_id_time_stamp_38` (`smart_genie_id`,`xgenie_id`,`time_stamp`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `p2p_active_code`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p2p_active_code` (

  `mac_addr` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `active_code` varchar(40) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`mac_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `p2p_active_code_file`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `p2p_active_code_file` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `items` int(11) DEFAULT NULL,
  `file_name` varchar(255) DEFAULT NULL,
  `s3_name` varchar(255) DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pairing_history_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `pairing_history_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `mac_addr` varchar(255) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `play_evolutions`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `play_evolutions` (

  `id` int(11) NOT NULL,
  `hash` varchar(255) NOT NULL,
  `applied_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `apply_script` text,
  `revert_script` text,
  `state` varchar(255) DEFAULT NULL,
  `last_problem` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `pn_relationship`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `pn_relationship` (

  `pn` varchar(767) NOT NULL DEFAULT '',
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_id` varchar(40) DEFAULT NULL,
  `pn_id` varchar(767) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `device_os_name` varchar(40) DEFAULT NULL,
  `dev_id` varchar(128) DEFAULT NULL,
  `app_code` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`pn`),
  KEY `ix_pn_relationship_user_5` (`user_id`),
  KEY `ix_pn_relationship_dev_id_39` (`dev_id`),
  CONSTRAINT `fk_pn_relationship_user_5` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `request_join`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `request_join` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `join_user_id` varchar(255) DEFAULT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `sg_owner_id` varchar(255) DEFAULT NULL,
  `request_type` int(11) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `send_hub_relationship`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `send_hub_relationship` (

  `cellphone` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `sh_id` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`cellphone`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mac_address` varchar(255) DEFAULT NULL,
  `is_pairing` tinyint(1) DEFAULT '0',
  `owner_id` varchar(40) DEFAULT NULL,
  `firmware_group_id` varchar(40) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `active_code` varchar(40) DEFAULT NULL,
  `dev_type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_smart_genie_owner_7` (`owner_id`),
  KEY `ix_smart_genie_firmwareGroup_8` (`firmware_group_id`),
  CONSTRAINT `fk_smart_genie_firmwareGroup_8` FOREIGN KEY (`firmware_group_id`) REFERENCES `firmware_group` (`id`),
  CONSTRAINT `fk_smart_genie_owner_7` FOREIGN KEY (`owner_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie_action_history`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie_action_history` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `action` varchar(255) DEFAULT NULL,
  `form` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_smart_genie_action_history_smart_genie_id_30` (`smart_genie_id`),
  KEY `ix_smart_genie_action_history_smart_genie_id_action_31` (`smart_genie_id`,`action`),
  KEY `ix_smart_genie_action_history_smart_genie_id_create_at_42` (`smart_genie_id`,`create_at`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie_action_record`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie_action_record` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `action` varchar(255) DEFAULT NULL,
  `form` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie_configuration`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie_configuration` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `smartGenie_id` varchar(255) DEFAULT NULL,
  `cfg_file_id` varchar(40) DEFAULT NULL,
  `cfg_version` bigint(20) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_smart_genie_configuration_smartGenie_11` (`smartGenie_id`),
  KEY `ix_smart_genie_configuration_cfgFile_12` (`cfg_file_id`),
  CONSTRAINT `fk_smart_genie_configuration_cfgFile_12` FOREIGN KEY (`cfg_file_id`) REFERENCES `file_entity` (`id`),
  CONSTRAINT `fk_smart_genie_configuration_smartGenie_11` FOREIGN KEY (`smartGenie_id`) REFERENCES `smart_genie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie_configuration_history`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie_configuration_history` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `smartGenie_id` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `smart_genie_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `smart_genie_log` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `log_date_time` datetime DEFAULT NULL,
  `record_date_time` datetime DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `ext_ip` varchar(255) DEFAULT NULL,
  `inn_ip` varchar(255) DEFAULT NULL,
  `net_mask` varchar(255) DEFAULT NULL,
  `wifi_mac_address` varchar(255) DEFAULT NULL,
  `wifi_ip` varchar(128) DEFAULT NULL,
  `ssid_name` varchar(255) DEFAULT NULL,
  `firmware_ver` varchar(255) DEFAULT NULL,
  `p2p_success` int(11) DEFAULT '0',
  `p2p_fail` int(11) DEFAULT '0',
  `lat` varchar(255) DEFAULT NULL,
  `lng` varchar(255) DEFAULT NULL,
  `timezone` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `sms_support`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `sms_support` (

  `country_code` varchar(64) NOT NULL,
  `available` tinyint(1) DEFAULT '0',
  `country` varchar(64) DEFAULT NULL,
  `sms_company` varchar(64) DEFAULT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`country_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `owner_userId` varchar(40) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `is_admin` tinyint(1) DEFAULT '0',
  `fb_user_id` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `country_code` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_email` (`email`),
  KEY `ix_user_owner_15` (`owner_userId`),
  CONSTRAINT `fk_user_owner_15` FOREIGN KEY (`owner_userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_dev_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_dev_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_session_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(40) DEFAULT NULL,
  `imei` varchar(255) DEFAULT NULL,
  `device_name` varchar(255) DEFAULT NULL,
  `device_os_name` varchar(255) DEFAULT NULL,
  `device_os_version` varchar(255) DEFAULT NULL,
  `device_manufacturer` varchar(255) DEFAULT NULL,
  `device_module` varchar(255) DEFAULT NULL,
  `app_ver` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_identifier`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_identifier` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `user_id` varchar(40) DEFAULT NULL,
  `ident_type` int(11) DEFAULT NULL,
  `ident_data` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uni_user_identifier_ident_data` (`ident_data`),
  KEY `ix_user_identifier_user_16` (`user_id`),
  CONSTRAINT `fk_user_identifier_user_16` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_relationship`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_relationship` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `country_code` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_relationship_history_log`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_relationship_history_log` (

  `id` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `user_relationship_keeper`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `user_relationship_keeper` (

  `id` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `owner_id` varchar(255) DEFAULT NULL,
  `user_id` varchar(255) DEFAULT NULL,
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `country_code` varchar(16) DEFAULT NULL,
  `cell_phone` varchar(255) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_user_relationship_keeper_owner_id_40` (`owner_id`),
  KEY `ix_user_relationship_keeper_owner_id_user_id_41` (`owner_id`,`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xgenie`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `xgenie` (

  `id` varchar(255) CHARACTER SET latin1 NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) CHARACTER SET latin1 DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) CHARACTER SET latin1 NOT NULL,
  `name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mac_addr` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `firmware_group_id` varchar(40) CHARACTER SET latin1 DEFAULT NULL,
  `smartGenie_id` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `device_type` int(11) NOT NULL,
  `info` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_xgenie_firmwareGroup_17` (`firmware_group_id`),
  KEY `ix_xgenie_smartGenie_18` (`smartGenie_id`),
  CONSTRAINT `fk_xgenie_firmwareGroup_17` FOREIGN KEY (`firmware_group_id`) REFERENCES `firmware_group` (`id`),
  CONSTRAINT `fk_xgenie_smartGenie_18` FOREIGN KEY (`smartGenie_id`) REFERENCES `smart_genie` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_general_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xgenie_relationship`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `xgenie_relationship` (

  `id` varchar(40) NOT NULL,
  `smart_genie_id` varchar(255) DEFAULT NULL,
  `parent_mac_addr` varchar(40) NOT NULL,
  `child_mac_addr` varchar(40) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `opt_lock` bigint(20) NOT NULL,
  `parent_param` varchar(64) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `ix_xgenie_relationship_parent_mac_addr_26` (`parent_mac_addr`),
  KEY `ix_xgenie_relationship_child_mac_addr_27` (`child_mac_addr`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `xgenie_type`
--



/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE IF NOT EXISTS `xgenie_type` (

  `device_type` varchar(255) NOT NULL,
  `create_at` datetime NOT NULL,
  `update_at` datetime DEFAULT NULL,
  `update_user_id` varchar(64) DEFAULT NULL,
  `status` int(11) NOT NULL,
  `create_user_id` varchar(64) NOT NULL,
  `device_name` varchar(40) DEFAULT NULL,
  `opt_lock` bigint(20) NOT NULL,
  PRIMARY KEY (`device_type`),
  KEY `ix_xgenie_type_device_type_19` (`device_type`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-06 15:05:10
