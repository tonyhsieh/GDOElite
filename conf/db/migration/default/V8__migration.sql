ALTER TABLE xgenie ADD COLUMN batt_change_date  VARCHAR(40) NULL;
ALTER TABLE xgenie ADD COLUMN fw_ver  VARCHAR(40) NULL;
ALTER TABLE xgenie ADD COLUMN alive bool NULL;
ALTER TABLE xgenie ADD COLUMN ip_addr  VARCHAR(40) NULL;
ALTER TABLE xgenie ADD COLUMN light bool NULL;
ALTER TABLE xgenie ADD COLUMN fps  integer null;
ALTER TABLE xgenie ADD COLUMN door_status bool NULL;
ALTER TABLE xgenie ADD COLUMN low_batt bool NULL;