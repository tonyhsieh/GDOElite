package uitls;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;

import models.Configuration;
import models.Contact;
import models.ContactGroup;
import models.FileEntity;
import models.FirmwareGroup;
import models.PnRelationship;
import models.SmartGenie;
import models.SmartGenieLog;
import models.SmsSupport;
import models.User;
import models.UserIdentifier;
import models.UserRelationship;
import models.XGenie;

import org.apache.commons.io.FilenameUtils;

import utils.generator.CredentialGenerator;

import com.avaje.ebean.Ebean;

import controllers.api.v1.app.cache.UserSessionUtils;
import frameworks.models.UserIdentifierType;

public class UserUtils {
	public UserIdentifier ui1;
	
	public User user1;
	public User user2;
	public User user3;

	public SmartGenie smartGenie1;
	public SmartGenie smartGenie2;
	public SmartGenie smartGenie3;

	public SmartGenie unpairSG;

	public XGenie xGenie1;

	public UserRelationship ur1;

	public FileEntity xgFileEntity;
	public FileEntity sgFileEntity;

	public FirmwareGroup xgFirmwareGroup;
	public FirmwareGroup sgFirmwareGroup;

	public ContactGroup contactGup1;
	public ContactGroup contactGup2;

	public Contact contactMember;

	public SmartGenieLog smartGenieLog;

	public String session1;
	public String session2;
	public String session3;

	public String sgSession1;
	public String sgSession2;
	public String sgSession3;
	
	public PnRelationship pnRelationship;
	
	public Configuration configuration; 
	
	public SmsSupport smsSupport;

	public void initial(){
		List<Object> list = new ArrayList<Object>();

		xgFileEntity = newFileEntity("uic.tar");
		list.add(xgFileEntity);

		sgFileEntity = newFileEntity("uicsg.tar");
		list.add(sgFileEntity);

		xgFirmwareGroup = newFirmwareGroup(xgFileEntity, "garage_genie_001");
		list.add(xgFirmwareGroup);

		sgFirmwareGroup = newFirmwareGroup(sgFileEntity, "asante_genie_001");
		list.add(sgFirmwareGroup);

		user1 = newUser("john", "holajohn9@gmail.com");
		list.add(user1);

		ui1 = newUserIdentifier("holajohn9@gmail.com", "123698745", user1);
		list.add(ui1);
		
		smartGenie1 = newSmartGenie(user1, "john's home in heaven", "2a:2a:2a:2a:2a:2a", sgFirmwareGroup
				, "201.53.123.82", "192.168.1.102", "255.255.255.0", "28ab3562bc91"
				);
		list.add(smartGenie1);

		smartGenieLog = newSmartGenieLog(smartGenie1
				, "201.53.123.82", "192.168.1.102", "255.255.255.0", "28ab3562bc91"
				);
		list.add(smartGenieLog);

		xGenie1 = newXGenie(xgFirmwareGroup, smartGenie1);
		list.add(xGenie1);

		user2 = newUser("Small Strong", "holajohn10@gmail.com");
		user2.nickname = "Hohohosala";
		user2.countryCode = "1";
		user2.cellPhone = "5104025127";
		list.add(user2);

		smartGenie2 = newSmartGenie(user2, "Small Strong's home in hell", "3a:2a:3a:2a:3a:2a", sgFirmwareGroup
				, "203.23.107.104", "192.168.0.10", "255.255.255.0", "98372839a49a"
				);
		list.add(smartGenie2);

		user3 = newUser("who lan john", "sakura750927@gmail.com");
		list.add(user3);

		smartGenie3 = newSmartGenie(user3, "who lan john's home in earth", "4a:3a:4a:3a:4a:3a", sgFirmwareGroup
				, "54.21.212.71", "192.168.1.7", "255.255.255.0", "426bc23ba51a"
				);
		list.add(smartGenie3);

		unpairSG = newSmartGenie(null, "unpairing smart genie", "1c:1c:1c:1c:1c:1c", sgFirmwareGroup
				, "123.45.67.89", "192.168.1.12", "255.255.255.0", "983b6aa5f3b1"
				);
		list.add(unpairSG);
		
		pnRelationship = newPnRelationship(user1, "1234566");
		list.add(pnRelationship);
		
		configuration = newConfiguration("irrigation_controller", "Home irrigation program A", xGenie1.macAddr);
		list.add(configuration);
		
		smsSupport = newSmsSupport("1", true);
		list.add(smsSupport);

		Ebean.save(list);

		session1 = newLogin(user1, "1234566");
		session2 = newLogin(user2, "1234567");
		session3 = newLogin(user3, "1234568");

		sgSession1 = SGSessionUtils.init(smartGenie1);
		sgSession2 = SGSessionUtils.init(smartGenie2);
		sgSession3 = SGSessionUtils.init(smartGenie3);
	}
	
	public UserIdentifier newUserIdentifier(String identData, String password, User user){
		UserIdentifier ui = new UserIdentifier();
		
		ui.id = UserIdentifier.genUserIdentifierId(UserIdentifierType.EMAIL, identData);
		ui.identData = identData;
		ui.password = UserIdentifier.pwdHash(password);
		ui.identType = UserIdentifierType.EMAIL;
		ui.user = user;
		
		return ui;
	}

	public SmartGenie newSmartGenie(User user, String name, String macAddr, FirmwareGroup firmwareGroup,
			String extIp, String innIp, String netMask, String wifiMacAddress
			){
		SmartGenie sg = new SmartGenie();
		sg.owner = user;
		sg.name = name;
		sg.macAddress = macAddr;
		sg.firmwareGroup = firmwareGroup;
		
		SmartGenieLog sgLog = new SmartGenieLog();
		sgLog.smartGenie = sg;

		sgLog.extIp = extIp;
		sgLog.innIp = innIp;
		sgLog.netMask = netMask;
		sgLog.wifiMacAddress = wifiMacAddress;

		if(user != null)
			sg.isPairing = true;
		else
			sg.isPairing = false;
		sg.id = CredentialGenerator.genSmartGenieId(macAddr);

		return sg;
	}

	public XGenie newXGenie(FirmwareGroup firmwareGroup, SmartGenie smartGenie){
		XGenie xGenie = new XGenie();
		xGenie.macAddr = "c1:c1:c1:c1:c1:c1";
		xGenie.name = "door";
		xGenie.id = CredentialGenerator.genXGenieId(xGenie.macAddr);
		xGenie.firmwareGroup = firmwareGroup;
		xGenie.smartGenie = smartGenie;

		return xGenie;
	}

	public User newUser(String nickName, String email){
		User user = new User();

		user.nickname = nickName;
		user.email = email;

		return user;
	}

	public String newLogin(User user, String pnId){

		String sessionId = UUID.randomUUID().toString();

		return UserSessionUtils.login(sessionId, pnId, user.id.toString())?sessionId:null;
	}

	public SmartGenieLog newSmartGenieLog(SmartGenie sg, String extIp, String innIp, String netMask, String wifiMacAddress){
		SmartGenieLog smartGenieLog = new SmartGenieLog();
		Calendar rightNow = Calendar.getInstance();
		smartGenieLog.smartGenie = sg;
		smartGenieLog.createAt = rightNow;
		smartGenieLog.id = sg.id;
		smartGenieLog.extIp = extIp;
		smartGenieLog.netMask = netMask;
		smartGenieLog.wifiMacAddress = wifiMacAddress;
		smartGenieLog.innIp = innIp;

		return smartGenieLog;
	}

	public FileEntity newFileEntity(String name){
		FileEntity fileEntity = new FileEntity();

		String fileName = name;

		fileEntity.fileName = FilenameUtils.getBaseName(fileName);
		fileEntity.fileExt = FilenameUtils.getExtension(fileName);
		fileEntity.fileSize = 12345L;

		fileEntity.s3Path = fileName;

		fileEntity.contentType = null;

		return fileEntity;
	}

	public FirmwareGroup newFirmwareGroup(FileEntity fileEntity, String firmwareName){
		FirmwareGroup firmwareGroup = new FirmwareGroup();

		firmwareGroup.fileEntity = fileEntity;
		firmwareGroup.firmwareVersion = "1.2.5";
		firmwareGroup.name = firmwareName;

		return firmwareGroup;
	}

	public void setupUserRelationsip(User owner, SmartGenie smartGenie, User user){
		ur1 = new UserRelationship();

		ur1.ownerId = owner.id.toString();
		ur1.smartGenieId = smartGenie.id.toString();
		ur1.userId = user.id.toString();
		
		ur1.id = UserRelationship.genUserRelationshipId(smartGenie.id, owner.id.toString(), user.id.toString());

		ur1.save();
	}

	public ContactGroup newContactGroup(SmartGenie smartGenie){
		ContactGroup contactGup = new ContactGroup();
		contactGup.sgId = smartGenie.id;
		contactGup.id = UUID.randomUUID();
		contactGup.msgType = "INFO";
		contactGup.name = "ABC";

		return contactGup;
	}
	
	public PnRelationship newPnRelationship(User user, String pn_id){
		PnRelationship pnr = new PnRelationship();
		pnr.user = user;
		pnr.pn = pn_id;
		pnr.pnId = pn_id;

		return pnr;
	}
	
	public Configuration newConfiguration(String cfg_type, String descript, String mac_addr){
		Configuration cfg = new Configuration();
		
		cfg.id = UUID.randomUUID().toString();
		cfg.avilable = false;
		cfg.cfg_type = cfg_type;
		cfg.descript = descript;
		cfg.mac_addr = mac_addr;
		
		return cfg;
	}
	
	public SmsSupport newSmsSupport(String countryCode, boolean available){
		SmsSupport spt = new SmsSupport();

		spt.countryCode = countryCode;
		spt.available = available;
		
		return spt;
	}

//	public Contact newContactMember(ContactGroup contactGup){
//		Contact contactMember = new Contact();
//		contactMember.contactGroupId = contactGup.id.toString();
//		contactMember.contactData = "holajohn9@gmail.com";
//		contactMember.contactName = "AJLa";
//		contactMember.contactType = "EMAIL";
//		contactMember.id = UUID.randomUUID();
//		contactMember.relationship = "FRIEND";
//
//		return contactMember;
//	}
}
