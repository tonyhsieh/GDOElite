package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;

import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;

public class ConfigurationControllerTest extends BaseModelTest{

	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	/*
	@Test
	public void addConfiguration(){

		String sessionId = uu.session1;
		
		String xGenieId = uu.xGenie1.macAddr;
		String desc = "The program of testing";
		String cfgClass = "irrigation_controller";
		String content = "TEST !!! ABC jhgvjhlhbklfhgfdjblhgbfklhgfklfhggfjhjgfhlfjhtyu8yuytrnbuburtuynbutryubktybtryutryutryubryr";
		
    	String url = "/api/app/dev/cfg";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("xGenieId", xGenieId);
    	data.put("desc", desc);
    	data.put("cfgClass", cfgClass);
    	data.put("data", content);
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	*/
	
	@Test
	public void listConfiguration(){

		String sessionId = uu.session1;
		String xGenieId = uu.xGenie1.macAddr;
		
		String url = "/api/app/dev/cfg/list?" +
				"sessionId=" + sessionId + "&" +
				"xGenieId=" + xGenieId;
    	
		Result result = route(fakeRequest(Helpers.GET, url)
				 .withHeader("content-Type", "application/json"));
    				
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
}
