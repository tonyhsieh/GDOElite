package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;
import static play.test.Helpers.status;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import json.models.api.v1.app.vo.EventContactData;
import json.models.api.v1.app.vo.EventContactVO;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

public class ContactControllerTest extends BaseModelTest{
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void addEventContactSuccess(){
		uu.setupUserRelationsip(uu.user1, uu.smartGenie1, uu.user2);
		
		String xgMAC = uu.xGenie1.macAddr;
    	String url = "/api/app/contact/" + xgMAC;
    	String sessionId = uu.session1;
    	ObjectMapper om = new ObjectMapper();
    	
    	List<EventContactData> ecDataList = new ArrayList<EventContactData>(1);
    	
    	EventContactData ecData = new EventContactData();
    	ecData.eventType = 1000;
    	ecData.checkEmail = true;
    	ecData.checkNotification = false;
    	ecData.checkSMS = true;
    	
    	ecDataList.add(ecData);
    	
    	EventContactData ecData2 = new EventContactData();
    	ecData2.eventType = 1001;
    	ecData2.checkEmail = true;
    	ecData2.checkNotification = false;
    	ecData2.checkSMS = true;
    	
    	ecDataList.add(ecData2);
    	
    	EventContactVO ecVO = new EventContactVO();
    	ecVO.ecDataList = ecDataList;
    	
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("userId", uu.user2.id.toString());
    	try {
			data.put("params", om.writeValueAsString(ecVO));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void addEventContactWrongMAC(){
		uu.setupUserRelationsip(uu.user1, uu.smartGenie1, uu.user2);
		
    	String url = "/api/app/contact/" + 123;
    	String sessionId = uu.session1;
    	ObjectMapper om = new ObjectMapper();
    	
    	List<EventContactData> ecDataList = new ArrayList<EventContactData>(1);
    	
    	EventContactData ecData = new EventContactData();
    	ecData.eventType = 1000;
    	ecData.checkEmail = true;
    	ecData.checkNotification = false;
    	ecData.checkSMS = true;
    	
    	ecDataList.add(ecData);
    	
    	EventContactData ecData2 = new EventContactData();
    	ecData2.eventType = 1001;
    	ecData2.checkEmail = true;
    	ecData2.checkNotification = false;
    	ecData2.checkSMS = true;
    	
    	ecDataList.add(ecData2);
    	
    	EventContactVO ecVO = new EventContactVO();
    	ecVO.ecDataList = ecDataList;
    	
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("userId", uu.user2.id.toString());
    	try {
			data.put("params", om.writeValueAsString(ecVO));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));

		LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("-14003")).isEqualTo(true);
	}
}
