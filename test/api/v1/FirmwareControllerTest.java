package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

public class FirmwareControllerTest extends BaseModelTest {
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void checkFwVerSG(){
		String sessionId = uu.sgSession1;
		String smartGenieFwVer = "0.0.1";
    	String url = "/api/sg/chk_fw_ver?" +
    					"sessionId=" + sessionId + "&" +
    					"smartGenieFwVer=" + smartGenieFwVer;
    	
		Result result = route(fakeRequest(Helpers.GET, url)
    				 .withHeader("Content-Type", "application/json"));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void checkFwVerXG(){
		String sessionId = uu.sgSession1;
		String smartGenieFwVer = "0.0.1";
		String xGenieId = uu.xGenie1.macAddr;
		String xGenieFwVer = "1.2.2";
		String url = "/api/sg/chk_fw_ver?" +
						"sessionId=" + sessionId + "&" +
						"smartGenieFwVer=" + smartGenieFwVer + "&" +
						"xGenieId=" + xGenieId + "&" +
						"xGenieFwVer=" + xGenieFwVer;
    	
		Result result = route(fakeRequest(Helpers.GET, url)
    				 .withHeader("Content-Type", "application/json"));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
}
