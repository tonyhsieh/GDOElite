package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

/**
 * 
 * @author johnwu
 *
 */
public class UserHierarchicalControllerTest extends BaseModelTest {
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
    public void addNewUser(){
		String sessionId = uu.session1;
		
    	String url = "/api/app/relation/user";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("loginId", uu.user1.email);
    	data.put("countryCode", "1");
    	data.put("cellPhone", "28825252");
    	data.put("nickname", uu.user1.nickname);
    	data.put("isTest", "true");
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    				 .withHeader("Content-Type", "application/json")
    				 .withFormUrlEncodedBody(data));
    	
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
	
	@Test
    public void addNewUser_fail(){
		String sessionId = uu.session1;
		
    	String url = "/api/app/relation/user";
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("loginId", "jjaacckk@ggmmaaiill.com");
    	data.put("countryCode", "1");
    	data.put("cellPhone", "28825252");
    	data.put("nickname", uu.user1.nickname);
    	data.put("isTest", "true");
    	
    	Result result = route(fakeRequest(Helpers.PUT, url)
    				 .withHeader("Content-Type", "application/json")
    				 .withFormUrlEncodedBody(data));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "addNewUser_fail() : " + contentAsString(result));
    	assertThat(contentAsString(result).contains("-10002")).isEqualTo(true);
    }
    
	@Test
    public void listUserInfo(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/relation/user?" +
				"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
    			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
    
	@Test
    public void listSmartGenie(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/user/dev/list?" + 
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
    			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
	
	@Test
    public void listSmartGenieNetworkInfo(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/user/dev/list_info?" +
				"sessionId=" + sessionId + "&" +
				"innIp=" + "192.168.1.10" + "&" +
				"netMask=" + "255.255.255.0" + "&" +
				"wifiMacAddress=" + "7829304acb45";
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
    			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
    
    @Test
    public void editUser(){
    	uu.setupUserRelationsip(uu.user1, uu.smartGenie1, uu.user2);
    	
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/relation/user";
    	    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("sessionId", sessionId);
    	data.put("loginId", uu.user2.email);
    	data.put("nickname", "dadada");
    	data.put("countryCode", "1");
    	data.put("cellPhone", uu.user2.cellPhone);
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data)
    								 );
    					
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }

    @Test
    public void getOwnerInfoBySgMac(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/sg/" + uu.smartGenie1.macAddress + "/owner?" +
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
    			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
    
    @Test
    public void getOwnerInfoBySgMac_fail_1(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/sg/" + "ffeeddccbbaa" + "/owner?" +
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "getOwnerInfoBySgMac_fail_1() : " + contentAsString(result));
    	
    	assertThat(contentAsString(result).contains("-12001")).isEqualTo(true);
    }
    
    @Test
    public void getOwnerInfoBySgMac_fail_2(){
    	String sessionId = uu.session2;
    	
    	String url = "/api/app/sg/" + uu.smartGenie1.macAddress + "/owner?" +
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "getOwnerInfoBySgMac_fail_2() : " + contentAsString(result));
    	
    	assertThat(contentAsString(result).contains("-12008")).isEqualTo(true);
    }
    
    @Test
    public void GetSgOwnershipByMac(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/sg/" + uu.smartGenie1.macAddress + "/ownership?" +
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );
    			
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
    }
    
    @Test
    public void GetSgOwnershipByMac_fail(){
    	String sessionId = uu.session1;
    	
    	String url = "/api/app/sg/" + "ffeeddccbbaa" + "/ownership?" +
    			"sessionId=" + sessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
    								 .withHeader("content-Type", "application/json")
    								 );

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "GetSgOwnershipByMac_fail() : " + contentAsString(result));
    	
    	assertThat(contentAsString(result).contains("-12001")).isEqualTo(true);
    }
}
