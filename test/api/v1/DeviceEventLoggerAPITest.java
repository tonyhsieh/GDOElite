/**
 *
 */
package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;

import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import uitls.XGUtils;
import controllers.api.v1.sg.form.DeviceEventLogReqPack.Attachment;
import frameworks.Constants;

/**
 * @author carloxwang
 *
 */
public class DeviceEventLoggerAPITest extends BaseModelTest {

	private UserUtils uu;
	private XGUtils xgu;

	@Before
	public void setupData(){
		uu = new UserUtils();
		uu.initial();

		xgu = new XGUtils();
		xgu.init(uu.smartGenie1);
	}

	@Test
	public void testEventLog() throws IOException, InterruptedException{

		String url = "/api/sg/event";

		Map<String, String> map = new HashMap<String, String>();
		map.put("smartGenieTimestamp", new SimpleDateFormat(Constants.JSON_CALENDAR_PATTERN).format(Calendar.getInstance().getTime()));
		map.put("sessionId", uu.sgSession1);
		map.put("xGenieId", xgu.xg.macAddr);
		map.put("eventType", "1");
		map.put("eventData", "Yeah baby...");

		Attachment attachment = new Attachment();
		attachment.contentType = "image/jpeg";
		attachment.rawData = "abcdefg";

		ObjectMapper om = new ObjectMapper();
		String json = om.writeValueAsString(attachment);

		map.put("attachment", json);

		Result result = route(fakeRequest(Helpers.PUT, url)
								.withFormUrlEncodedBody(map));

    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);

    	// 等一下Akka跑完：
    	Thread.sleep(30000);

	}
}
