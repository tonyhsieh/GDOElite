package api.v1;

import static org.fest.assertions.Assertions.assertThat;
import static play.test.Helpers.contentAsString;
import static play.test.Helpers.fakeRequest;
import static play.test.Helpers.route;

import java.util.HashMap;
import java.util.Map;

import org.junit.Before;
import org.junit.Test;


import play.mvc.Result;
import play.test.Helpers;
import uitls.UserUtils;
import utils.LoggingUtils;

public class SmartGenieInfoControllerTest extends BaseModelTest{
	private UserUtils uu;
	
	@Before
	public void dataCreate(){
		uu = new UserUtils();
		uu.initial();
	}
	
	@Test
	public void setSmartGenieInfo(){
		
		String smartGenieId = uu.smartGenie1.id.toString();
		
    	String url = "/api/app/sg/" + smartGenieId;
    	
    	String sessionId = uu.session1;

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "sessionId : " + sessionId);
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, "url : " + url);
    	
    	Map<String, String> data = new HashMap<String, String>();
    	data.put("smartGenieId", smartGenieId);
    	data.put("sessionId", sessionId);
    	data.put("smartGenieName", "BaMeiMei9BooBaTou");
    	data.put("xGenieId", uu.xGenie1.macAddr);
    	data.put("xGenieName", "BaMeiMei9BooBaTou2");
    	
    	Result result = route(fakeRequest(Helpers.POST, url)
    								 .withHeader("content-Type", "application/json")
    								 .withFormUrlEncodedBody(data));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void listUserInfoBySgSessionId(){
		String sgSessionId = uu.sgSession1;
		
    	String url = "/api/sg/list_user?" +
    					"sessionId=" + sgSessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
				 .withHeader("content-Type", "application/json"));

        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));

    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
	
	@Test
	public void listXG(){
		String sgSessionId = uu.sgSession1;
		
    	String url = "/api/sg/dev/list?" +
    					"sessionId=" + sgSessionId;
    	
    	Result result = route(fakeRequest(Helpers.GET, url)
				 .withHeader("content-Type", "application/json"));
        LoggingUtils.log(LoggingUtils.LogLevel.INFO, contentAsString(result));
    	assertThat(contentAsString(result).contains("SUCCESS")).isEqualTo(true);
	}
}
